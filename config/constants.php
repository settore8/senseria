
<?php

return [
    'vendita' => [
        'proprietari' => [
            'Persone fisiche',
            'Persone giuridiche',
            'Ente pubblico'
        ],
    ],
    'titolo-imprese' => [
        'titolare_impresa'                      => 'Titolare impresa',
        'amministratore_unico'                  => 'Amministratore unico',
        'amministratore_delegato'               => 'Amministratore deletato',
        'legale_rappresentante'                 => 'Legale rappresentante',
        'socio_di_capitale'                     => 'Socio di capitale',
        'incaricato_gestione_profilo_utente'    => 'Incaricato gestione profilo utente'
    ],
    'tipologie-imprese' => [
        'società_in_nome_collettivo'                        => 'S.n.c.',
        'società_in_accomandita_semplice'                   => 'S.a.s.',
        'società_per_azioni'                                => 'S.p.a.',
        'società_a_responsabilità_limitata'                 => 'S.r.l.',
        'lavoro_autonomo'                                   => 'Lavoro autonomo',
        'cooperativa'                                       => 'Cooperativa',
    ],
    'inquadramento-professionista' => [
        'libero_professionista' => 'Libero professionista',
        'socio_studio'          => 'Socio studio'
    ],
    'type_annunci' => [
        'resp_tecnico' => 'responsabiletecnico', // per annunci di Responsabile Tecnico
        'agente_imm' => 'agenteimmobiliare', // per annunci agente immobiliare
        'vendita_imm' => 'immobili',  // per annunci di immobili in vendita
        'acquirente' => 'acquirenti',  // per annunci di acquisto immobili
        'collaborazione' => 'collaborazioni' //  per annunci di collaborazione sugli immobili
    ],
    'oggetto-annunci' => [
        'responsabiletecnico' => 'Annuncio per responsabile tecnico',
        'agenteimmobiliare' => 'Annuncio per agente immobiliare',
        'immobili' => 'Annuncio immobile in vendita',
        'acquirenti' => 'Annuncio acquirente immobile',
        'collaborazioni' => 'Annuncio di collaborazione',

    ],
    'categorie-allegati' => [
        'jpg, jpeg, png' => 'Foto',
        'doc, xls, xlsx, csv, docx, ppt, pptx' => 'Allegati',
        'dwg' => 'Elaborati tecnici'
    ],
    'stato-conservazione' => [
        'Ottimo',
        'Buono',
        'Sufficiente',
        'Scarso',
        'Pessimo'
    ],
    'sino' => [
        'No',
        'Si'
    ],
    'lati-affacci' => [
        'Lato Nord',
        'Lato Sud',
        'Lato Ovest',
        'Lato Est',
        'Lato Nord-Ovest',
        'Lato Nord-Est',
        'Lato Sud-Ovest',
        'Lato Sud-Est'
    ],
    'colore-modulo-noncompletato' => '#e0e0e0',

    'materiale-radiatore' => [
        'Ghisa' => 'Ghisa',
        'Alluminio' => 'Alluminio'
    ],
    'produzione-acqua-calda-sanitaria' => [
        'Integrato con il generatore di calore' => 'Integrato con il generatore di calore',
        'produzioneIndipendenteAcquaCaldaSanitaria' => 'Produzione Indipendente Acqua Calda Sanitaria'
    ],

]

?>
