<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelToken extends Model
{
    public $table = 'model_token';

    public $timestamps = false;
    
    protected $fillable = ['token_id', 'modelable_id', 'modelable_type'];

    public function modelable()
    {
        return $this->morphTo();
    }
}
