<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImmobileVendita extends Model
{
    protected $table = 'immobile_info_vendita';

    public $timestamps = false;

    protected $fillable = ['user_id', 'immobile_id', 'percentuale_mediazione', 'informazioni'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function immobile()
    {
        return $this->belongsTo('App\Immobile', 'immobile_id');
    }

    public function getPrezzoVenditaAttribute(){
        return array_key_exists("prezzo_vendita", $this->info) ? $this->info['prezzo_vendita'] : null;
    }

    public function getPrezzoLocazioneAttribute(){
        return array_key_exists("prezzo_locazione", $this->info) ? $this->info['prezzo_locazione'] : null;
    }
    
}
