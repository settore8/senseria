<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NotificaAccountCancellatoDaUtente extends Mailable
{
    use Queueable, SerializesModels;
    public $token;
    public $encoding_type;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($token, $encoding_type)
    {
        $this->token = $token;
        $this->encoding_type = $encoding_type;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.NotificaAccountCancellatoDaUtente');
    }
}
