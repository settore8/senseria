<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class InvitoConfermaMail extends Mailable
{
    use Queueable, SerializesModels;
    public $user;
    public $token;
    public $richiamo;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $token, $richiamo)
    {
        $this->user = $user;
        $this->token = $token;
        $this->richiamo = $richiamo;
    }

    /**
     * Build the message.
     *
     * @return $this
     */

    public function build()
    {
        return $this->subject('Verifica la tua email')->view('email.invitoConfermaMail');
    }
}
