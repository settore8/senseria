<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class emailPerRiepilogoAnnuncio extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $contatore;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $contatore)
    {
        $this->user = $user;
        $this->contatore = $contatore;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.emailPerRiepilogoAnnuncio');
    }
}
