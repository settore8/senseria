<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MailNuovoCandidatoResponsabileTecnico extends Mailable
{
    use Queueable, SerializesModels;

    public $candidato;
    public $candidature;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($candidato, $candidature)
    {
        $this->candidato = $candidato;
        $this->candidature = $candidature;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->candidato->nome_cognome. 'si é candidato come responsabile tecnico '. $this->candidature->first()->dettaglio->annuncio->immobile->nome)->view('email.nuovoCandidato');
    }
}
