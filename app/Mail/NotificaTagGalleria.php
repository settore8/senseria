<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NotificaTagGalleria extends Mailable
{
    use Queueable, SerializesModels;
    public $galleria, $token, $taggatore, $collaborazione;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($galleria, $token, $taggatore, $collaborazione)
    {
        $this->galleria = $galleria;
        $this->token = $token;
        $this->taggatore = $taggatore;
        $this->collaborazione = $collaborazione;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->taggatore. 'ti ha taggato in una galleria')->view('email.notificaTagGalleria');
    }
}
