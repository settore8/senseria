<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RimossoModuloDaAnnuncio extends Mailable
{
    use Queueable, SerializesModels;

    public $annuncio;
    public $moduli;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($annuncio, $moduli)
    {
        $this->annuncio = $annuncio;
        $this->moduli = $moduli;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->annuncio->immobile->nome.' - Rimossi servizi tecnici')->view('email.rimossiModuli');
    }
}
