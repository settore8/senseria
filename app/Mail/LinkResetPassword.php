<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class LinkResetPassword extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $token;
    public $type;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $token, $type)
    {
        $this->user = $user;
        $this->token = $token;
        $this->type = $type;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Link reset password')->view('email.inviaLinkResetPassword');
    }
}
