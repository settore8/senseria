<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NotificaTagArticolo extends Mailable
{
    use Queueable, SerializesModels;
    public $articolo, $token, $taggatore, $contributo;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($articolo, $token, $taggatore, $contributo)
    {
        $this->articolo = $articolo;
        $this->token = $token;
        $this->taggatore = $taggatore;
        $this->contributo = $contributo;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.notificaTagArticolo');
    }
}
