<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Archivio extends Model
{
    public $table = 'archivio';

    protected $fillable = ['sopralluogo'];

    protected $casts = [
        'dati' => 'array',
    ];

    public function scopeSopralluogo($query){
        return $query->whereType('sopralluogo');
    }
}
