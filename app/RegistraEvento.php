<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RegistraEvento extends Model
{
    public $table = 'registra_evento';

    public $timestamps = false;

    protected $fillable = ['user_id', 'evento', 'created_at'];
}
