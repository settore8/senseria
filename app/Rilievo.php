<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rilievo extends Model
{
    public $table = 'rilievi';
    public $json;

    public $fillable = ['titolo', 'stato', 'standard', 'personalizza', 'esterni', 'area', 'note'];


    protected $casts = [
        'standard' => 'array',
        'personalizza' => 'array',
        'esterno' => 'array',
        'area' => 'array'
    ];

    public function customer()
    {
        return $this->belongsTo('App\Customer');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }



}

