<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    public $timestamps = false;

    protected $fillable = ['nome'];

    public function assegnazioni()
    {
        return $this->hasMany('App\TagAssegnati', 'tag_id');
    }
}
