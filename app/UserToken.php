<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserToken extends Model
{
    public $table = 'user_token';

    public $timestamps = false;

    protected $fillable = ['user_id', 'token', 'encoding_type', 'type', 'scadenza'];

    public function model()
    {
        return $this->hasOne('App\ModelToken', 'token_id');
    }

    public function user(){

        return $this->belongsTo('App\User', 'user_id');

    }
}
