<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CandidaturaEsterna extends Model
{
    protected $table = 'candidature_esterne';

    public $timestamps = false;

    protected $fillable = ['email', 'token', 'immobile_id'];
}
