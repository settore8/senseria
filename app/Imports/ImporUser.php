<?php

namespace App\Imports;

use DB;
use App\User;
use App\Collegio;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;

class ImporUser implements ToCollection, WithHeadingRow, WithCustomCsvSettings
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        foreach($collection as $user){
            $collegio = null;
            if(Collegio::whereNome($user['collegio'])->first()){
                $collegio = Collegio::whereNome($user['collegio'])->first()->id;
            }
            $listaCoordinate = $user['area'] != 'NULL' ? str_replace(['POLYGON((', '))'], '', $user['area']) : null;
            if(count(explode(',', $listaCoordinate)) == 2){
                continue;
            }
            if(count(explode(',', $listaCoordinate)) == 3){
                list($primo, $secondo, $terzo) = explode(',', $listaCoordinate);
                $listaCoordinate.= ','.$terzo;
            }
            $userFirst = User::whereEmail($user['email'])->first();
            if($userFirst){
                $userFirst->update([
                    'id' => $user['id'],
                    'qualifica_id' => $user['qualifica_id'] == 'NULL' ? null : $user['qualifica_id'],
                    'collegio_id' => $collegio,
                    'categoria_id' => $user['categoria_id'] == 'NULL' ? null : $user['categoria_id'],
                    'email' => $user['email'],
                    'email_verified_at' => $user['confirmed'] == '1' ? Carbon::now() : null,
                    'password' => $user['password'],
                    'attivo' => true,
                    'slug' => $user['slug'],
                    'activeCampaignId' => null,
                    'completed_at' => $user['completed'] == '1' ? Carbon::now() : null,
                    'created_at' => $user['created_at'],
                    'updated_at' => $user['updated_at']
                ]);
                $userFirst->update([
                    'area_operativa' => $listaCoordinate ? DB::raw("PolygonFromText('POLYGON((".$listaCoordinate."))')") : null 
                ]);
            }
        }
    }

    //salta la prima riga in quanto intestazione
    public function headingRow(): int
    {
        return 1;
    }

    //mi permette di gestire come una collection id dati
    public function getCsvSettings(): array
    {
        return [
            'delimiter' => ',',
            'input_encoding' => 'ISO-8859-1'
        ];
    }

}
