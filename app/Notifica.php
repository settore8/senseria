<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notifica extends Model
{
    const UPDATED_AT = null;
    protected $table = 'notifiche';
    
    protected $fillable = ['mittente_id', 'destinatario_id', 'route', 'coverable_id', 'coverable_type', 'messaggio', 'readed', 'created_at'];

    public function destinatario()
    {
        return $this->belongsTo('App\User', 'destinatario_id');
    }

    public function mittente()
    {
        return $this->belongsTo('App\User', 'mittente_id');
    }

    public function coverable(){
        return $this->morphTo();
    }

    public function scopeUnreaded() {
        return $this->where('readed', 0);
    }
}
