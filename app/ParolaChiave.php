<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ParolaChiave extends Model
{
    public $table = 'parole_chiavi';
    
    public $timestamps = false;
    protected $fillable = ['categoria_id', 'descrizione'];

    public function categoria()
    {
        return $this->belongsTo('App\Categoria', 'categoria_id');
    }
}
