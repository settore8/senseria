<?php

namespace App\Custom;

use App\Archivio;
use Auth;
use Illuminate\Support\Facades\Lang;
use Request;
use Storage;
use Carbon\Carbon;
use App\User;
use Illuminate\Support\Str;
use Illuminate\Contracts\Encryption\DecryptException;

class Custom {

    public static function addNull($item){
        if(is_array($item)){
            $item = collect($item);
        }
        $item->prepend('--', '');
        return $item;
    }

    public static function createUsersSlug($user){
        if($user->tipo == 'impresa'){
            $slug = Str::slug($user->meta->ragione_sociale);
        }elseif($user->tipo == 'professionista'){
            $slug = Str::slug($user->qualifica->slug.' '.$user->meta->nome.' '.$user->meta->cognome);
        }else{
            //qui dovrebbe essere un agente
            $slug = Str::slug($user->meta->nome.' '.$user->meta->cognome);
        }

        $checkUserSlug = User::where('slug', $slug)->get();

        // user->id si aggiunge solo se sono presenti slug uguali
        if($checkUserSlug->isNotEmpty()) {
            $slug = $slug.'-'.$user->id;
        }

        return $slug;
    }    

    public static function formatCounter($counter) {
        $c = '';
        if($counter > 0) {
            $c = $counter;
        } elseif($counter > 1000 ) {
            $c = $counter / 1000 .'K';
        } elseif($counter > 1000000 ) {
            $c = $counter / 1000000 .'M';
        }
        return $c;
    }

    public static function formatTitle($title) {
        $arr = explode(' ', $title);
        $title = [];
        $notallowed = array('IL, DI, DEL, CON, SI, NO, PER, DA, DEI, IN, NEI');
        foreach($arr as $a) {
            if( (strlen($a) > 4 && ctype_upper($a)) || in_array($a, $notallowed)) {
                $a = strtolower($a);
            }
            array_push($title, $a);
        }
        $title = implode(' ', $title);
        $title = rtrim($title, '.');
        $title = preg_replace('/\s+/', ' ',$title);

        return ucfirst($title);
    }

    public static function createDate($date){

        $created = new Carbon($date);
        $now = Carbon::now();

        $minuti = $created->diffInMinutes($now);
        $ore = $created->diffInHours($now);
        $giorni = $created->diffInDays($now);
        $settimane = $created->diffInWeeks($now);

        if($minuti < 1) {
            $result = 'Adesso';
        } elseif($minuti < 3) {
            $result = 'Pochi istanti fa';
        } elseif($minuti < 59) {
            $result = $minuti. ' minuti fa';
        } elseif($minuti >= 60 && $minuti < 119) {
            $result = 'Un\'ora fa';
        } elseif($minuti >= 120 && $minuti < 1439) {
            $result = $ore . ' ore fa';
        } elseif($ore >= 24 && $ore < 48) {
            $result = 'Ieri';
        } elseif($giorni > 1 && $giorni < 14) {
            $result = $giorni.' giorni fa';
        } else {
            $result = $settimane . ' settimane fa';
        }

        return $result;

    }



    public static function createNameFile($ext, $image){
        if(Auth::user()->isProfessionista()){

            $nomeFile = Auth::id().'-'.
                        Auth::user()->qualifica->slug.'-'.
                        Str::slug(Auth::user()->nome, "-").'-'.
                        Str::slug(Auth::user()->cognome, "-").'-'.$ext.'.'.$image->getClientOriginalExtension();
        }else{
            $nomeFile = Auth::id().'-'.
                        Str::slug(Auth::user()->meta->ragione_sociale).'-'.
                        Str::slug(Auth::user()->meta->comune, "-").'-'.$ext.'.'.$image->getClientOriginalExtension();
        }

        return strtolower($nomeFile);
    }



    public static function getImageProfile($user, $size = '_n') {

        $return = '/images/users/user_placeholder.gif';

        if($user) {

            if($user->meta->image) {

                $image = $user->meta->image;

                switch ($size) {
                    case '36':
                        $size = '_t36';
                        break;
                    case '60':
                        $size = '_t60';
                        break;
                    case 's':
                        $size = '_s';
                        break;
                }

                $filename = str_replace('_n', $size, $image);
                $exists = Storage::disk('s3')->exists('users/profilo/'.$user->id.'/'.$filename);

                if($exists) {
                    $return =  Storage::disk('profilo')->url($user->id.'/'.$filename);
                } else {
                    return $return;
                }

            }

            return $return;

        }

    }


    public static function getInquadramento($key = null){
        if(array_key_exists($key, config('constans.inquadramento-professionista'))){
            return config('constans.inquadramento-professionista')[$key];
        }
        return $key;
    }

    public static function gettipologiaImpresa($key = null){
        if(array_key_exists($key, config('constans.tipologie-imprese'))){
            return config('constans.tipologie-imprese')[$key];
        }
        return $key;
    }

    public static function getTitoloImpresa($key = null){
        if(array_key_exists($key, config('titolo-imprese'))){
            return config('titolo-imprese')[$key];
        }
        return $key;
    }

    public static function getStepByUser(User $user){
        if(is_null($user->password) || $user->password == ""){
            return 'boarding.step1';
        }

        if($user->tipo != 'agente' && $user->prestazioni->isEmpty()){
            return 'boarding.step2';
        }

        if($user->area_operativa == ""){
            return 'boarding.step3';
        }

        if(is_null($user->collegio_id)){
            return 'boarding.step4';
        }

        return true;
    }

    public static function decrypt($crypt){
        try {
            $decrypted = decrypt($crypt);
            return $decrypted;
        } catch (DecryptException $e) {
            return false;
        }
    }

    public static function array_keys_unset($array, $keys){
        foreach($keys as $key){
            if(array_key_exists($key, $array)){
                unset($array[$key]);
            }
        }
        return $array;
    }

    // serve per stampare current nella voce di menu
    public static function current($name) {
        $val = explode(',', $name);
        $request =  explode('/',Request::path());

        if(array_intersect($request, $val))
        {
            echo 'current';
        }
    }

    public static function countObject(object $object){
        $count = 0;
        foreach($object as $obj){
            $count++;
        }
        return $count;
    }

    public static function getFacciateByAffatti(object $affacci){
        $latiAffacciPresenti = array_keys(get_object_vars($affacci));
        $latiAffacciDisponibili = config('constants.lati-affacci');
        //mi serve per avere la chiave dell'array uguale a quella che é nelle costanti ma solo con i lati salvati negli affacci
        foreach($latiAffacciDisponibili as $index => $lato){
            if(!in_array($lato, $latiAffacciPresenti)){
                unset($latiAffacciDisponibili[$index]);
            }
        }
        return $latiAffacciDisponibili;
    }

    public static function getStatoConservazione($key){
        if(is_null($key)){
            return 'Non disponbile';
        }

        return config('constants.stato-conservazione')[$key];
    }

    public static function is_array_null(array $array){
        /**Controlla se tutti i parametri sono NULL all'interno dell'array */
        $totale = count($array);
        $i = 0;
        foreach($array as $a){
            if(is_null($a)){
                $i++;
            }
        }

        return $i == $totale;
    }

    // CONVERT COORDINATE TO SVG POLYGON
    // DA OTTIMIZZARE non funziona bene
    public static function svgPolygon($area){
        if(!$area)  {
            return;
        }
        $coords = explode(',', $area);

        array_pop($coords);

        $result = [];
        foreach($coords as $coord) {
            $coord = explode(' ', $coord);

            $coppia = [];
            foreach($coord as $c) {
                array_push($coppia, substr($c, 1) * 100);
            }
            $coo = implode(",", $coppia);

            array_push($result, $coo);
        }

        return implode(" ", $result);
    }

    public static function getSopralluogoModuli(){
        $array = [];
        $moduli = Archivio::whereType('sopralluogo')->first()->dati;
        foreach($moduli as $modulo => $dati){
            array_push($array, $modulo);
        }
        return $array;
    }

    public static function getVerificaTecnicaModuli(){
        $array = [];
        $moduli = Archivio::whereType('verifica_tecnica')->first()->dati;
        foreach($moduli as $modulo => $dati){
            array_push($array, $modulo);
        }
        return $array;
    }
}
