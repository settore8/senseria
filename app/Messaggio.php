<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Messaggio extends Model
{
    protected $table = 'messaggi';

    public $timestamps = false;

    protected $fillable = ['conversazione_id', 'from', 'messaggio', 'data'];
    /** RELAZIONI **/
    public function conversazione()
    {
        return $this->belongsTo('App\Conversazione', 'conversazione_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'from');
    }
    /** FINE RELAZIONI **/

    /** MUTATOR **/
    public function getDataAttribute($value){
        return Carbon::parse($value);
    }
    /** FINE MUTATOR **/
}
