<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Intervento extends Model
{
    public $table = 'interventi';

    public $timestamps = false;

    protected $fillable = ['nome'];

}
