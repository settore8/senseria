<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TagAssegnati extends Model
{
    public $table = 'tag_assegnati';

    public $timestamps = false;

    protected $fillable = ['tag_id', 'modelable_id', 'modelable_type'];

    public function tag()
    {
        return $this->belongsTo('App\Tag', 'tag_id');
    }
    
    public function modelable()
    {
        return $this->morphTo();
    }
}
