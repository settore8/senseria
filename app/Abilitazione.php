<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Abilitazione extends Model
{
    public $table = 'abilitazioni';

    public $timestamps = false;

    protected $fillable = ['albo_id', 'nome', 'nome_pubblico'];

    public function albo(){
        return $this->belongsTo(Albo::class, 'albo_id');
    }
}
