<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImmobileInformazione extends Model
{
    public $table = 'immobile_informazioni';

    protected $primaryKey = 'immobile_id'; 

    public $timestamps = false;

    protected $fillable = ['immobile_id', 'descrizione', 'metri_quadri', 'indirizzo', 'civico', 'localita', 'comune', 'provincia', 'regione', 'address', 'addressResult', 'locali'];

    protected $casts = [
        'locali' => 'array'
    ];

    public function immobile()
    {
        return $this->belongsTo('App\Immobile', 'immobile_id');
    }
}
