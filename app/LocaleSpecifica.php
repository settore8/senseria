<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LocaleSpecifica extends Model
{
    public $table = 'locali_specifiche';

    public $timestamps = false;

    protected $fillable = ['locale_id', 'nome', 'descrizione', 'ordine', 'attivo'];

    public function locale()
    {
        return $this->belongsTo('App\LocaleDestinazione', 'locale_id');
    }
}
