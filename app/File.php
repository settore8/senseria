<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    public $timestamps = false;
    
    protected $fillable = ['filename', 'size', 'filemime'];

    public function allegati()
    {
        return $this->hasMany('App\Allegato', 'file_id');
    }
}
