<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Suggerimento extends Model
{
    public $table = 'suggerimenti';

    protected $fillable = ['nome', 'testo', 'filename'];
}
