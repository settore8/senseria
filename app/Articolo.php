<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use CyrildeWit\EloquentViewable\InteractsWithViews;
use CyrildeWit\EloquentViewable\Contracts\Viewable;

class Articolo extends Model implements Viewable
{

    use InteractsWithViews;
    
    public $table = 'articoli';

    protected $fillable = ['user_id', 'categoria_id', 'immagine_id', 'titolo', 'testo', 'riassunto', 'slug', 'pubblicato'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function categoria()
    {
        return $this->belongsTo('App\Categoria', 'categoria_id');
    }

    public function tag()
    {
        return $this->morphMany('App\TagAssegnati', 'modelable');
    }

    public function like()
    {
        return $this->morphMany('App\Like', 'coverable');
    }

    public function raccolta()
    {
        return $this->morphMany('App\Raccolta', 'coverable');
    }

    public function allegati()
    {
        return $this->morphMany('App\Allegato', 'modelable');
    }

    public function partecipazioni(){
        return $this->morphMany('App\Partecipazione', 'modelable');
    }
    
    // scope 

    public function scopePubblicati($query)
    {
        return $query->where('pubblicato', 1);
    }

    public function scopeRecenti($query)
    {
        return $query->orderByDesc('created_at');
    }


    // mutator

    public function getAnnoAttribute(){
        return  $this->created_at->year;
    }

    public function getMeseAttribute(){
        return  $this->created_at->month;
    }

    public function scopePubblicato($query){
        return $query->where('pubblicato', 1);
    }

}
