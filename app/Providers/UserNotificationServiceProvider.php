<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Auth;
use App\Notifica;

class UserNotificationServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

        view()->composer('app.layouts.master', function ($view) {
            $notifiche = Auth::user()->notifiche->sortByDesc('created_at')->take(50);

            $unreaded = Auth::user()->notifiche()->where('readed', 0)->count();
            
            if($unreaded > 0) {
                $unreaded = $unreaded;
            } elseif($unreaded > 99 ) {
                $unreaded = '99+';
            } elseif($unreaded > 1000 ) {
                $unreaded = $unreaded / 1000 .'k';
            }

            $view->with('notifiche', $notifiche)->with('unreaded', $unreaded);
        });
    }
}
