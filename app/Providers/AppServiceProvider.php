<?php

namespace App\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Schema::defaultStringLength(191); //per creare campi unique nel database, per mysql inferiore a 5.7

 
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
               //FORM
               Blade::component('app.components.form.formgrouphorizontal', 'formgrouph');
               Blade::component('app.components.form.text', 'inputtext');
               Blade::component('app.components.form.number', 'inputnumber');
               Blade::component('app.components.form.checkbox', 'checkbox');
               Blade::component('app.components.form.toggle', 'toggle');
               Blade::component('app.components.form.button', 'button');
               Blade::component('app.components.form.select', 'select');
               Blade::component('app.components.form.selectmultiple', 'selectmultiple');
               Blade::component('app.components.form.textarea', 'textarea');
               Blade::component('app.components.form.selectKeyValue', 'selectkv');
               Blade::component('app.components.form.payment_option', 'payment_option');
               Blade::component('app.components.form.indirizzo_fatturazione', 'indirizzo_fatturazione');
               Blade::component('app.components.form.selectProfessionisti', 'selectprofessionisti');
               Blade::component('app.components.form.selectProfessionistiImprese', 'selectprofessionistiimprese');
               Blade::component('app.components.form.salvaoPubblica', 'salvaopubblica');
       
               Blade::component('app.components.lista_carte', 'lista_carte');
               Blade::component('app.components.tableinput', 'tableinput');
               Blade::component('app.components.tablecheckbox', 'tablecheckbox');
               Blade::component('app.components.tableaffacci', 'tableaffacci');
       
               // ELEMENTS
               Blade::component('app.components.suggerimento', 'suggerimento');
               Blade::component('app.components.block', 'block');
               Blade::component('app.components.modal', 'modal');
               Blade::component('app.components.empty', 'emptyblock');

               Blade::component('app.components.row', 'row');

    }
}
