<?php

namespace App\Providers;

use App\Listeners\GestioneEmailListener;
use App\Listeners\SalvaCompletamentoImmobile;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */

    protected $subscribe = [
        GestioneEmailListener::class,
        SalvaCompletamentoImmobile::class,
    ];

    protected $listen = [
        'App\Events\InserisciToken' => [
            'App\Listeners\InserisciTokenDiSicurezza'
        ],
        'App\Events\SalvaThumbnail' => [
            'App\Listeners\InserisciThumbnailToModel'
        ],
        'App\Events\RegistraEvento' => [
            'App\Listeners\SalvaEventoInTabella'
        ],
        'App\Events\AssegnaToken' => [
            'App\Listeners\AssegnaTokenAlModel'
        ],
        'App\Events\SyncImmobiliToUser' => [
            'App\Listeners\SincronizzaImmobiliConNuovoUtente'
        ],
        'App\Events\SyncUserToImmobile' => [
            'App\Listeners\SincronizzaNuovoImmobileUtentiRegistrati'
        ],
        'App\Events\SalvaAllegati' => [
            'App\Listeners\SalvaAllegatiToModel'
        ],
        'App\Events\SalvaNote' => [
            'App\Listeners\SalvaNoteNelDatabase'
        ],
        'App\Events\SyncAgentiToVendita' => [
            'App\Listeners\SincronizzaVenditaAgentiRegistrati'
        ],

    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

    }
}
