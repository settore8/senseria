<?php

namespace App\Repository;

class FabbricatoRepository{
    public function rulesStepOne(){
        return [
            'anno_costruzione' => 'required_without:checkbox_costruzione',
            'anno_ristrutturazione' => 'required_without:checkbox_ristrutturazione',
            'piani_fuori_terra' => 'required',
            'piani_entro_terra' => 'required'
        ];
    }

    public function rulesStepTwo(){
        return [];
    }
}
