<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use function GuzzleHttp\Psr7\_parse_request_uri;

class Partecipazione extends Model
{
    public $table = 'partecipazioni';

    protected $fillable = ['modelable_id', 'modelable_type', 'contributore','contributo','token', 'user_id'];

    public function modelable(){
        return $this->morphTo();
    }

    public function user(){
        return $this->belongsTo('App\User', 'user_id');
    }
}
