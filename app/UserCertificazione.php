<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserCertificazione extends Model
{
    public $table = 'user_certificazione';

    protected $fillable = ['user_id', 'scadenza', 'status', 'filename'];
}
