<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoriaConsistenza extends Model
{
    protected $table = 'categoria_consistenza';

    protected $fillable = ['nome', 'slug'];

    public function consistenze()
    {
        return $this->hasMany('App\Consistenza', 'categoria_id');
    }
}
