<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImmobileStima extends Model
{
    public $table = 'immobile_stima';
    
    public $timestamps = false;

    protected $fillable = ['immobile_id', 'attivo'];

    public function immobile()
    {
        return $this->belongsTo('App\Immobile', 'immobile_id');
    }           
}
