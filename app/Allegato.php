<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Allegato extends Model
{
    public $table = 'allegati';

    protected $fillable = ['file_id', 'modelable_id', 'modelable_type', 'dettagli', 'type', 'order'];

    //relazioni
    public function file()
    {
        return $this->belongsTo('App\File', 'file_id');
    }

    public function modelable()
    {
        return $this->morphTo();
    }
    //fine relazioni


    public function getDettagliAttribute($value){
        return !is_null($value) ? unserialize($value) : [];
    }

}
