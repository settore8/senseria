<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gruppo extends Model
{
    public $table = 'gruppi';

    public $timestamps = false;

    protected $fillable = ['nome','nome_pubblico','slug','tipo','ordine','visibile'];

    public function categorie()
    {
        return $this->hasMany('App\Categoria', 'gruppo_id');
    }
}
