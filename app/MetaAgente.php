<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MetaAgente extends Model
{
    public $table = 'user_meta_agente';

    public $timestamps = false;

    protected $fillable = ['nome', 'cognome'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

}
