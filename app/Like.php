<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Like extends Model
{
    use SoftDeletes;

    protected $table = 'likes';

    protected $fillable = ['user_id', 'coverable_id', 'coverable_type'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function coverable(){
        return $this->morphTo();
    }
}
