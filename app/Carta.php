<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Carta extends Model
{
    protected $table = 'carte';

    public function user(){
        $this->belongsTo('App\User', 'user_id');
    }

    protected $fillable = [
        'carta_id', 'user_id', 'card_holder', 'card_brand', 'last_four', 'exp_month', 'exp_year', 'default', 'fingerprint'
    ];
}
