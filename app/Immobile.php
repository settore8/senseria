<?php

namespace App;

use Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Lang;

class Immobile extends Model
{
    public $table = 'immobili';

    protected $fillable = ['user_id', 'codice', 'specifica_id', 'tecnico_id', 'agente_id', 'nome', 'punto', 'share_code', 'moduli', 'statistiche', 'archiviato'];

    public $collectionNecessita = [];

    protected $casts = [
        'moduli' => 'array',
        'statistiche' => 'array'
    ];

    //relation
    public function getRouteKeyName() {
        return 'codice';
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    
    public function specifica()
    {
        return $this->belongsTo('App\Specifica', 'specifica_id');
    }

    public function tecnico()
    {
        return $this->belongsTo('App\User', 'tecnico_id');
    }

    public function agente()
    {
        return $this->belongsTo('App\User', 'agente_id');
    }

    public function informazione()
    {
        return $this->hasOne('App\ImmobileInformazione', 'immobile_id');
    }

    public function vendita()
    {
        return $this->hasOne('App\InfoVendita', 'immobile_id');
    }

    public function specificalocale()
    {
        return $this->belongsToMany('App\LocaleSpecifica', 'immobile_locali_specifiche', 'immobile_id', 'locale_specifica_id');
    }

    public function token()
    {
        return $this->morphOne('App\ModelToken', 'modelable');
    }

    public function sopralluogo(){
        return $this->hasOne('App\ImmobileSopralluogo', 'immobile_id');
    }

    public function verifica_tecnica(){
        return $this->hasOne('App\ImmobileVerificaTecnica', 'immobile_id');
    }

    public function relazione_tecnica(){
        return $this->hasOne('App\ImmobileRelazioneTecnica', 'immobile_id');
    }

    public function stima(){
        return $this->hasOne('App\ImmobileStima', 'immobile_id');
    }

    public function istruttoria(){
        return $this->hasOne('App\ImmobileIstruttoria', 'immobile_id');
    }

    public function collaborazione(){
        return $this->hasOne('App\ImmobileCollaborazione', 'immobile_id');
    }

    public function capitolato(){
        return $this->hasOne('App\ImmobileCapitolato', 'immobile_id');
    }


    public function rilievo(){
        return $this->hasOne('App\Rilievo', 'immobile_id');
    }


    public function annunci(){
        return $this->morphMany('App\Annuncio', 'model');
    }

    public function annuncioRespTecnico(){
        return $this->morphOne('App\Annuncio', 'model')->where('type', config('constants.type_annunci.resp_tecnico'));
    }

    public function annuncioVendita(){
        return $this->morphOne('App\Annuncio', 'model')->where('type', config('constants.type_annunci.vendita_imm'));
    }

    public function annuncioAgenteImmobiliare(){
        return $this->morphOne('App\Annuncio', 'model')->where('type', config('constants.type_annunci.agente_imm'));
    }


    public function gallerie()
    {
        return $this->hasMany('App\Galleria', 'immobile_id');
    }

    public function condivisioni()
    {
        return $this->hasMany('App\ImmobileCondiviso', 'immobile_id');
    }
    //fine relazioni

    //mutator
    public function getidCriptatoAttribute(){
        return encrypt($this->id);
    }

    public function getDestinazioneAttribute()
    {
        return $this->specifica->destinazione;
    }

    public function getSpecificaNomeAttribute(){
        return "{$this->specifica->nome_pubblico}: {$this->nome}";
    }

    public function getCountLocaliAttribute(){
        $locali = 0;
        if(array_key_exists('localiPrincipaliAccessoriDiretti', $this->informazione->locali)){
            $locali = array_sum($this->informazione->locali['localiPrincipaliAccessoriDiretti']);
        }

        return $locali;
    }
    //fine mutator

    //scope

    public function scopeNonArchiviati($query){
        $query->where('archiviato', 0);
    }

    public function scopeFindByUser($query, $iduser){
        $query->whereRaw('ST_Contains((select area_operativa from users where id = '.$iduser.'), punto)');
    }

    public function scopeMiei($query){
        $query->where('user_id', Auth::id())->nonArchiviati();
    }

    public function scopeGestibili($query){
        $query->where( 'user_id', Auth::id() )->orWhere( 'tecnico_id', Auth::id() );
    }

    public function scopeArchiviati($query){
        $query->where('archiviato', 1);
    }

    public function scopeInPubblicita($query)
    {
        // aggiungere solo gli immobili che hanno agente
        // aggiungere solo gli immobili che hanno un annuncio di vendita collegato
        // viene usato nel middleware CheckImmobilePubblictà per Senseria
        return $query->whereNotNull('agente_id')->has('annuncioVendita');
    }

   
}
