<?php

namespace App\Events;

use App\Immobile;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class CompletamentoModulo
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $path;
    public $immobile;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(string $path, Immobile $immobile)
    {
        $this->path = $path;
        $this->immobile = $immobile;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
