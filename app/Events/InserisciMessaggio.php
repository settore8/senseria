<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class InserisciMessaggio implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $conversazione;
    public $messaggio;
    public $senderid;
    public $sender;
    public $now;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($conversazione, $messaggio, $senderid, $sender, $now)
    {
        $this->conversazione = $conversazione;
        $this->messaggio = $messaggio;
        $this->senderid = $senderid;
        $this->sender = $sender;
        $this->now = $now;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return ['messaggi'];
    }

    public function broadcastAs()
    {
        return 'inserisci-messaggio';
    }
}
