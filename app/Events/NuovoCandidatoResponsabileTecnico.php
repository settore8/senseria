<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class NuovoCandidatoResponsabileTecnico
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $email_proprietario_immobile;
    public $candidato;
    public $candidature;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($email_proprietario_immobile, $candidato, $candidature)
    {
        $this->email_proprietario_immobile = $email_proprietario_immobile;
        $this->candidato = $candidato;
        $this->candidature = $candidature;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
