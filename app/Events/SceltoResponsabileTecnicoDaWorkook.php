<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class SceltoResponsabileTecnicoDaWorkook
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user;  //utente che ha scelto
    public $email;  //email dell'utente scelto
    public $token; //token di sicurezza
    public $type;   //tipologia di token
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($user, $email, $token, $type)
    {
        $this->user = $user;
        $this->email = $email;
        $this->token = $token;
        $this->type = $type;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
