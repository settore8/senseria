<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class NotificaModuloRimossoAlResponsabileTecnico implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $destinatario;
    public $messaggio;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($destinatario, $messaggio)
    {
        $this->destinatario = $destinatario;
        $this->messaggio = $messaggio;
    }

    public function broadcastOn()
    {
        return ['annunci'];
    }

    public function broadcastAs()
    {
        return 'modulo-rimosso';
    }
}
