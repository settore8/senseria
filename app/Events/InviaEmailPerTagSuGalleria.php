<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class InviaEmailPerTagSuGalleria
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $destinatario;
    public $galleria;
    public $token;
    public $nome;
    public $collaborazione;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($destinatario, $galleria, $token, $nome, $collaborazione)
    {
        $this->destinatario = $destinatario;
        $this->galleria = $galleria;
        $this->token = $token;
        $this->nome = $nome;
        $this->collaborazione = $collaborazione;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
