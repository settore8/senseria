<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class SceltoAgenteImmobiliare
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $immobile;
    public $logged;
    public $email; // email dell'agente immobiliare che é stato scelto
    public $token;
    public $type;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($immobile, $logged, $email, $token, $type)
    {
        $this->immobile = $immobile;
        $this->logged = $logged;
        $this->email = $email;
        $this->token = $token;
        $this->type = $type;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
