<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Specifica extends Model
{
    protected $table = 'specifiche';

    public $timestamps = false;

    protected $filalble = ['destinazione_id', 'nome', 'nome_pubblico'];

    public function destinazione()
    {
        return $this->belongsTo('App\Destinazione', 'destinazione_id');
    }

    public function immobili()
    {
        return $this->hasMany('App\Immobile', 'specifica_id');
    }
}
