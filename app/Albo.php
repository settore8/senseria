<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Albo extends Model
{
    public $table = 'albi';

    public $timestamps = false;

    protected $fillable = [
        'nome', 'nome_pubblico', 'ordine'
    ];

    public function qualifiche()
    {
        return $this->hasMany('App\Qualifica', 'albo_id');
    }

    public function collegi()
    {
        return $this->belongsToMany('App\Collegio', 'albo_collegio', 'albo_id', 'collegio_id');
    }

    public function abilitazioni()
    {
        return $this->hasMany('App\Abilitazione', 'albo_id');
    }


}
