<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Encryption\DecryptException;

class Modulo extends Model
{
    protected $table = 'moduli';

    public $timestamps = false;

    protected $fillable = ['nome', 'nome_pubblico', 'route_name'];

    /** RELAZIONI **/
    public function dettagli()
    {
        return $this->morphMany('App\Dettaglio', 'coverable');
    }
    /** FINE RELAZIONI **/

    public function getRouteNameAttribute($value){
        try {
            return $decrypted = decrypt($value);
        } catch (DecryptException $e) {
            return $e;
        }
    }
}
