<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use CyrildeWit\EloquentViewable\InteractsWithViews;
use CyrildeWit\EloquentViewable\Contracts\Viewable;

class Annuncio extends Model implements Viewable
{

    use InteractsWithViews;
    public $table = 'annunci';

    protected $fillable = ['codice', 'creatore_id', 'model_id', 'model_type', 'immobile_id', 'type', 'stato'];

    //relazioni
    public function immobile()
    {
        return $this->belongsTo('App\Immobile', 'model_id');
    }

    public function acquirente()
    {
        return $this->belongsTo('App\Acquirente', 'model_id');
    }

    public function creatore()
    {
        return $this->belongsTo('App\User', 'creatore_id');
    }

    public function dettagli(){
        return $this->hasMany('App\Dettaglio', 'annuncio_id');
    }

    public function candidature()
    {
        return $this->hasManyThrough('App\Candidatura', 'App\Dettaglio', 'annuncio_id', 'dettaglio_id');
    }

    public function candidatureWGS(){
        return $this->hasManyThrough('App\Candidatura', 'App\Dettaglio', 'annuncio_id', 'dettaglio_id')->orderBy('attivo', 'DESC')->withoutGlobalScopes();
    }

    public function candidatureFilterByUser($user_id)
    {
        return $this->hasManyThrough('App\Candidatura', 'App\Dettaglio', 'annuncio_id', 'dettaglio_id')->where('user_id', $user_id);
    }
    
    public function compatibilita(){
        return $this->morphMany('App\AnnunciCompatibili', 'coverable');
    }

    public function notificaFilterByUser($user_id){
        return $this->morphMany('App\AnnunciCompatibili', 'coverable')->where('ricevente_id', $user_id)->first();
    }

    public function model(){
        return $this->morphTo();
    }
    //fine relazioni
}
