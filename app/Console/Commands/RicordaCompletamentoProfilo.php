<?php

namespace App\Console\Commands;

use Log;
use Mail;
use App\User;
use App\Events\RegistraEvento;
use Illuminate\Console\Command;
use App\Mail\RicordaDiCompletareIlProfilo;

class RicordaCompletamentoProfilo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:completa-profilo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Invia un\'email per ricorda a coloro che non hanno completato il profilo di farlo';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $userSenzaPassword = User::whereNull('password')->get();
        // $userSenzaCompetenze = User::doesntHave('prestazioni')->get();
        // $userSenzaAreaOperativa = User::whereNull('area_operativa')->get();

        // $user = $userSenzaPassword->union($userSenzaCompetenze);
        // $user = $user->union($userSenzaAreaOperativa);

        // $unique = $user->unique();
        // $users = $unique->values()->all();

        foreach($userSenzaPassword as $user){
            try{
                Mail::to($user->email)->send(new RicordaDiCompletareIlProfilo($user));
                event(new RegistraEvento($user->id, $this->signature));
            }catch(\Exception $e){
                Log::channel('evento')->info('Errore invio '.$this->signature.' a '. $user->email.'');
            }
        }
    }
}
