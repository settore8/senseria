<?php

namespace App\Console\Commands;

use App\AnnunciCompatibili;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class emailPerRiepilogoAnnuncio extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:emailPerRiepilogoAnnuncio';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $ac = AnnunciCompatibili::selectRaw('COUNT(*) as contatore, ricevente_id')->groupBy('ricevente_id')->get();
        foreach($ac as $annuncio){
            Mail::to('alessio@alessiomealli.com')->send(new \App\Mail\emailPerRiepilogoAnnuncio($annuncio->user->fullname, $annuncio->contatore));
        }
    }
}
