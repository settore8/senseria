<?php

namespace App\Console\Commands;

use App\MetaImpresa;
use App\MetaProfessionista;
use App\User;
use App\UserMeta;
use Illuminate\Console\Command;

class InsertDataFromUserMetaProfessionistaImprestaIntoUserMeta extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:InsertDataFromUserMetaProfessionistaImprestaIntoUserMeta';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $professionisti = MetaProfessionista::all();
        $imprese = MetaImpresa::all();/*
        foreach($professionisti as $professionista){
            UserMeta::create([
                'user_id' => $professionista->user_id,
                'ragione_sociale' => $professionista->ragione_sociale,
                'nome' => $professionista->nome,
                'cognome' => $professionista->cognome,
                'image' => $professionista->image,
                'descrizione_comune' => $professionista->descrizione_comune,
                'coordinate_comune' => $professionista->coordinate_comune,
                'info_generali' => [
                    'descrizione' => $professionista->descrizione,
                    'sesso' => $professionista->sesso,
                    'numero_iscrizione' => $professionista->numero_iscrizione,
                    'numero_albo' => $professionista->numero_albo,
                    'inizio_attivita' => $professionista->inizio_attivita,
                    'provincia' => $professionista->provincia,
                    'indirizzo' => $professionista->indirizzo,
                    'inquadramento' => $professionista->inquadramento,
                    'partita_iva' => $professionista->partita_iva,
                    'cellulare' => $professionista->cellulare,
                ],
                'created_at' => $professionista->created_at,
                'updated_at' => $professionista->updated_at
            ]);
        }

        foreach ($imprese as $impresa){
            UserMeta::create([
                'user_id' => $impresa->user_id,
                'ragione_sociale' => $impresa->ragione_sociale,
                'nome' => $impresa->nome,
                'cognome' => $impresa->cognome,
                'image' => $impresa->image,
                'descrizione_comune' => $impresa->descrizione_comune,
                'coordinate_comune' => $impresa->coordinate_comune,
                'info_generali' => [
                    'descrizione' => $impresa->descrizione,
                    'tipologia' => $impresa->tipologia,
                    'partita_iva' => $impresa->partita_iva,
                    'provincia' => $impresa->provincia,
                    'localita' => $impresa->localita,
                    'indirizzo' => $impresa->indirizzo,
                    'telefono' => $impresa->telefono,
                    'cellulare' => $impresa->cellulare,
                    'inizio_attività' =>$impresa->inizio_attivita,
                    'rappresentante' => $impresa->rappresentate,
                ],
                'created_at' => $impresa->created_at,
                'updated_at' => $impresa->updated_at
            ]);
        }*/
        $users = User::where('tipo', 'professionista')->get();
        foreach ($users as $user) {
            $user->update([
                'tipo' => 'professionista',
            ]);
        }
        $users = User::where('tipo', 'impresa')->get();
        foreach ($users as $user) {
            $user->update([
                'tipo' => 'impresa',
            ]);
        }

    }
}
