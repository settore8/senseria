<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;

class insertNomeCognomeInFullnameColumn extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'insertinto:fullname';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Inserisco i Campi e Cognome dentro la colonna Fullname di Users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users_prof = User::where('tipo', 'professionista')->get();
        foreach ($users_prof as $user_prof){
            $fullname = $user_prof->user_meta_professionista->nome.' '.$user_prof->user_meta_professionista->cognome ;
            $user_prof->update([
                'fullname' => $fullname,
            ]);
        }
        $users_imp = User::where('tipo', 'impresa')->get();
        foreach ($users_imp as $user_imp){
            $fullname = $user_imp->user_meta_impresa->ragione_sociale;
            $user_imp->update([
                'fullname' => $fullname,
            ]);
        }
    }
}
