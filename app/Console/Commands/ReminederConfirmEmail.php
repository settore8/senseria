<?php

namespace App\Console\Commands;

use App\Mail\InvitoConfermaMail;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Facades\Mail;

class ReminederConfirmEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:reminderconfirmmail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $users = User::where('created_at', Carbon::now()->subDays())->whereNull('email_verified_at')->get();
            foreach ($users as $user) {
                Mail::to('alessio@alessiomealli.com')->send(new invitoConfermaMail($user, $user->token, 1));
            }
            /*
            $users = User::where('created_at', Carbon::now()->subDays(2))->whereNull('email_verified_at');
            foreach ($users as $user) {
                Mail::to('alessio@alessiomealli.com')->send(new invitoConfermaMail($user, $user->token, 2));
            }
            $users = User::where('created_at', Carbon::now()->subDays(3))->whereNull('email_verified_at');
            foreach ($users as $user) {
                Mail::to('alessio@alessiomealli.com')->send(new invitoConfermaMail($user, $user->token, 3));
            }
            */
        }catch(\Exception $e){
            dd($e);
        }
    }
}
