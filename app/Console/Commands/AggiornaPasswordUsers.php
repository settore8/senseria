<?php

namespace App\Console\Commands;

use DB;
use App\User;
use Illuminate\Console\Command;

class AggiornaPasswordUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'aggiorna-password:users';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Imposta le password di tutti gli utenti su 12345678';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $userTable = (new User())->getTable();
        DB::table($userTable)->update(['password' => bcrypt('12345678')]);
    }
}
