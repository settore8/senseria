<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LocaleDestinazione extends Model
{
    public $table = 'locale_destinazione';

    public $timestamps = false;

    protected $fillable = ['locale_id', 'destinazione_id'];

    public function locale()
    {
        return $this->belongsTo('App\Locale', 'locale_id');
    }

    public function destinazione()
    {
        return $this->belongsTo('App\Destinazione', 'destinazione_id');
    }

    public function specifiche()
    {
        return $this->hasMany('App\LocaleSpecifica', 'locale_destinazione_id');
    }
}
