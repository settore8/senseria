<?php

namespace App;

use App\Scopes\FilterByAttivo;
use Illuminate\Database\Eloquent\Model;

class Candidatura extends Model
{
    public $table = 'candidature';

    public $timestamps = false;

    protected $fillable = ['dettaglio_id', 'user_id', 'candidato', 'quotazione', 'attivo'];

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new FilterByAttivo);
    }

    //relazioni
    public function dettaglio()
    {
        return $this->belongsTo('App\Dettaglio', 'dettaglio_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function note()
    {
        return $this->morphOne('App\Note', 'coverable');
    }

    public function chat()
    {
        return $this->morphOne('App\Conversazione', 'coverable');
    }
    //fine relazioni

    /*** SCOPE ***/

    public function scopeAttive($query){
        return $query->where('attivo', '1');
    }

    /*** FINE SCOPE ***/
}
