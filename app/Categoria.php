<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    public $table = 'categorie';

    public $timestamps = false;

    protected $fillable = ['gruppo_id', 'nome', 'nome_pubblico', 'slug', 'ordine'];

    public function gruppo()
    {
        return $this->belongsTo('App\Gruppo', 'gruppo_id');
    }

    public function articoli()
    {
        return $this->hasMany('App\Articolo', 'categoria_id');
    }

    public function prestazioni()
    {
        return $this->hasMany('App\Prestazione', 'categoria_id');
    }

    public function parolechiavi()
    {
        return $this->hasMany('App\ParolaChiave', 'categoria_id');
    }
}
