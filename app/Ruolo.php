<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ruolo extends Model
{
    public $table = 'ruoli';

    public $timestamps = false;

    protected $fillable = ['nome', 'slug'];

    public function users()
    {
        return $this->belongsToMany('App\User', 'user_ruolo', 'ruolo_id', 'user_id');
    }
}
