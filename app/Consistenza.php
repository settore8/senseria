<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Consistenza extends Model
{
    protected $table = 'consistenze';

    protected $fillable = ['destinazione_id', 'categoria_id', 'nome_pubblico', 'slug'];

    public function destinazione()
    {
        return $this->belongsTo('App\Destinazione', 'destinazione_id');
    }

    public function categoria(){
        return $this->belongsTo('App\CategoriaConsistenza', 'categoria_id');
    }
}
