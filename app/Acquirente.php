<?php

namespace App;
use Auth;
use DB;
use Illuminate\Database\Eloquent\Model;

class Acquirente extends Model
{
    public $table = 'acquirenti';

    protected $fillable = ['codice', 'user_id', 'nome', 'tipo_acquirente',  'budget',  'dettagli', 'share_code', 'status', 'specifica_id', 'archiviato', 'area_immobile'];

    protected $casts = [
        'dettagli' => 'array'
    ];
    //relation

    public function getRouteKeyName() {
        return 'codice';
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function specifica()
    {
        return $this->belongsTo('App\Specifica', 'specifica_id');
    }

    public function getDestinazioneAttribute()
    {
        return $this->specifica->destinazione;
    }
    
    public function getAreaImmobileAttribute() {
        $areaimmobile = DB::select('SELECT ST_AsText(area_immobile) as area from acquirenti WHERE id ='. $this->id .' ');
        $area = (!empty($areaimmobile[0])) ? str_replace(["POLYGON((", "))"], "", $areaimmobile[0]->area) : '' ;
        return $area;
    }

    public function annuncio(){
        return $this->morphOne('App\Annuncio', 'model')->where('type', config('constants.type_annunci.acquirente'));
    }

    public function annunci(){
        return $this->morphMany('App\Annuncio', 'model');
    }

    // scope
    public function scopeMiei($query){
        $query->where('user_id', Auth::id());
    }

    public function scopeArchiviati($query){
        $query->where('archiviato', 1);
    }

    public function scopeAttivi($query){
        $query->where('archiviato', 0);
    }

}
