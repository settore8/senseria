<?php

namespace App;

use DB;
use Auth;
use App\Suggerimento;
use App\Scopes\FilterByAttivo;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Contracts\Encryption\DecryptException;
use CyrildeWit\EloquentViewable\InteractsWithViews;
use CyrildeWit\EloquentViewable\Contracts\Viewable;

class User extends Authenticatable  implements Viewable
{
    use InteractsWithViews;

    use Notifiable;

    protected $table = 'users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'qualifica_id', 'collegio_id', 'categoria_id', 'fullname', 'email', 'email_verified_at', 'password', 'attivo', 'slug', 'activeCampaignId', 'completed_at', 'area_operativa', 'preferences','stripe_id', 'last_four', 'card_brand', 'isadmin', 'tipo'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
        protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'preferences' => 'array'
    ];

    public function getidCriptatoAttribute(){
        return encrypt($this->id);
    }

    public function idDecriptato(){
        try {
            return $decrypted = decrypt($encryptedValue);
        } catch (DecryptException $e) {
            return $e;
        }
    }
    //relazioni

    public function notifiche(){
        return $this->hasMany('App\Notifica', 'destinatario_id');
    }

    public function raccolte(){
        return $this->hasMany('App\Raccolta', 'user_id')->orderBy('updated_at', 'DESC');
    }

    public function raccolteuser(){
        return $this->raccolte()->where('coverable_type', get_class(new User));
    }

    public function raccoltecontenuti(){
        return $this->raccolte()->where('coverable_type', get_class(new Articolo))->orWhere('coverable_type', get_class(new Galleria));
    }

    public function raccolteannunci(){
        return $this->raccolte()->where('coverable_type', get_class(new Annuncio));
    }

    public function raccolta() {
        return $this->morphMany('App\Raccolta', 'coverable');
    }

    public function likes(){
        return $this->hasMany('App\Like', 'user_id');
    }

    public function partecipazioni(){
        return $this->hasMany('App\Partecipazione', 'user_id');
    }

    public function carte(){
        return $this->hasMany('App\Carta', 'user_id');
    }

    public function subscriptions(){
        return $this->hasMany('App\Subscription', 'user_id');
    }

    public function roles()
    {
        return $this->belongsToMany('App\Ruolo', 'user_ruolo', 'user_id', 'ruolo_id');
    }

    public function qualifica()
    {
        return $this->belongsTo('App\Qualifica', 'qualifica_id');
    }

    public function collegio()
    {
        return $this->belongsTo('App\AlboCollegio', 'collegio_id');
    }

    public function categoria()
    {
        return $this->belongsTo('App\Categoria', 'categoria_id');
    }

    public function prestazioni()
    {
        return $this->belongsToMany('App\Prestazione', 'user_prestazione', 'user_id', 'prestazione_id');
    }

    public function abilitazioni()
    {
        return $this->belongsToMany('App\Abilitazione', 'user_abilitazione', 'user_id', 'abilitazione_id');
    }

    public function immobili()
    {
        return $this->hasMany('App\Immobile')->orderBy('updated_at', 'desc');
    }

    public function certificazione()
    {
        return $this->hasOne('App\UserCertificazione', 'user_id');
    }

    public function suggerimenti()
    {
        return $this->belongsToMany('App\Suggerimento', 'user_suggerimento', 'user_id', 'suggerimento_id')->withPivot('visibile');
    }

    public function meta(){
        return $this->hasOne(UserMeta::class, 'user_id');
    }

    public function token()
    {
        return $this->hasMany('App\UserToken', 'user_id');
    }

    public function articoli()
    {
        return $this->hasMany('App\Articolo', 'user_id');
    }

    public function articolipubblicati()
    {
        return $this->hasMany('App\Articolo', 'user_id')->where('pubblicato', 1);
    }

    public function sonoautore()
    {
        return $this->hasMany('App\Immobile', 'user_id');
    }

    public function sonotecnico()
    {
        return $this->hasMany('App\Immobile', 'tecnico_id');
    }

    public function sonoagente()
    {
        return $this->hasMany('App\Immobile', 'agente_id');
    }

    public function candidature(){
        return $this->hasMany('App\Candidatura', 'user_id');
    }

    public function candidatureFilterByAnnuncio($annuncioid){
        return $this->hasMany('App\Candidatura', 'user_id')->whereHas('dettaglio', function($query) use ($annuncioid) {
            $query->where('annuncio_id', $annuncioid);
        })->withoutGlobalScope(FilterByAttivo::class);
    }

    public function annunci_compatibili()
    {
        return $this->hasMany('App\AnnunciCompatibili', 'ricevente_id')->with(['coverable.immobile.specifica.destinazione', 'coverable.immobile.informazione', 'coverable.immobile.user','coverable.immobile.tecnico']);
    }

    public function annunciComeResponsabileTecnico(){
        return $this->hasMany('App\AnnunciCompatibili', 'ricevente_id')->whereHasMorph('coverable', ['App\Annuncio'], function(Builder $query){
            $query->where('type', config('constants.type_annunci.resp_tecnico'));
        });
    }

    public function conversazioni()
    {
        return $this->hasMany('App\Conversazione', 'creatore_id');
    }

    public function sonopartecipe()
    {
        return $this->belongsToMany('App\Conversazione', 'conversazione_user', 'user_id', 'conversazione_id');
    }

    public function gallerie()
    {
        return $this->hasMany('App\Galleria', 'user_id');
    }

    public function galleriepubblicate()
    {
        return $this->hasMany('App\Galleria', 'user_id')->where('pubblicata', 1);
    }

    //FINE RELAZIONI

    public function abbonamento_attuale(){
        return $this->subscriptions()->where('status', 1)->first();
    }


    public function newSubscription($stripe, $customer_id, $stripe_plan){
        return $stripe->subscriptions->create([
            'customer' => $customer_id,
            'items' => [
                ['price' => $stripe_plan],
            ],
        ]);
    }

    public function isProfessionista(){
        return !$this->tipo == 'professionista' ? false : true;
    }

    public function isImpresa(){
        return !$this->tipo == 'impresa' ? false : true;
    }

    public function isAgente(){
        return !$this->tipo == 'agente' ? false : true;
    }

    public function isAdmin(){
        return $this->roles->where('nome', 'admin')->isNotEmpty();
    }

    public function isCertified(){
        return $this->certificazione->where('status', '????')->isNotEmpty();
    }

    public function viewSuggerimento($suggerimento){
        $suggerimento = Suggerimento::where('nome', $suggerimento)->first();
        if($suggerimento) {
            $filterSuggerimento = Auth::user()->suggerimenti->where('id', $suggerimento->id)->first();
            if(!$filterSuggerimento){
                return false;
            }
            return  $filterSuggerimento->pivot->visibile == 1 ? true : false;
        }
        return false;
    }


    public function isInRaccolta(int $raccolta, string $class) {
      return count(Auth::user()->raccolte->where('coverable_id', $raccolta)->where('coverable_type', $class)) == 1;
    }

    public function isInLike(int $like, string $class) {
        return count(Auth::user()->likes->where('coverable_id', $like)->where('coverable_type', $class)) == 1;
    }

    //mutator
    public function getDefaultPaymentMethodAttribute()
    {
        return $this->carte()->where('default', 1)->first();
    }

    public function getNomeCognomeAttribute(){
        return  $this->fullname;
    }

    public function getDisplayNameAttribute(){
        #TO_REVIEW#
        return  $this->tipo == 'professionista' || $this->tipo == 'impresa' ? "<span class=\"qualifica\">{$this->qualifica->nome_pubblico}</span> {$this->nome_cognome}" : "{$this->meta->ragione_sociale}";
    }

    

    public function getComuneAttribute()
    {
        return $this->meta->descrizione_comune;
    }

    public function getInfoAttribute()
    {
        return $this->meta->info_generali;
    }
    
    public function getProvinciaAttribute()
    {
        return $this->info['provincia'];
    }

    public function getCognomeNomeAttribute(){
        return  $this->tipo == 'professionista' ? "{$this->meta->cognome} {$this->meta->nome}" : "{$this->meta->ragione_sociale}";
    }

    public function getNomeAttribute(){
        return  $this->tipo == 'professionista' ? "{$this->meta->nome}" : null;
    }

    public function getCognomeAttribute(){
        return  $this->tipo == 'professionista' ? "{$this->meta->cognome}" : null;
    }

    public function getRagioneSocialeAttribute(){
        return  $this->tipo == 'impresa' ? "{$this->meta->ragione_sociale}" : null;
    }

    public function getRappresentanteAttribute(){
        #TO_REVIEW#
        return !is_null($this->user_meta_impresa()->first()->rappresentante) ? unserialize($this->user_meta_impresa()->first()->rappresentante) : [];
    }

    public function getNomeRappresentanteAttribute(){
        #TO_REVIEW#
        return array_key_exists('nome', $this->rappresentante) ? $this->rappresentante['nome'] : null;
    }

    public function getCognomeeRappresentanteAttribute(){
        #TO_REVIEW#
        return array_key_exists('cognome', $this->rappresentante) ? $this->rappresentante['cognome'] : null;
    }

    public function getTitoloRappresentanteAttribute(){
        #TO_REVIEW#
        return array_key_exists('titolo', $this->rappresentante) ? $this->rappresentante['titolo'] : null;
    }

    public function getInizialiAttribute(){
        return  $this->tipo == 'professionista' ? "{$this->meta->nome[0]}{$this->meta->cognome[0]}" : "{$this->meta->ragione_sociale[0]}";
    }

    public function getAreaOperativaAttribute() {
        $areaoperativa = DB::select('SELECT ST_AsText(area_operativa) as area from users WHERE id ='. $this->id .' ');
        $area = (!empty($areaoperativa[0])) ? str_replace(["POLYGON((", "))"], "", $areaoperativa[0]->area) : '' ;
        return $area;
    }

    public function getNomeEmailAttribute(){
        return "{$this->nome_cognome} - {$this->email}";
    }

    public function getImmobiliForGallerieAttribute(){
        return $this->sonotecnico->union($this->immobili);
    }
    /********* FINE MUTATOR **********/
    // scopes
    public function scopeIsAttivo($query) {
       return $query->where('attivo', 1);
    }

    public function scopeIsCompletato($query){
        return $query->whereNotNull('completed_at');
    }

    public function scopeNotAdmin($query){
        return $query->where('isadmin', 0);
    }

    public function scopeProfessionista($query)
    {
        return $query->where('attivo', 1)->whereNotNull('completed_at')->where('tipo', 'professionista');
    }

    public function scopeImpresa($query)
    {
        return $query->where('attivo', 1)->whereNotNull('completed_at')->where('tipo', 'impresa');
    }

    public function scopeFindByImmobile($query, $immobile){
        return $query->whereRaw('ST_Contains(area_operativa, (SELECT punto FROM immobili where id = '.$immobile.'))');
        //return $query->where('id', 'NOT LIKE', Auth::id())->whereRaw('ST_Contains(area_operativa, (SELECT punto FROM immobili where id = '.$immobile.'))');
    }

    public function scopeFindByAcquirente($query, $acquirente){

        dd($acquirente);

        return $query->whereRaw('ST_Intersects(area_operativa, (SELECT area_immobile FROM immobili where id = '.$acquirente.'))');
    }

}
