<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AlboCollegio extends Model
{
    
    public $table = 'albo_collegio';

    public function collegio()
    {
        return $this->belongsTo('App\Collegio', 'collegio_id');
    }

    public function albo()
    {
        return $this->belongsTo('App\Albo', 'albo_id');
    }


}
