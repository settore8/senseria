<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Immagine extends Model
{
    public $table = 'immagini';

    protected $fillable = ['width', 'height', 'titolo', 'didascalia', 'filename', 'filemime', 'size'];

    public function galleria()
    {
        return $this->belongsToMany('App\Galleria', 'galleria_immagine', 'immagine_id', 'galleria_id');
    }
    /** MUTATOR **/
    public function getGalleriaAttribute(){
        return $this->galleria()->first();
    }

    public function geturlgalleria(Galleria $galleria){
        return Storage::exists('gallerie/'.$galleria->user_id.'/'.$galleria->codice.'/'.$this->filename) ? Storage::url('gallerie/'.$galleria->user_id.'/'.$galleria->codice.'/'.$this->filename) : null;
    }
}
