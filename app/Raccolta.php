<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Raccolta extends Model
{
    use SoftDeletes;

    protected $table = 'raccolte';

    protected $fillable = ['user_id', 'coverable_id', 'coverable_type'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function coverable(){
        return $this->morphTo();
    }

    public function scopeProfessionisti($query) {
        return $query->whereHasMorph('coverable', ['App\User'], function(Builder $q){
            $q->where('tipo', 'professionista');
        });
    }

}
