<?php

namespace App\Http\Controllers;

use Auth;
use Mail;
use App\Annuncio;
use App\Notifica;
use App\TipoNotifica;
use App\Events\SalvaNote;
use App\AnnunciCompatibili;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Events\NuovoCandidatoResponsabileTecnico;
use App\Events\CandidaturaResponsabileTecnicoAggiornata;

class AnnunciController extends Controller
{
    public function index($type = null){
        $annunci = Auth::user()->annunci_compatibili();

        if($type) {
            $annunci = $annunci->whereHasMorph('coverable', [get_class(new Annuncio)], function ($query) use ($type) {
                $query->where('type', $type);
            });
        }
        $annunci = $annunci->simplePaginate(24);

      
        return view('app.annunci.index', compact('annunci', 'type'));
    }

    public function search(Request $request, $type = null){
        
        $s = $request->s;
        $type = $request->type;
        $area = $request->area;

        /*
        if (is_null($s)) {
            return redirect()->route('annunci')->with('message', ['type' => 'error', 'text' => 'nessun termine di ricerca']);
        }
        */
        if($area) {
            // ricerco solo tra gli annunci compatibili
            $annunci = Auth::user()->annunci_compatibili();
        } else {
            // ricerco tra tutti gli annunci
            $annunci = new Annuncio;
        }

        if($type) {
            // ricerco solo tra gli annunci compatibili
            $annunci = $annunci->orderBy('created_at', 'DESC')->whereHasMorph('coverable', [get_class(new Annuncio)], function ($query) use ($type) {
                $query->where('type', $type);
            });
        }

        if($s) {
            // QUI VA CERCATO DENTRO LE INFO DELL'IMMOBILE
            $annunci = $annunci->whereHas('immobile.informazione', function ($query) use ($s) {
                $query->where('comune', 'LIKE', '%' . $s . '%');
                $query->orWhere('indirizzo', 'LIKE', '%' . $s . '%');
            })
            // QUI VA CERCATO DENTRO LA SPECIFICA DELL'IMMOBILE
            ->orWhereHas('immobile.specifica', function ($query) use ($s) {
                $query->where('nome_pubblico', 'LIKE', '%' . $s . '%');
            })
            // QUI SI CERCA IL CODICE ANNUNCIO
            ->orWhere('codice', $s);
        }

        
        $annunci =  $annunci->orderBy('updated_at', 'DESC')->simplePaginate(24);

        return view('app.annunci.index', compact('annunci', 'type'));
    }


    public function candidature(){
        
        return view('app.annunci.candidature');
    }


    public function show($codice){
        $annuncio = Annuncio::where('codice', $codice)->first();
        return view('app.annunci.annuncio.show', compact('annuncio'));
    }

    public function candidatura(Annuncio $annuncio){

        return view('app.annunci.candidatura', compact('annuncio'));
    }

    public function storeCandidatura(Request $request, Annuncio $annuncio){
        try{
            $dettagli = $annuncio->dettagli->count();
            $this->validate($request, [
                'prezzo' => 'required|min:'.$dettagli,
            ], [
                'prezzo.required' => 'Prezzo obbligatorio',
                'prezzo.min' => 'Inserire il prezzo per ogni servizio',
            ]);
    
            foreach($request->prezzo as $dettaglio => $prezzo){
                $dettaglio_create = $annuncio->candidature()->create(['dettaglio_id' => $dettaglio, 'user_id' => Auth::id(),'quotazione' => $prezzo]);
                if($request->note[$dettaglio] != ""){
                    event(new SalvaNote($request->note[$dettaglio], $dettaglio_create));
                } 
            }

            event(new NuovoCandidatoResponsabileTecnico($annuncio->immobile->user->email, Auth::user(), $annuncio->candidatureFilterByUser(Auth::id())));
    
            return redirect()->route('dashboard')->with('message', ['type' => 'success', 'text' => 'Candidatura avvenuta con successo']);
        }catch(\Exception $e){
            dd($e);
            return redirect()->route('dashboard')->with('message', ['type' => 'error', 'text' => 'Errore salvataggio candidatura']);
        }
    }

    public function aggiornacandidatura(Annuncio $annuncio){
        $candidatura = Auth::user()->candidatureFilterByAnnuncio($annuncio->id)->get();
        return view('app.annunci.edit.candidatura', compact('annuncio', 'candidatura'));
    }

    public function updateCandidatura(Request $request, Annuncio $annuncio){
        $dettagli = $annuncio->dettagli->count();
        $this->validate($request, [
            'prezzo' => 'required|min:'.$dettagli,
        ], [
            'prezzo.required' => 'Prezzo obbligatorio',
            'prezzo.min' => 'Inserire il prezzo per ogni servizio',
        ]);

        foreach($request->prezzo as $dettaglio => $prezzo){
            $dettaglio_update = $annuncio->candidatureFilterByUser(Auth::id())->withoutGlobalScopes()->whereDettaglioId($dettaglio)->first();
            if($dettaglio_update){
                $dettaglio_update->update(['dettaglio_id' => $dettaglio],['quotazione'  => $prezzo, 'attivo' => '1']);

            }else{
                $$dettaglio_update = $annuncio->candidature()->create(['dettaglio_id' => $dettaglio, 'user_id' => Auth::id(), 'quotazione' =>  $prezzo]);
            }
            if($request->note[$dettaglio] != ""){
                event(new SalvaNote($request->note[$dettaglio], $dettaglio_update));
            } 
        }

        $tn = TipoNotifica::findOrFail(2);
        $messaggioNotifica = sprintf($tn->messaggio, Auth::user()->nome_cognome, $annuncio->immobile->nome);

        $notifica = Notifica::create([
            'mittente_id' => Auth::id(),
            'destinatario_id' => $annuncio->immobile->user->id,
            'route' => 'immobile.responsabiletecnico.candidature',
            'coverable_id' => $annuncio->immobile->id,
            'coverable_type' => get_class($annuncio->immobile),
            'messaggio' => $messaggioNotifica
        ]);

        event(new CandidaturaResponsabileTecnicoAggiornata(sha1($annuncio->immobile->user->id), $messaggioNotifica));

        return redirect()->route('action.aggiornacandidatura', $annuncio)->with('message', ['type' => 'success' , 'text' => 'Candidatura salvata']);
        
    }
}
