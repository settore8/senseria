<?php

namespace App\Http\Controllers\Auth;


use Mail;
use App\Mail\RicordaDiCompletareIlProfilo;

use App\User;
use App\Qualifica;
use App\Custom\Custom;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Events\InserisciToken;
use App\Events\InviaConfermaEmail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showRegistrationForm($tipo = null)
    {
        $qualifiche = Custom::addNull(Qualifica::pluck('nome_pubblico', 'id'));
        return view('auth.register', compact('qualifiche','tipo'));
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        Validator::extend('checkType', function ($attribute, $value, $parameters, $validator) {
            return in_array($value, ['professionista', 'impresa', 'agente']);
        }, 'Tipologia regitrazione non valida');

        $law['type']  = ['required', 'checkType'];
        $law['email'] = ['required', 'email'];
        $law['privacy'] = ['required'];
        $law['condizioni'] = ['required'];

        switch($data['type']){
            case 'professionista':
                $law['qualifica'] = ['required'];
                $law['nome'] = ['required', 'string', 'max:255'];
                $law['cognome'] = ['required', 'string', 'max:255'];
            break;
            case 'impresa':
                $law['ragione_sociale'] = ['required'];
            break;
        }
        return Validator::make($data, $law, $this->validateMessage($data));
    }

    protected function validateMessage($data){
        $message['email.required'] = 'Email obbligatoria';
        $message['email.emal'] = 'Email obbligatoria';
        $message['privacy.required'] = 'Devi accettare la privacy policy';
        $message['condizioni.required'] = 'Devi accettare termini e condizioni';

        switch($data['type']){
            case 'professionista':
                $message['qualifica.required'] = 'Qualifica obbligatoria';
                $message['nome.required'] = 'Nome obbligatorio';
                $message['nome.string'] = 'Il nome non deve contenere numeri';
                $message['nome.max:255'] = 'Massimo 255 caratteri';
                $message['cognome.required'] = 'Cognome obbligatorio';
                $message['cognome.string'] = 'Il cognome non deve contenere numeri';
                $message['cognome.max:255'] = 'Massimo 255 caratteri';
            break;
            case 'impresa':
                $message['ragione_sociale.required'] = 'Ragione sociale obbligatoria';
            break;
        }

        return $message;
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        if($data['type'] == 'professionista'){
            $user = User::create([
                'tipo' => $data['type'],
                'qualifica_id' => $data['qualifica'],
                'email' => $data['email'],
                'fullname' => $data['nome'] . ' ' . $data['cognome']
            ]);
            $user->meta()->create([
                'nome' => $data['nome'],
                'cognome' => $data['cognome']
            ]);
        }else{
            $user = User::create([
                'tipo' => $data['type'],
                'email' => $data['email'],
                'fullname' => $data['ragione_sociale'],
                ]);
            $user->meta()->create([
                'ragione_sociale' => $data['ragione_sociale']
             ]);
        }

        $user->update([
            'slug' => Custom::createUsersSlug($user)
        ]);

        return $user;
    }

    public function register(Request $request)
    {
        try{
            $this->validator($request->all())->validate();
            $user =  User::where('email', $request->email)->whereNotNull('email_verified_at')->first();
            if(!$user){
                //se esiste di già l'utente non lo vado a ricreare
                $user = $this->create($request->all());
            }else{
                return redirect()->route('boarding.step1');
            }

            // questo avviene solo per colore nuovi
            $token = Str::random(60);
            $type = 'confermaEmail';
            $encoding_type = base64_encode($type);

            event(new InserisciToken($user, $token, $type));
            event(new InviaConfermaEmail($user, $token, $encoding_type));

            return redirect()->route('registrazione')->with('registrazione', 'Abbiamo inviato una mail alla tua casella di posta');
        }catch(\Exception $e){
            dd($e);
            return redirect()->route('registrazione')->with('registrazione', 'Errore');
        }
    }

    public function redirectPath()
    {
        if (method_exists($this, 'redirectTo')) {
            return $this->redirectTo();
        }

        return property_exists($this, 'redirectTo') ? $this->redirectTo : '/home';
    }
}
