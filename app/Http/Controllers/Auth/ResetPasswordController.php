<?php

namespace App\Http\Controllers\Auth;

use Crypt;
use App\User;
use Validator;
use App\UserToken;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Contracts\Encryption\DecryptException;
class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/';

    public function showResetForm(Request $request, $token = null)
    {
        return view('auth.passwords.reset')->with(
            ['token' => $request->token, 'email' => $request->email, 'user' => $request->user, 'type' => $request->type]
        );
    }

    public function reset(Request $request)
    {
        // $this->validate($request, $this->rules(), $this->validationErrorMessages());
        $request->validate($this->rules($request), $this->validationErrorMessages());
        $user = User::where('email', $request->email)->first();
        $user->token()->where('token', $request->token)->where('type', $request->type)->first()->delete();
        $user->update([
            'password' => bcrypt($request->password)
        ]);
        $user->setRememberToken(Str::random(60));
        //////////// Da capire se vogliamo farlo loggare o meno //////////
        return redirect($this->redirectPath());

    }

    protected function rules($request){
        try {
            $user_id = Crypt::decrypt($request->user);
        } catch (Illuminate\Contracts\Encryption\DecryptException $e) {
            return redirect()->route('login')->with('Parametri passati non validi');
        }
        Validator::extend('checkToken', function ($attribute, $value, $parameters, $validator) use ($request, $user_id) {
            return UserToken::where('user_id', $user_id)->where('token', $request->token)->where('type', $request->type)->get()->isNotEmpty();
        }, 'Token non valido');

        return [
            'token' => 'required|checkToken',
            'email' => 'required|email|exists:users,email',
            'password' => 'required|confirmed'
        ];
    }

    protected function validationErrorMessages(){
        return [
            'token.required' => 'Tokejn obbligatorio',
            'email.required' => 'Email obbligatoria',
            'email.email' => 'Formato email non corretto',
            'email.exists' => 'Nessun utente trovato on questa email',
            'password.required' => 'Password obbligatoria',
            'password.confirmed' =>  'Le password non combaciano'
        ];
    }
}
