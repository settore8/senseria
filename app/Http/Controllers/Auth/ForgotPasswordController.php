<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Support\Str;
use App\Events\InserisciToken;
use App\Events\InviaLinkResetPassword;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    public function showLinkRequestForm()
    {
        return view('auth.passwords.email');
    }

    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function sendResetLinkEmail(Request $request)
    {
        $this->validateEmail($request);
        $user = User::where('email', $request->email)->first();

        $token = Str::random(60);
        $type = 'resetPassword';

        event(new InserisciToken($user, $token, $type));
        event(new InviaLinkResetPassword($user, $token, $type));

        return redirect()->route('auth.password.request.email')->with('registrazione', 'Abbiamo inviato una mail alla tua casella di posta');
        // ['title' => 'Email inviata', 'text' => 'Ti abbiamo inviato una mail all\'indirizzo '.$request->email .'. Segui le istruzioni']
    }

    protected function validateEmail(Request $request)
    {
        $request->validate(['email' => 'required|email|exists:users,email'], $this->validateMessage());
    }

    protected function validateMessage(){
        return [
            'email.required' => 'Email obbligatoria',
            'email.email' => 'Formato email non corretto',
            'email.exists' => 'Email non trovata'
        ];
    }
}
