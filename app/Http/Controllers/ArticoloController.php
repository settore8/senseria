<?php

namespace App\Http\Controllers;

use App\Mail\NotificaTagArticolo;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Storage;
use App\Tag;
use App\Gruppo;
use App\Articolo;
use App\Categoria;
use App\ParolaChiave;
use Illuminate\Support\Str;
use App\Events\SalvaAllegati;
use App\Events\SalvaThumbnail;
use Illuminate\Http\Request;

class ArticoloController extends Controller
{
    /**** Mostra tutti gli argomenti non filtrati per categoria ****/
    public function index($categoria = null)
    {
        $articoli = Articolo::pubblicati()->with(['user.meta', 'user.categoria', 'user.qualifica', 'categoria', 'like', 'raccolta'])->orderBy('created_at', 'DESC')->simplePaginate(24);
        if ($categoria) {
            $articoli = Articolo::whereHas('categoria', function ($query) use ($categoria) {
                $query->where('slug', $categoria);
            })->with(['user.meta', 'user.categoria', 'user.qualifica', 'categoria'])->orderBy('created_at', 'DESC')->simplePaginate(24);
            $categoria = Categoria::where('slug', $categoria)->first();
        }

        $view = Auth::check() ? 'app.articoli.index' : 'articoli.index';
        return view($view, compact('articoli', 'categoria'));
    }

    public function suggeriti()
    {
        $articoli = Articolo::pubblicati()->with(['user.meta', 'user.categoria', 'user.qualifica', 'categoria'])->simplePaginate(24);
        $view = Auth::check() ? 'app.articoli.index' : 'articoli.index';
        return view($view, compact('articoli', 'categoria'));
    }

    public function popolari()
    {
        $articoli = Articolo::pubblicati()->with(['user.meta', 'user.categoria', 'user.qualifica', 'categoria'])->simplePaginate(24);
        $view = Auth::check() ? 'app.articoli.index' : 'articoli.index';
        return view($view, compact('articoli', 'categoria'));
    }

    public function search(Request $request)
    {
        $s = $request->s;
        $cat = $request->categoria;
        if (is_null($s) && !is_null($cat)) {
            return redirect()->route('articoli', $cat);
        }

        $articoli = Articolo::pubblicati()->where('titolo', 'LIKE', '%' . $s . '%')
            ->orWhere('testo', 'LIKE', '%' . $s . '%')
            ->orWhereHas('user', function ($query) use ($s) {
                $query->where('fullname', 'LIKE', '%' . $s . '%');
            });

        if (!is_null($cat)) {
            $articoli = $articoli->whereHas('categoria', function ($query) use ($cat) {
                $query->where('slug', $cat);
            });
            dd($articoli);
        } else {
            dd($articoli);
            $articoli = $articoli->orWhereHas('categoria', function ($query) use ($s) {
                $query->where('nome_pubblico', 'LIKE', '%' . $s . '%');
            });
        }

        $articoli = $articoli->orderBy('updated_at', 'DESC')->simplePaginate(12);

        $view = Auth::check() ? 'app.articoli.index' : 'articoli.index';
        return view($view, compact('articoli'));
    }


    public function gestione()
    {
        $articoli = Auth::user()->articoli()->simplePaginate(24);
        return view('app.articoli.gestione', compact('articoli'));
    }


    public function gestionetag()
    {
        $articoli = Auth::pubblicati()->user()->articoli()->simplePaginate(24);
        return view('app.articoli.gestione', compact('articoli'));
    }


    /**** Mostra l'articolo nel dettaglio ****/
    public function show($categoria, $slug, Request $request)
    {
        $articolo = Articolo::pubblicati()->where('slug', $slug)->with(['user.meta', 'user.categoria', 'user.qualifica', 'categoria', 'like'])->first();
        if (!$articolo) {
            return redirect()->route('articoli')->with('message', 'Nessun articolo trovato');
        }
        $seo = true;

        // registra la visita
        views($articolo)->cooldown((int)config('app.resetafter'))->record();

        return view('articoli.show', compact('articolo', 'categoria', 'seo'));
    }

    public function nuovo()
    {
        /** Puo scrivere articoli solo per le categorie assegnate alle sue prestazioni  **/
        $users = User::all();
        $categorie = Categoria::whereIn('id', Auth::user()->prestazioni->pluck('categoria.id')->unique()->values()->all())->pluck('nome_pubblico', 'id');
        return view('app.articoli.articolo.create', compact('categorie', 'users'));
    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'categoria' => 'required',
        ], [
            'categoria.required' => 'Categoria obbligatoria',
        ]);

        $articolo = Articolo::create([
            'user_id' => Auth::id(),
            'categoria_id' => $request->categoria,
        ]);

        return redirect()->route('articolo.edit', compact('articolo'))->with('message', ['type' => 'success', 'text' => 'Completa il tuo articolo']);
    }


    public function edit(Articolo $articolo)
    {
        $categorie = Categoria::whereIn('id', Auth::user()->prestazioni->pluck('categoria.id')->unique()->values()->all())->pluck('nome_pubblico', 'id');
        $parole_chiavi = $articolo->tag;
        $keywords = [];
        foreach ($parole_chiavi as $parola) {
            ;
            array_push($keywords, $parola->tag->nome);
        }
        return view('app.articoli.articolo..edit', compact('categorie', 'articolo', 'keywords'));
    }

    public function update(Request $request, Articolo $articolo)
    {
        $this->validate($request, [
            'categoria' => 'required',
            'titolo' => 'required',
            'testo' => 'required',
            'parole_chiavi' => 'required',
            'immagine' => 'dimensions:min_width=960'
        ], [
            'categoria.required' => 'Categoria obbligatoria',
            'titolo.required' => 'Titolo obbligatorio',
            'testo.required' => 'Testo obbligatoria',
            'parole_chiavi.required' => 'Parole chiavi obbligatorie',
            'immagine.dimensions' => 'Lunghezza minima: 960px'
        ]);

        $articolo->update([
            'categoria_id' => $request->categoria,
            'titolo' => $request->titolo,
            'testo' => $request->testo,
            'slug' => Str::slug($request->titolo, '-') . '.' . Str::random(6),
        ]);

        if ($request->collaboratori) {
            foreach ($request->collaboratori as $chiave => $collaboratore) {
                $contributo = $request->descrizione_collaboratore[$chiave];
                $esito = $articolo->partecipazioni()->updateOrCreate([
                    'user_id' => $collaboratore,
                ], [
                    'user_id' => $collaboratore,
                    'contributo' => $contributo,
                    'token' => $token = Str::random(64),
                ]);
            }
        }
        if ($request->pubblicabile) {
            if ($articolo->pubblicato == 0) {
                foreach ($articolo->partecipazioni as $partecipazione) {
                    Mail::to('alessio@alessiomealli.com')->send(new NotificaTagArticolo($articolo, $partecipazione->token, auth::user()->nomecognome, $partecipazione->contributo));
                }
            } else {
                if ($esito->wasRecentlyCreated) {
                    foreach ($articolo->partecipazioni as $partecipazione) {
                        Mail::to('alessio@alessiomealli.com')->send(new NotificaTagArticolo($articolo, $partecipazione->token, auth::user()->nomecognome, $partecipazione->contributo));
                    }
                }
            }
        }

        $articolo->update([
            'pubblicato' => $request->pubblicabile
        ]);

        foreach ($request->parole_chiavi as $parola => $value) {
            $tag = Tag::firstOrCreate(['nome' => $value]);
            $articolo->tag()->create(['tag_id' => $tag->id]);
        }

        if ($request->hasFile('immagine')) {
            event(new SalvaThumbnail($request->file('immagine'), $articolo));
        }

        return redirect()->route('articoli')->with('message', ['type' => 'success', 'text' => 'Articolo Aggiornato correttamente']);
    }
}
