<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use App\User;
use Validator;
use App\Gruppo;
use App\Immobile;
use Carbon\Carbon;
use App\Qualifica;
use App\MetaImpresa;
use App\Custom\Custom;
use App\MetaProfessionista;
use Illuminate\Support\Str;
use App\CandidaturaEsterna;
use Illuminate\Http\Request;
use App\Events\SyncImmobiliToUser;
use Illuminate\Contracts\Encryption\DecryptException;
class OnBoardingController extends Controller
{
    public function step1()
    {
        return view('boarding.password');
    }

    public function saveStep1(Request $request){
        $this->validate($request, [
            'password' => 'required|min:8|confirmed|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[@!$#%]).*$/'
        ], [
            'password.required' => 'Password obbligatoria',
            'password.min' => 'Password deve essere almeno di 8 caratteri',
            'password.regex' => 'La password deve contenere almeno una lettera, un numero e un carattere speciale',
            'password.confirmed' => 'Le password non combaciano',
        ]);

        Auth::user()->update([
            'password' => bcrypt($request->password)
        ]);

        return redirect()->route('boarding.step2');
    }



    public function step2()
    {
        //Faccio selezionare le prestazioni
        $competenze = Gruppo::where('tipo', Auth::user()->isProfessionista() ? 'professionista' : 'impresa')->get();
        return view('boarding.competenze', compact('competenze'));
    }

    public function saveStep2(Request $request){
        $this->validate($request, [
            'prestazioni' => 'required',
            'specializzazione' => 'required'
        ], [
            'prestazioni.required' => 'Devi scegliere almeno una competenza',
            'specializzazione.required' => 'Devi scegliere una categoria dove sei specializzato selezionando la stella'
        ]);

        if($request->specializzazione){
            Auth::user()->update(['categoria_id' => $request->specializzazione]);
        }

        if(!empty($request->prestazioni)){
            Auth::user()->prestazioni()->sync($request->prestazioni);
        }

        return redirect()->route('boarding.step3')->with('message', ['type' => 'success', 'text' => 'Competenze aggiunte']);
    }

    public function step3(){
        return view('boarding.area');
    }

    /************ SALVA COORDINATE ************/
    public function saveStep3(Request $request){
        $this->validate($request, [
            'area' => 'required'
        ], [
            'area.required' => 'Seleziona area operativa'
        ]);

        try{
            $coord = $request->area;

            if(Str::contains($request->area, ['(', ')'])){
                $coord = explode(")", str_replace([",(", "(",], "", $coord));
                $listaCoordinate = "";
                //l'ultimo valore deve essere uguale al primo
                $coord[count($coord)-1] = $coord[0];

                foreach($coord as $key => $value){
                    $listaCoordinate .= str_replace(",", " ", $value). ", ";
                }

                $listaCoordinate = substr($listaCoordinate, 0, strlen($listaCoordinate)-2);
            }else{
                $listaCoordinate = $coord;
            }
            Auth::user()->update([
                'area_operativa' => DB::raw("PolygonFromText('POLYGON((".$listaCoordinate."))')")
            ]);

            return redirect()->route('boarding.step4')->with('message', ['type' => 'success', 'text' => 'Area operativa salvata correttamente']);
        }catch(\Exception $e){
            return redirect()->route('account.area')->with('message', ['type' => 'error', 'text' => 'Errore salvataggio area operativa']);
        }
    }

    public function step4(){
        $collegi = Auth::user()->qualifica->albo->collegi;
        return view('boarding.info', compact('collegi')); //vista per completare il profilo --> dati generici
    }

    public function saveStep4(Request $request){
        $loggato = Auth::user();
        Validator::extend('uniquenumeroiscrizione', function ($attribute, $value, $parameters, $validator) use ($loggato){
            return MetaProfessionista::where('numero_iscrizione', $value)->where('user_id', '!=', $loggato->id)->get()->isEmpty(); //l'impresa non ha il numero iscrizione
        }, 'Numero iscrizione gia presente');

        $this->validate($request, [
            'collegio' => 'required',
            'numero_iscrizione' => 'required|uniquenumeroiscrizione',
            'partita_iva' => 'required',
        ], [
            'collegio.required' => 'Collegio obbligatorio',
            'numero_iscrizione.required' => 'Numero iscrizione obbligatorio',
            'partita_iva.required' => 'Partita iva obbligatoria',
        ]);

        if($loggato->isProfessionista()){
            $loggato->update([ 'collegio_id' => $request->collegio ]);
            $loggato->meta()->update([
                'info_generali' => [
                    'numero_iscrizione' => $request->numero_iscrizione,
                    'partita_iva' => $request->partita_iva,
                ],
            ]);
            $loggato->certificazione()->create(['status' => 'attesa']);
        }

        Auth::user()->update(['completed_at' => Carbon::now()]);
        //questo lo metto prima del SyncImmobiliToUser(), perché metto su gli immobili qui sotto il tecnico e nell'eventi gli escludo quelli
        $ce = CandidaturaEsterna::where('email', Auth::user()->email)->get();
        if($ce->isNotEmpty()){
            foreach($ce as $key => $value){
                $immobile = Immobile::find($value->immobile_id);
                if($immobile){
                    $immobile->update(['tecnico_id' => Auth::id()]);
                }
                $value->delete();
            }
        };

        event(new SyncImmobiliToUser($loggato));  /******** CONTROLLARE GLI IMMOBILI CHE PRENDE  ********/

        return redirect()->route('dashboard');
    }

    public function redirectToStep($id){
        try {
            $idDecriptato = decrypt($id);
        } catch (DecryptException $e) {
            return redirect()->route()->with('message', ['type' => 'error', 'text' => 'Id passato non valido']);
        }

        $user = User::findOrFail($idDecriptato);
        Auth::loginUsingId($user->id);

        $route = Custom::getStepByUser($user);
        return redirect()->route($route);

    }
}
