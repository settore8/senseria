<?php

// SubscriptionController.php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Plan;
use Illuminate\Support\Facades\Auth;
use Stripe\Stripe;

class SubscriptionController extends Controller
{
    private $stripe;

    public function __construct()
    {
        $this->stripe = new \Stripe\StripeClient(
            config('cashier.secret')
        );
    }

    public function index()
    {
        $plans = Plan::all();
        return view('app.account.abbonamento.piani', compact('plans'));
    }

    public function show(Plan $plan)
    {
        return view(Auth::user()->stripe_id ? 'app.account.abbonamento.upgrade' : 'app.account.abbonamento.first', compact('plan'));
    }

    public function upgrade(Request $request, Plan $plan)
    {

        $customer = $this->stripe->customers->retrieve(
            auth::user()->stripe_id,
            []
        );

        $subscription = $request->user()->newSubscription(
            $this->stripe, $customer->id,
            $plan->stripe_plan
        );

        $card = $this->stripe->customers->retrieveSource(
            $customer->id,
            $request->selected_card,
            []
        );

        $this->stripe->subscriptions->cancel(
            auth::user()->subscriptions()->where('status', 1)->first()->subscription,
            []
        );

        auth::user()->subscriptions()->update([
            'status' => '0'
        ]);

        auth::user()->subscriptions()->create([
            'subscription' => $subscription->id,
            'card_id' => $request->selected_card,
            'invoice_id' => $subscription->latest_invoice,
            'card_holder' => auth::user()->carte()->where('carta_id', $request->selected_card)->first()->card_holder,
            'card_brand' => $card->brand,
            'last_four' => $card->last4,
            'exp_month' => $card->exp_month,
            'exp_year' => $card->exp_year,
            'plan' => $plan->name,
            'price' => $plan->cost,
            'end_at' => Carbon::now()->addYear(),
            'fatturazione' => $this->indirizzofatturazioneparser($request),
            'status' => '1',
            'auto_renew' => '1',
        ]);

        return redirect()->route('home')->with('success', 'Il tuo piano è stato aggiornato con successo!');

    }

    public function first(Request $request, Plan $plan)
    {
        Stripe::setApiKey(config('cashier.secret'));

        $customer = $this->stripe->customers->create([
            'description' => $request->user()->email,
            'source' => $request->stripeToken,
        ]);

        $subscription = $request->user()->newSubscription(
            $this->stripe, $customer->id,
            $plan->stripe_plan
        );

        $card = $this->stripe->customers->retrieveSource(
            $customer->id,
            $customer->default_source,
            []
        );

        $fingerprint = $this->stripe->tokens->retrieve(
            $request->stripeToken,
            []
        );

        $request->user()->update([
            'stripe_id' => $customer->id,
            'card_brand' => $card->brand,
            'last_four' => $card->last4,
        ]);

        $request->user()->carte()->create([
            'carta_id' => $customer->default_source,
            'fingerprint' => $fingerprint->card->fingerprint,
            'card_holder' => $request->card_holder,
            'card_brand' => $card->brand,
            'last_four' => $card->last4,
            'exp_month' => $card->exp_month,
            'exp_year' => $card->exp_year,
        ]);

        $request->user()->subscriptions()->create([
            'subscription' => $subscription->id,
            'card_id' => $customer->default_source,
            'invoice_id' => $subscription->latest_invoice,
            'card_holder' => $request->card_holder,
            'card_brand' => $card->brand,
            'last_four' => $card->last4,
            'exp_month' => $card->exp_month,
            'exp_year' => $card->exp_year,
            'plan' => $plan->name,
            'price' => $plan->cost,
            'end_at' => Carbon::now()->addYear(),
            'fatturazione' => $this->indirizzofatturazioneparser($request),
            'status' => '1',
            'auto_renew' => '1',
        ]);

        return redirect()->route('home')->with('success', 'Il tuo piano è stato aggiornato con successo!');
    }

    private function indirizzofatturazioneparser(Request $request){

        $indirizzo_fatturazione = [
            "indirizzo" => $request->indirizzo,
            "citta" => $request->citta,
            "provincia" => $request->provincia,
            "telefono" => $request->telefono,
        ];

        return $indirizzo_fatturazione;
    }
}
