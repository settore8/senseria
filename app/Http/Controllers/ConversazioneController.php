<?php

namespace App\Http\Controllers;

use Auth;
use App\Conversazione;
use App\Messaggio;
use Illuminate\Http\Request;
use App\Custom\Custom;
use App\Events\InserisciMessaggio;
use Carbon\Carbon;
use Illuminate\Support\Str;

class ConversazioneController extends Controller
{
    public function indexConversazioni() {
        $conversazioni = Auth::user()->sonopartecipe()->attive()->simplePaginate(24);

        return view('app.conversazioni.index', compact('conversazioni'));
    }

    public function store(Request $request)
    {

        try {
            $conversazione = Conversazione::where('creatore_id', Auth::id())->where('coverable_id', Custom::decrypt($request->coverable_id))->where('coverable_type', Custom::decrypt($request->coverable_type))->first();
            
            if(!$conversazione) {
                $conversazione = Conversazione::create([
                    'codice' => self::createCodiceConversazione(6),
                    'creatore_id' => Auth::id(),
                    'coverable_id' => Custom::decrypt($request->coverable_id),
                    'coverable_type' => Custom::decrypt($request->coverable_type),
                    'attivo' => 1
                ]);
                $conversazione->users()->attach(Auth::id());
                $conversazione->users()->attach(Custom::decrypt($request->destinatario_id));
            }

            if($conversazione) {
                try {
                    Messaggio::create([
                        'conversazione_id' => $conversazione->id,
                        'from' => Auth::id(),
                        'messaggio' => $request->messaggio
                    ]);
                    event(new InserisciMessaggio($conversazione->id, $request->messaggio, Auth::id(), Auth::user()->fullname, Carbon::now()->format('H:i')));
                } catch (\Exception $e) {
                    dd($e);
                    return $e;
                }
            }

        } catch (\Exception $e) {
            dd($e);
            return $e;
        }

        return redirect()->route('conversazione', $conversazione)->with('message', ['type' => 'success', 'text' => 'Messaggio inviato']);
    }

    public function showConversazione(Conversazione $conversazione) {

        return view('app.conversazioni.show', compact('conversazione'));
    }

    public function archivioConversazioni() {
        $user = Auth::user();
        return view('app.conversazioni.archivio', compact('user'));
    }

    private function createCodiceConversazione($lunghezza)
    {

        $codice = Str::lower(Str::random($lunghezza));
        $exist = Conversazione::where('codice', $codice)->first();

        if ($exist) {
            self::createCodiceConversazione($lunghezza);
        } else {
            return $codice;
        }

    }

}
