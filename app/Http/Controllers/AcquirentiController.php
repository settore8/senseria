<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Illuminate\Http\Request;
use App\Acquirente;
use App\Annuncio;
use Illuminate\Support\Str;

// eventi

use App\Events\SyncUserToAcquirente;

class AcquirentiController extends Controller
{
    public function index() {
        $acquirenti = Acquirente::miei()->attivi()->orderBy('updated_at', 'DESC')->simplePaginate(12);
        return view('app.acquirenti.index', compact('acquirenti'));
    }

    public function gestionearchiviati(){
        $acquirenti = Acquirente::miei()->archiviati()->orderBy('updated_at', 'DESC')->simplePaginate(12);
        return view('app.acquirenti.index', compact('acquirenti'));
    }

    public function search(Request $request) {
        $s = $request->s;
        if(is_null($s)) {
            return redirect()->route('acquirenti')->with('message', ['type' => 'error', 'text' => 'nessun termine di ricerca']);
        }
        $acquirenti = Acquirente::miei()
                    ->where('nome', 'LIKE', '%'.$s.'%')
                    ->orWhereHas('specifica', function( $query ) use ( $s ){
                        $query->where('nome_pubblico', 'LIKE', '%'.$s.'%' );
                    })
                    ->orderBy('updated_at', 'DESC')->simplePaginate(12);
        return view('app.acquirenti.index', compact('acquirenti'));
    }

    public function store(Request $request){
        $this->validate($request, [
            'nome' => 'required',
            'tipo_acquirente' => 'required',
            'specifica' => 'required',
            'area_immobile' => 'required',
            'descrizione' => 'required'
        ], [
            'nome.required' => 'Inserisci un nome',
            'tipo_acquirente.required' => 'Seleziona la tipologia di acquirente',
            'specifica.required' => 'Seleziona una specifica',
            'area_immobile.required' => 'Seleziona l\'area dove si ricerca l\'immobile',
            'descrizione.required' => 'Descrivi l\'immobile che sta cercando l\'acquirente',
        ]);

        $coord = $request->area_immobile;

        if (Str::contains($request->area_immobile, ['(', ')'])) {
            $coord = explode(")", str_replace([",(", "(",], "", $coord));
            $listaCoordinate = "";
            //l'ultimo valore deve essere uguale al primo
            $coord[count($coord) - 1] = $coord[0];

            foreach ($coord as $key => $value) {
                $listaCoordinate .= str_replace(",", " ", $value) . ", ";
            }

            $listaCoordinate = substr($listaCoordinate, 0, strlen($listaCoordinate) - 2);
        } else {
            $listaCoordinate = $coord;
        }

        $dettagli = [
            'camere' => $request->camere,
            'bagni' => $request->bagni,
            'descrizione' => $request->descrizione,
        ];

        $acquirente =  Acquirente::create([
            'codice' => Str::lower(Str::random(6)),
            'user_id' => Auth::id(),
            'nome' => $request->nome,
            'tipo_acquirente' => $request->tipo_acquirente,
            'budget' => $request->budget,
            'dettagli' => $dettagli,
            'specifica_id' => $request->specifica,
            'area_immobile' => DB::raw("PolygonFromText('POLYGON((" . $listaCoordinate . "))')")
        ]);

        return redirect()->route('acquirente.edit', $acquirente->codice)->with('modal', ['visual' => 'acquirente-creato.svg', 'close_button' => 'Visualizza','text' => 'Acquirente creato con successo']);
    }

    public function edit(Acquirente $acquirente){
        return view('app.acquirenti.acquirente.edit', compact('acquirente'));
    }

    public function update(Request $request, Acquirente $acquirente){
        $this->validate($request, [
            'nome' => 'required',
            'destinazione' => 'required',
            'specifica' => 'required',
            'area_immobile' => 'required',
            'tipo_acquirente' => 'required'
        ], [
            'nome.required' => 'Inserisci un nome',
            'destinazione.required' => 'Seleziona una destinazione',
            'tipo_acquirente.required' => 'Seleziona la tipologia di acquirente',
            'specifica.required' => 'Selezionan una tipologia',
            'area_immobile.required' => 'Seleziona l\'area dove si ricerca l\'immobile'
        ]);

        $coord = $request->area_immobile;

        if (Str::contains($request->area_immobile, ['(', ')'])) {
            $coord = explode(")", str_replace([",(", "(",], "", $coord));
            $listaCoordinate = "";
            //l'ultimo valore deve essere uguale al primo
            $coord[count($coord) - 1] = $coord[0];
            foreach ($coord as $key => $value) {
                $listaCoordinate .= str_replace(",", " ", $value) . ", ";
            }
            $listaCoordinate = substr($listaCoordinate, 0, strlen($listaCoordinate) - 2);
        } else {
            $listaCoordinate = $coord;
        }

        $acquirente->update([
            'nome' => $request->nome,
            'budget' => $request->budget,
            'area_immobile' => DB::raw("PolygonFromText('POLYGON((" . $listaCoordinate . "))')")
        ]);

        return redirect()->route('acquirente.edit', $acquirente)->with('message', ['type' => 'success', 'text' => 'Acquirente salvato']);
    }


        public function impostazioni(Acquirente $acquirente)
        {   

            return view('app.acquirenti.acquirente.impostazioni.index', compact('acquirente'));
        }

    /* ANNUNCIO ACQUIRENTE */
    public function storeAnnuncioAcquirente(Request $request, Acquirente $acquirente) {


            $annuncio = $acquirente->annunci()->updateOrCreate([
                'type' => config('constants.type_annunci.acquirente')
            ], [
                'codice' => self::createCodiceAnnuncio(6),
                'type' => config('constants.type_annunci.acquirente'),
                'creatore_id' => $acquirente->user_id,
            ]);

            event(new SyncUserToAcquirente($acquirente, $annuncio));

            return redirect()->route('acquirente.edit', $acquirente)->with('modal', ['text' => 'Annuncio creato']);

    }

    /* ARCHIVIA ACQUIRENTE */

    public function archivia(Request $request, Acquirente $acquirente) {
        $this->validate($request, [
            'action' => 'required'
        ], [
            'action.required' => 'Per favore digita "ARCHIVIA" nel campo',
        ]);
        
        if($request->action != 'ARCHIVIA') {
            return redirect()->back()->with('message', ['type' => 'error', 'text' => 'Digita "ARCHIVIA nel campo"']);
        }
        $acquirente->update([
            'archiviato' => 1
        ]);

        return redirect()->route('acquirenti')->with('message', ['type' => 'success', 'text' => 'Acquirente cancellato']);

    }


    /* CANCELLAZIONE ACQUIRENTE */

    public function destroy(Request $request, Acquirente $acquirente) {
        
        $this->validate($request, [
            'action' => 'required'
        ], [
            'action.required' => 'Per favore digita "CANCELLA" nel campo',
        ]);
        
        if($request->action != 'CANCELLA') {
            return redirect()->back()->with('message', ['type' => 'error', 'text' => 'Digita "CANCELLA nel campo"']);
        }

        $acquirente->delete();

        return redirect()->route('acquirenti')->with('message', ['type' => 'success', 'text' => 'Acquirente cancellato']);

    }



    private function createCodiceAnnuncio($lunghezza)
    {

        $codice = Str::lower(Str::random($lunghezza));
        $exist = Annuncio::where('codice', $codice)->first();

        if ($exist) {
            self::createCodiceAnnuncio($lunghezza);
        } else {
            return $codice;
        }

    }


    
}
