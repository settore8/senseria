<?php

namespace App\Http\Controllers;

use App\Events\CompletamentoModulo;
use App\Events\CompletamentoVerificaTecnica;
use App\Events\InvioMailPerImmobileCondiviso;
use DB;
use Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;
use phpDocumentor\Reflection\Types\Null_;
use Storage;
use App\User;
use App\File;
use test\Mockery\ArgumentObjectTypeHint;
use App\Modulo;
use App\Locale;
use App\Allegato;
use App\Archivio;
use App\Immobile;
use App\Annuncio;
use Carbon\Carbon;
use App\UserToken;
use App\Intervento;
use App\Candidatura;
use App\Custom\Custom;
use App\ImmobileCondiviso;
use App\ImmobileSopralluogo;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

//Events
use App\Events\AssegnaToken;
use App\Events\RimossoModulo;
use App\Events\AggiuntoModulo;
use App\Events\SyncAgentiToVendita;
use App\Events\ImmobileCondiviso as Condivisione;
use App\Events\SyncUserToImmobile;
use App\Events\SceltoAgenteImmobiliare;
use App\Events\ResponsabileTecnicoNonRegistrato;
use App\Events\SceltoResponsabileTecnicoDaWorkook;
use App\Events\NotificaModuloAggiuntoAlResponsabileTecnico;

//repository
use App\Repository\FabbricatoRepository;

class ImmobileController extends Controller
{
    public function index()
    {   /*
        // #TO_REVIEW#  Ordinare per ultimi modificati.
        $iMieiImmobili = Immobile::miei()->get()->keyBy('id');
        $union = $iMieiImmobili->union(Auth::user()->sonotecnico->keyBy('id'));
        $immobili = $union->take(6);
        */
        $immobili = Immobile::with('specifica.destinazione', 'informazione', 'annuncioRespTecnico', 'annuncioVendita', 'annuncioAgenteImmobiliare', 'tecnico.qualifica')->gestibili()->nonArchiviati()->orderBy('updated_at', 'DESC')->simplePaginate(12);

        // questo dato serve come promemoria per vedere come salvare i dati percentuali dentro immobile poi lo recuperiamo da $immobile->percentuali o qualcosa del genere
        $perc = [
            'totale' => rand(5, 90),
            'sopralluogo' => '15',
            'verificatecnica' => '25'
        ];

        return view('app.immobili.index', compact('immobili', 'perc'));
    }


    public function search(Request $request)
    {

        $s = $request->s;
        if (is_null($s)) {
            return redirect()->route('immobili')->with('message', ['type' => 'error', 'text' => 'nessun termine di ricerca']);
        }
        /*
        $iMieiImmobili = Immobile::miei()->get()->keyBy('id');
        $union = $iMieiImmobili->union(Auth::user()->sonotecnico->keyBy('id'));
        $immobili = $union->take(6);
        */

        $immobili = Immobile::where('nome', 'LIKE', '%' . $s . '%')
            ->orWhere('codice', $s)
            ->orWhereHas('informazione', function ($query) use ($s) {
                $query->where('comune', 'LIKE', '%' . $s . '%');
                $query->orWhere('indirizzo', 'LIKE', '%' . $s . '%');
            })
            ->orWhereHas('tecnico', function ($query) use ($s) {
                $query->where('fullname', 'LIKE', '%' . $s . '%');
            })
            ->orWhereHas('specifica', function ($query) use ($s) {
                $query->where('nome_pubblico', 'LIKE', '%' . $s . '%');
            })->gestibili()
            ->orderBy('archiviato', 'ASC')
            ->orderBy('updated_at', 'DESC')
            ->simplePaginate(12);

        return view('app.immobili.index', compact('immobili'));
    }


    public function gestione($type = null)
    {
        $immobili = Immobile::miei()->orderBy('updated_at', 'DESC')->simplePaginate(24);

        return view('app.immobili.index', compact('immobili'));
    }

    public function gestioneassegnati()
    {
        $immobili = Auth::user()->sonotecnico()->orderBy('updated_at', 'DESC')->simplePaginate(24);
        return view('app.immobili.index', compact('immobili'));
    }

    public function gestionearchiviati()
    {
        // #TO_REVIEW#  va fatto un local scope che prenda solo gli immobili creati da me archiviati.
        $immobili = Immobile::gestibili()->archiviati()->orderBy('updated_at', 'DESC')->simplePaginate(24);

        return view('app.immobili.index', compact('immobili'));
    }


    public function immobile(Immobile $immobile)
    {

        return view('app.immobili.immobile.riepilogo', compact('immobile'));
    }

    public function impostazioni(Immobile $immobile)
    {
        return view('app.immobili.immobile.impostazioni.index', compact('immobile'));
    }

    public function statistiche(Immobile $immobile)
    {
        return view('app.immobili.immobile.statistiche.index', compact('immobile'));
    }

    public function condivisione(Immobile $immobile)
    {
        $moduli = Modulo::all();
        $today = Carbon::now();
        return view('app.immobili.immobile.condivisione.index', compact('immobile', 'today', 'moduli'));
    }

    public function creaDownload(Immobile $immobile)
    {
        return view('app.immobili.immobile.download.index', compact('immobile'));
    }

    public function informazioni(Immobile $immobile)
    {
        $consistenze = Archivio::sopralluogo()->first()->dati['unitaImmobiliare']['consistenza'][$immobile->destinazione->nome];
        return view('app.immobili.immobile.informazioni', compact('immobile', 'consistenze'));
    }

    public function allegati(Immobile $immobile)
    {
        return view('app.immobili.immobile.allegati', compact('immobile'));
    }

    public function vendita(Immobile $immobile)
    {
        $interventi = Custom::addNull(Intervento::pluck('nome', 'id'));
        return view('app.immobili.immobile.vendita', compact('immobile', 'interventi'));
    }

    public function sopralluogo(Immobile $immobile)
    {
        return view('app.immobili.immobile.sopralluogo.index', compact('immobile'));
    }

    public function sopralluogo_contesto(Immobile $immobile)
    {
        $contesto = Archivio::sopralluogo()->first()->dati['contesto'];
        return view('app.immobili.immobile.sopralluogo.contesto', compact('immobile', 'contesto'));
    }

    public function sopralluogo_fabbricato(Immobile $immobile)
    {
        $fabbricato = Archivio::sopralluogo()->first()->dati['fabbricato'];
        return view('app.immobili.immobile.sopralluogo.fabbricato', compact('immobile', 'fabbricato'));
    }

    public function sopralluogo_fabbricato_struttura(Immobile $immobile)
    {

        // se non sono tecnico faccio il redirect verso fabbricato
        // se sono tecnico ma non esiste fabbricato faccio il redirect verso fabbricato

        $fabbricato = Archivio::sopralluogo()->first()->dati['fabbricato'];
        if (Auth::id() != $immobile->tecnico_id) {
            return redirect()->route('immobile.sopralluogo.fabbricato', $immobile);
        }
        if ($immobile->sopralluogo->fabbricato == null) {
            return redirect()->route('immobile.sopralluogo.fabbricato', $immobile)->with('modal', ['title' => 'Attenzione', 'text' => 'Per accedere alla struttura devi prima completare il fabbricato', 'visual' => 'message-attention.svg', 'visual_size' => 'small', 'close_button' => 'Ho capito',]);
        }
        return view('app.immobili.immobile.sopralluogo.sopralluogo_struttura', compact('immobile', 'fabbricato'));
    }

    public function sopralluogo_unitaimmobiliare(Immobile $immobile)
    {
        $unita_immobiliare = Archivio::sopralluogo()->first()->dati['unitaImmobiliare'];
        return view('app.immobili.immobile.sopralluogo.unitaimmobiliare', compact('immobile', 'unita_immobiliare'));
    }

    public function sopralluogo_infissi_unitaimmobiliare(Immobile $immobile)
    {
        $unita_immobiliare = Archivio::sopralluogo()->first()->dati['unitaImmobiliare'];
        if (Auth::id() != $immobile->tecnico_id) {
            return redirect()->route('immobile.sopralluogo.unitaimmobiliare', $immobile);
        }
        if ($immobile->sopralluogo->unitaimmobiliare == null) {
            return redirect()->route('immobile.sopralluogo.unitaimmobiliare', $immobile)->with('modal', ['text' => 'Per accedere agli infissi devi prima completare unità immobiliare']);
        }
        return view('app.immobili.immobile.sopralluogo.infissi', compact('immobile', 'unita_immobiliare'));
    }

    public function sopralluogo_impianti_unitaimmobiliare(Immobile $immobile)
    {
        $unita_immobiliare = Archivio::sopralluogo()->first()->dati['unitaImmobiliare'];
        if (Auth::id() != $immobile->tecnico_id) {
            return redirect()->route('immobile.sopralluogo.unitaimmobiliare', $immobile);
        }
        if ($immobile->sopralluogo->unitaimmobiliare == null) {
            return redirect()->route('immobile.sopralluogo.unitaimmobiliare', $immobile)->with('modal', ['text' => 'Per accedere agli impianti devi prima completare unità immobiliare']);
        }
        return view('app.immobili.immobile.sopralluogo.impianti', compact('immobile', 'unita_immobiliare'));
    }

    public function sopralluogo_parti_condominiali(Immobile $immobile)
    {
        $parti_condominiali = Archivio::sopralluogo()->first()->dati['partiCondominiali'];
        return view('app.immobili.immobile.sopralluogo.particondominiali', compact('immobile', 'parti_condominiali'));
    }

    public function sopralluogo_fotovideo(Immobile $immobile)
    {

        return view('app.immobili.immobile.sopralluogo.particondominiali', compact('immobile', 'parti_condominiali'));
    }

    /*
   ############ VERIFICA TECNICA
   #############################
   */

    public function verificatecnica(Immobile $immobile)
    {
        return view('app.immobili.immobile.verificatecnica.index', compact('immobile'));
    }

    public function verificatecnica_datiurbanistici(Immobile $immobile)
    {
        return view('app.immobili.immobile.verificatecnica.datiurbanistici', compact('immobile'));
    }

    public function verificatecnica_catasto(Immobile $immobile)
    {
        return view('app.immobili.immobile.verificatecnica.catasto', compact('immobile'));
    }

    public function verificatecnica_provenienza(Immobile $immobile)
    {
        return view('app.immobili.immobile.verificatecnica.provenienza', compact('immobile'));
    }

    public function verificatecnica_agibilita(Immobile $immobile)
    {
        return view('app.immobili.immobile.verificatecnica.agibilita', compact('immobile'));
    }

    public function verificatecnica_impianti(Immobile $immobile)
    {
        return view('app.immobili.immobile.verificatecnica.impianti', compact('immobile'));
    }

    public function verificatecnica_caldaia(Immobile $immobile)
    {
        return view('app.immobili.immobile.verificatecnica.caldaia', compact('immobile'));
    }

    public function verificatecnica_postuma(Immobile $immobile)
    {
        return view('app.immobili.immobile.verificatecnica.postuma', compact('immobile'));
    }

    public function verificatecnica_altridocumenti(Immobile $immobile)
    {
        return view('app.immobili.immobile.verificatecnica.altridocumenti', compact('immobile'));
    }

    /*
    ############ SCHEDA TECNICA
    #############################
    */


    public function relazionetecnica(Immobile $immobile)
    {
        return view('app.immobili.immobile.relazionetecnica', compact('immobile'));
    }

    public function stima(Immobile $immobile)
    {
        return view('app.immobili.immobile.stima', compact('immobile'));
    }

    public function istruttoria(Immobile $immobile)
    {
        return view('app.immobili.immobile.istruttoria', compact('immobile'));
    }

    public function collaborazioni(Immobile $immobile)
    {
        return view('app.immobili.immobile.collaborazioni', compact('immobile'));
    }

    public function capitolati(Immobile $immobile)
    {
        return view('app.immobili.immobile.capitolati', compact('immobile'));
    }

    public function visuraipotecaria(Immobile $immobile)
    {
        if (!$immobile) {
            abort(404);
        }
        return view('app.immobili.visuraipotecaria.immobile.visuraipotecaria', compact('immobile'));
    }

    public function rilievo3d(Immobile $immobile)
    {
        if (!$immobile) {
            abort(404);
        }
        if ($immobile->rilievo && $immobile->rilievo->stato == 'Bozza') {
            return redirect()->route('immobile.rilievo.edit', $immobile);
        }
        return view('app.immobili.immobile.rilievo3d.rilievo3d', compact('immobile'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'destinazione' => 'required',
            'specifica' => 'required',
            'nome' => 'required'
        ], [
            'destinazione.required' => 'Seleziona una destinazione',
            'specifica.required' => 'Selezionanuna tipologia',
            'nome.required' => 'Inserisci un nome di riferimento'
        ]);
        //faccio un foreach per quanti sono i sottomoduli
        $data = $background = $labels = [];
        foreach (Custom::getSopralluogoModuli() as $modulo) {
            $data['sopralluogo'][$modulo] = [
                'perc' => 0,
                'label' => Lang::get('messages.' . $modulo),
                'color' => config('constants.colore-modulo-noncompletato')
            ];

        }
        $agente = null;

        if (Auth::user()->tipo == 'agente') {
            $agente = Auth::id();
        }

        $immobile = Immobile::create([
            'user_id' => Auth::id(),
            'codice' => self::createCodiceImmobile(6),
            'agente_id' => $agente,
            'specifica_id' => $request->specifica,
            'nome' => $request->nome,
            'statistiche' => $data
        ]);

        return redirect()->route('immobile.informazioni', $immobile)->with('modal', ['visual' => 'immobile-creato.svg', 'close_button' => 'Completa', 'text' => 'Immobile creato con successo']);
    }

    public function storeInformazioni(Request $request, Immobile $immobile)
    {
        $this->validate($request, [
            'addressResult' => 'required',
            'descrizione' => 'required',
            'metri_quadri' => 'required',
            'valore_consistenza' => 'required'
        ], [
            'addressResult.required' => 'Digita l\'indirizzo dell\'immobile e selezionalo dai suggerimenti proposti',
            'descrizione.required' => 'Descrizione obbligatoria',
            'metri_quadri.required' => 'Metri quadri obbligatori',
            'valore_consistenza.required' => 'Selezionare almeno una consistenza',
        ]);

        $indirizzo = json_decode($request->addressResult);
        list($lat, $lng) = $indirizzo->geo;

        $arraySaveSpecifiche = [];

        foreach ($request->consistenze as $categoria => $consistenze) {
            $arraySaveSpecifiche[$categoria] = array_combine($consistenze, $request->valore_consistenza[$categoria]);
        }
        $immobile->informazione()->updateOrCreate(['immobile_id' => $immobile->id], [
            'descrizione' => $request->descrizione,
            'metri_quadri' => $request->metri_quadri,
            'indirizzo' => $indirizzo->indirizzo,
            'civico' => $indirizzo->numerocivico,
            'localita' => $indirizzo->localita,
            'comune' => $indirizzo->comune,
            'provincia' => $indirizzo->provincia,
            'regione' => $indirizzo->regione,
            'address' => $request->address,
            'addressResult' => str_replace(array('.', ' ', "\n", "\t", "\r"), '', $request->addressResult), //il dato mi arriva con spazi e valori che non devono essere salvati nel database, cosi li rimuovo ed ho un JSON pulito
            'locali' => $arraySaveSpecifiche
        ]);

        $immobile->update(['punto' => DB::raw("(GeomFromText('POINT($lat $lng)'))")]);

        return redirect()->route('immobile', $immobile)->with('message', ['type' => 'success', 'text' => 'Informazioni salvate con successo']);
    }

    public function storeVendita(Request $request, Immobile $immobile)
    {
        if ($request->agente_immobiliare) {
            try {

                $scelto = User::find($request->agente_immobiliare);

                $token = Str::random(120);
                $type = 'accettaSceltaAgente';
                $encodig_type = base64_encode($type);

                $tokenid = $scelto->token()->create([
                    'token' => $token,
                    'encoding_type' => base64_encode($type),
                    'type' => $type,
                    'scadenza' => Carbon::now()->addDays(7)
                ]);

                event(new AssegnaToken($tokenid, $immobile));
                event(new SceltoAgenteImmobiliare($immobile, Auth::user(), $scelto->email, $token, $encodig_type));

                return redirect()->route('immobile.vendita', $immobile)->with('message', ['type' => 'success', 'text' => 'Email mandata, per completare l\'assegnazione l\'agente deve confermare tramite la email']);
            } catch (\Exception $e) {
                return redirect()->route('immobile.vendita', $immobile)->with('message', ['type' => 'error', 'text' => 'Errore scelta agente immobiliare']);
            }
        }


        // qui é nel caso non scegliere direttamente un agente immobiliare
        $informazioni['proprietario'] = $request->proprietario;

        if ($request->intervento) {
            $informazioni['intervento'] = Intervento::find($request->intervento)->nome;
        }

        $informazioni['prezzo_vendita'] = $request->prezzo_vendita;
        $informazioni['prezzo_locazione'] = $request->prezzo_locazione;
        $informazioni['note'] = $request->note;
        $immobile->vendita()->create([
            'user_id' => Auth::id(),
            'informazioni' => serialize($informazioni)
        ]);


        $annuncio = $immobile->annunci()->updateOrCreate([
            'type' => config('constants.type_annunci.vendita_imm')
        ], [
            'codice' => self::createCodiceAnnuncio(6),
            'type' => config('constants.type_annunci.vendita_imm'),
            'creatore_id' => $immobile->user_id,
        ]);

        event(new SyncAgentiToVendita($immobile, $annuncio));

        return redirect()->route('immobile.vendita', $immobile)->with('message', ['type' => 'success', 'text' => 'Annuncio di vendita creato']);
    }

    public function responsabiletecnico(Immobile $immobile)
    {

        $moduli = Modulo::all();
        return view('app.immobili.immobile.responsabiletecnico.index', compact('immobile', 'moduli'));
    }

    public function storeResponsabileTecnico(Request $request, Immobile $immobile)
    {
        $this->validate($request, [
            'moduli' => 'required'
        ], [
            'moduli.required' => 'Seleziona almeno un servizio tecnico'
        ]);
        try {
            //queste é la parte dove é gia stata selezionato un responsabile descrinatecnico
            if ($immobile->annuncioRespTecnico && $immobile->annuncioRespTecnico->candidatureWGS->isNotEmpty()) {
                if (count($immobile->moduli) != count($request->moduli)) {

                    $candidati = $immobile->annuncioRespTecnico->candidature->pluck('user.email')->unique();
                    if ($immobile->annuncioRespTecnico->dettagli->count() > count($request->moduli)) {

                        $moduliRimossi = Modulo::whereIn('id', array_diff($immobile->annuncioRespTecnico->dettagli->pluck('coverable_id')->toArray(), $request->moduli))->get();

                        $immobile->annuncioRespTecnico->dettagli()->whereIn('coverable_id', $moduliRimossi)->delete();

                        event(new RimossoModulo($candidati, $immobile->annuncioRespTecnico, $moduliRimossi));

                    } else {

                        $moduliaggiunti = Modulo::whereIn('id', array_diff($request->moduli, $immobile->annuncioRespTecnico->dettagli->pluck('coverable_id')->toArray()))->get();

                        foreach ($moduliaggiunti as $modulo) {
                            $immobile->annuncioRespTecnico->dettagli()->create(['coverable_id' => $modulo->id, 'coverable_type' => get_class(new Modulo)]);
                        }

                        event(new AggiuntoModulo($candidati, $immobile->annuncioRespTecnico, $moduliaggiunti));
                    }

                    $immobile->annuncioRespTecnico->candidature()->update(['attivo' => '0']);
                }
            }


            //qui ancora non é stato selezionato nessun ma é stato chiesto a workook di trovarne uno compatibile creado un annuncio che avra dei dettagli
            if ($request->compatibile) {

                $annuncio = $immobile->annunci()->firstOrCreate(
                    [
                        'type' => config('constants.type_annunci.resp_tecnico')
                    ],
                    [
                        'type' => config('constants.type_annunci.resp_tecnico'),
                        'codice' => self::createCodiceAnnuncio(6),
                        'creatore_id' => $immobile->user_id,
                        'stato' => 'attivo'
                    ]
                );

                foreach ($request->moduli as $modulo) {
                    $annuncio->dettagli()->updateOrCreate(
                        ['annuncio_id' => $annuncio->id, 'coverable_id' => $modulo, 'coverable_type' => get_class(new Modulo)],
                        ['coverable_id' => $modulo, 'coverable_type' => get_class(new Modulo)]
                    );
                }

                if (User::FindByImmobile($immobile->id)->get()->isEmpty()) {
                    return redirect()->route('immobile.responsabiletecnico', $immobile)->with('message', ['type' => 'info', 'text' => 'Nessun utente trovato con area compatible a questo immobile']);
                }

                //crea un notifica per quell'annuncio come responsabile tecnico a quelli che hanno l'area compatibile con quell'immobile
                event(new SyncUserToImmobile($immobile, $annuncio));

                return redirect()->route('immobile.responsabiletecnico', $immobile)->with('message', ['type' => 'success', 'text' => 'Notifica come responabile tecnico creata']);
            } elseif ($request->internoaworkook) {
                //qui é se seleziona una persona gia iscritta a workook
                if (is_numeric($request->internoaworkook)) {

                    $scelto = User::find($request->internoaworkook);
                    //imposto i moduli abilitati per quell'immobile
                    $immobile->update(['tecnico_id' => $request->internoaworkook, 'moduli' => $request->moduli]);

                    $token = Str::random(120);
                    $type = 'declinaSceltaTecnico';
                    $encodig_type = base64_encode($type);

                    $tokenid = $scelto->token()->create([
                        'token' => $token,
                        'encoding_type' => base64_encode($type),
                        'type' => $type,
                        'scadenza' => Carbon::now()->addDays(7)
                    ]);

                    event(new AssegnaToken($tokenid, $immobile));
                    event(new SceltoResponsabileTecnicoDaWorkook(Auth::user(), $scelto->email, $token, $encodig_type));

                } else {
                    //qui invece é se mette una email non presente dentro workook
                    $token = Str::random(120);
                    $email = $request->internoaworkook;
                    event(new ResponsabileTecnicoNonRegistrato($email, $token, $immobile->id));
                }

                return redirect()->route('immobile.responsabiletecnico', $immobile)->with('message', ['type' => 'success', 'text' => 'Richiesta inviata con successo']);
            } else {
                //Qui é se viene assegnato a sestesso
                $immobile->update(['tecnico_id' => Auth::id(), 'moduli' => $request->moduli]);
                return redirect()->route('immobile.responsabiletecnico', $immobile)->with('message', ['type' => 'success', 'text' => 'Assegnazione avvenuta con successo']);
            }

        } catch (\Exception $e) {
            dd($e);
            return redirect()->route('immobile.responsabiletecnico', $immobile)->with('message', ['type' => 'error', 'text' => 'Errore salvataggio responsabile tecnico']);
        }
    }

    public function deleteResponsabileTecnico(Request $request, Immobile $immobile)
    {

        $this->validate($request, [
            'action' => 'required'
        ], [
            'action.required' => 'Per favore digita "RIMUOVI" nel campo',
        ]);

        if ($request->action != 'RIMUOVI') {
            return redirect()->back()->with('message', ['type' => 'error', 'text' => 'Digita "RIMUOVI nel campo"']);
        }

        // metto null nella colonna del responsabile tecnico
        $immobile->update(['tecnico_id' => null]);


        // cancello i dati dei servizi tecnici e poi svuoto la colonna moduli
        if ($request->dati == 'cancella') {

            $immobile->sopralluogo()->delete();
            $immobile->verifica_tecnica()->delete();
            $immobile->relazione_tecnica()->delete();
            $immobile->stima()->delete();
            $immobile->istruttoria()->delete();
            $immobile->collaborazione()->delete();
            $immobile->capitolato()->delete();

            $immobile->update(['moduli' => null]);
        }

        return redirect()->route('immobile.responsabiletecnico', $immobile)->with('message', ['type' => 'success', 'text' => 'Responsabile tecnico rimosso']);

    }


    public function storedatasopralluogo(Request $request, Immobile $immobile)
    {

        $this->validate($request, [
            'datasopralluogo' => 'required',
        ], [
            'datasopralluogo.required' => 'Seleziona la data',
        ]);

        if ($immobile->sopralluogo) {
            $immobile->sopralluogo()->update(['data' => $request->datasopralluogo]);
        } else {
            $immobile->sopralluogo()->create(['data' => $request->datasopralluogo]);
        }

        return redirect()->route('immobile.sopralluogo', compact('immobile'))->with('message', ['type' => 'success', 'text' => 'data sopralluogo salvata']);
    }


    public function storecontesto(Request $request, Immobile $immobile)
    {

        $validation = $this->validate($request, [
            'paesaggistico' => 'required',
            'territoriale' => 'required',
            'tipologia_viabilita' => 'required',
            'finitura_viabilita' => 'required',
            'pendenza_viabilita' => 'required',
            'stato_conservazione' => 'required',

        ], [
            'paesaggistico.*' => 'Il contesto paesaggistico è obbligatorio',
            'territoriale.*' => 'Il contesto territoriale è obbligatorio',
            'tipologia_viabilita.*' => 'La tipologia della viabilità di accesso è obbligatoria',
            'finitura_viabilita.*' => 'La finitura della viabilità di accesso è obbligatoria',
            'pendenza_viabilita.*' => 'La pendenza della viabilità di accesso è obbligatoria',
            'stato_conservazione.*' => 'Lo stato di conservazione della viabilità di accesso è obbligatoria',
        ]);

        $trasporti = [];
        $servizi = [];
        $archivio = Archivio::wheretype('sopralluogo')->first()->dati['contesto'];
        $requesttrasporti = array_filter($request->trasporti, function ($val) {
            return !is_null($val);
        });

        foreach ($requesttrasporti as $trasporto => $valore) {
            $trasporti[$archivio['trasporti'][$trasporto]] = $valore;
        }
        if ($request->servizi_vicinanze) {
            foreach ($request->servizi_vicinanze as $servizio => $valore) {
                array_push($servizi, $archivio['serviziVicinanze'][$servizio]);
            }
        }
        $contesto = [
            'paesaggistico' => $request->paesaggistico,
            'territoriale' => $request->territoriale,
            'viabilita' => [
                'tipologia' => $request->tipologia_viabilita,
                'finitura' => $request->finitura_viabilita,
                'pendenza' => $request->pendenza_viabilita,
                'stato_conservazione' => $request->stato_conservazione
            ],
            'trasporti' => $trasporti,
            'servizi' => $servizi
        ];

        foreach ($contesto as $key => $value) {
            if (is_array($value)) {
                foreach ($value as $subkey => $subvalue) {
                    if ($contesto[$key][$subkey] == '--') {
                        $contesto[$key][$subkey] = null;
                    }
                }
            }
            if ($contesto[$key] == '--') {
                $contesto[$key] = null;
            }
        }
        if ($immobile->sopralluogo) {
            $immobile->sopralluogo()->update(['contesto' => $contesto]);
        } else {
            $immobile->sopralluogo()->create(['contesto' => $contesto]);
        }

        //event(new CompletamentoModulo('sopralluogo.contesto', $immobile));
        return redirect()->route('immobile.sopralluogo.fabbricato', compact('immobile'));
    }

    public function storefabbricatoStepOne(Request $request, Immobile $immobile, FabbricatoRepository $fr)
    {
//        dd($request);
        $archivioFabbricato = Archivio::wheretype('sopralluogo')->first()->dati['fabbricato'];

        $this->validate($request, $fr->rulesStepOne());

        if ($request->complesso_immobiliare == 'No') {
            //nel caso non facesse parte di un complesso immobiliare cancello tutte le input text vuote
            $request->request->remove('complessoimmobiliare');
            $complesso = 'No'; //setto questa cosi posso salvarla nel json
        } else {
            $complesso = array_map(function ($val) {
                return is_null($val) ? 0 : $val;
            }, $request->complessoImmobiliare);
            foreach ($complesso as $key => $value) {
                $complesso[$archivioFabbricato['complessoImmobiliare'][$key]] = $value;
                unset($complesso[$key]);
            }
        }
        $array_affacci = [];

        foreach ($request->affaccio as $key => $affaccio) {
            if ($affaccio != '--') {
                $lato = $request->latoaffaccio[$key];
                $array_affacci[$lato] = $affaccio;
            }
        }

        $fabbricato = [
            'annocostruzione' => $request->checkbox_costruzione ?: $request->anno_costruzione,
            'annoristrutturazione' => $request->checkbox_ristrutturazione ?: $request->anno_ristrutturazione,
            'pianifuoriterra' => $request->piani_fuori_terra,
            'pianientroterra' => $request->piani_entro_terra,
            'complessoimmobiliare' => $complesso,
            'affacci' => $array_affacci
        ];
        $immobile->sopralluogo->update(['fabbricato' => $fabbricato]);
        //event(new CompletamentoModulo('sopralluogo.fabbricato', $immobile));
        return redirect()->route('immobile.sopralluogo.fabbricato.struttura', $immobile)->with('message', ['type' => 'success', 'text' => 'Continua inserendo la struttura']);
    }

    public function storefabbricatoStepTwo(Request $request, Immobile $immobile, FabbricatoRepository $fr)
    {
        $this->validate($request, $fr->rulesStepTwo());

        $fabbricato = $immobile->sopralluogo['fabbricato']; //precedente fabbricato creato

        $facciate = [];
        if ($request->facciata_tipologia && !Custom::is_array_null($request->facciata_tipologia) && !Custom::is_array_null($request->facciata_stato_conservazione)) {
            foreach (array_combine($request->facciata_tipologia, $request->facciata_stato_conservazione) as $tipo => $stato) {
                $facciate[] = [$tipo => $stato];
            }
        }

        $struttura_portante_verticale = [];
        if (!Custom::is_array_null($request->struttura_portante_verticale_materiale) && !Custom::is_array_null($request->struttura_portante_verticale_stato_conservazione)) {

            foreach (array_combine($request->struttura_portante_verticale_materiale, $request->struttura_portante_verticale_stato_conservazione) as $materiale => $stato) {
                $struttura_portante_verticale[] = [$materiale => $stato];
            }
        }

        $copertura = [];
        if (!Custom::is_array_null($request->copertura_tipologia) && !Custom::is_array_null($request->copertura_finitura) && !Custom::is_array_null($request->copertura_stato_conservazione)) {
            $i = 0;
            foreach (array_combine($request->copertura_tipologia, $request->copertura_finitura) as $tipologia => $finitura) {
                $copertura[$i][$tipologia] = [
                    'Finitura' => $finitura,
                    'Stato conservazione' => $request->copertura_stato_conservazione[$i++]
                ];
            }
        }
        $struttura_portante_orizzontale = [];
        if (!Custom::is_array_null($request->struttura_portante_orizzontale_materiale) && !Custom::is_array_null($request->struttura_portante_orizzontale_stato_conservazione)) {
            foreach (array_combine($request->struttura_portante_orizzontale_materiale, $request->struttura_portante_orizzontale_stato_conservazione) as $materiale => $stato) {
                $struttura_portante_orizzontale[] = [$materiale => $stato];
            }
        }

        $fabbricato['struttura'] = [
            'struttura_portante_verticale' => $struttura_portante_verticale,
            'copertura' => $copertura,
            'struttura_portante_orizzontale' => $struttura_portante_orizzontale,
            'facciate' => $facciate
        ];

        $immobile->sopralluogo->update(['fabbricato' => $fabbricato]);
        //event(new CompletamentoModulo('sopralluogo.fabbricato.struttura', $immobile));
        return redirect()->route('immobile.sopralluogo.unitaimmobiliare', compact('immobile'))->with('message', ['type' => 'success', 'text' => 'Fabbricato completato con successo']);
    }

    public function storeunitaimmobiliare(Request $request, Immobile $immobile)
    {
        $validation = $this->validate($request, [
            'locali.localiPrincipaliAccessoriDiretti.*' => 'required|nullable',
            'latoa.localiPrincipaliAccessoriDiretti.*' => 'required|nullable',
            'latob.localiPrincipaliAccessoriDiretti.*' => 'required|nullable',
            'latoab.localiPrincipaliAccessoriDiretti.*' => 'required|nullable',
            'altezza.localiPrincipaliAccessoriDiretti.*' => 'required|nullable',
            'piani.localiPrincipaliAccessoriDiretti.*' => 'required|nullable',
            'accessi' => 'required|nullable'
        ], [
            'locali.localiPrincipaliAccessoriDiretti.*' => 'I Locali principali e Accessori diretti sono obbligatori',
            'latoa.localiPrincipaliAccessoriDiretti.*' => 'Il lato A è obbligatorio',
            'latob.localiPrincipaliAccessoriDiretti.*' => 'Il lato B è obbligatorio',
            'latoab.localiPrincipaliAccessoriDiretti.*' => 'Il lato A * B è obbligatorio',
            'altezza.localiPrincipaliAccessoriDiretti.*' => 'Altezza è obbligatoria',
            'piani.localiPrincipaliAccessoriDiretti.*' => 'Il piano è obbligatorio',
            'accessi.*' => 'Gli accessi sono obbligatori'
        ]);

        $tipo_struttura = Archivio::wheretype('sopralluogo')->first()->dati['unitaImmobiliare'];
        $tipo_struttura['consistenza'][$immobile->destinazione->nome]['altreSuperfici'][] = '';
        foreach ($tipo_struttura['consistenza'][$immobile->destinazione->nome] as $key => $value) {
            $parametri_key[] = $key;
        }

        foreach ($parametri_key as $parametro_key) {
            foreach ($request->all() as $key => $value) {
                if ($key == "_token" || $key == 'accessi' || $key == 'confine') continue;
                $preliminar_array[$parametro_key][$key] = $this->request_iterator($request->$key[$parametro_key]);
            }
        }
        foreach ($preliminar_array as $preliminarkey => $preliminarvalue) {
            foreach ($preliminarvalue as $key => $value) {
                if ($value == null) continue;
                foreach ($value as $subkey => $subvalue)
                    $consistenza[$preliminarkey][$subkey][$key] = $subvalue;
            }
        }

        if ($immobile->sopralluogo->unitaimmobiliare) {
            $unitaimmobiliare = $immobile->sopralluogo->unitaimmobiliare;
        }

        $unitaimmobiliare['consistenza'] = $consistenza;
        $unitaimmobiliare['accessi'] = $request->accessi;
        $unitaimmobiliare['confini'] = $request->confine;

        $immobile->sopralluogo->update(['unitaimmobiliare' => $unitaimmobiliare]);

        //event(new CompletamentoModulo('sopralluogo.unitaimmobiliare', $immobile));
        return redirect()->route('immobile.sopralluogo.unitaimmobiliare.infissi', $immobile)->with('message', ['type' => 'success', 'text' => 'Continua inserendo gli infissi']);
    }

    //Funizione per scorrere i dati di un array(usato per la request) che elimina i null e i -- dallo stesso
    public function request_iterator($request)
    {
        $array = null;
        foreach ($request as $key => $value) {
            if ($value != null && $value != '--') {
                $array[] = $value;
            }
        }
        return $array;
    }

    public function storeinfissiunitaimmobiliare(Request $request, Immobile $immobile)
    {

        foreach ($request['larghezza'] as $key => $value) {
            $parametri_key[] = $key;
        }

        foreach ($parametri_key as $parametro_key) {
            foreach ($request->all() as $key => $value) {
                if ($key == "_token") continue;
                $preliminar_array[$parametro_key][$key] = $this->request_iterator($request->$key[$parametro_key]);
            }
        }

        $infissi = null;
        foreach ($preliminar_array as $preliminarkey => $preliminarvalue) {
            foreach ($preliminarvalue as $key => $value) {
                if ($value == null) continue;
                foreach ($value as $subkey => $subvalue)
                    $infissi[$preliminarkey][$subkey][$key] = $subvalue;
            }
        }

        if ($immobile->sopralluogo->unitaimmobiliare) {                       //unità immobiliare parziale salvata
            $unitaimmobiliare = $immobile->sopralluogo->unitaimmobiliare;
        }

        $unitaimmobiliare['infissi'] = $infissi;

        $immobile->sopralluogo->update(['unitaimmobiliare' => $unitaimmobiliare]);

        return redirect()->route('immobile.sopralluogo.unitaimmobiliare.impianti', $immobile)->with('message', ['type' => 'success', 'text' => 'Continua inserendo gli impianti']);
    }

    public function storeimpiantiunitaimmobiliare(Request $request, Immobile $immobile)
    {

        foreach ($request['fontirinnovabili'] as $key => $value) {
            $parametri_key[] = $key;
        }
        foreach ($parametri_key as $parametro_key) {
            foreach ($request->all() as $key => $value) {
                if ($key != "fontirinnovabili") continue;
                $preliminar_array[$parametro_key] = $this->request_iterator($request->$key[$parametro_key]);
            }
        }
        $fontirinnovabili = null;
        foreach ($preliminar_array as $preliminarkey => $preliminarvalue) {
            foreach ($preliminarvalue as $key => $value) {
                if ($value == null) continue;
                foreach ($value as $subkey => $subvalue)
                    $fontirinnovabili[$preliminarkey][$subkey][$key] = $subvalue;
            }
        }

        foreach ($request['finiture'] as $key => $value) {
            $finiture[$key] = $this->request_iterator($value);
        }
        foreach ($request['impiantoriscaldamento'] as $key => $value) {
            $impiantoriscaldamento[$key] = $this->request_iterator($value);
        }
        foreach ($request['impiantoidricoescarichi'] as $key => $value) {
            $impiantoidricoescarichi[$key] = $this->request_iterator($value);
        }
        foreach ($request['impiantielettricitecnologici'] as $key => $value) {
            $impiantielettricitecnologici[$key] = $this->request_iterator($value);
        }

        $unitaimmobiliare = $immobile->sopralluogo['unitaimmobiliare']; //unità immobiliare parziale salvata

        $unitaimmobiliare['finiture'] = $finiture;
        $unitaimmobiliare['impiantoriscaldamento'] = $impiantoriscaldamento;
        $unitaimmobiliare['impiantoidricoescarichi'] = $impiantoidricoescarichi;
        $unitaimmobiliare['impiantielettricitecnologici'] = $impiantielettricitecnologici;
        $unitaimmobiliare['fontirinnovabili'] = $fontirinnovabili;

        $immobile->sopralluogo->update(['unitaimmobiliare' => $unitaimmobiliare]);

        return redirect()->route('immobile.sopralluogo.particondominiali', $immobile)->with('message', ['type' => 'success', 'text' => 'Continua inserendo le parti condominiali']);
    }

    public function storeparticondominiali(Request $request, Immobile $immobile)
    {

        $particondominiali['tipologia'] = $request->tipologia;
        $particondominiali['ncondomini'] = $request->ncondomini;
        $particondominiali['ripartizionicosti'] = $request->ripartizionicosti;

        $immobile->sopralluogo->update(['particondominiali' => $particondominiali]);

        return redirect()->route('immobile.sopralluogo', $immobile)->with('message', ['type' => 'success', 'text' => 'Completa le parti condominiali']);
    }

    public function upload_fotovideo(Request $request, Immobile $immobile)
    {
        $immagine = $request->file('file');
        try {
            if ($request->hasFile('file')) {
                if ($immagine->isValid()) {
                    $data = getimagesize($immagine);
                    $width = $data[0];
                    $height = $data[1];
                    $filename = pathinfo($immagine->getClientOriginalName(), PATHINFO_FILENAME);
                    $filename = $filename . '-' . Str::random(6) . '.' . $immagine->getClientOriginalExtension();

                    $img = Immagine::create([
                        'width' => $width,
                        'height' => $height,
                        'filename' => $filename,
                        'size' => $immagine->getSize(),
                        'filemime' => $immagine->getmimeType()
                    ]);

                    if ($img) {
                        Storage::putFileAs('immobile/sopralluogo/fotovideo' . $immobile->id, $immagine, $filename);
                        $immobile->sopralluogo->immagini()->attach($img);
                    }

                } else {
                    return 'File non valido';
                }
            } else {
                return 'File non presente';
            }
        } catch (\Exception $e) {
            return $e;
        }
    }

    public function sopralluogo_particondominiali(Immobile $immobile)
    {
        $unita_immobiliare = Archivio::wheretype('sopralluogo')->first()->dati['unitaImmobiliare'];
        return view('app.immobili.immobile.sopralluogo.particondominiali', compact('immobile', 'unita_immobiliare'));
    }


    /*
    ########## STORE VERIFICA TECNICA
    ################################
    */

    public function storeverificatecnica(Request $request, Immobile $immobile)
    {
//         dd($request);

        Validator::extend('checktoggle', function ($attribute, $value, $parameters, $validator) {
            if ($value) {
                foreach ($value as $key => $val) {
                    foreach ($val as $subkey => $subvalue) {
                        if ($subkey == 0 && $subvalue != 'Necessario') return false;
                        if ($subkey == 1 && $subvalue != 'Disponibile') return false;
                    }
                }
            } else {
                return true;
            }
            return true;
        }, 'Selezione non corretta');
        $this->validate($request, [
            'verificatecnica' => 'checktoggle'
        ]);


        $non_necessari = [];
        $valori = json_decode($request->valori);

        foreach ($valori as $key => $value) {
            $non_necessari[$key] = 'Non necessario';
        }
        if ($request->verificatecnica) {
            foreach ($request->except('_token')['verificatecnica'] as $key => $value) {
                $non_necessari[$key] = last($value);
            }
        }


        $dichiarazioni = [];
        foreach ($non_necessari as $key => $value) {
            if ($value == 'Non necessario') {
                $val[0] = false;
                $val[1] = null;
                $val[2] = null;
            } else if ($value == 'Necessario') {
                $val[0] = true;
                $val[1] = null;
                $val[2] = null;

            } else if ($value == 'Disponibile') {
                $val[0] = true;
                $val[1] = true;
                $val[2] = null;

            } else if ($value == 'Non Disponibile') {
                $val[0] = true;
                $val[1] = false;
                $val[2] = null;

            } else if ($value == 'true') {
                $val[0] = true;
                $val[1] = true;
                $val[2] = true;

            } else if ($value == 'false') {
                $val[0] = true;
                $val[1] = true;
                $val[2] = false;
            }
            $dichiarazioni[$key] = [
                'Necessario' => $val[0],
                'Disponibile' => $val[1],
                'Conforme' => $val[2]
            ];
            $immobile->verifica_tecnica->$key = $dichiarazioni[$key];
        }

        $statistiche_immobile = $immobile->statistiche;
        $statistiche_generali = Archivio::wheretype('percentuali_immobili')->first()->dati['verificatecnica'];

        foreach ($non_necessari as $key => $value) {
            if ($value == "Non necessario") {
                $statistiche_immobile->verificatecnica->$key['perc'] = $statistiche_generali[$key]['perc'];
                dd("sono qui");
            }
        }
        dd($statistiche_immobile->verificatecnica);

//        return redirect()->route('immobile.verificatecnica', $immobile)->with('message', ['type' => 'success', 'text' => 'Verifica tecnica salvata']);
    }


    public function declinaResponsabileTecnico(Request $request, $token, $type)
    {
        //per evitare di rifare la query per trovare l'immobile glielo passo dal middleware
        $immobile = $request->immobile;
        return view('app.immobili.immobile.action.declinaSceltaTecnico', compact('immobile', 'token', 'type'));
    }

    public function accettAssegnazione(Request $request, $token, $type)
    {
        //per evitare di rifare la query per trovare l'immobile glielo passo dal middleware
        $immobile = $request->immobile;
        return view('app.immobili.immobile.action.accettAssegnazione', compact('immobile', 'token', 'type'));
    }

    public function storeDeclinaResponsabileTecnico(Request $request, Immobile $immobile)
    {
        $this->validate($request, [
            'token' => 'required|exists:user_token,token'
        ], [
            'token.required' => 'Token di sicurezza mancante',
            'token.exists' => 'Token non trovato'
        ]);

        $immobile->update(['tecnico_id' => null]);

        UserToken::where('token', $request->token)->where('encoding_type', $request->type)->first()->delete();

        return redirect()->route('dashboard')->with('message', ['type' => 'success', 'text' => 'Sei stato rimosso come responsabile tecnico!']);
    }

    public function confermAssegnazioneAgenteImmobiliare(Request $request, Immobile $immobile)
    {
        $this->validate($request, [
            'token' => 'required|exists:user_token,token',
            'email' => 'required'
        ], [
            'token.required' => 'Token di sicurezza mancante',
            'email.required' => 'Email richiesta',
            'token.exists' => 'Token non trovato'
        ]);

        $immobile->update(['agente_id' => Auth::id()]);

        $ut = UserToken::where('token', $request->token)->where('encoding_type', $request->type)->first();

        return redirect()->route('dashboard')->with('message', ['type' => 'success', 'text' => 'Assegnazione confermata']);

    }

    public function immobilecondiviso($token, $id)
    {
        $ic = ImmobileCondiviso::where('token', $token)->first();
        if (!$ic) {
            abort(404);
            //return redirect()->route()->with('message', ['type' => 'error', 'text' => 'Indirizzo non valido']);
        }

        $immobile = $ic->immobile;
        $today = Carbon::now();

        if ($immobile->id != $id || $today > $ic->scadenza || !$immobile) {
            abort(404);
        }

        return view('app.immobili.immobile.viewer.index', compact('ic', 'immobile'));
    }


    public function condividiImmobile(Request $request, Immobile $immobile)
    {
        try {
            $emails = explode(';', $request->email);
            //il controllo che ci sia gia una email lo faccio tramite javascript
            $immobile->update([
                'share_code' => Str::random(6)
            ]);
            $condivisione = ImmobileCondiviso::create([
                'options' => [
                    'dettagli' => json_encode($request->dettagli),
                    'moduli' => json_encode($request->moduli),
                ],
                'immobile_id' => $immobile->id,
                'scadenza' => $request->scadenza,
                'token' => Str::random(64),
                'email' => json_encode($emails)
            ]);

            // *** ATTENZIONE RIMUOVERE LA LOGICA CHE SALVAVA I MODULI DENTRO LA TABELLA IMMOBILE_CONDIVISO_MODULI

            event(new Condivisione(explode(';', $request->email), $condivisione->token, $immobile->id));
            event(new InvioMailPerImmobileCondiviso());
            return redirect()->back()->with('modal', ['visual' => 'visual-onboarding-scelta-password.svg', 'text' => 'Immobile condiviso con successo']);
        } catch (\Exception $e) {
            dd($e);
            return redirect()->back()->with('message', ['type' => 'error', 'text' => 'Errore condivisione immobile']);
        }
    }

    public function candidatureResponsabileTecnico(Immobile $immobile)
    {
        $candidature = $immobile->annuncioRespTecnico->candidatureWGS->groupBy('user.nome_cognome');
        return view('app.immobili.immobile.responsabiletecnico.candidature', compact('immobile', 'candidature'));
    }

    public function caricaAllegati(Request $request, Immobile $immobile)
    {
        if ($request->hasFile('file')) {
            if ($request->file->isValid()) {
                $fileDaCaricare = $request->file;
                try {
                    $storagePath = Storage::disk('s3')->put($immobile->id, $fileDaCaricare);
                    $tipo = array_filter(config('constants.categorie-allegati'), function ($value) use ($fileDaCaricare) {
                        return Str::contains($value, $fileDaCaricare->getClientOriginalExtension());
                    }, ARRAY_FILTER_USE_KEY);

                    $file = File::create([
                        'filename' => basename($storagePath),
                        'size' => $fileDaCaricare->getSize(),
                        'filemime' => $fileDaCaricare->getmimeType()
                    ]);

                    $allegato = Allegato::create([
                        'file_id' => $file->id,
                        'modelable_id' => $immobile->id,
                        'modelable_type' => get_class($immobile),
                        'type' => head($tipo),
                        'dettagli' => serialize([
                            'titolo' => $fileDaCaricare->getClientOriginalName()
                        ])
                    ]);

                    if ($allegato) {
                        Storage::putFileAs('allegati/' . $request->user()->id . '/' . $immobile->codice, $fileDaCaricare, basename($storagePath));
                    }

                    return 'file_caricato_con_successo';
                } catch (\Exception $e) {
                    return $e;
                }


            } else {
                return 'File non valido';
            }
        } else {
            return 'file_non_presenti';
        };
    }

    /* ARCHIVIA IMMOBILE */

    public function archivia(Request $request, Immobile $immobile)
    {


        $this->validate($request, [
            'action' => 'required'
        ], [
            'action.required' => 'Per favore digita "ARCHIVIA" nel campo',
        ]);

        if ($request->action != 'ARCHIVIA') {
            return redirect()->back()->with('message', ['type' => 'error', 'text' => 'Digita "ARCHIVIA nel campo"']);
        }

        $immobile->update([
            'archiviato' => 1
        ]);

        return redirect()->route('immobili')->with('message', ['type' => 'success', 'text' => 'Immobile archiviato con successo']);

    }


    /* CANCELLAZIONE IMMOBILE */

    public function destroy(Request $request, Immobile $immobile)
    {

        $this->validate($request, [
            'action' => 'required'
        ], [
            'action.required' => 'Per favore digita "CANCELLA" nel campo',
        ]);

        if ($request->action != 'CANCELLA') {
            return redirect()->back()->with('message', ['type' => 'error', 'text' => 'Digita "CANCELLA nel campo"']);
        }

        $immobile->delete();


        return redirect()->route('immobili')->with('message', ['type' => 'success', 'text' => 'Immobile cancellato']);

    }


    private function createCodiceImmobile($lunghezza)
    {

        $codice = Str::lower(Str::random($lunghezza));
        $exist = Immobile::where('codice', $codice)->first();

        if ($exist) {
            self::createCodiceImmobile($lunghezza);
        } else {
            return $codice;
        }

    }


    private function createCodiceAnnuncio($lunghezza)
    {

        $codice = Str::lower(Str::random($lunghezza));
        $exist = Annuncio::where('codice', $codice)->first();

        if ($exist) {
            self::createCodiceAnnuncio($lunghezza);
        } else {
            return $codice;
        }

    }


    public function inutile()
    {
//        $immobile = Immobile::findOrFail('28');
//        event(new CompletamentoVerificaTecnica('catasto', $immobile));
//        dd(Archivio::wheretype('percentuali_immobili')->first()->dati);

//        $json = [
//            'sopralluogo' => [
//                'contesto' => [
//                    '3',
//                    'Contesto',
//                    '#ffffff'
//                ],
//                'fabbricato' => [
//                    '3',
//                    'Fabbricato',
//                    '#ffffff'
//                ],
//                'unitaimmobiliare' => [
//                    '3',
//                    'Unità Immobiliare',
//                    '#ffffff'
//                ],
//                'particondominiali' => [
//                    '3',
//                    'Parti Condominiali',
//                    '#ffffff'
//                ],
//                'media' => [
//                    '3',
//                    'Media',
//                    '#ffffff'
//                ],
//                '15',
//                'Sopralluogo',
//                '#ffffff'
//            ],
//            'verificatecnica' => [
//                'datiurbanistica' => [
//                    '30',
//                    'Dati urbanistici',
//                    '#ffffff'
//                ],
//                'catasto' => [
//                    '10',
//                    'Catasto',
//                    '#ffffff'
//                ],
//                'provenienza' => [
//                    '10',
//                    'Provenienza',
//                    '#ffffff'
//                ],
//                'agibilita' => [
//                    '10',
//                    'Agiblita',
//                    '#ffffff'
//                ],
//                'impianti' => [
//                    '5',
//                    'Impianti',
//                    '#ffffff'
//                ],
//                'caldaia' => [
//                    '5',
//                    'Caldaia',
//                    '#ffffff'
//                ],
//                'ape' => [
//                    '5',
//                    'APE',
//                    '#ffffff'
//                ],
//                'cdu' => [
//                    '5',
//                    'CDU',
//                    '#ffffff'
//                ],
//                'postuma' => [
//                    '5',
//                    'Postuma',
//                    '#ffffff'
//                ],
//                '100',
//                'Verifica Tecnica',
//                '#ffffff'
//            ],
//        ];

//        return json_encode($json);
    }

}
