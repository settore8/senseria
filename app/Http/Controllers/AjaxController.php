<?php

namespace App\Http\Controllers;

use App\Events\NotificaModuloAggiuntoAlResponsabileTecnico;
use App\Events\NotificaModuloRimossoAlResponsabileTecnico;
use App\Immagine;
use App\Modulo;
use App\Partecipazione;
use Auth;
use App\File;
use App\User;
use App\Notifica;
use Carbon\Carbon;
use App\Messaggio;
use App\Immobile;
use App\Categoria;
use App\MetaAgente;
use App\Candidatura;
use App\Destinazione;
use App\Suggerimento;
use App\TipoNotifica;
use App\Conversazione;
use App\Raccolta;
use App\Like;
use App\Custom\Custom;
use App\MetaProfessionista;
use App\Events\LikeProfilo;
use App\Events\InserisciMessaggio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Stripe\Stripe;

class AjaxController extends Controller
{
    public function removePartecipazione(Request $request)
    {
        Partecipazione::whereToken($request->token)->first()->delete();
    }

    public function addcard(Request $request)
    {

        try {
            $this->stripe = new \Stripe\StripeClient(
                config('cashier.secret')
            );

            $customer = $this->stripe->customers->retrieve(
                \Illuminate\Support\Facades\Auth::user()->stripe_id,
                []
            );

            $fingerprint = $this->stripe->tokens->retrieve(
                $request->stripeToken,
                []
            );

            if ($request->user()->carte()->where('fingerprint', '=', $fingerprint->card->fingerprint)->count() == 0) {

                $card = $this->stripe->customers->createSource(
                    $customer->id,
                    ['source' => $request->stripeToken]
                );

                if ($request->isdefault) {
                    $request->user()->update([
                        'stripe_id' => $customer->id,
                        'card_brand' => $card->brand,
                        'last_four' => $card->last4,
                    ]);
                    $request->user()->carte()->update([
                        "default" => 0
                    ]);
                }

                $request->user()->carte()->create([
                    'carta_id' => $card->id,
                    'fingerprint' => $fingerprint->card->fingerprint,
                    'card_holder' => $request->card_holder,
                    'card_brand' => $card->brand,
                    'last_four' => $card->last4,
                    'exp_month' => $card->exp_month,
                    'exp_year' => $card->exp_year,
                    'default' => $request->isdefault ? true : false,
                ]);

                $esito = 'true';

            } else {

                $esito = 'false';

            }

            return $esito;

        } catch (Exception $e) {

            return $e;
        }
    }

    public function getSpecifiche(Request $r)
    {
        $destinazione = Destinazione::findOrfail($r->destinazione);
        $specifiche = $destinazione->specifiche->toJson();
        return $specifiche;
    }

    public function getAgenti(Request $r)
    {
        $agenti = User::where('id', '!=', Auth::user()->id)->isAgente()->IsAttivo()->with('meta')->get();

        dd($agenti);
        $agenti->toJson();
        return $agenti;
    }
    /*
    public function updateVisibilitaSuggerimento(Request $request)
    {
        $suggerimento = Suggerimento::where('nome', $request->suggerimento)->first();
        if ($suggerimento) {
            $filterSuggerimento = Auth::user()->suggerimenti->where('id', $suggerimento->id)->first();

            if (!$filterSuggerimento) {
                return 'suggerimento_non_presente';
            }

            $visibilita = $filterSuggerimento->pivot->visibile;
            // return !$visibilita;
            Auth::user()->suggerimenti()->detach([$suggerimento->id]);
            Auth::user()->suggerimenti()->attach([$suggerimento->id => ['visibile' => !$visibilita]]);

        }
        return 'suggerimento_aggiornato';
    }
    */

    public function getProfessionistiImprese(Request $request)
    {
        $results = [];
        if ($request->search) {

            $professionisti = User::whereTipo('professionista')->whereEmail($request->search)->orWhereHas('meta', function ($query) use ($request) {
                $query->where('nome', 'LIKE', "%$request->search%")->orWhere('cognome', 'LIKE', "%$request->search")->orWhereRaw("CONCAT(nome, ' ', cognome) = '$request->search'");
            })->IsAttivo()->IsCompletato()->NotAdmin()->get()->keyBy('id')->pluck('nome_cognome', 'id');

           $imprese = User::whereTipo('impresa')->whereEmail($request->search)->orWhereHas('tipo', function ($query) use ($request) {
                $query->where('ragione_sociale', 'LIKE', "%$request->search%");
            })->IsAttivo()->IsCompletato()->NotAdmin()->get()->keyBy('id')->pluck('meta.ragione_sociale', 'id');
            $professionisti_imprese = $professionisti->union($imprese);

            foreach ($professionisti_imprese->all() as $id => $nc) {
                array_push($results, [
                    'id' => $id,
                    'text' => $nc
                ]);
            }

            return json_encode([
                'total_count' => count($results),
                "incomplete_results" => false,
                'results' => $results
            ]);

        } else {
            return json_encode([
                'results' => []
            ]);
        }

    }

    public function getProfessionisti(Request $request)
    {
        $results = [];
        if ($request->search) {
            $users = User::whereTipo('professionista')->whereEmail($request->search)->orWhereHas('meta',  function ($query) use ($request) {
                $query->where('nome', 'LIKE', "%$request->search%")->orWhere('cognome', 'LIKE', "%$request->search")->orWhereRaw("CONCAT(nome, ' ', cognome) = '$request->search'");
            })->IsAttivo()->IsCompletato()->NotAdmin()->get()->pluck('nome_cognome', 'id');

            foreach ($users as $id => $email) {
                array_push($results, [
                    'id' => $id,
                    'text' => $email
                ]);
            }

            return json_encode([
                'total_count' => count($results),
                "incomplete_results" => false,
                'results' => $results
            ]);
        } else {
            return json_encode([
                'results' => []
            ]);
        }
    }

    public function selectResponsabileTecnico(Request $request)
    {
        $results = [];
        if ($request->search) {
            $users = User::whereTipo('professionista')->whereEmail($request->search)->orWhereHas('meta', function ($query) use ($request) {
                $query->where('nome', 'LIKE', "%$request->search%")->orWhere('cognome', 'LIKE', "%$request->search%")->orWhereRaw("CONCAT(nome, ' ', cognome) = '$request->search'");
            })->IsAttivo()->IsCompletato()->NotAdmin()->get()->pluck('nome_cognome', 'id');

            foreach ($users as $id => $email) {
                array_push($results, [
                    'id' => $id,
                    'text' => $email
                ]);
            }

            return json_encode([
                'total_count' => count($results),
                "incomplete_results" => false,
                'results' => $results
            ]);
        } else {
            return json_encode([
                'results' => []
            ]);
        }
    }

    public function getKeyWords(Request $request)
    {
        $parole = Categoria::find($request->categoria)->parolechiavi;
        return $parole->toJson();
    }

    public function removeAllegato(Request $request)
    {
        try {
            File::find($request->file_id)->delete();
        } catch (\Exception $e) {
            return $e;
        }
    }

    public function createConversazione(Request $request)
    {
        try {
            $conversazione = Auth::user()->conversazioni()->create(['coverable_id' => $request->object_id, 'coverable_type' => $request->object_class]);
            $conversazione->users()->attach(Auth::id());
            $conversazione->users()->attach($request->recipient_id);
        } catch (\Exception $e) {
            return $e;
        }
    }

    public function sendMessage(Request $request)
    {
        try {
            Messaggio::create([
                'conversazione_id' => $request->conversazione,
                'from' => Auth::id(),
                'messaggio' => $request->messaggio
            ]);
            event(new InserisciMessaggio($request->conversazione, $request->messaggio, Auth::id(), Auth::user()->display_name, Carbon::now()->format('H:i')));
        } catch (\Exception $e) {
            return $e;
        }
    }

    // TO_REVIEW forse non serve più?
    public function likeProfilo(Request $request)
    {
        try {
            $notifica = Custom::decrypt($request->notifica);
            $tn = TipoNotifica::find($notifica);
            $messaggioNotifica = sprintf($tn->messaggio, Auth::user()->nome_cognome);
            Notifica::create([
                'mittente_id' => Auth::id(),
                'destinatario_id' => Custom::decrypt($request->destinatario),
                'messaggio' => $messaggioNotifica,
            ]);
            event(new LikeProfilo(sha1(Custom::decrypt($request->destinatario)), $messaggioNotifica));
        } catch (\Exception $e) {
            return $e;
        }
    }

    public function postRaccolta(Request $request)
    {

        try {
            $user_id = Auth::id();
            $id = Custom::decrypt($request->id);
            $type = Custom::decrypt($request->type);
            $destinatario = Custom::decrypt($request->destinatario_id);
            
            // verifico se esiste già la raccolta cercando tra tutte le righe
            $raccolta = Raccolta::where('user_id', $user_id)->where('coverable_id', $id)->where('coverable_type', $type)->withTrashed()->first();
            
            // se esiste
            if($raccolta) {
                // se è già stato cancellato ripristino
                if($raccolta->trashed()) {
                    
                    $raccolta->restore();
                // altrimenti imposto il soft delete
                } else {
                    $raccolta->delete();
                } 
            } else {
                Raccolta::create([
                    'user_id' => $user_id,
                    'coverable_id' => $id,
                    'coverable_type' => $type,
                ]);
                // qui facciamo if ed a seconda della coverable_type inviamo la notifica
            }

        } catch (\Exception $e) {
            dd($e);
            return $e;
        }
    }


    public function postLike(Request $request)
    {

        try {
            $user_id = Auth::id();
            $id = Custom::decrypt($request->id);
            $type = Custom::decrypt($request->type);
            $destinatario = Custom::decrypt($request->destinatario_id);
            
            // verifico se esiste già il like cercando tra tutte le righe
            $raccolta = Like::where('user_id', $user_id)->where('coverable_id', $id)->where('coverable_type', $type)->withTrashed()->first();
            
            // se esiste
            if($raccolta) {
                // se è già stato cancellato ripristino
                if($raccolta->trashed()) {
                    
                    $raccolta->restore();
                // altrimenti imposto il soft delete
                } else {
                    $raccolta->delete();
                } 
            } else {
                Like::create([
                    'user_id' => $user_id,
                    'coverable_id' => $id,
                    'coverable_type' => $type,
                ]);
                // qui facciamo if ed a seconda della coverable_type inviamo la notifica
            }

        } catch (\Exception $e) {
            return $e;
        }
    }


    public function scegliCandidato(Request $request)
    {
        try {
            $immobile = Custom::decrypt($request->immobile);
            $candidato = Custom::decrypt($request->candidato);
            if (!$immobile) {
                return 'immobile_non_codificabile';
            }
            if (!$candidato) {
                return 'candidato_non_codificabile';
            }

            $immobile = Immobile::findOrFail($immobile);
            Candidatura::where('user_id', '!=', $candidato)->whereIn('dettaglio_id', $immobile->annuncioRespTecnico->dettagli->pluck('id')->toArray())->update(['attivo' => '0']);

            $immobile->update(['tecnico_id' => $candidato]);

        } catch (\Exception $e) {
            return $e;
        }
    }

    public function searchAllUser(Request $request)
    {
        $results = [];
        if ($request->search) {
            $professionisti = User::whereTipo('professionista')->whereEmail($request->search)->orWhereHas('meta', function ($query) use ($request) {
                $query->where('nome', 'LIKE', "%$request->search%")->orWhere('cognome', 'LIKE', "%$request->search%")->orWhereRaw("CONCAT(nome, ' ', cognome) = '$request->search'");
            })->IsAttivo()->IsCompletato()->NotAdmin()->get()->keyBy('id')->pluck('nome_cognome', 'id');

            $imprese = User::whereTipo('impresa')->whereEmail($request->search)->orWhereHas('meta', function ($query) use ($request) {
                $query->where('ragione_sociale', 'LIKE', "%$request->search%");
            })->IsAttivo()->IsCompletato()->NotAdmin()->get()->keyBy('id')->pluck('meta.ragione_sociale', 'id');

            $users = $professionisti->union($imprese);
            foreach ($users as $id => $email) {
                array_push($results, [
                    'id' => $id,
                    'text' => $email
                ]);
            }

            return json_encode([
                'total_count' => count($results),
                "incomplete_results" => false,
                'results' => $results
            ]);
        } else {
            return json_encode([
                'results' => []
            ]);
        }
    }

    public function removeImmagineByGalleria(Request $request){
        try{
            $immagine= Immagine::find($request->immagine);

            if($immagine){
                $galleria = $immagine->galleria->first();
                Storage::delete('gallerie/'.$galleria->user_id.'/'.$galleria->id.'/'.$immagine->filename);
                $immagine->delete();
            }else{
                return 'Immagine non trovata';
            }
        }catch(\Exception $e){
            return $e;
        }
    }

    public function attivaDisattivaModulo(Request $request){
        $immobile = Immobile::find(decrypt($request->immobile));
        $moduli = $immobile->moduli;

        if(in_array($request->modulo, $moduli)){
            $tn = TipoNotifica::whereNome('modulo-rimosso')->first();
            $messaggioNotifica = sprintf($tn->messaggio, $immobile->user->nome_cognome, Modulo::whereNome($request->modulo)->first()->nome_pubblico, $immobile->specifica->nome_pubblico, $immobile->informazione->comune);
            Notifica::create([
                'mittente_id' => $immobile->user_id,
                'destinatario_id' => Custom::decrypt($request->destinatario),
                'route' => '/immobile/'.$immobile->codice,
                'messaggio' => $messaggioNotifica
            ]);

            event(new NotificaModuloRimossoAlResponsabileTecnico(sha1(Custom::decrypt($request->destinatario)), $messaggioNotifica));

            unset($moduli[array_search($request->modulo, $moduli)]);
            $immobile->update(['moduli' => $moduli]);
            return 'Disabilitato';
        }else{
            $tn = TipoNotifica::whereNome('modulo-aggiunto')->first();
            $messaggioNotifica = sprintf($tn->messaggio, $immobile->user->nome_cognome, Modulo::whereNome($request->modulo)->first()->nome_pubblico, $immobile->specifica->nome_pubblico, $immobile->informazione->comune);

            Notifica::create([
                'mittente_id' => $immobile->user_id,
                'destinatario_id' => Custom::decrypt($request->destinatario),
                'route' => '/immobile/'.$immobile->codice.'/'.$request->modulo,
                'messaggio' => $messaggioNotifica
            ]);

            event(new NotificaModuloAggiuntoAlResponsabileTecnico(sha1(Custom::decrypt($request->destinatario)), $messaggioNotifica));
            array_push($moduli, $request->modulo);
            $immobile->update(['moduli' => $moduli]);
            return 'Abilitato';
        }

    }

    public function setPreferenzeUser(Request $request){
        $user = Auth::user();
        // recupero le preferenze attuali
        $userpref = $user->preferences;

        $req = $request->all();
        $preferences = [];
        // faccio un ciclo dei vari dati passati e creo un array
        foreach($req as $key => $val ) {
            $preferences[$key] = $val;
        }

        //dd($preferences, $userpref);

        // aggiungo se non esiste il valore, aggiornare quelli da aggiornare
        $array = array_merge($userpref, array_diff($preferences, $userpref));
        //$array = array_merge($userpref, $preferences);

        $user->update([
            'preferences' => $array,
        ]);
       
    }


    public function signNotificationAsReaded(Request $request) {
        $user = Auth::user();
        $notifica = Notifica::findOrFail(Custom::decrypt($request->id));

        // per sicurezza controllo che il destinatario id sia loggato
        if($user->id == $notifica->destinatario_id) {
            $notifica->update([
                'readed' => 1,
            ]);
        }

    }

    public function deleteNotification(Request $request) {
        $user = Auth::user();
        $notifica = Notifica::findOrFail(Custom::decrypt($request->id));

        // per sicurezza controllo che il destinatario id sia loggato
        if($user->id == $notifica->destinatario_id) {
            $notifica->delete();
        }

    }

    
}
