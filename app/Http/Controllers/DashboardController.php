<?php

namespace App\Http\Controllers;

use App\Annuncio;
use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Immobile;
use App\Articolo;
use App\Galleria;

class DashboardController extends Controller
{
    public function index() {
        $user = Auth::user();

        $immobili = Immobile::gestibili()->orderBy('updated_at')->limit(5)->get();
        $professionisti = User::isCompletato()->impresa()->get();
        $imprese = User::isCompletato()->professionista()->get();

        $articoliRecenti = Articolo::pubblicati()->with('categoria', 'user')->orderByDesc('created_at')->get();
        $articoliPopolari = Articolo::pubblicati()->with('categoria', 'user')->orderByViews()->get();

        $gallerieRecenti = Galleria::pubblicate()->with('immagini','user')->orderByDesc('created_at')->get();
        $galleriePopolari = Galleria::pubblicate()->with('immagini','user')->orderByViews()->get();

        return view('app.dashboard', compact('user', 'immobili', 'professionisti', 'imprese', 'articoliRecenti', 'articoliPopolari','gallerieRecenti', 'galleriePopolari'));
    }
}
