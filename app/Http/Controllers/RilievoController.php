<?php

namespace App\Http\Controllers;

use App\Immobile;
use Illuminate\Http\Request;
use App\Rilievo;
use Auth;

class RilievoController extends Controller
{

    public function store(Request $request, Immobile $immobile) {

        $rilievo = new Rilievo;
        $immobile->rilievo()->firstOrcreate([
            'customer_id' => null,
            'immobile_id' => $immobile->id,
            'titolo' => 'Rilievo immobile '.$immobile->codice,
            'stato' => 'Bozza'
        ]);

        return redirect()->route('immobile.rilievo.edit', $immobile);
    }


    public function edit(Request $request, Immobile $immobile) {
        $rilievo = $immobile->rilievo;
        return view('app.immobili.immobile.rilievo3d.edit', compact('immobile'));
    }


    public function update(Request $request, Immobile $immobile) {

        dd($request);
        
        $this->validate($request, [
            'titolo' => 'required',
            'mq' => 'required_if:action,send|numeric|min:1|max:400',
            'piani' => 'required_if:action,send|numeric|min:1',
            'vani' => 'required_if:action,send|numeric|min:1',
            'indirizzo' => 'required_if:action,send'
        ], [
            'titolo.required' => 'Il titolo è obligatorio',
            'mq.max' => 'Mq massimi consentiti 400',
            'mq.required_if' => 'Metri quadri obbligatori',
            'piani.min' => 'Piani minimi consentiti 1',
            'piani.required_if' => 'Numero Piani obbligatorio',
            'vani.min' => 'Vani minimi consentiti 1',
            'vani.required_if' => 'Numero Vani obbligatorio',
            'indirizzo' => 'L\'indirizzo è obbligatorio',
            'distanza.max' => 'Destinazione superiore a 400km',
        ]);

        $rilievo = $immobile->rilievo;

        $rilievodata = [
            'titolo' => $request->titolo,
            'standard' => [
                'mq' => $request->mq,
                'piani' => $request->piani,
                'vani' => $request->vani,
                'address' => $request->address, // è l'indirizzo in forma stringa
                'indirizzo' => json_decode($request->indirizzo), // è l'array di gmaps
                'prestazioni' => $request->prestazioni,
                'preventivo' => [
                    'distanza' => base64_decode($request->distanza),
                    'durata' => base64_decode($request->durata)
                ]
            ],

            'personalizza' => [
                'restituzione' => $request->restituzione,
                'fotopiani' => $request->fotopiani,
            ],

            'note' => $request->note,
        ];

        switch ($request->input('action')) {
            case 'save':
                $rilievodata['stato'] = 'Bozza';
                $messaggio = 'Rilievo salvato correttamente';
                $route = 'gaiagroup.rilievo.edit';
                break;
            case 'send':
                $rilievodata['stato'] = 'Richiesta inviata';
                $messaggio = 'Richiesta inviata correttamente';
                $route = 'gaiagroup.rilievo.show';
                break;
        }

        $rilievo->update($rilievodata);

        return redirect( route($route, $immobile->rilievo))->with('message', $messaggio);

    }

    public function richiesta($immobile) {


        $rilievo = $immobile->rilievo;
  
        return view('immobile.rilievo', compact('rilievo'));
    }


    public function preventivo( Immobile $immobile) {
        $rilievo = $immobile->rilievo;
        
        if($rilievo->stato == 'Bozza') {
            return redirect( route('immobile.rilievo.edit', $immobile));
        }
        return view('immobile.rilievo.preventivo', compact('rilievo'));
    }

    public function messaggi( Immobile $immobile) {
        $rilievo = $immobile->rilievo;

        if($rilievo->stato == 'Bozza') {
            return redirect( route('immobile.rilievo.edit', $rilievo->id));
        }
        return view('immobile.rilievo.messaggi', compact('rilievo'));
    }


    public function cloud( Immobile $immobile) {
        $rilievo = $immobile->rilievo;

        if($rilievo->stato == 'Bozza') {
            return redirect( route('immobile.rilievo.edit', $rilievo->id));
        }

        return view('immobile.rilievo.cloud', compact('rilievo'));
    }


    public function destroy( Immobile $immobile)
    {
        $rilievo = $immobile->rilievo;
        $titolo = $rilievo->titolo;
        $rilievo->delete();
        return redirect()->route('mmobile.rilievo.destroy')->with('message', 'Rilievo <strong>'. $titolo .'</strong> eliminato');

    }

}
