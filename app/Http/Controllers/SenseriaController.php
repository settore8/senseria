<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Immobile;

class SenseriaController extends Controller
{
    

    /*
    ############ ANNUNCI SENSERIA VETRINA PUBBLICA
    #############################
    */

    public function annunciIndex() {
        $immobili = Immobile::inPubblicita()->simplePaginate(24);
        return view('site.immobili.index', compact('immobili'));
    }

    public function annunciSearch(Request $request) {
        $immobili = Immobile::inPubblicita()->simplePaginate(24);
        return view('site.immobili.index', compact('immobili', 'request'));
    }

    public function annunciShow($immobile) {
        return view('site.immobili.index', compact('immobile'));
    }

    ######### FINE


}
