<?php

namespace App\Http\Controllers;

use App\Events\InviaEmailPerTagSuGalleria;
use App\Immobile;
use App\Mail\NotificaTagArticolo;
use App\Partecipazione;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Excel;
use App\Immagine;
use App\Galleria;
use App\Imports\ImporUser;
use Illuminate\Http\Request;
use Image;

class GalleriaController extends Controller
{
    // GALLERIE RECENTI
    public function index()
    {
        $gallerie = Galleria::pubblicate()->orderBy('created_at', 'ASC')->with(['user.meta', 'user.categoria', 'user.qualifica', 'like', 'immagini'])->simplePaginate(24);
        return view('gallerie.index', compact('gallerie'));
    }

    // GALLERIE POPOLARI. Vanno ordinate per like + visite ricevute
    public function popolari()
    {
        $gallerie = Galleria::pubblicate()->simplePaginate(24);
        return view('gallerie.index', compact('gallerie'));
    }

    /*
    GALLERIE POPOLARI. Vanno ordinate per like + visite ricevute
    public function mie(){
        $gallerie = auth::user()->gallerie()->simplePaginate(24);
        return view('gallerie.recenti', compact('gallerie'));
    }
    */

    /*
    GESTIONE GALLERIE
    */

    public function gestione()
    {
        $gallerie = Auth::user()->gallerie()->simplePaginate(24);
        return view('app.gallerie.gestione', compact('gallerie'));
    }

    public function gestionetag()
    {
        $gallerie = Auth::user()->gallerie()->pubblicate()->simplePaginate(24);
        return view('app.gallerie.gestione', compact('gallerie'));
    }


    public function store(Request $request)
    {
        $slug = Str::slug($request->titolo);
        $galleria = $request->user()->gallerie()->create([
            'titolo' => $request->titolo,
            'slug' => $slug
        ]);
        $code = Str::random(6);
        $newSlug = $slug . '-' . $code;
        $galleria->update([
            'slug' => $newSlug,
            'codice' => $code
        ]);

        return redirect()->route('galleria.edit', $newSlug)->with('message', ['type' => 'success', 'text' => 'Completa la tua galleria']);
    }

    public function edit(Request $request, Galleria $galleria)
    {
        $immobili = $request->user()->immobiliforGallerie;
        if (!$galleria) {
            abort(404);
        }

        return view('app.gallerie.galleria.edit', compact('galleria', 'immobili'));
    }

    public function update(Request $request, Galleria $galleria)
    {
        if ($request->pubblicabile) {

            if (!$galleria->immagine_evidenza) {
                return redirect()->back()->with('message', ['type' => 'error', 'text' => 'Immagini obbligatorie per la pubblicazione'])->withInput($request->input());
            }

            $this->validate($request, [
                'immobile_id' => 'required',
            ], [
                'immobile_id.required' => 'Campo obbligatorio',
            ]);

        }

        $galleria->update([
            'immobile_id' => $request->immobile_id,
            'crediti' => $request->crediti,
        ]);

        if ($request->collaboratori) {

            foreach ($request->collaboratori as $chiave => $collaboratore) {
                $collaborazione = $request->descrizione_collaboratore[$chiave];

                $esito = $galleria->partecipazioni()->updateOrCreate([
                    'user_id' => $collaboratore
                ], [
                    'user_id' => $collaboratore,
                    'contributo' => $collaborazione,
                    'token' => $token = Str::random(64),
                ]);

                if ($request->pubblicabile) {
                    if ($galleria->pubblicata == 0) {
                        event(new InviaEmailPerTagSuGalleria('web@settore8.it', $galleria, $token, auth::user()->nomecognome, $collaborazione));
                    } else {
                        if ($esito->wasRecentlyCreated) {
                            event(new InviaEmailPerTagSuGalleria('web@settore8.it', $galleria, $token, auth::user()->nomecognome, $collaborazione));
                        }
                    }
                }
            }
        }

        $galleria->update([
            'pubblicata' => $request->pubblicabile
        ]);

        return redirect()->route('gallerie')->with('message', ['type' => 'success', 'text' => 'Galleria caricata correttamente']);
    }

    public function upload(Request $request, Galleria $galleria)
    {
        $immagine = $request->file('file');
        try {
            if ($request->hasFile('file')) {
                if ($immagine->isValid()) {
                    $data = getimagesize($immagine);
                    $width = $data[0];
                    $height = $data[1];
                    $filename = pathinfo($immagine->getClientOriginalName(), PATHINFO_FILENAME);
                    $filename = $filename . '-' . Str::random(6) . '.' . $immagine->getClientOriginalExtension();

                    $img = Immagine::create([
                        'width' => $width,
                        'height' => $height,
                        'filename' => $filename,
                        'size' => $immagine->getSize(),
                        'filemime' => $immagine->getmimeType()
                    ]);

                    if ($img) {
                        Storage::putFileAs('gallerie/' . $request->user()->id . '/' . $galleria->codice, $immagine, $filename);
                        $galleria->immagini()->attach($img);
                        //caricamento immagine tagliata
                        $img_crop = Image::make($immagine->getPathname());
                        $img_crop->orientate();
                        $img_crop->fit(720, 440)->stream();
                        Storage::put('gallerie/' . $request->user()->id . '/' . $galleria->codice . '/crop/' . $filename, $img_crop->__toString());
                    }

                } else {
                    return 'File non valido';
                }
            } else {
                return 'File non presente';
            }
        } catch (\Exception $e) {
            return $e;
        }
    }

    public function import(Request $request)
    {
        Storage::disk('local')->put('user.csv', file_get_contents($request->file('import')));
        Excel::import(new ImporUser, 'user.csv', 'local');
    }

    public function show(Galleria $galleria)
    {

        views($galleria)->cooldown((int)config('app.resetafter'))->record();

        return view('gallerie.show', compact('galleria'));
    }

    private function generateRiferimento(int $lengh)
    {
        $rand = Str::random($lengh);

        $checkIfExistsRiferimento = Galleria::whereRiferimento($rand)->first();

        if ($checkIfExistsRiferimento) {
            self::generateRiferimento(6);
        } else {
            return $rand;
        }
    }
}
