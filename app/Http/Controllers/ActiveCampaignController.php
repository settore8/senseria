<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class ActiveCampaignController extends Controller
{
    private $url;
    private $key;

    public function __construct($url, $key){
        $this->url = $url;
        $this->key = $key;
    }

    public function createAccount(){
        $url = $this->url.'/api/3/accounts';
        $jayParsedAry = [
            "account" => [
                "name" => "Settore8", 
                "accountUrl" => "https://www.settore8.com" 
            ] 
        ];         
        $data_string = json_encode($jayParsedAry);                                                                                   
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Api-Token: '.$this->key, 'Content-Type: application/json']);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        
        $head = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        
        curl_close($ch);
        
        $data = json_decode($head, true);
        
        return $httpCode == '201' ? $data : [];
    }
    
    public function createContact(User $user){
        $url = $this->url.'/api/3/contacts';
        if($user->isProfessionista()){
            $data_string = json_encode([
                "contact" => [
                    "email" => $user->email, 
                    "firstName" => $user->nome, 
                    "lastName" => $user->cognome
                ] 
            ]);
        }elseif($user->isImpresa()){
            $data_string = json_encode([
                "contact" => [
                    "email" => $user->email, 
                    "firstName" => $user->ragione_sociale, 
                    "lastName" => ""
                ] 
            ]);
        }
                                                                                           
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Api-Token: '.$this->key, 'Content-Type: application/json']);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                              
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $head = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);
        $data = json_decode($head, true);

        if(array_key_exists('errors', $data)){
            return head($data['errors'])['title'];
        }

        return $data['contact'];
    }

    public function createList(){
        $url = $this->url.'/api/3/lists';

        $data_string = json_encode([
            "list" => [
                "name" => date('d-m-Y h:i:s'), 
                "stringid" => Str::slug(date('d-m-Y h:i:s'), '-'), 
                "sender_url" => "https://workook.com", 
                "sender_reminder" => "Hai ricevuto questa email, perché ti sei iscitto al nostro sito.", 
                "send_last_broadcast" => 0, 
                "carboncopy" => "", 
                "subscription_notify" => "", 
                "unsubscription_notify" => "", 
                "user" => 9
            ] 
        ]);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Api-Token: '.$this->key, 'Content-Type: application/json']);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $head = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);
        $data = json_decode($head, true);

        if(array_key_exists('errors', $data)){
            return head($data['errors'])['title'];
        }

        return $data['list'];
    }

    public function addContactToList($idLista, $idContatto, User $user){

        $url = $this->url.'/api/3/contactLists';
        $data_string = json_encode( [
            "contactList" => [
                  "list" => $idLista, 
                  "contact" => $idContatto, 
                  "status" => true
               ] 
         ]);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Api-Token: '.$this->key, 'Content-Type: application/json']);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $head = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);
        $data = json_decode($head, true);

        if(array_key_exists('errors', $data)){
            return head($data['errors'])['title'];
        }
        $user->update(['activeCampaignId' => $idContatto]);
        return true;
    }

    public function createMessage(){
        $url = $this->url.'/api/3/messages';
        $data_string = json_encode( [
            "message" => [
                  "fromname" => "AC Admin", 
                  "fromemail" => "noreply@example.com", 
                  "reply2" => "hello@example.com", 
                  "subject" => "You are subscribing to %LISTNAME%", 
                  "preheader_text" => "Pre-header Text" 
               ] 
         ]);
        $ch = curl_init();

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Api-Token: '.$this->key, 'Content-Type: application/json']);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $head = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);
        $data = json_decode($head, true);
        return true;
    }

    public function allContact($email = null){
        $url = $this->url.'/api/3/contacts?search=web@settore8.it';
        if(!is_null($email)){
            $url = $this->url.'/api/3/contacts?search='.$email;
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Api-Token: '.$this->key, 'Content-Type: application/json']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $head = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);
        
        $data = json_decode($head, true);

        if(!array_key_exists('contacts', $data)){
            return false;
        }

        return $data['contacts'];  
    }

    public function findContactByEmail($email){
        $contatti = $this->allContact($email);
        
        $contatto = array_filter($contatti, function($val) use ($email){
           return $val['email'] == $email;
        });
        if(!empty($contatto)){
            $contatto = head($contatto);
        }

        if(array_key_exists('id', $contatto)){
            return $contatto['id'];
        }

        return false;
    }
}
