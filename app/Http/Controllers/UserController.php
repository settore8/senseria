<?php

namespace App\Http\Controllers;

use App\Events\NotificaAccountCancellatoDaUtente;
use App\UserToken;
use DB;
use File;
use Auth;
use Event;
use Crypt;
use Illuminate\Support\Facades\Hash;
use Image;
use Storage;
use PDF;
use App\User;
use App\Annuncio;
use App\Articolo;
use App\Galleria;
use App\Gruppo;
use Carbon\Carbon;
use App\Abilitazione;
use App\Custom\Custom;
use App\Events\NuovaCertificazione;
use Illuminate\Support\Str;
use App\Http\Controllers\ActiveCampaignController;
use Illuminate\Contracts\Encryption\DecryptException;
use CyrildeWit\EloquentViewable\Support\Period;

use Illuminate\Http\Request;

class UserController extends Controller
{
    public function confcancaccount($token , $encoding_type){
        $user_token = UserToken::whereToken($token)->where('encoding_type', $encoding_type)->first();
        $user_token->user->delete();
        return redirect('/');
    }

    public function cancellaaccount(Request $request)
    {
        $this->validate($request, [
            'password' => 'required|password'
        ], [
            'password.required' => 'Inserisci la password per confermare la cancellazione',
            'password.password' => 'La Password inserita non è corretta'
        ]);
        $token = str::random(64);
        $encoding_type = base64_encode('deletepassword');
        auth::user()->token()->create([
            'user_id' => auth::user()->id,
            'token' => $token,
            'type' => 'deletepassword',
            'encoding_type' => $encoding_type
        ]);
        event(new NotificaAccountCancellatoDaUtente($token, $encoding_type));
        return redirect()->back()->with('msg', ['type' => 'success', 'text' => 'Ti abbiamo inviato una mail per confermare la cancellazione del profilo']);
    }

    public function confermaEmail(Request $request)
    {
        try {
            $user_id = Crypt::decrypt($request->user);
        } catch (DecryptException $e) {
            return redirect()->route('registrazione')->with('msg', 'Parametri passati non validi');
        }
        $user = User::find($user_id);

        if (!$user) {
            return redirect()->route('registrazione')->with('msg', 'Utente non trovato');
        }

        if (!$user->token()->where('user_id', $user_id)->where('token', $request->token)->where('encoding_type', $request->type)->first()) {
            return redirect()->route('registrazione')->with('msg', 'Indirizzo non valido');
        }

        $user->update(['email_verified_at' => Carbon::now()]);

        $user->token()->where('user_id', $user_id)->where('token', $request->token)->where('encoding_type', $request->type)->first()->delete();

        Auth::loginUsingId($user->id);

        // $this->registerOnActiveCampaign($user);

        return redirect()->route(Custom::getStepByUser($user))->with('emailconfermata', 'Email confermata con successo');

    }

    public function professionisti()
    {
        $users = User::with('raccolta', 'categoria', 'qualifica', 'meta', 'articoli', 'gallerie')->professionista()->isCompletato()->simplePaginate(36);
        return view('users.professionisti', compact('users'));
    }

    public function professionistiPiuAttivi()
    {   
        #TO_REVIEW# ordinati per punti activity
        $users = User::with('categoria', 'qualifica', 'meta', 'articoli', 'viewss')->professionista()->isCompletato()->simplePaginate(36);
        return view('users.professionisti', compact('users'));
    }

    public function professionistiSearch(Request $request)
    {
        $keyword = $request->input('s');
        $users = User::whereNotNull('collegio_id')->where('tipo', 'professionista', function ($q) use ($keyword, $request) {
            if ($request) {
                $q->where('nome', 'LIKE', '%' . $keyword . '%');
                $q->orWhere('cognome', 'LIKE', '%' . $keyword . '%');
                $q->orWhereRaw("CONCAT(`nome`, ' ', `cognome`) LIKE ?", ['%' . $keyword . '%']);
            }
        })->simplePaginate(36);

        return view('users.professionisti', compact('users', 'keyword'));
    }


    public function imprese()
    {
        $users = User::impresa()->simplePaginate(36);
        return view('users.imprese', compact('users'));
    }


    public function intro($slug)
    {
        $user = User::where('slug', $slug)->with('prestazioni.categoria')->first();
        if (!$user) {
            abort(404);
        }

        // registra la visita
        $view = views($user)->cooldown( (int) config('app.resetafter') )->collection('intro')->record();

        // verifico chi sta visitando la pagina
        $visitType = 'customer';
        if(Auth::check()) {
            $visitType = Auth::user()->tipo;
        }
        // qui si potrebbe scrivere in una tabella il tipo di user

        return view('users.profilo.intro', compact('user'));
    }

    public function gallerie($slug)
    {
        $user = User::where('slug', $slug)->first();
        if (!$user) {
            abort(404);
        }

        $view = views($user)->cooldown( (int) config('app.resetafter') )->collection('gallerie')->record();

        return view('users.profilo.gallerie', compact('user'));
    }

    public function articoli($slug)
    {
        $user = User::where('slug', $slug)->first();
        if (!$user) {
            abort(404);
        }
        $view = views($user)->cooldown( (int) config('app.resetafter') )->collection('articoli')->record();
        return view('users.profilo.articoli', compact('user'));
    }

    public function competenze($slug)
    {
        $user = User::where('slug', $slug)->first();
        if (!$user) {
            abort(404);
        }
        $view = views($user)->cooldown( (int) config('app.resetafter') )->collection('competenze')->record();
        return view('users.profilo.competenze', compact('user'));
    }


    public function registerOnActiveCampaign(User $user)
    {
        $ac = new ActiveCampaignController(config('app.activecampaign_url'), config('app.activecampaign_key'));

        //creo il contatto che sarebbe quello che si iscrive
        $contact = $ac->createContact($user);

        if ($this->checkIfIdExists($contact)) {
            $id_contatto = $contact['id'];
        } else {
            $id_contatto = $ac->findContactByEmail($user->email);
        }

        $syncContact = $ac->addContactToList(config('app.id_list_workook'), $id_contatto, $user);
    }

    public function checkIfIdExists($data)
    {
        if (is_array($data)) {
            if (!array_key_exists('id', $data)) {
                return false;
            }
        } else {
            return false;
        }
        return true;
    }

    /* account */

    /*******
     * STATISTICHE
     */

    public function statistichegenerali()
    {
        
        $user = Auth::user();
        $lastday = views($user)->period(Period::pastDays(1))->count();
        $lastweek = views($user)->period(Period::pastWeeks(1))->count();
        $lasmonth= views($user)->period(Period::pastMonths(1))->count();

        return view('app.account.statistiche.generali', compact('user', 'lastday'));
    }

    public function statisticheprofilo()
    {
        $user = Auth::user();
        return view('app.account.statistiche.profilo', compact('user'));
    }

    public function statistichearticoli()
    {
        $user = Auth::user();
        return view('app.account.statistiche.articoli', compact('user'));
    }

    public function statistichearticolo($id)
    {
        $user = Auth::user();
        return view('app.account.statistiche.articolo', compact('user', 'id'));
    }

    public function statistichegallerie()
    {
        $user = Auth::user();
        return view('app.account.statistiche.gallerie', compact('user'));
    }


    /*******
     * CONDIVISIONE
     */

    public function shareprofilo(Request $request)
    {
        try {
            $this->validate($request, [
                'email' => 'required|email:rfc,dns'
            ], [
                'email.required' => 'Inserisci un indirizzo email',
                'email.email' => 'Inserisci una email nel formato corretto'
            ]);

            $message = 'Profilo condiviso con <strong>' . $request->email . '</strong>';
            return redirect()->back()->with('message', ['type' => 'success', 'title' => 'Complimenti!', 'text' => $message]);

        } catch (\Exception $e) {
            return redirect()->back()->with('message', ['type' => 'error', 'title' => 'Attenzione', 'text' => 'Errore condivisione profilo']);
        }
    }


    /*******
     * DONWLOAD
     */


    public function downloadprofilo()
    {
        try {
            $user = Auth::user();
            $pdf = PDF::loadView('app.pdf.profilo', compact('user'));
            $filename = $user->slug . '_workook.pdf';
            $message = 'Download della presentazione iniziato';
            return $pdf->download($filename);
        } catch (\Exception $e) {
            return redirect()->back()->with('message', ['type' => 'error', 'title' => 'Attenzione', 'text' => 'Errore download']);
        }
    }


    /*******
     * NOTIFICHE
     */


    public function notifiche(Request $request)
    {
        return view('app.account.notifiche.index', compact('user'));
    }


    /*******
     * RACCOLTE
    */

    public function raccolte(Request $request, $type = null)
    {   
        $raccolte = Auth::user()->raccolte();
        
        if($type == 'utenti') {
            $raccolte = $raccolte->where('coverable_type', get_class(new User));
        }
        elseif($type == 'annunci') {
            $raccolte = $raccolte->where('coverable_type', get_class(new Annuncio));
        }
        elseif($type == 'contenuti') {
            $raccolte = $raccolte->where('coverable_type', get_class(new Articolo))->orWhere('coverable_type', get_class(new Galleria));
        }
        
        $raccolte = $raccolte->orderBy('updated_at', 'DESC')->simplePaginate(24);

        return view('app.account.raccolte.index', compact('raccolte', 'user'));
    }

    public function searchraccolte(Request $request)
    {   
        $keyword = $request->s;

        $raccolte = Auth::user()->raccolte();

        if($keyword) {
            $raccolte = $raccolte->whereHasMorph('coverable', get_class(new User), function($query) use ($keyword){
                $query->where('fullname', 'LIKE', '%' . $keyword . '%')
                        ->orWhereHas('qualifica', function ($query) use ($keyword) {
                                $query->where('nome_pubblico', 'LIKE', '%' . $keyword . '%');
                        });
                })->orWhereHasMorph('coverable', get_class(new Articolo), function($query) use ($keyword){
                    $query->where('titolo', 'LIKE', '%' . $keyword . '%')
                        ->orWhereHas('user', function($query) use ($keyword){
                        $query->where('fullname', 'LIKE', '%' . $keyword . '%');
                    });
                })->orWhereHasMorph('coverable', get_class(new Galleria), function($query) use ($keyword){
                    $query->where('titolo', 'LIKE', '%' . $keyword . '%');

                })->orWhereHasMorph('coverable', get_class(new Annuncio), function($query) use ($keyword){
                        $query->whereHas('immobile.informazione', function ($query) use ($keyword) {
                                $query->where('comune', 'LIKE', '%' . $keyword . '%')
                            ->orWhere('indirizzo', 'LIKE', '%' . $keyword . '%');
                        })
                        ->orWhereHas('immobile.specifica', function ($query) use ($keyword) {
                        $query->where('nome_pubblico', 'LIKE', '%' . $keyword . '%');
                        // TO_REVIEW mancano oltre all'immobile anche gli annunci di acquirenti, responsabile tecnico e collaborazioni
                    });
                });
        }

        $raccolte = $raccolte->orderBy('updated_at', 'DESC')->simplePaginate(10);

        return view('app.account.raccolte.index', compact('raccolte', 'user', 'keyword'));
    }



    /*******
     * ACCOUNT EDIT
    */

    public function editdashboard()
    {
        $user = Auth::user();
        return view('app.account.dashboard', compact('user'));
    }


    public function editprofilo()
    {
        $user = Auth::user();
        return view('app.account.profilo.generali', compact('user'));
    }

    public function updateprofilo(Request $request)
    {
        if ($request->hasFile('image_profilo')) {
            $this->createThumbnailProfilo($request->file('image_profilo'));
        }

        foreach ($request->social as $social) {
            $socialToSave[] = $social;
        }

        $indirizzo = json_decode($request->addressResult);

        if ($indirizzo) {
            if ($indirizzo->geo) {
                list($lat, $lng) = $indirizzo->geo;
            }
        } else {
            list($lat, $lng) = explode(",", $request->coordinate_comune);
        }


        if (Auth::user()->isProfessionista()) {
            Auth::user()->meta()->update([
                'email_pubblica' => $request->email_pubblica,
                'pec' => $request->pec,
                'descrizione' => $request->descrizione,
                'numero_iscrizione' => $request->numero_iscrizione,
                'numero_albo' => $request->numero_albo,
                'inquadramento' => $request->inquadramento,
                'partita_iva' => $request->partita_iva,
                'descrizione_comune' => $indirizzo ? $indirizzo->comune : $request->comune,
                'coordinate_comune' => DB::raw("(GeomFromText('POINT($lat $lng)'))"),
                'localita' => $indirizzo ? $indirizzo->localita : $request->localita,
                'indirizzo' => $indirizzo ? $indirizzo->indirizzo : $request->indirizzo,
                'provincia' => $indirizzo ? $indirizzo->provincia : $request->provincia,
                'cellulare' => $request->cellulare,
                'social' => serialize($socialToSave)
            ]);
        } else {
            Auth::user()->meta->update([
                'email_pubblica' => $request->email_pubblica,
                'tipologia' => $request->tipologia,
                'descrizione' => $request->descrizione,
                'partita_iva' => $request->partita_iva,
                'provincia' => $request->provincia,
                'descrizione_comune' => $$indirizzo ? $indirizzo->comune : $request->comune,
                'coordinate_comune' => DB::raw("(GeomFromText('POINT($lat $lng)'))"),
                'localita' => $indirizzo ? $indirizzo->localita : $request->localita,
                'indirizzo' => $indirizzo ? $indirizzo->indirizzo : $request->indirizzo,
                'provincia' => $indirizzo ? $indirizzo->provincia : $request->provincia,
                'telefono' => $request->telefono,
                'cellulare' => $request->cellulare,
                'inizio_attivita' => $request->inizio_attivita,
                'pec' => $request->pec,
                'social' => serialize($socialToSave),
                'rappresentante' => serialize([
                    'nome' => $request->nome,
                    'cognome' => $request->cognome,
                    'titolo' => $request->titolo
                ]),
            ]);
        }
        return redirect()->route('account.profilo')->with('message', 'Caricamento avvenuto con successo');
    }

    public function riepilogoabbonamento()
    {
        return view('app.account.abbonamento.riepilogo');
    }

    public function editprofiloimmagine()
    {
        $user = Auth::user();
        return view('app.account.profilo.immagine', compact('user'));
    }


    public function editprofilocontatti()
    {
        $user = Auth::user();
        return view('app.account.profilo.contatti', compact('user'));
    }


    public function editcompetenze()
    {
        $competenze = Gruppo::where('tipo', Auth::user()->isProfessionista() ? 'professionista' : 'impresa')->get();
        return view('app.account.competenze', compact('competenze'));
    }

    public function updatecompetenze(Request $request)
    {

        try {
            $this->validate($request, [
                'specializzazione' => 'required'
            ], [
                'specializzazione.required' => 'Specializzazione obbligatoria'
            ]);

            if ($request->specializzazione) {
                Auth::user()->update(['categoria_id' => $request->specializzazione]); // non so perchè era così  Auth::user()->update(['categoria_id' => $request->categoria_id]);
            }

            if (!empty($request->prestazioni)) {
                Auth::user()->prestazioni()->sync($request->prestazioni);
            }

            return redirect()->route('account.competenze')->with('message', ['type' => 'success', 'text' => 'Competenze aggiornate con successo']);
        } catch (\Exception $e) {
            return redirect()->route('account.competenze')->with('message', ['type' => 'error', 'text' => 'Errore aggiornamento competenze']);
        }

    }

    public function editabilitazioni()
    {
        $abilitazioni = Abilitazione::where('albo_id', Auth::user()->qualifica->albo_id)->get();
        return view('app.account.abilitazioni', compact('abilitazioni'));
    }

    public function updateabilitazioni(Request $request)
    {
        try {
            $this->validate($request, [
                'abilitazioni' => 'required'
            ], [
                'abilitazioni.required' => 'Abilitazioni obbligatorie'
            ]);

            Auth::user()->abilitazioni()->sync($request->abilitazioni);

            return redirect()->route('account.abilitazioni')->with('message', ['type' => 'success', 'text' => 'Abilitazioni aggiornate']);

        } catch (\Exception $e) {

            return redirect()->route('account.abilitazioni')->with('message', ['type' => 'error', 'text' => 'Errore salvataggio abilitazioni']);

        }
    }

    public function editarea()
    {
        return view('app.account.areaoperativa');
    }

    public function updatearea(Request $request)
    {
        try {
            $coord = $request->area;

            if (Str::contains($request->area, ['(', ')'])) {
                $coord = explode(")", str_replace([",(", "(",], "", $coord));
                $listaCoordinate = "";
                //l'ultimo valore deve essere uguale al primo
                $coord[count($coord) - 1] = $coord[0];

                foreach ($coord as $key => $value) {
                    $listaCoordinate .= str_replace(",", " ", $value) . ", ";
                }

                $listaCoordinate = substr($listaCoordinate, 0, strlen($listaCoordinate) - 2);
            } else {
                $listaCoordinate = $coord;
            }
            Auth::user()->update([
                'area_operativa' => DB::raw("PolygonFromText('POLYGON((" . $listaCoordinate . "))')")
            ]);

            return redirect()->route('account.area')->with('message', ['type' => 'success', 'text' => 'Area operativa salvata correttamente']);
        } catch (\Exception $e) {
            return redirect()->route('account.area')->with('message', ['type' => 'error', 'text' => 'Errore salvataggio area operativa']);
        }

    }

    public function editcertificazione()
    {
        $status = Auth::user()->certificazione ? Auth::user()->certificazione->status : 'non_presente';
        return view('app.account.certificazione', compact('status'));
    }

    public function updatecertificazione(Request $request)
    {
        try {
            $this->validate($request, [
                'certificazione' => 'mimes:pdf',
            ], [
                'certificazione.mimes' => 'Formati accettati: pdf'
            ]);

            if (!$certificazione = Auth::user()->certificazione) {

                $certificazione = Auth::user()->certificazione()->create([
                    'status' => 'attesa',
                    'filename' => $request->file('certificazione')->getClientOriginalName()
                ]);
            } else {
                $vecchioFile = $certificazione->filename;
                $certificazione = Auth::user()->certificazione()->update([
                    'scadenza' => null,
                    'status' => 'attesa',
                    'filename' => $request->file('certificazione')->getClientOriginalName()
                ]);
            }

            if ($request->hasFile('certificazione')) {
                if ($request->file('certificazione')->isValid()) {
                    if (is_int($certificazione)) {
                        //se è un intero vuol dire che lo sta aggiornando e quindi cancello il vecchio file
                        Storage::delete('/users/certificazione/' . Auth::id() . '/' . $vecchioFile);
                    }
                    Storage::putFileAs('/users/certificazione/' . Auth::id(), $request->file('certificazione'), $request->file('certificazione')->getClientOriginalName());
                }
            }

            $link = '/users/certificazione/' . Auth::id() . '/' . $request->file('certificazione')->getClientOriginalName();

            event(new NuovaCertificazione(Auth::user(), $link));

            return redirect()->route('account.certificazione')->with('message', ['type' => 'error', 'text' => 'Errore richiesta certificazione']);
        } catch (\Exception $e) {
            dd($e);
            return redirect()->route('account.certificazione')->with('message', ['type' => 'error', 'text' => 'Errore richiesta certificazione']);
        }
    }


    public function editimpostazioninotifiche()
    {
        return view('app.account.impostazioni.notifiche');
    }

    public function editimpostazionipassword()
    {
        return view('app.account.impostazioni.password');
    }

    public function updatepassword(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|unique:users,email,' . Auth::id(),
            'password' => 'confirmed'
        ], [
            'email.required' => 'Email obbligatoria',
            'email.email' => 'Formato email non valido',
            'email.unique' => 'Email gia usata da un altro utente',
            'password.confirmed' => 'Le password non coincidono'
        ]);

        try {
            Auth::user()->update(['email' => $request->email]);
            if ($request->password) {
                Auth::user()->update(['password' => bcrypt($request->password)]);
            }
            return redirect()->route('account.impostazioni.password')->with(['type' => 'success', 'text' => 'Dati di accesso aggiornati correttamente']);
        } catch (\Exception $e) {
            return redirect()->route('account.impostazioni.password')->with(['type' => 'error', 'text' => 'Errore salvataggio dati di accesso']);
        }

    }

    public function editimpostazioniprivacy()
    {
        return view('app.account.impostazioni.privacy');
    }

    public function editimpostazioniavanzate()
    {
        return view('app.account.impostazioni.avanzate');
    }


    public function createThumbnailProfilo($file)
    {
        if ($file->isValid()) {
            $imageInfo = getimagesize($file);

            $width = $imageInfo[0];
            $height = $imageInfo[1];
            $mime = $file->getmimeType();
            $size = $file->getSize();

            $filename = $file->getClientOriginalName();

            if (!Storage::disk('temp-images')->exists('/')) {
                Storage::disk('temp-images')->makeDirectory('/', 0755, true);
            }
            if (!Storage::disk('temp-images')->exists('/' . Auth::id())) {
                Storage::disk('temp-images')->makeDirectory('/' . Auth::id(), 0755, true);
            }

            $imgToSave200 = Image::make($file->getRealPath());
            $imgToSave200->orientate(); //tengo l'orientamento delle foto
            $imgToSave200->fit(200);
            //nome del file con la 's' finale per identificare il file200x200
            $filename_S = Custom::createNameFile('s', $file);
            $file_S = public_path('temp-images/' . Auth::id() . '/' . $filename_S);

            /***** Immagine 60x60 *****/
            $imgToSave60 = Image::make($file->getRealPath());
            $imgToSave60->orientate(); //tengo l'orientamento delle foto
            $imgToSave60->fit(60);
            $filename_T60 = Custom::createNameFile('t60', $file);
            $file_T60 = public_path('temp-images/' . Auth::id() . '/' . $filename_T60);

            /***** Immagine 36x36 *****/
            $imgToSave36 = Image::make($file->getRealPath());
            $imgToSave36->orientate(); //tengo l'orientamento delle foto
            $imgToSave36->fit(36);
            $filename_T36 = Custom::createNameFile('t36', $file);
            $file_T36 = public_path('temp-images/' . Auth::id() . '/' . $filename_T36);

            if ($file->getClientOriginalExtension() == 'png' || $file->getClientOriginalExtension() == 'PNG') {
                //se è in png la foto rischia di avere una trasparenza cosi la levo e mettendoci un sfondo bianco
                $canvas200 = Image::canvas($imgToSave200->width(), $imgToSave200->height(), '#fff');
                $canvas60 = Image::canvas($imgToSave60->width(), $imgToSave60->height(), '#fff');
                $canvas36 = Image::canvas($imgToSave36->width(), $imgToSave36->height(), '#fff');

                $canvas200->insert($imgToSave200);
                $canvas60->insert($imgToSave60);
                $canvas36->insert($imgToSave36);
                if (Auth::user()->isProfessionista()) {
                    $canvas200->greyscale()->save($file_S);
                    $canvas60->greyscale()->save($file_T60);
                    $canvas36->greyscale()->save($file_T36);
                    Auth::user()->meta()->update(['image' => $filename]);
                } else {
                    $canvas200->save($file_S);
                    $canvas60->save($file_T60);
                    $canvas36->save($file_T36);
                    Auth::user()->meta()->update(['image' => $filename]);
                }
            } else {
                if (Auth::user()->isProfessionista()) {
                    $imgToSave200->greyscale()->save($file_S);
                    $imgToSave60->greyscale()->save($file_T60);
                    $imgToSave36->greyscale()->save($file_T36);
                    Auth::user()->meta()->update(['image' => $filename]);
                } else {
                    $imgToSave200->save($file_S);
                    $imgToSave60->save($file_T60);
                    $imgToSave36->save($file_T36);
                    Auth::user()->meta()->update(['image' => $filename]);
                }
            }

            foreach (Storage::disk('temp-images')->allFiles(Auth::id()) as $file) {
                Storage::put('/users/profilo/' . $file, Storage::disk('temp-images')->get($file));
                Storage::disk('temp-images')->delete($file);
            }


            return redirect()->route('account.profilo')->with('message', 'Caricamento avvenuto con successo');
        }
    }


}
