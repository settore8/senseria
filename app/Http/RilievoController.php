<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Rilievo;

class RilievoController extends Controller
{

    public function store(Request $request) {

        $this->validate($request, [
            'titolo' => 'required',
        ], [
            'titolo.required' => 'Il titolo è obligatorio',
        ]);

        // qui si ketterà l'auth

        $rilievo = new Rilievo;

        $rilievo->titolo = $request->titolo;
        $rilievo->stato = 'Bozza';
        $rilievo->save();

        return redirect( route('immobile.rilievo.edit', $rilievo->immobile));

    }


    public function edit($rilievo) {

        $rilievo = Rilievo::findOrFail($id);

        return view('immobile.rilievo.edit', $rilievo->immobile);
    }


    public function update(Request $request, $id) {

        $this->validate($request, [
            'titolo' => 'required',
            'mq' => 'required_if:action,send|numeric|min:1|max:400',
            'piani' => 'required_if:action,send|numeric|min:1',
            'vani' => 'required_if:action,send|numeric|min:1',
            'indirizzo' => 'required_if:action,send'
        ], [
            'titolo.required' => 'Il titolo è obligatorio',
            'mq.max' => 'Mq massimi consentiti 400',
            'mq.required_if' => 'Metri quadri obbligatori',
            'piani.min' => 'Piani minimi consentiti 1',
            'piani.required_if' => 'Numero Piani obbligatorio',
            'vani.min' => 'Vani minimi consentiti 1',
            'vani.required_if' => 'Numero Vani obbligatorio',
            'indirizzo' => 'L\'indirizzo è obbligatorio',
            'distanza.max' => 'Destinazione superiore a 400km',
        ]);

        $rilievo = Rilievo::findOrFail($id);

        $rilievodata = [
            'titolo' => $request->titolo,
            'standard' => [
                'mq' => $request->mq,
                'piani' => $request->piani,
                'vani' => $request->vani,
                'address' => $request->address, // è l'indirizzo in forma stringa 
                'indirizzo' => json_decode($request->indirizzo), // è l'array di gmaps
                'prestazioni' => $request->prestazioni,
                'preventivo' => [
                    'distanza' => base64_decode($request->distanza),
                    'durata' => base64_decode($request->durata)
                ]
            ],

            'personalizza' => [
                'restituzione' => $request->restituzione,
                'fotopiani' => $request->fotopiani,
            ],

            'note' => $request->note,
        ];

        switch ($request->input('action')) {
            case 'save':
                $rilievodata['stato'] = 'Bozza';
                $messaggio = 'Rilievo salvato correttamente';
                $route = 'gaiagroup.rilievo.edit';
                break;
            case 'send':
                $rilievodata['stato'] = 'Richiesta inviata';
                $messaggio = 'Richiesta inviata correttamente';
                $route = 'gaiagroup.rilievo.show';
                break;
        }

        $rilievo->update($rilievodata);

        return redirect( route($route, $rilievo->id))->with('message', $messaggio);

    }

    public function show($id) {
        $rilievo = Rilievo::findOrFail($id);

        if($rilievo->stato == 'Bozza') {
            return redirect( route('gaiagroup.rilievo.edit', $rilievo->id));
        }

        

        return view('app.rilievi.show', compact('rilievo'));
    }

    public function destroy($id)
    {
        $rilievo = Rilievo::findOrFail($id);
        $titolo = $rilievo->titolo;
        $rilievo->delete();
        return redirect()->route('gaiagroup.rilievi.index')->with('message', 'Rilievo <strong>'. $titolo .'</strong> eliminato');

    }

}
