<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckIsMyImmobile
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // controllo che il creatore dell'immobile sia la persona loggata
        if($request->immobile->user_id == auth::user()->id) {
            return $next($request);
        }

        // se la route è quella del riepilogo butto fuori dall'immobile
        if($request->route()->getName() == 'immobile' || $request->route()->getName() == 'immobile.informazioni') {
            return redirect()->route('immobili');;
        }

        return redirect()->route('immobile', $request->immobile);

    }
}
