<?php

namespace App\Http\Middleware;

use Closure;
use Immobile;
use Auth;

class CheckIfExistResponsabileTecnico
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $modulo)
    {
        // TO_REVIEW Propabilmente dovremo abilitare il creatore dell'immobile a visualizzare i moduli (anche se disabilitati) nel caso in cui fossero popolati.qualifica
        // può infatti essere possibile che io disabiliti il modulo perchè non voglio che il responsabile tecnico lo editi, ma voglio cmq poter accedere in lettura
        if(Auth::id() == $request->immobile->user_id && ($request->immobile->moduli && in_array($modulo, $request->immobile->moduli))){
            return $next($request);
        }

        if(!$request->immobile->tecnico_id){
            return redirect()->route('immobile.responsabiletecnico', $request->immobile)->with('modal', ['title' => 'Attenzione', 'text' => "Per poter accedere al servizio <strong>$modulo</strong> è necessario assegnare il ruolo di responsabile tecnico ed abilitare il servizio desiderato.", 'visual' => 'visual-assegna-tecnico.svg', 'visual_size' => 'small', 'close_button' => 'Ho capito'] );
        }

        if(!$request->immobile->moduli || !in_array($modulo, $request->immobile->moduli) ){
            return redirect()->route('immobile.responsabiletecnico', $request->immobile)->with('modal', ['title' => 'Attenzione!', 'text' => 'Necessario abilitare questo modulo!'] );
        }

        return $next($request);
    }
}
