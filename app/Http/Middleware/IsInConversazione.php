<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class IsInConversazione
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        ;
        return $request->conversazione->users->contains(Auth::user()) ? $next($request) : redirect()->route('conversazioni')->with('message', ['type' => 'error', 'text' => 'Accesso a questa conversazione non consentito']);
    }
}
