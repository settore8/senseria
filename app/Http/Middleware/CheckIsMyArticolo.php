<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckIsMyArticolo
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->articolo->pubblicato || ($request->articolo->user_id != auth::user()->id)){
            return redirect()->route('articoli');
        }
        return $next($request);
    }
}
