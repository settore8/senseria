<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use App\UserToken;

class CheckTokenAgenteImmobiliare
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $ut = UserToken::where('token', $request->route('token'))->where('encoding_type' , $request->route('type'))->first();
        
        if(!$ut){
            return redirect()->route('dashboard')->with('message', ['type' => 'error', 'text' => 'Accesso non consentito']);
        }

        if(!Auth::check()){
            Auth::loginUsingId($ut->user_id);
        }

        if($ut->user_id != Auth::id()){
            return redirect()->route('dashboard')->with('message', ['type' => 'error', 'text' => 'Accesso non consentito']);
        }
        
        $request->request->add(['immobile' => $ut->model->modelable]);

        return $next($request);
    }
}
