<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CanEditImmobile
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        if($request->immobile->user_id == Auth::user()->id || $request->immobile->tecnico_id == auth::user()->id) {
            return $next($request);
        }

        abort(404);
        //return redirect()->route('immobili');
    }
}
