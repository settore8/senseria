<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use App\Custom\Custom;

class CheckIfUserIsCompleted
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(!Auth::check()){
            return redirect()->route('login');
        }

        // Auth::loginUsingId(Auth::id());
        if(is_string($route = Custom::getStepByUser(Auth::user()))){ // se va tutto bene ritorna true
            if($request->route()->getName() == $route){ //questo impedisce di andare negli step successivi nella fase di onboarding
                return $next($request);
            };
            return redirect()->route($route);
        }

        return $next($request);
    }
}
