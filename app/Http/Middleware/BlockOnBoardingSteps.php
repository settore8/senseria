<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class BlockOnBoardingSteps
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $loggato = Auth::user(); //questo va dopo il middlware auth non ho bisogno di controllare se è loggato
        if(!is_null($loggato->completed_at)){
             //se è compleato non completato non può andare 
            return redirect()->route('dashboard');
        }
        return $next($request);
    }
}
