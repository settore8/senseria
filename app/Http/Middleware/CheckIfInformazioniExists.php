<?php

namespace App\Http\Middleware;

use Closure;

class CheckIfInformazioniExists
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
        if(is_null($request->immobile->informazione)){
            if(in_array($request->route()->getName(),['immobile.store.informazioni', 'immobile.informazioni'])){
                return $next($request);
            }
            return redirect()->route('immobile.informazioni', $request->immobile)->with('modal', ['title' => 'Attenzione', 'visual' => 'message-attention.svg', 'visual_size' => 'small', 'close_button' => 'Ho capito', 'text' => 'Per andare avanti inserisci prima le informazioni dell\'immobile']);
        }

        return $next($request);
    }
}
