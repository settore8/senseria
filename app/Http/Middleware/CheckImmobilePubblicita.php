<?php

namespace App\Http\Middleware;

use Closure;


class CheckImmobilePubblicita
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        if (!$request->inPubblicita()) {
            return redirect('home');
        }
        
        return $next($request);
    }
}
