<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class UserMeta extends Model
{

    protected $table = 'user_meta';

    protected $fillable = ['user_id', 'ragione_sociale', 'nome', 'cognome', 'image', 'descrizione_comune', 'coordinate_comune', 'info_generali'];

    protected $casts = [
        'info_generali' => 'array'
    ];

    public function user(){
        return $this->belongsTo(User::class, 'users_id');
    }

    public function getLatAttribute(){
        $lat = DB::select("SELECT ST_X(coordinate_comune) as latitudine FROM user_meta where user_id = $this->user_id ");
        return !is_null(head($lat)) ? head($lat)->latitudine: '';
    }

    public function getLngAttribute(){
        $lng = DB::select("SELECT ST_Y(coordinate_comune) as longitudine FROM user_meta where user_id = $this->user_id ");
        return !is_null(head($lng)) ? head($lng)->longitudine: '';
    }
}
