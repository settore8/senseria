<?php

namespace App\Listeners;

use App\Events\InserisciToken;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class InserisciTokenDiSicurezza
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  InserisciToken  $event
     * @return void
     */
    public function handle(InserisciToken $event)
    {
        return $event->user->token()->create([
            'token' => $event->token,
            'type' => $event->type,
            'encoding_type' =>  base64_encode($event->type)
        ]);
    }
}
