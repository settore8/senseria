<?php

namespace App\Listeners;

use App\Events\AssegnaToken;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class AssegnaTokenAlModel
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AssegnaToken  $event
     * @return void
     */
    public function handle(AssegnaToken $event)
    {
        $event->token->model()->create([
            'modelable_id' => $event->model->id,
            'modelable_type' => get_class($event->model)
        ]);
    }
}
