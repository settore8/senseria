<?php

namespace App\Listeners;

use App\Immobile;
use App\Events\SyncImmobiliToUser;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SincronizzaImmobiliConNuovoUtente
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SyncImmobiliToUser  $event
     * @return void
     */
    public function handle(SyncImmobiliToUser $event)
    {   
        /**
         * trovo tutti gli immobili compatibili con l'area dell'utente appena registratro
         * che hanno un annuncio come responsabile tecnico ancora non scelto
         */
        
        $immobili = Immobile::whereHas('annuncioRespTecnico.candidature', function($query){
            $query->whereNull('candidato');
        })->orDoesntHave('annuncioRespTecnico.candidature')->whereNull('tecnico_id')->has('annuncioRespTecnico')->FindByUser($event->user->id)->has('informazione')->get();
        foreach($immobili as $immobile){
            $immobile->annuncioRespTecnico->compatibilita()->create(['ricevente_id' => $event->user->id]);
        }
    }
}
