<?php

namespace App\Listeners;

use App\CandidaturaEsterna;
use App\Mail\AggiuntoModuloDaAnnuncio;
use App\Mail\CondivisioneImmobile;
use App\Mail\ConfermaAgenteImmobiliare;
use App\Mail\ConfermaEmail;
use App\Mail\inviaEmailPerDeclinareSceltaResponsabileTecnico;
use App\Mail\InvitoConfermaMail;
use App\Mail\LinkResetPassword;
use App\Mail\MailNuovoCandidatoResponsabileTecnico;
use App\Mail\NotificaAccountCancellatoDaUtente;
use App\Mail\NotificaMailRimozioneAccount;
use App\Mail\NotificaTagGalleria;
use App\Mail\ReminderCreazionePrimaGalleria;
use App\Mail\ReminderCreazionePrimoArticolo;
use App\Mail\ReminderCreazionePrimoImmobile;
use App\Mail\RimossoModuloDaAnnuncio;
use App\User;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class GestioneEmailListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param object $event
     * @return void
     */
    public function inviaEmailPerTagSuGalleria($event)
    {
        Mail::to($event->destinatario)->send(new NotificaTagGalleria($event->galleria, $event->token, $event->nome, $event->collaborazione));
    }

    public function inviaEmailPerTagSuArticolo($event)
    {
        Mail::to($event->destinatario)->send(new NotificaTagGalleria($event->articolo, $event->token, $event->nome, $event->collaboratore));
    }

    public function immobileCondiviso($event)
    {
        foreach ($event->email as $key => $email) {
            Mail::to($email)->send(new CondivisioneImmobile($event->token, $event->id));
        }
    }

    public function responsabileTecnicoNonRegistrato($event)
    {
        CandidaturaEsterna::create([
            'email' => $event->email,
            'token' => $event->token,
            'immobile_id' => $event->immobile
        ]);
    }

    public function aggiuntoModulo($event)
    {
        foreach ($event->candidati as $candidato) {
            Mail::to('web@settore8.it')->send(new AggiuntoModuloDaAnnuncio($event->annuncio, $event->moduli));
        }
    }

    public function rimossoModulo($event)
    {
        foreach ($event->candidati as $candidato) {
            Mail::to('web@settore8.it')->send(new RimossoModuloDaAnnuncio($event->annuncio, $event->moduli));
        }
    }

    public function nuovoCandidatoResponsabileTecnico($event)
    {
        Mail::to($event->email_proprietario_immobile)->send(new MailNuovoCandidatoResponsabileTecnico($event->candidato, $event->candidature));
    }

    public function inviaEmailPerDeclinareSceltaResponsabileTecnico($event)
    {
        Mail::to($event->email)->send(new inviaEmailPerDeclinareSceltaResponsabileTecnico($event));
    }

    public function settaggioPassword($event)
    {

    }

    public function nuovaCertificazione($event)
    {
        Mail::to('web@settore8.it')->send(new InvioCertificazionePerCertificazione($event->user, $event->link));
    }

    public function inviaLinkResetPassword($event)
    {
        Mail::to($event->user->email)->send(new LinkResetPassword($event->user, $event->token, $event->type));
    }

    public function inviaConfermaEmail($event)
    {
        Mail::to($event->user->email)->send(new ConfermaEmail($event->user, $event->token, $event->type));
    }

    public function invitoConfermaEmail($event)
    {
        $users = User::whereNull('email_verified_at');
        $users_overdays = $users->whereBetween('created_at', [Carbon::now()->subDays(2), Carbon::now()->subDays(1)])->get();
        foreach ($users_overdays as $user) {
            $token = $user->token->first();
            Mail::to($user->mail)->send(new invitoConfermaMail($user, $token, 1));
        }
        $users_overdays = $users->whereBetween('created_at', [Carbon::now()->subDays(3), Carbon::now()->subDays(2)])->get();
        foreach ($users_overdays as $user) {
            $token = $user->token->first();
            Mail::to($user->mail)->send(new invitoConfermaMail($user, $token, 2));
        }
        $users_overdays = $users->whereBetween('created_at', [Carbon::now()->subDays(4), Carbon::now()->subDays(3)])->get();
        foreach ($users_overdays as $user) {
            $token = $user->token->first();
            Mail::to($user->mail)->send(new invitoConfermaMail($user, $token, 3));
        }
        //Cancellazione utenti che non hanno confermato la mail
        $users_overdays = $users->whereBetween('created_at', [Carbon::now()->subDays(5), Carbon::now()->subDays(4)])->get();
        foreach ($users_overdays as $user) {
            Mail::to('alessio@alessiomealli.com')->send(new NotificaMailRimozioneAccount());
            $user->remove();
        }
    }

    public function sceltoAgenteImmobiliare($event)
    {
        Mail::to($event->email)->send(new ConfermaAgenteImmobiliare($event));
    }

    public function ReminderMailCompletamentoAccount($event)
    {

    }

    public function ReminderCreazionePrimoArticolo($event)
    {
        //Invio la mail a tutti gli utenti che non hanno mai creato articoli, solo gli utenti che hanno completato l'account tra 30 e 90 giorni prima
        $users = User::doesntHave('articoli')->whereBetween('completed_at', [Carbon::now()->subDays(90), Carbon::now()->subDays(30)])->get();
        foreach($users as $user){
            Mail::to('alessio@alessiomealli.com')->send(new ReminderCreazionePrimoArticolo());
        }
    }

    public function ReminderCreazionePrimaGalleria($event)
    {
        //Invio la mail a tutti gli utenti che non hanno mai creato gallerie, solo gli utenti che hanno completato l'account tra 30 e 90 giorni prima
        $users = User::doesntHave('gallerie')->whereBetween('completed_at', [Carbon::now()->subDays(90), Carbon::now()->subDays(30)])->get();
        foreach($users as $user){
            Mail::to('alessio@alessiomealli.com')->send(new ReminderCreazionePrimaGalleria());
        }
    }

    public function ReminderCreazionePrimoImmobile($event)
    {
        //Invio la mail a tutti gli utenti che non hanno mai creato un immobile, solo gli utenti che hanno completato l'account tra 30 e 90 giorni prima
        $users = User::doesntHave('immobili')->whereBetween('completed_at', [Carbon::now()->subDays(90), Carbon::now()->subDays(30)])->get();
        foreach($users as $user){
            Mail::to('alessio@alessiomealli.com')->send(new ReminderCreazionePrimoImmobile());
        }
    }

    public function NotificaAccountCancellatoDaUtente($event){
        Mail::to(auth::user()->email)->send(new NotificaAccountCancellatoDaUtente($event->token, $event->encoding_type));
    }

    public function InvioMailPerImmobileCondiviso($event){
        dd("sono qui");
    }

    public function subscribe($events)
    {
        $events->listen(
            'App\Events\InviaEmailPerTagSuGalleria',
            'App\Listeners\GestioneEmailListener@inviaEmailPerTagSuGalleria'
        );
        $events->listen(
            'App\Events\InviaEmailPerTagSuArticolo',
            'App\Listeners\GestioneEmailListener@inviaEmailPerTagSuArticolo'
        );
        $events->listen(
            'App\Events\ImmobileCondiviso',
            'App\Listeners\GestioneEmailListener@InviaEmailPerCondivisioneImmobile'
        );
        $events->listen(
            'App\Events\ResponsabileTecnicoNonRegistrato',
            'App\Listeners\GestioneEmailListener@InviaEmailPerResponsabileTecnicoEsternoAWorkook'
        );
        $events->listen(
            'App\Events\AggiuntoModulo',
            'App\Listeners\GestioneEmailListener@InviaEmailAggiuntaModuloResponsabileTecnico'
        );
        $events->listen(
            'App\Events\RimossoModulo',
            'App\Listeners\GestioneEmailListener@InviaEmailRimozioneModuloResponsabileTecnico'
        );
        $events->listen(
            'App\Events\NuovoCandidatoResponsabileTecnico',
            'App\Listeners\GestioneEmailListener@InviaEmailNuovoCandidatoResponsabileTecnico'
        );
        $events->listen(
            'App\Events\SceltoResponsabileTecnicoDaWorkook',
            'App\Listeners\GestioneEmailListener@InviaEmailPerDeclinareSceltaResponsabileTecnico'
        );
        $events->listen(
            'App\Events\SettaggioPassword',
            'App\Listeners\GestioneEmailListener@InviaEmailSettaggioPassword'
        );
        $events->listen(
            'App\Events\NuovaCertificazione',
            'App\Listeners\GestioneEmailListener@InviaEmailPerVerificaCertificazione'
        );
        $events->listen(
            'App\Events\InviaLinkResetPassword',
            'App\Listeners\GestioneEmailListener@inviaLinkResetPassword'
        );
        $events->listen(
            'App\Events\InviaConfermaEmail',
            'App\Listeners\GestioneEmailListener@inviaConfermaEmail'
        );
        $events->listen(
            'App\Events\InvitoConfermaEmail',
            'App\Listeners\GestioneEmailListener@InvitoConfermaEmail'
        );
        $events->listen(
            'App\Events\SceltoAgenteImmobiliare',
            'App\Listeners\GestioneEmailListener@InviaEmailPerConfermaAgenteImmobiliare'
        );
        $events->listen(
            'App\Events\ReminderMailCompletamentoAccount',
            'App\Listeners\GestioneEmailListener@ReminderMailCompletamentoAccount'
        );
        $events->listen(
            'App\Events\ReminderCreazionePrimoArticolo',
            'App\Listeners\GestioneEmailListener@ReminderCreazionePrimoArticolo'
        );
        $events->listen(
            'App\Events\ReminderCreazionePrimaGalleria',
            'App\Listeners\GestioneEmailListener@ReminderCreazionePrimaGalleria'
        );
        $events->listen(
            'App\Events\ReminderCreazionePrimoImmobile',
            'App\Listeners\GestioneEmailListener@ReminderCreazionePrimoImmobile'
        );
        $events->listen(
            'App\Events\NotificaAccountCancellatoDaUtente',
            'App\Listeners\GestioneEmailListener@NotificaAccountCancellatoDaUtente'
        );
        $events->listen(
            'App\Events\InvioMailPerImmobileCondiviso',
            'App\Listeners\GestioneEmailListener@InvioMailPerImmobileCondiviso'
        );
    }

}
