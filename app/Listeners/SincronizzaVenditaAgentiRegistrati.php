<?php

namespace App\Listeners;

use App\User;
use App\Events\SyncAgentiToVendita;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SincronizzaVenditaAgentiRegistrati
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SyncAgentiToVendita  $event
     * @return void
     */
    public function handle(SyncAgentiToVendita $event)
    {
        $users = User::FindByImmobile($event->immobile->id)->pluck('id');

        foreach($users as $id){
            $event->annuncio->compatibilita()->updateOrCreate(['ricevente_id' => $id, 'coverable_id' => $event->annuncio->id, 'coverable_type' => get_class($event->annuncio)], ['ricevente_id' => $id]);
        }
    }
}
