<?php

namespace App\Listeners;

use Mail;
use App\Mail\LinkResetPassword;
use App\Events\InviaLinkResetPassword;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class InviaEmailConLinkResetPassword
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  InviaLinkResetPassword  $event
     * @return void
     */
    public function handle(InviaLinkResetPassword $event)
    {
        Mail::to($event->user->email)->send(new LinkResetPassword($event->user, $event->token, $event->type));
    }
}
