<?php

namespace App\Listeners;

use Storage;
use App\Immagine;
use App\Events\SalvaThumbnail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class InserisciThumbnailToModel
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SalvaThumbnail  $event
     * @return void
     */
    public function handle(SalvaThumbnail $event)
    {
        $info = getimagesize($event->immagine);
        $width = $info[0];
        $height = $info[1];
        $filename = $event->immagine->getClientOriginalName();
        $size = $event->immagine->getSize();
        $mimetype = $event->immagine->getmimeType();
        
        $immagine = Immagine::create([
            'width' => $width,
            'height' => $height,
            'filename' => $filename,
            'size' => $size,
            'filemime' => $mimetype
        ]);
        Storage::putFileAs('articoli/'.$event->model->id.'/thumbnail', $event->immagine, $event->immagine->getClientOriginalName());
        $event->model->update(['immagine_id' => $immagine->id]);
    }
}
