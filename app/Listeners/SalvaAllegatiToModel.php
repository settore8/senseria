<?php

namespace App\Listeners;

use Storage;
use App\File;
use App\Allegato;
use Carbon\Carbon;
use App\Events\SalvaAllegati;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SalvaAllegatiToModel
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SalvaAllegati  $event
     * @return void
     */
    public function handle(SalvaAllegati $event)
    {
        foreach($event->files as $file){
            if($file->isValid()){
                $fileCreate = File::create([
                    'size' =>  $file->getSize(),
                    'filename' => $file->getClientOriginalName(),
                    'filemime' => $file->getmimeType(),
                    'created_at' => Carbon::now()
                ]);

                $event->model->allegati()->create(['file_id' => $fileCreate->id]);
                Storage::putFileAs('articoli/'.$event->model->id, $file, $file->getClientOriginalName());
            }
        }
        
    }
}
