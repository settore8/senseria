<?php

namespace App\Listeners;

use App\Archivio;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SalvaCompletamentoImmobile
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Attualmente prende massimo due parametri, ma va capito come renderlo dinamico
     *
     * @param object $event
     * @return void
     */
    public function completamentoModulo($event)
    {

        $percentuali = Archivio::wheretype('percentuali_immobili')->first()->dati;

        $statsImmobile = $event->immobile->statistiche;
        if (!$statsImmobile) {
            $statsImmobile = [];
        }

        $valori = explode('.', $event->path);
        //Gestisce un input fino a 3 livelli di profondità per aumentare i livelli è sufficente aumentare un elseif
        if(count($valori) == 2){
            $statsImmobile[$valori[0]][$valori[1]] = $percentuali[$valori[0]][$valori[1]];
        }elseif (count($valori) == 3){
            $statsImmobile[$valori[0]][$valori[1]][$valori[2]] = $percentuali[$valori[0]][$valori[1]][$valori[2]];
        }elseif (count($valori) == 4){
            $statsImmobile[$valori[0]][$valori[1]][$valori[2]][$valori[3]] = $percentuali[$valori[0]][$valori[1]][$valori[2]][$valori[3]];
        }


        $event->immobile->update(['statistiche' => $statsImmobile]);
    }


    public function subscribe($events)
    {
        $events->listen(
            'App\Events\CompletamentoModulo',
            'App\Listeners\SalvaCompletamentoImmobile@completamentoModulo'
        );
    }
}
