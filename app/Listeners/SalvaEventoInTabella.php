<?php

namespace App\Listeners;

use Carbon\Carbon;
use App\RegistraEvento as Evento;
use App\Events\RegistraEvento;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SalvaEventoInTabella
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RegistraEvento  $event
     * @return void
     */
    public function handle(RegistraEvento $event)
    {
        Evento::updateOrCreate([
            'user_id' => $event->user_id,
            'evento' => $event->class
        ],[
            'user_id' => $event->user_id,
            'evento' => $event->class,
            'created_at' => Carbon::now()
        ]);
    }
}
