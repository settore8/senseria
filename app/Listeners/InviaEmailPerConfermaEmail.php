<?php

namespace App\Listeners;

use Mail;
use App\Mail\ConfermaEmail;
use App\Events\InserisciToken;
use App\Events\InviaConfermaEmail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class InviaEmailPerConfermaEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  InviaConfermaEmail  $event
     * @return void
     */
    public function handle(InviaConfermaEmail $event)
    {
        event(new InserisciToken($event->user, $event->token, $event->type));
        Mail::to($event->user->email)->send(new ConfermaEmail($event->user, $event->token, $event->type));
    }
}
