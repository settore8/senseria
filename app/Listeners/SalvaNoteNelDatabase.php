<?php

namespace App\Listeners;

use App\Note;
use App\Events\SalvaNote;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SalvaNoteNelDatabase
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SalvaNote  $event
     * @return void
     */
    public function handle(SalvaNote $event)
    {
        Note::updateOrCreate(
            [
                'coverable_id' => $event->model->id,
                'coverable_type' => get_class($event->model)
            ],
            [
                'testo' => $event->text,
                'coverable_id' => $event->model->id,
                'coverable_type' => get_class($event->model)
            ]
        );
        
    }
}
