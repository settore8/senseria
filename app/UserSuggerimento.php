<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSuggerimento extends Model
{
    public $table = 'user_suggerimento';

    public $timestamps = false;

    protected $fillable = ['user_id', 'suggerimento_id', 'visible'];

}
