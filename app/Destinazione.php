<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Destinazione extends Model
{
    public $table = 'destinazioni';

    public $timestamps = false;

    protected $fillable = ['nome', 'nome_pubblico'];

    public function specifiche()
    {
        return $this->hasMany('App\Specifica', 'destinazione_id');
    }

    public function locali()
    {
        return $this->belongsToMany('App\Locale', 'locale_destinazione', 'destinazione_id', 'locale_id');
    }
    
    public function pivot_locali(){
        return $this->hasMany('App\LocaleDestinazione', 'destinazione_id');
    }

    public function consistenze()
    {
        return $this->hasMany('App\Consistenza', 'destinazione_id');
    }
}
