<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InfoVendita extends Model
{
    protected $table = 'immobile_vendita';

    public $timestamps = false;

    protected $fillable = ['user_id', 'immobile_id', 'percentuale_mediazione', 'informazioni'];

    //relazioni
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function immobile()
    {
        return $this->belongsTo('App\Immobile', 'immobile_id');
    }

    //mutator
    public function getInfoAttribute(){
        return !is_null($this->informazioni) ? unserialize($this->informazioni) : [];
    }

    public function getProprietarioAttribute(){
        return array_key_exists("proprietario", $this->info) ? $this->info['proprietario'] : null;  
    }

    public function getInterventoAttribute(){
        return array_key_exists("intervento", $this->info) ? $this->info['intervento'] : null;  
    }

    public function getPrezzoVenditaAttribute(){
        return array_key_exists("prezzo_vendita", $this->info) ? $this->info['prezzo_vendita'] : null;
    }

    public function getPrezzoLocazioneAttribute(){
        return array_key_exists("prezzo_locazione", $this->info) ? $this->info['prezzo_locazione'] : null;
    }

    public function getNoteAttribute(){
        return array_key_exists("note", $this->info) ? $this->info['note'] : null;
    }
}
