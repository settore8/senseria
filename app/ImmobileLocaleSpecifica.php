<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImmobileLocaleSpecifica extends Model
{
    public $table = 'immobile_locali_specifiche';

    public $timestamps = false;

    protected $fillable = ['immobile_id', 'locale_specifica_id', 'quantita', 'attivo'];

    public function immobile()
    {
        return $this->belongsTo('App\Immobile', 'immobile_id');
    }

    public function specifica()
    {
        return $this->belongsTo('App\LocaleSpecifica', 'locale_specifica_id');
    }
}
