<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoNotifica extends Model
{
    protected $table = 'tipologia_notifica';

    public $timestamps = false;

    protected $fillable = ['nome', 'descrizione', 'messaggio'];
}
