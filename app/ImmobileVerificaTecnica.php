<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImmobileVerificaTecnica extends Model
{
    public $table = 'immobile_verifica_tecnica';
    
    
    public $timestamps = false;

    protected $fillable = ['immobile_id', 'attivo', 'datiurbanistici', 'catasto', 'provenienza', 'agibilita', 'impianti', 'caldaia', 'ape', 'cdu', 'postuma'];

    protected $casts = [
        'datiurbanistici' => 'array',
        'catasto' => 'array',
        'provenienza' => 'array',
        'agibilita' => 'array',
            'impianti' => 'array',
            'caldaia' => 'array',
            'ape' => 'array',
            'cdu' => 'array',
            'postuma' => 'array'
    ];

    public function immobile()
    {
        return $this->belongsTo('App\Immobile', 'immobile_id');
    }     
}
