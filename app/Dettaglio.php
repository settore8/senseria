<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dettaglio extends Model
{
    #TO_REVIEW#
    #Problabilmente model e tabella da cancellare#
    #Cambiare la logica della tabella con tabella dove i moduli sono assegnati direttamente all'immobile
    protected $table = 'dettagli';

    public $timestamps = false;

    protected $fillable = ['annuncio_id', 'coverable_id', 'coverable_type'];

    public function coverable()
    {
        return $this->morphTo();
    }

    public function annuncio()
    {
        return $this->belongsTo('App\Annuncio', 'annuncio_id');
    }

    public function candidature()
    {
        return $this->hasMany('App\Candidatura', 'dettaglio_id');
    }
}
