<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    protected $table = 'subscriptions';

    public function user(){
        $this->belongsTo('App\User', 'user_id');
    }

    protected $fillable = [
        'subscription', 'card_id', 'invoice_id', 'card_holder', 'card_brand', 'last_four',
        'exp_month', 'exp_year', 'plan', 'price', 'end_at', 'fatturazione', 'status', 'auto_renew'
    ];

    public function getEndAtAttribute($value){
        $value= Carbon::parse($value);
        return $value->format('d/m/y');
    }

    protected $casts = [
        'fatturazione' => 'array',
    ];
}
