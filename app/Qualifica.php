<?php

namespace App;

use App\Scopes\OrdinaByOrdine;
use Illuminate\Database\Eloquent\Model;

class Qualifica extends Model
{
    public $table = 'qualifiche';

    public $timestamps = false;

    protected $fillable = [
        'albo_id', 'nome', 'nome_pubblico', 'slug', 'ordine'
    ];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new OrdinaByOrdine);
    }

    public function albo()
    {
        return $this->belongsTo('App\Albo', 'albo_id');
    }
}
