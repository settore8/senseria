<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Collegio extends Model
{
    public $table = 'collegi';

    public $timestamps = false;

    protected $fillable = ['nome', 'nome_pubblico'];

    public function albo()
    {
        return $this->belongsToMany('App\Albo', 'albo_collegio', 'collegio_id', 'albo_id')->first();
    }
}
