<?php

namespace App\Jobs\StripeWebhooks;

use App\Payment;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ChargeSucceededJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $webhookCall;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(WebhookCall $webhookCall)
    {
        $this->webhookCall = $webhookCall;
    }

    public function handle()
    {
        $charge = $this->webhookCall->payload['data']['object'];

        $user = User::where('stripe_id', $charge['customer'])->first();
        if($user) {
            Payment::create([
                'user_id' => $user->id,
                'stripe_id' => $charge[''],
                'subtotal' => $charge['amount'],
                'total' => $charge['amount']
            ]);
        }
        // do your work here

        // you can access the payload of the webhook call with `$this->webhookCall->payload`
    }
}
