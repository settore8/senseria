<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImmobileSopralluogo extends Model
{
    public $table = 'immobile_sopralluogo';

    public $timestamps = false;

    protected $fillable = ['immobile_id', 'data', 'attivo', 'contesto', 'fabbricato', 'unitaimmobiliare', 'particondominiali'];
    protected $dates = ['data'];
    protected $casts = [
        'particondominiali' => 'array',
        'unitaimmobiliare' => 'array',
        'fabbricato' => 'array',
        'contesto' => 'array',
    ];

    public function immobile()
    {
        return $this->belongsTo('App\Immobile', 'immobile_id');
    }
}
