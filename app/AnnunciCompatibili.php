<?php

namespace App;

use App\Scopes\FilterByAttivo;
use Illuminate\Database\Eloquent\Model;

class AnnunciCompatibili extends Model
{
    protected $table = 'annunci_compatibili';

    public $timestamps = false;

    protected $fillable = ['ricevente_id', 'coverable_id', 'coverable_type', 'attivo'];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new FilterByAttivo);
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'ricevente_id');
    }

    public function coverable()
    {
        return $this->morphTo();
    }
}
