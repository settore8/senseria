<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImmobileCapitolato extends Model
{
    public $table = 'immobile_capitolato';
    
    public $timestamps = false;

    protected $fillable = ['immobile_id', 'attivo'];

    public function immobile()
    {
        return $this->belongsTo('App\Immobile', 'immobile_id');
    }
}
