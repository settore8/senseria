<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Conversazione extends Model
{
    protected $table = 'conversazioni';

    public $timestamps = false;

    protected $fillable = ['creatore_id', 'codice','coverable_id', 'coverable_type', 'attivo'];


    public function getRouteKeyName() {
        return 'codice';
    }
    
    public function coverable()
    {
        return $this->morphTo();
    }

    public function users()
    {
        return $this->belongsToMany('App\User', 'conversazione_user', 'conversazione_id', 'user_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'creatore_id');
    }

    public function messaggi()
    {
        return $this->hasMany('App\Messaggio', 'conversazione_id');
    }

    // scope
    public function scopeAttive($query)
    {
        $query->where('attivo', 1);
    }

}
