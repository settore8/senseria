<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Locale extends Model
{
    public $table = 'locali';

    public $timestamps = false;

    protected $fillable = ['nome'];

    public function destinazioni()
    {
        return $this->belongsToMany('App\Destinazione', 'locale_destinazione', 'locale_id', 'destinazione_id');
    }
}
