<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use CyrildeWit\EloquentViewable\InteractsWithViews;
use CyrildeWit\EloquentViewable\Contracts\Viewable;

class Galleria extends Model implements Viewable
{

    use InteractsWithViews;

    protected $table = 'gallerie';

    public $timestamps = false;

    protected $fillable = ['user_id', 'immobile_id', 'titolo', 'crediti', 'slug', 'token', 'pubblicata', 'codice'];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function immagini()
    {
        return $this->belongsToMany('App\Immagine', 'galleria_immagine', 'galleria_id', 'immagine_id');
    }

    public function associati()
    {
        return $this->belongsToMany('App\User', 'user_galleria', 'galleria_id', 'user_id');
    }

    public function partecipazioni(){
        return $this->morphMany('App\Partecipazione', 'modelable');
    }

    public function like()
    {
        return $this->morphMany('App\Like', 'coverable');
    }

    /** SCOPE **/
    public function scopePubblicate($query)
    {
        return $query->where('pubblicata', 1);
    }

    /** MUTATOR **/
    public function getImmagineEvidenzaAttribute(){
        return $this->immagini()->first();
    }

    

}
