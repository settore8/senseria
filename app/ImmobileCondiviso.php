<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImmobileCondiviso extends Model
{
    protected $table = 'immobili_condivisi';

    public $timestamps = false;
    protected $casts = [
        'options' => 'array',
        'email' => 'array',
    ];
    public $fillable = ['immobile_id', 'token', 'scadenza', 'options', 'email'];

    public function immobile()
    {
        return $this->belongsTo('App\Immobile', 'immobile_id');
    }

    public function moduli()
    {
        return $this->belongsToMany('App\Modulo', 'immobile_condiviso_moduli', 'immobile_condiviso_id', 'modulo_id');
    }
}
