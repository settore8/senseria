<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    protected $table = 'note';

    public $timestamps = false;

    protected $fillable = ['testo', 'coverable_id', 'coverable_type'];
    public function coverable()
    {
        return $this->morphTo();
    }
}
