<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prestazione extends Model
{
    public $table = 'prestazioni';

    public $timestamps = false;

    protected $fillable = ['categoria_id', 'nome', 'nome_pubblico', 'ordine', 'visibile'];

    public function categoria()
    {
        return $this->belongsTo('App\Categoria', 'categoria_id');
    }
}
