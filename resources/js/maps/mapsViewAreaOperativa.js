var map;
var newLat, newLng;
var polygon;
var area = document.getElementById("area");
if (area != null) {
    var coords = area.value;
}


(function($) {

    if ($("#map").length > 0) {

        initMap();

    }

})(jQuery)

function initMap() {


    var fillcolor = '#ffed00';
    var pathcolor = '#ffed00'; // #2f4cd7

    map = new google.maps.Map(document.getElementById('map'), {
        center: { lat: 41.8523718, lng: 12.234799 },
        zoom: 8,
        minZoom: 6,
        maxZoom: 12,
        scrollwheel: false,
        streetViewControl: false,
        mapTypeControl: false,
        styles: [{
            "featureType": "administrative",
            "elementType": "all",
            "stylers": [{
                "saturation": "-100"
            }]
        }, {
            "featureType": "landscape",
            "elementType": "all",
            "stylers": [{
                    "saturation": -100
                },
                {
                    "lightness": 65
                },
                {
                    "visibility": "on"
                }
            ]
        }, { "featureType": "administrative.country", "stylers": [{ "visibility": "off" }] }, { "featureType": "administrative.land_parcel", "stylers": [{ "visibility": "off" }] }, { "featureType": "administrative.locality", "stylers": [{ "visibility": "on" }] }, { "featureType": "administrative.neighborhood", "stylers": [{ "visibility": "off" }] }, { "featureType": "administrative.province", "stylers": [{ "visibility": "on" }] }, { "featureType": "administrative.province", "elementType": "geometry.fill", "stylers": [{ "visibility": "simplified" }] }, { "featureType": "poi", "elementType": "labels.text", "stylers": [{ "visibility": "off" }] }, { "featureType": "poi.attraction", "stylers": [{ "visibility": "off" }] }, { "featureType": "poi.business", "stylers": [{ "visibility": "off" }] }, { "featureType": "poi.government", "stylers": [{ "visibility": "off" }] }, { "featureType": "poi.medical", "stylers": [{ "visibility": "off" }] }, { "featureType": "poi.park", "stylers": [{ "visibility": "off" }] }, { "featureType": "poi.place_of_worship", "stylers": [{ "visibility": "off" }] }, { "featureType": "poi.school", "stylers": [{ "visibility": "off" }] }, { "featureType": "poi.sports_complex", "stylers": [{ "visibility": "off" }] }, { "featureType": "road", "stylers": [{ "visibility": "off" }] }, { "featureType": "road", "elementType": "labels", "stylers": [{ "visibility": "off" }] }, { "featureType": "road", "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "transit", "stylers": [{ "visibility": "off" }] }, { "featureType": "water", "elementType": "labels.text", "stylers": [{ "visibility": "off" }] }]
    });

    /******* parametri per bloccare la mappa in una certa area ********/
    var allowedBounds = new google.maps.LatLngBounds(
        new google.maps.LatLng(36.339885, 6.959388),
        new google.maps.LatLng(47.314974, 17.879798)
    );

    var boundLimits = {
        maxLat: allowedBounds.getNorthEast().lat(),
        maxLng: allowedBounds.getNorthEast().lng(),
        minLat: allowedBounds.getSouthWest().lat(),
        minLng: allowedBounds.getSouthWest().lng()
    };

    var polyOptions = {
        fillColor: fillcolor,
        fillOpacity: 0.35,
        strokeWeight: 5,
        strokeColor: pathcolor,
        strokeOpacity: 0.7,
        editable: true,
        draggable: true
    };

    if (coords) {
        var myCoords = coords.split(",");
        var res = myCoords[0].replace(" ", ", ");

        var triangleCoords = [];
        myCoords.forEach(function(entry) {
            var x = entry.split(" ");
            triangleCoords.push(new google.maps.LatLng(x[0], x[1]));
        });

        polygon = new google.maps.Polygon({
            paths: triangleCoords,
            draggable: document.getElementById('map').getAttribute('data-draggable') ? true : false, // turn off if it gets annoying
            editable: document.getElementById('map').getAttribute('data-editable') ? true : false,
            strokeColor: fillcolor,
            strokeOpacity: 0.50,
            strokeWeight: 2,
            fillColor: pathcolor,
            fillOpacity: 0.50
        });
        polygon.setMap(map);

        var bounds = new google.maps.LatLngBounds();

        polygon.getPath().forEach(function(path, index) {
            bounds.extend(path);
        });

        map.fitBounds(bounds);

        var lastValidCenter = map.getCenter();

        google.maps.event.addListener(map, 'center_changed', function() {
            center = map.getCenter();
            if (allowedBounds.contains(center)) {
                // still within valid bounds, so save the last valid position
                lastValidCenter = map.getCenter();
                return;
            }
            newLat = lastValidCenter.lat();
            newLng = lastValidCenter.lng();
            if (center.lng() > boundLimits.minLng && center.lng() < boundLimits.maxLng) {
                newLng = center.lng();
            }
            if (center.lat() > boundLimits.minLat && center.lat() < boundLimits.maxLat) {
                newLat = center.lat();
            }
            map.panTo(new google.maps.LatLng(newLat, newLng));

        });

        /**************************************************************/

        //quando sposti uno dei punti originari
        google.maps.event.addListener(polygon.getPath(), "set_at", function() {
            $('#area').val(this.getArray());
        });
        //quando sposti uno dei punti creati successivamente
        google.maps.event.addListener(polygon.getPath(), "insert_at", function() {
            $('#area').val(this.getArray());
        });


        //questo evento viene scaturito quando completi il poligono
        google.maps.event.addListener(polygon, 'draged', function(p) {
            var coordinates = p.getPath().getArray(); //coordinate dell'area
            $('#area').val(coordinates); //setta
        });

        /*************************** ZONA DISEGNO NUOVA AREA ***********************************/


        var drawingManager = new google.maps.drawing.DrawingManager({
            drawingMode: google.maps.drawing.OverlayType.POLYGON,
            drawingControl: false,
            polygonOptions: polyOptions,
            markerOptions: {
                draggable: true
            },
        });

        $('#resetPolygon').click(function() {
            if (polygon) {
                polygon.setMap(null);
            }
            drawingManager.setMap(null);
            $('#area').val('');
            drawingManager.setMap(map);
        });

        google.maps.event.addListener(drawingManager, 'polygoncomplete', function(polygon) {
            polygon.setOptions({
                editable: true,
                fillColor: fillcolor,
                strokeWeight: 5,
                strokeColor: pathcolor,
                clickable: true,
                draggable: true
            });

            drawingManager.setDrawingMode(null);

            google.maps.event.addListener(polygon.getPath(), "set_at", function() {
                if (this.getArray().length >= 3) {
                    $('#area').val(this.getArray());
                }
            });

            var myButton = document.getElementById('resetPolygon'); //bottone rimozione area
            google.maps.event.addDomListener(myButton, 'click', function() {
                polygon.setMap(null); //cancello la selezione
                $('#area').val(""); //azzero il valore della input
                drawingManager.setDrawingMode(google.maps.drawing.OverlayType.POLYGON);
            });

            var coordinates = polygon.getPath().getArray(); //coordinate dell'area
            if (polygon.getPath().getArray().length >= 3) {
                $('#area').val(coordinates); //setta
            }
        })

    } else {
        var drawingManager = new google.maps.drawing.DrawingManager({
            drawingMode: google.maps.drawing.OverlayType.POLYGON,
            drawingControl: false,
            polygonOptions: polyOptions,
            markerOptions: {
                draggable: true
            },
        });

        drawingManager.setMap(map);

        var lastValidCenter = map.getCenter();
        var newLat, newLng;

        google.maps.event.addListener(map, 'center_changed', function() {
            center = map.getCenter();
            if (allowedBounds.contains(center)) {
                // still within valid bounds, so save the last valid position
                lastValidCenter = map.getCenter();
                return;
            }
            newLat = lastValidCenter.lat();
            newLng = lastValidCenter.lng();
            if (center.lng() > boundLimits.minLng && center.lng() < boundLimits.maxLng) {
                newLng = center.lng();
            }
            if (center.lat() > boundLimits.minLat && center.lat() < boundLimits.maxLat) {
                newLat = center.lat();
            }
            map.panTo(new google.maps.LatLng(newLat, newLng));

        });

        google.maps.event.addListener(drawingManager, 'polygoncomplete', function(polygon) {
            polygon.setOptions({
                editable: true,
                fillColor: fillcolor,
                strokeWeight: 5,
                strokeColor: pathcolor,
                clickable: true,
                draggable: true
            });

            drawingManager.setDrawingMode(null);

            //questo viene chiamato quando si sposta un punto del poligono
            google.maps.event.addListener(polygon.getPath(), "set_at", function() {
                if (this.getArray().length >= 3) {
                    $('#area').val(this.getArray());
                }
            });

            var myButton = document.getElementById('resetPolygon'); //bottone rimozione area

            google.maps.event.addDomListener(myButton, 'click', function() {
                polygon.setMap(null); //cancello la selezione
                $('#area').val(""); //azzero il valore della input
                drawingManager.setDrawingMode(google.maps.drawing.OverlayType.POLYGON);
            });

            var coordinates = polygon.getPath().getArray(); //coordinate dell'area
            if (polygon.getPath().getArray().length >= 3) {
                $('#area').val(coordinates); //setta
            }
        });
    }
}