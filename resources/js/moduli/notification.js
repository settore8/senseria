/* ***********************************
**************************************
NOTIFICATION
**************************************
************************************** */
export function createNotification(message, link) {
    var notification = '<div class="notification"><div class="notification__title">Nuova notifica <button class="notification__close">X</button></div><a href="' + link + '" class="notification__body">' + message + '</a></div>';
    var outerHeight = 16;
    var limitnotification = $(window).outerHeight() / 2; // 
    var self = $(this);

    $('.notification').each(function() {
        outerHeight += $(this).outerHeight() + 8;
    });
    if (limitnotification < outerHeight) {
        $('.notification').first().remove();
    }
    $(notification).css('bottom', outerHeight + 'px').appendTo('body');
    repositionNotification();

    setTimeout(function() {
        $('.notification').first().remove();
        repositionNotification();
    }, 20000);

}

function repositionNotification() {
    var outerHeight = 16;
    $('.notification').each(function() {
        $(this).css('bottom', outerHeight + 'px');
        outerHeight += $(this).outerHeight() + 8;
    });
}

$(document).on('click', '.notification__close', function(e) {
    e.preventDefault();
    $(this).closest('.notification').remove();
    repositionNotification();
});


$(document).on('click', '#testnotification', function() {
    createNotification('Prova prova prova');
});

$(document).on('click', '#toggle_notification_area, #close_notification_area', function(e) {
    e.preventDefault();
    $('html').scrollTop(0);
    $('html').toggleClass('notification_area_active');
});