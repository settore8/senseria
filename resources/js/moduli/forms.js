/* ***********************************
**************************************
REPEATABLE ELEMENT NEI FORM
**************************************
************************************** */

export function repeatableElement() {
    var target = '.is-repeatable';

    $('.form-group-repeatable').append('<div class="add">+</div>').find(target).each(function() {
        var target = $(this);
        var originalName = $(this).attr('name');
        var name = originalName.replace('[]', '');
        var array = name + '[]';
        var container = $(this).parent('.form-group-repeatable');

        $(this).attr('name', array);
        $(target).wrap('<div class="repeatableElement"></div>');

    });

    function addRemoveButton() {
        $('.form-group-repeatable').find(target + ':not(:first)').each(function() {
            $(this).next('.remove').remove();
            $(this).after('<div class="remove">-</div>');
        });
    }

    addRemoveButton();

    function removeEmpty() {
        $('.form-group-repeatable').find('.repeatableElement:not(:first)').each(function() {
            var checkValue = $(this).find(target).val();
            if (checkValue === '' || checkValue.length == 0) {
                $(this).fadeOut().remove();
            }
        });
    }

    function removeElement() {
        $('.form-group-repeatable').find('.repeatableElement:not(:first)').each(function() {
            $(this).find('.remove').click(function() {
                $(this).parent('.repeatableElement').fadeOut().remove();
            });
        });
    }

    removeElement();


    $('body').click(function(evt) {
        if ($(evt.target).hasClass('repeatable')) {} else {
            removeEmpty();
        }
    });

    function addButton() {

        $('.form-group-repeatable').each(function() {
            var checkNumber = $(this).find('.repeatableElement');
            console.log(checkNumber);
            if (checkNumber.length >= 1) {
                $(this).find('.add').css('display', '');
            } else {
                $(this).find('.add').css('display', 'none');
            }

        });

    }

    addButton();

    $(target).on('keyup', function() {
        if ($(this).val().length >= 3) {
            //$(this).css('border-color', 'lime');
            $(this).parent('.form-group-repeatable').addClass('dddd');
            $(this).closest('.form-group-repeatable').find('.add').fadeIn();
        } else {
            $(this).css('border-color', 'white');
            $(this).parent('.form-group-repeatable').removeClass('dddd');
            $(this).closest('.form-group-repeatable').find('.add').fadeOut();
        }
    });

    $('.form-group-repeatable').find('.add').click(function(e) {
        e.stopPropagation();
        removeEmpty();
        var last = $(this).parent('.form-group-repeatable').find('.repeatableElement:last');
        $(last).clone().insertAfter(last).addClass('cloned');
        $(this).parent('.form-group-repeatable').find('.repeatableElement:last ' + target).val('').focus();
        addRemoveButton()
        removeElement();
    });

}


/* ***********************************
**************************************
FILTER LIST
**************************************
************************************** */

export function filterList() {

    var input, filter, container, element, div, i, titolo, txtValue;
    input = document.getElementById("filterInput");
    filter = input.value.toUpperCase();
    container = document.getElementById("filterContainer");
    element = container.getElementsByTagName("li");
    //console.log(filter);
    for (i = 0; i < element.length; i++) {
        div = element[i].getElementsByTagName("div")[0];
        titolo = element[i].getElementsByTagName("h3")[0];
        txtValue = div.textContent || div.innerText || titolo.textContent || titolo.innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            element[i].style.display = "";
        } else {
            element[i].style.display = "none";
        }

    }
}