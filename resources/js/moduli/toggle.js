/* ***********************************
**************************************
TOGGLE
**************************************
************************************** */

export function toggle() {
    $(document).on('click', '*[data-toggle]', function(e) {
        e.preventDefault();
        var tag = this.tagName;

        var toggle = $(this).attr('data-toggle');

        if (tag == 'A') {
            var target = $(this).attr('href');
        } else {
            var target = $(this).attr('data-target');
        }
        $(target).toggleClass('show');
    });

    $(document).on('click', '.modal__overlay, .modal__close, .button__close', function(e) {
        e.preventDefault();
        $(this).closest('.emptyOnClose').find('select').each(function() {
            $(this).val(null).trigger('change');
        });
        $(this).closest('.modal').removeClass('show');
    });
}