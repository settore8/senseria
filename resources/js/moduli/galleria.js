/* ***********************************
**************************************
RICERCA
**************************************
************************************** */


export function toggleRicerca() {
    $(document).on('click', '*[data-search]', function(e) {
        e.preventDefault();
        var target = $(this).attr('data-search');
        $(this).toggleClass('opened');
        $('#' + target).toggleClass('show');
    });
}


/* ***********************************
**************************************
GALLERIA
**************************************
************************************** */
/*
export function toggleGalleria() {
    $(document).on('click', '.gallery-item', function(e) {
        e.preventDefault();
        var target = $(this).closest('.gallery');
        $(target).toggleClass('grid');
        var position = $(this).position();
        $('html, body').animate({
            scrollTop: position.top
        }, 400);
    });
}
*/