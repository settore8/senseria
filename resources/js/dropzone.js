import Dropzone from 'dropzone';
import 'dropzone/dist/dropzone.css';
/*
import 'ngdropzone/dist/ng-dropzone.css';
import 'ngdropzone';
*/

window.Dropzone = Dropzone;
window.Dropzone.autoDiscover = false;
Dropzone.prototype.defaultOptions.dictInvalidFileType = "File non supportato";
Dropzone.prototype.defaultOptions.dictFallbackMessage = "Il tuo broswer non supporta l'upload tramite trascinamento";
Dropzone.prototype.defaultOptions.dictFallbackText = "Please use the fallback form below to upload your files like in the olden days.";
Dropzone.prototype.defaultOptions.dictFileTooBig = "File troppo grande ({{filesize}}MiB). Dimensione massima: {{maxFilesize}}MiB.";
Dropzone.prototype.defaultOptions.dictResponseError = "Il server ha risposto con code {{statusCode}}.";
Dropzone.prototype.defaultOptions.dictCancelUpload = "Cancella upload";
Dropzone.prototype.defaultOptions.dictCancelUploadConfirmation = "Se sicuro di voler cancellare questo upload?";
Dropzone.prototype.defaultOptions.dictRemoveFile = "Rimuovi file";
Dropzone.prototype.defaultOptions.dictMaxFilesExceeded = "Limite caricamento file raggiunto";
Dropzone.prototype.defaultOptions.dictDefaultMessage = "Trascina i File qui per caricarlo";


if (document.getElementById('dropzoneallegati')) {
    var dropzoneallegati = new Dropzone("form#dropzoneallegati", {
        acceptedFiles: "",
        maxFilesize: 7,
        uploadMultiple: false,
        minImageWidth: 960,
        init: function() {
            this.on('thumbnail', function(file) {
                if (file.width < this.options.minImageWidth) {
                    file.rejectDimensions();
                } else {
                    file.acceptDimensions();

                }
            })
        },
        accept: function(file, done) {
            file.acceptDimensions = done;
            file.rejectDimensions = function() {
                done("Immagine con risoluzione inferiore a 960px");
            };
        }
    });
}

if (document.getElementById('dropzonegalleria')) {
    var galleria = new Dropzone("form#dropzonegalleria", {
        acceptedFiles: "image/jpeg,image/png,image/jpg",
        maxFilesize: 7,
        uploadMultiple: false,
        minImageWidth: 960,
        init: function() {
            this.on('thumbnail', function(file) {
                if (file.width < this.options.minImageWidth) {
                    file.rejectDimensions();
                } else {
                    file.acceptDimensions();

                }
            })
        },
        accept: function(file, done) {
            file.acceptDimensions = done;
            file.rejectDimensions = function() {
                done("Immagine con risoluzione inferiore a 960px");
            };
        }
    });
}