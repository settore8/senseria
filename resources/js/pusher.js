import { createNotification } from './moduli/notification';

var pusher = new Pusher('d130d2d1a3e48f95d28c', {
    cluster: 'eu',
    forceTLS: true
});

var messaggi_channel = pusher.subscribe('messaggi');
messaggi_channel.bind('inserisci-messaggio', function(data) {
    var classe = 'left';
    if (data['senderid'] == $('#conversazione_content').attr('data-sender')) {
        var classe = 'right';
    }
    var lastMessage = '<div class="messaggio messaggio--' + classe + '"> <div class="messaggio__content"><div class="messaggio__header">' + data['sender'] + '</div> ' + data['messaggio'] + ' <time>' + data['now'] + '</time></div></div>';
    $(lastMessage).insertAfter('.messaggio:last-child');
    $('#conversazione_content').scrollTop($('#conversazione_content')[0].scrollHeight);
});

var users_channel = pusher.subscribe('users');

users_channel.bind('like-profilo', function(data) {
    if($('meta[name=user-token]').attr('content') == $data['destinatario']){
        createNotification(data['messaggio']);
    }
});

var annunci_channel = pusher.subscribe('annunci');

annunci_channel.bind('aggiorna-candidatura-resp-tecnico', function(data) {
    if($('meta[name=user-token]').attr('content') == data['destinatario']){
        createNotification(data['messaggio']);
    }
});

annunci_channel.bind('modulo-aggiunto', function(data) {
    if($('meta[name=user-token]').attr('content') == data['destinatario']){
        createNotification(data['messaggio']);
    }
});

annunci_channel.bind('modulo-rimosso', function(data) {
    if($('meta[name=user-token]').attr('content') == data['destinatario']){
        createNotification(data['messaggio']);
    }
});