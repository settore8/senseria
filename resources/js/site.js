/*
Jquery
*/
import $ from 'jquery';
window.jQuery = $;
window.$ = $;

import { toggle } from './moduli/toggle';
import { toggleRicerca } from './moduli/galleria';


$(document).ready(function() {
    toggleRicerca();
    toggle();
});


function initMenuOpener() {
    var elements = document.getElementsByClassName('togglemenu');
    var navHolder = document.getElementsByTagName('body')[0];

    for (var i = 0; i < elements.length; i++) {
        var element = elements[i];

        element.addEventListener('click', function() {
            var allClasses = navHolder.className.split(' ');
            var checkIfHasClass = allClasses.indexOf('active');

            if (checkIfHasClass >= 0) {
                allClasses.splice(checkIfHasClass, 1);
            } else {
                allClasses.push("active");
            }

            navHolder.className = allClasses.join(" ");
        });

    }
}

initMenuOpener();