/*
Jquery
*/
import $ from 'jquery';
window.jQuery = $;
window.$ = $;


/*
Select2
*/

import 'select2'; // globally assign select2 fn to $ element

$(() => {
    $('.select').select2({
        width: 'resolve',
        minimumResultsForSearch: -1
    });
});



/* ***********************************
MODULI WORKOOK
**************************************
************************************** */

import { toggleRicerca } from './moduli/galleria';
import { repeatableElement, filterList } from './moduli/forms';
import { toggle } from './moduli/toggle';

$(() => {
    toggleRicerca();
    toggle();
    repeatableElement();
});

$("#filterInput").on('keyup', function() {
    filterList()
});

/*
Sweet Alert
*/
import Swal from 'sweetalert2/dist/sweetalert2.js'

/*
Swal.fire({
    title: 'Error!',
    text: 'Do you want to continue',
    icon: 'error',
    confirmButtonText: 'Cool'
  })
*/


/* ***********************************
**************************************
GOOGLE MAPS
**************************************
************************************** */


function createAddressResult(lat, lng, address) {
    var locationInfo = {
        geo: null,
        paese: null,
        regione: null,
        provincia: null,
        comune: null,
        localita: null,
        cap: null,
        indirizzo: null,
        numerocivico: null,
        reset: function() {
            this.geo = null;
            this.paese = null;
            this.regione = null;
            this.provincia = null;
            this.comune = null;
            this.localita = null;
            this.cap = null;
            this.indirizzo = null;
            this.numerocivico = null;
        }
    };

    locationInfo.reset();

    locationInfo.geo = [lat, lng];
    for (var i = 0; i < address.length; i++) {
        var component = address[i].types[0];
        switch (component) {
            case "country":
                locationInfo.paese = address[i]["long_name"];
                break;
            case "administrative_area_level_1":
                locationInfo.regione = address[i]["long_name"];
                break;
            case "administrative_area_level_2":
                locationInfo.provincia = address[i]["short_name"];
                break;
            case "administrative_area_level_3":
                locationInfo.comune = address[i]["long_name"];
                break;
            case "locality":
                locationInfo.localita = address[i]["long_name"];
                break;
            case "postal_code":
                locationInfo.cap = address[i]["long_name"];
                break;
            case "route":
                locationInfo.indirizzo = address[i]["long_name"];
                break;
            case "street_number":
                locationInfo.numerocivico = address[i]["long_name"];
                break;
            default:
                break;
        }
    }
    var result = JSON.stringify(
        locationInfo,
        null,
        4
    );
    document.getElementById("addressResult").value = result;

}

$('#address').focusin(function() {

    var input = document.getElementById('address');

    var options = {
        types: ['geocode'],
        componentRestrictions: { country: 'it' }
    };

    var autocomplete = new google.maps.places.Autocomplete(input, options);

    google.maps.event.addListener(autocomplete, "place_changed", function() {
        var place = autocomplete.getPlace(),
            address = place.address_components,
            lat = place.geometry.location.lat(),
            lng = place.geometry.location.lng();
        createAddressResult(lat, lng, address);
    });

});


var map;

function initMap() {

    var address = document.getElementById("addressResult").value;

    if (!address) {

        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };

                var mapOptions = {
                    zoom: 14,
                    center: pos
                }

                var map = new google.maps.Map(document.getElementById("addressMap"), mapOptions);

                var marker = new google.maps.Marker({
                    position: pos,
                    map: map,
                    draggable: true,
                    title: "Posiziona il marker"
                });


            });
        }

    } else {
        var obj = JSON.parse(address);
        var pos = {
            lat: obj.geo[0],
            lng: obj.geo[1]
        };

        var mapOptions = {
            zoom: 14,
            center: pos
        }

        var map = new google.maps.Map(document.getElementById("addressMap"), mapOptions);

        var marker = new google.maps.Marker({
            position: pos,
            map: map,
            draggable: true,
            title: "Posiziona il marker"
        });

    }

    google.maps.event.addListener(marker, 'dragend', function(evt) {
        codeLatLng(evt.latLng.lat(), evt.latLng.lng());
    });

    // serve per aggiungere un valore alt alle immagini della mappa google
    google.maps.event.addListener(this.map, 'tilesloaded', function() {
        var images = document.querySelectorAll('#map img');
        images.forEach(function(image) {
            image.alt = "Google Maps Image";
        });
    });

}



function codeLatLng(lat, lng) {
    var geocoder = new google.maps.Geocoder();
    var latlng = new google.maps.LatLng(lat, lng);
    geocoder.geocode({
        'latLng': latlng
    }, function(results, status) {
        if (status === google.maps.GeocoderStatus.OK) {
            if (results[1]) {

                createAddressResult(lat, lng, results[1]['address_components']);

            } else {
                alert('No results found');
            }
        } else {
            alert('Geocoder failed due to: ' + status);
        }
    });
}

$('a[data-offcanvas]').click(function() {
    initMap();
});

$('.offcanvas_conferma').click(function(e) {
    e.preventDefault();
    $('#offcanvas_overlay').remove();
    $(this).closest('.offcanvas').removeClass('show');
});

$('.offcanvas_annulla').click(function(e) {
    e.preventDefault();
    $('#offcanvas_overlay').remove();
    $(this).closest('.offcanvas').removeClass('show');
});

$('#address').focusout(function() {
    if (!$('#addressResult').val()) {
        $('#address').val('');
        $('#addressResult').val('');
    }
    if (!$(this).val()) {
        $('#addressResult').val('');
    }
});


/* ***********************************
**************************************
TOGGLE MENU
**************************************
************************************** */

$(document).on("click", "#nav_toggle, #nav_close", function(e) {
    e.preventDefault();
    $("html").toggleClass("nav-open");
});
$(document).on("click", "#offcanvas_overlay", function() {
    $("html").removeClass("nav-open");
});


$('#nav_toggle').on('click', function(e) {
    e.preventDefault();
    var nav = $('#nav');

    if ($('#offcanvas_overlay').length == 0) {
        var overlay = '<div id="offcanvas_overlay"></div>';
        $(overlay).appendTo('body').addClass('show');
    } else {
        $('#offcanvas_overlay').addClass('show');
    }
    if ($('#nav_close').length == 0) {
        nav.prepend('<button id="nav_close">chiudi</button>');
    }
    nav.toggleClass('show');
});

$(document).on('click', '#offcanvas_overlay, #nav_close', function() {
    $('#offcanvas_overlay').removeClass('show').remove();
    $('#nav').toggleClass('show')
    $('#nav_close').remove();
});

$(window).resize(function() {
    var w = window.outerWidth;

    if (w >= 1024) {
        $('#nav').removeClass('show');
        $('#offcanvas_overlay').removeClass('show');
        $('#nav').find('#nav_close').hide();
    } else {
        $('#offcanvas_overlay').addClass('show');
        $('#nav').find('#nav_close').show();
    }

});

/* ***********************************
PREFERENZE UTENTE
**************************************
************************************** */

$(document).on('click touchstart', '#nav_collapse', function(e) {
    e.preventDefault();
    $('body').toggleClass('fullscreen');

    if ($('body').hasClass('fullscreen')) {
        $('#logo svg').attr('viewBox', '0 0 33 33').attr('width', '33');
        var data = {
            fullscreen: true,
        }
        setPreferenzeUser(data);
    } else {
        $('#logo svg').attr('viewBox', '0 0 212 33').attr('width', '212');
        var data = {
            fullscreen: false
        }
        setPreferenzeUser(data);
    }
});

/* TO_REVIEW CORREGGERE BUG QUANDO SI FA IL RESIZE DELLA FINESTRA deve tornare a 212 nella versione mobile */
$(document).ready(function() {
    if ($('body').hasClass('fullscreen')) {
        $('#logo svg').attr('viewBox', '0 0 33 33').attr('width', '33');
    }
});

/* no va 
$(document).on('click touchstart', '*[data-tip]', function(e) {
    e.preventDefault();
    var tip = $(this).attr('data-tip');
    var tips = [];
    if ($('#'+tip).hasClass('show')) {
        tips[tip] = false;
    } else {
        tips[tip] = true;
    }
    var data = {
        tips
    }
    setPreferenzeUser(data);
});
*/

function setPreferenzeUser(data) {
    var self = $(this);
    $.post({
        url: '/ajax/setPreferenzeUser',
        type: 'POST',
        data: data,
        success: function(response) {
            self.html(response);
        },
        error: function(error) {
            console.log(error);
        }
    })
}


/* ***********************************
**************************************
TOGGLE SIDEBAR
**************************************
************************************** */


$(window).resize(function() {
    sidebartoggle();
    sidebarCenterCurrent(false);
});

$(document).ready(function() {
    sidebartoggle();
    sidebarCenterCurrent();
});


$(document).on('click', '#sidebar_toggle', function(e) {
    e.preventDefault();
    $(this).toggleClass('opened');
    $('#sidebar').toggleClass('show');
});


function sidebartoggle() {

    if ($('#sidebar').hasClass('collapsable')) {

        var w = window.outerWidth;
        var sidebartoggle = '<button id="sidebar_toggle" class="col"><span></span><span></span><span></span></button>';

        if (w < 992) {
            if ($('#sidebar_toggle').length == 0 && $('#sidebar').length == 1) {

                if ($('#sidebar.show').length == 1) {
                    $(sidebartoggle).wrap('li').appendTo('#headermenu ul').addClass('opened');
                } else {
                    $(sidebartoggle).wrap('li').appendTo('#headermenu ul');
                }
                $('#sidebar').insertBefore('#maincontent');
            }
        } else {
            $('#sidebar_toggle').remove();
            $('#sidebar').insertBefore('#maincontent');
        }

    }
}

function sidebarCenterCurrent(animate = true) {
    $('#sidebar.scrollable').find('.current').each(function() {
        var current = $(this);
        var currentW = current.outerWidth()
        var position = current.position();
        var container = current.closest('#sidebar.scrollable');
        var w = container.outerWidth();
        var calc = w / 2 - currentW / 2;

        if (animate) {
            container.animate({
                scrollLeft: $(current).offset().left - (calc)
            }, 1000)


        } else {
            setTimeout(function() {
                container.scrollLeft((current).offset().left - (calc)), 100
            });

        }


    });
}

/* ***********************************
**************************************
SUBMIT FORM
**************************************
************************************** */

$('input[type=submit]:not(".nopreloader"), button[type=submit]:not(".nopreloader"), button[type=submit]:not(".requiredJs")').click(function() {
    $(this).attr('disabled', 'disabled');
    $('body').append('<span id="preloader" class="show"></span>');
    $(this).parents('form').submit();
});

/* ***********************************
**************************************
COUNTER FORM
**************************************
************************************** */

$(document).ready(function() {
    $('input[maxlength], textarea[maxlength]').each(function() {
        var max = $(this).attr('maxlength');
        var text = $(this).attr('maxlength');
        if ($(this).hasClass('showCounter')) {
            $(this).after('<div class="max-counter"><span class="count">0</span> di ' + max + '</div>');
        }
    });
});


$(document).on('keyup', 'input[maxlength], textarea[maxlength]', function(e) {
    e.preventDefault();
    var max = $(this).attr('maxlength');
    var counter = this.value.length;
    $(this).next('.max-counter').find('.count').html(counter);
});


/* ***********************************
**************************************
DETECT FORM CHANGED
**************************************
************************************** */

/*
$(document).ready(function() {
    let form = $('form');
    form.data('serialize', form.serialize());
    form.find('input[type=submit], button[type=submit], button').on('click', function() {
        $(window).off('beforeunload');
    });
    $('.canLeavePage').on('submit', function() {
        $(window).off('beforeunload');
    });
});

// On load save form current state
$(window).bind('beforeunload', function(e) {
    let form = $('form');
    if (form.serialize() !== form.data('serialize')) {
        return e.originalEvent.returnValue = 'Attenzione, hai dei dati non salvati';
    } else {
        e = null;
    }
});
*/

/* ***********************************
**************************************
TOGGLE USER
**************************************
************************************** */


$(document).on('click touchstart', '#user_toggle', function(e) {
    e.preventDefault();
    var toggle = $(this);
    var target = '#user_menu';
    $(target).toggleClass('show');
});


$(document).on('click', function(event) {
    var $trigger = $("#user_toggle");
    if ($trigger !== event.target && !$trigger.has(event.target).length) {
        $("#user_menu").removeClass("show");
    }
});


/* ***********************************
**************************************
DROPDOWN
**************************************
************************************** */


$(document).on('click touchstart', '.dropdown-toggle', function(e) {
    e.preventDefault();
    var elem = $(this);
    elem.closest('.dropdown').toggleClass('show');
    elem.toggleClass('opened');
});


$(document).on('click', function(event) {
    var $trigger = $(".dropdown");
    if ($trigger !== event.target && !$trigger.has(event.target).length) {
        $(".dropdown-toggle").removeClass("opened")
        $(".dropdown").removeClass("show");
    }
});


/* ***********************************
**************************************
SUBMENU
**************************************
************************************** */

$(document).on('click touchstart', '.has-submenu a', function(e) {
    e.stopPropagation();
    $(".has-submenu a").removeClass("opened")
    $(".submenu").removeClass("show");
    var elem = $(this);
    elem.next('.submenu').toggleClass('show');
    elem.toggleClass('opened');
});


$(document).on('click', function(event) {
    var $trigger = $(".submenu");
    if ($trigger !== event.target && !$trigger.has(event.target).length) {
        $(".has-submenu a").removeClass("opened")
        $(".submenu").removeClass("show");
    }
});

$(document).ready(function() {
    $('.has-submenu').each(function() {
        var currentText = $(this).find('.submenu .current').text();
        if (!currentText) {
            return;
        }
        $(this).find('.label').text(currentText);
    });
});



/* ***********************************
**************************************
TOGGLE COMPONENTS
**************************************
************************************** */
/*
$(document).on('click', '*[data-toggle]', function(e) {
    alert('test');
    e.preventDefault();
    var tag = this.tagName;
    var toggle = $(this).attr('data-toggle');
    if (toggle == 'modal') {

    }
    if (tag == 'A') {
        var target = $(this).attr('href');
    } else {
        var target = $(this).attr('data-toggle');
    }

    $(target).toggleClass('show');
});

*/


$(document).on('click', '.collapse__close', function(e) {
    e.preventDefault();
    $(this).closest('.collapse').toggleClass('show');
    $(this).closest('.collapse').prev('.tip_button').removeClass('opened');
});

$(document).on('click', '.tip_button', function() {
    $(this).next('.tip').toggleClass();
    $(this).addClass('opened');
});


/* ***********************************
**************************************
BLOCK TABS
**************************************
************************************** */


$(document).on('click touchstart', '*[data-tab]', function(e) {
    e.preventDefault();
    var target = $(this).attr('data-tab');
    var container = $(this).closest('.has-tabs');
    var tabs = container.find('.tabs');
    $(container).find('[data-tab]').each(function() {
        $(this).removeClass('current');
    });
    $(tabs).find('.tab').each(function() {
        $(this).removeClass('show');
    });
    $(this).addClass('current');
    $(target).addClass('show');
});


/* ***********************************
**************************************
SEARCHFORM
**************************************
************************************** */


$(document).on('click touchstart', '#searchform .toggle-searchadvanced', function(e) {
    e.preventDefault();
    e.stopPropagation();
    $(this).closest('#searchform').toggleClass('opened');
    $('#searchadvanced').toggleClass('show');
    $('#searchoverlay').toggleClass('show');
});

$(document).on('click', function(event) {
    var $trigger = $("#searchform");
    if ($trigger !== event.target && !$trigger.has(event.target).length) {
        $trigger.removeClass('opened');
        $('#searchadvanced').removeClass('show');
        $('#searchoverlay').removeClass('show');
    }
});



/* ***********************************
**************************************
TOAST
**************************************
************************************** */


$(document).on('click', '.toast button', function(e) {
    e.preventDefault();
    $(this).closest('.toast').remove();
});


/* ***********************************
**************************************
TOGGLE OFFCANVAS
**************************************
************************************** */


$(document).on('click', '*[data-offcanvas]', function(e) {
    e.preventDefault();
    var target = $(this).attr('data-offcanvas');

    if ($('#offcanvas_overlay').length == 0) {
        var overlay = '<div id="offcanvas_overlay"></div>';
        $(overlay).appendTo('body').addClass('show');
    } else {
        $('#offcanvas_overlay').addClass('show');
    }
    $('#' + target).toggleClass('show');
});

$(document).on('click', '#offcanvas_overlay', function(e) {
    $(this).remove();
    $('.offcanvas').removeClass('show');
});


/* ***********************************
**************************************
ACCORDIONS
**************************************
************************************** */

$(document).on('click', '.accordion-item__title', function(event) {
    event.stopPropagation();

    var accordion = $(this).closest('.accordion');
    var item = $(this).parent('.accordion-item');
    if ($(accordion).hasClass('multiple')) {
        $(item).toggleClass('opened');
    } else {
        $(accordion).find('.accordion-item').removeClass('opened');
        $(item).addClass('opened');
    }

});

/* ***********************************
**************************************
CONVERSAZIONI
**************************************
************************************** */

$(document).ready(function() {
    if ($('.conversazione_content').length != 0) {
        $('.conversazione_content').scrollTop($('.conversazione_content')[0].scrollHeight);
        $('.conversazione_content .overlay').fadeOut();
        $('.conversazione_input textarea').focus();
    }
});



/* ***********************************
**************************************
SPECIALIZZAZIONI
**************************************
************************************** */

/*
function specializzazioniCounter(id) {
    var categoryCounter = $('#prestazioni-' + id + ' .item').find('input[name="prestazioni[]"]:checked ').length;

    // var specializzazioniCounter = $('#prestazioni-' + id + ' li').find('input[name="specializzazioni[]"]:checked ').length;
    if (categoryCounter > 0) {
        var categoryEvidenzaOld = $('input#categoriaEvidenzaHidden').val();

        if (categoryEvidenzaOld == id) {
            var c = 'checked="checked"';
        } else {
            var c = '';
        }

        $('div[data-value="' + id + '"]').find('.specializzazioni-counter').show();
    } else {
        $('div[data-value="' + id + '"]').find('.specializzazioni-counter').hide();
    }
}
*/


function counterCompetenze(counter, item) {
    if (counter > 0) {
        $(item).addClass('checked');
        $(item).find('.accordion-item__title .counter').show();
        $(item).find('.radio-specializzazione').addClass('visible');
        $(item).find('.accordion-item__title .counter .counter-number').html(counter);
    } else {
        $(item).removeClass('checked');
        $(item).find('.accordion-item__title .counter').hide();
        $(item).find('.radio-specializzazione').removeClass('visible');
        $(item).find('.accordion-item__title .counter .counter-number').html(counter);
    }
}



$(document).on('change', 'input[name="prestazioni[]"]', function() {

    var item = $(this).closest('.accordion-item');
    var counter = $(item).find('input[name="prestazioni[]"]:checked ').length;
    counterCompetenze(counter, item);

    /*

    var t = $(this);
    var catId = $(t).data('parent');
    var parent = $('.select-all[data-select=' + catId + ']');

    console.log(catId);

    var li = $(t).closest('li');
    var liId = $(t).closest('li').data('value');

    var checked = $('.sub-item li').find('[data-parent=' + catId + ']:checked').length;
    var total = $('.sub-item li').find('[data-parent=' + catId + ']').length;

    if (checked == total) {
        $(parent).prop('checked', 'checked').parent('.wk-accordion-title').removeClass('has-checked non-checked').addClass('all-checked');
    } else if (checked > 0) {
        $(parent).prop('checked', '').parent('.wk-accordion-title').removeClass('all-checked non-checked').addClass('has-checked');
    } else {
        $(parent).prop('checked', '').parent('.wk-accordion-title').removeClass('all-checked non-checked');
    }

    if ($(t).is(':checked')) {
        specializzazioniCounter(catId);
    } else {
        specializzazioniCounter(catId);
    }


    $("span.categorie-counter-" + catId).text(checked + ' di ' + total);

    */

});




/* ***********************************
**************************************
SELECT ALL NEXT
**************************************
************************************** */

$(document).on('change', '.select-all', function(e) {
    e.preventDefault();

    var item = $(this).closest('.accordion-item__content');
    if ($(this).is(':checked')) {
        $(item).find('input[type="checkbox"]').each(function() {
            $(this).prop('checked', 'checked');
        });
    } else {
        $(item).find('input[type="checkbox"]').each(function() {
            $(this).prop('checked', '');
        });
    }

    var item = $(this).closest('.accordion-item');
    var counter = $(item).find('input[name="prestazioni[]"]:checked').length;
    counterCompetenze(counter, item);


});

$(document).ready(function() {
    var accordion = $('.accordion-item');
    accordion.each(function() {
        var counter = $(this).find('input[name="prestazioni[]"]:checked ').length;
        counterCompetenze(counter, this);
    });
});


$(document).on('change', 'input.radio-specializzazione', function(e) {
    e.stopPropagation();
});


/* ***********************************
**************************************
ADJUST GRID SIZE
**************************************
************************************** */

function adjustGrid() {
    var container = $('.articoli #maincontent').outerWidth();
    var containerW = $(container).outerWidth();
    if (containerW > 1200) {
        $(container).find('article').removeClass('col-4').addClass('col-3');
    } else {
        $(container).find('article').removeClass('col-3').addClass('col-4');
    }
}
$(document).ready(function() {
    adjustGrid();
});

$(window).resize(function() {
    adjustGrid();
});



/* ***********************************
**************************************
HELPERS
**************************************
************************************** */

$(".priceFormat").on('keyup', function() {
    var n = parseInt($(this).val().replace(/\D/g, ''), 10);
    $(this).val(n.toLocaleString());
});

$(".priceFormat").on('focusout', function() {
    var n = $(this).val();
    if (isNaN(n)) {
        $(this).val('');
    }
});



$('.piepercent').each(function() {
    var val = $(this).attr('data-pct');
    var $circle = $(this).find('.svg .bar');

    if (isNaN(val)) {
        val = 100;
    } else {
        var r = $circle.attr('r');
        var c = Math.PI * (r * 2);

        if (val < 0) { val = 0; }
        if (val > 100) { val = 100; }

        var pct = ((100 - val) / 100) * c;

        $circle.css({ strokeDashoffset: pct });

    }
});


$(".fakeradio").change(function() {
    var check = $(this).attr('data-radio');
    var ischeck = $(this).is(':checked');
    $("input[data-radio='" + check + "']").prop('checked', false);
    if (ischeck != true) {
        $(this).prop('checked', false);
    } else {
        $(this).prop('checked', true);
    }
});