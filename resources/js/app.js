import Swal, { resumeTimer } from "sweetalert2";
$(".js-select2").select2();

$(document).on('click', 'input[name*="consistenze"]', function(e) {
    var self = $(this);
    var contenitore = self.closest('.consistenza');
    var checked = self.prop('checked');
    if (checked) {
        contenitore.find('input[name*=valore_consistenza]').removeClass('hidden').removeAttr('disabled');
    } else {
        contenitore.find('input[name*=valore_consistenza]').addClass('hidden').attr('disabled', true);
    }
});

$(document).ready(function() {
    $('input[name*="consistenze"]').each(function() {
        var self = $(this);
        var contenitore = self.closest('.consistenza');
        var checked = self.prop('checked');
        if (checked) {
            contenitore.find('input[name*=valore_consistenza]').removeClass('hidden').removeAttr('disabled');
        } else {
            contenitore.find('input[name*=valore_consistenza]').addClass('hidden').attr('disabled', true);
        }
    });
});


function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

/*
$(document).on('click', '*[data-annuncio]', function(e) {
    var type = $(this).attr('data-conversazione');
    var codice = $(this).attr('data-annuncio');
    var destinatario = $(this).attr('data-destinatario');
    $('#nuovaConversazione').find('#conversazioneType').val(type);
    $('#nuovaConversazione').find('#annuncioCodice').val(codice);
    $('#nuovaConversazione').find('#annuncioCodice').val(codice);
    $('#destinatarioConversazione').text('@' + atob(destinatario));
    $('#oggettoConversazione').text(atob(codice));
});

*/


$(document).on('click', '#condividi-immobile', function(e) {
    e.preventDefault();
    var self = $(this);
    if (!$('input[name=email]').val()) {
        alert('Email mancante');
        return false;
    }
    /*
    if ($('input[name*=moduli]:checked').length == 0) {
        alert('Nessun modulo selezionato.');
        return false;
    }
    */
    var errorValidation = 0;
    var email = $('input[name=email]').val();
    var splitted = Object.values(email.split(';'));

    //rimuovo tutti gli elementi vuoti nel caso uno inserisse un ; ma senza nulla dopo
    var filtered = splitted.filter(function(el) {
        return el != '';
    });

    //controllo che tutte le email siano valide
    filtered.forEach(element => {
        if (!validateEmail(element)) {
            errorValidation = 1;
        }
    });

    if (errorValidation == 1) {
        alert("Controlla che tutte le email inserite sia corrette.");
        return false;
    }

    self.closest('form').submit();
});

$(document).on('click', '*[data-target-disabled]', function(e) {
    var self = $(this);
    $('#' + self.attr('data-target-disabled')).attr('disabled', self.prop('checked'));
})

$(document).on('click', 'button[data-pubblicabile]', function(e) {
    e.preventDefault();
    $('input[name=pubblicabile]').val($(this).attr('data-pubblicabile'));
    $(this).closest('form').submit();
})

function checkLatiAffacciFacciate($this, $nameselect) {
    $('select[name*=' + $nameselect + ']').not($this).each(function() {
        //console.log($($this).val());
        if ($(this).find('option[value="' + $($this).val() + '"]:selected').length > 0) {
            Swal.fire({
                title: 'Lato gia selezionato!',
                icon: 'warning'
            }).then(function() {
                $($this).find('option:first').prop('selected', true);
            });
        };
    });
}

$(document).on('click', "*[data-toggle-clone]", function(e) {
    e.preventDefault();
    var target = $(this).attr('data-target');
    var selected = $(target + ':first').find('select[name*=target]').val();
    if ($(this).attr('data-toggle-clone') == 'clone') {
        $(target + ':first').clone().insertBefore($(target + ':first'));
    }
});

$(document).on('change', 'select[name*=latoaffaccio]', function(e) {
    e.preventDefault();
    return checkLatiAffacciFacciate(this, 'latoaffaccio');
});

$(document).on('click', "#aggiungi-affaccio", function(e) {
    e.preventDefault();
    var max = $(this).attr('data-max-affacci');
    if ($('.row-affacci').length == max) {
        Swal.fire({
            title: 'Numero massimo affacci raggiunto!',
            icon: 'warning'
        });
        return false;
    }
    var selected = $('.row-affacci:first').find('select[name*=latoaffaccio]').val();
    $('.row-affacci:first').clone().insertBefore($('.row-affacci:first'));
});


$(document).on('click', '*[data-clone]', function(e) {
    e.preventDefault();
    var self = $(this);
    var data_clone = self.attr('data-clone');
    var data_message_clone = (typeof $('*[data-target-clone="' + data_clone + '"]:first').attr('data-message-clone') == 'undefined') ? 'Numero massimo raggiunto!' : $('*[data-target-clone="' + data_clone + '"]:first').attr('data-message-clone');
    if ($('*[data-target-clone="' + data_clone + '"]').length == $('*[data-target-clone="' + data_clone + '"]:first').attr('data-max-clone')) {
        Swal.fire({
            title: data_message_clone,
            icon: 'warning'
        });
        return false;
    }
    $('*[data-target-clone="' + data_clone + '"]:first').clone().insertBefore($('*[data-target-clone="' + data_clone + '"]:first'));
    $('*[data-target-clone="' + data_clone + '"]:first > select').find('option[value=""]').prop('selected', true);
    hideRemoveCloneButton();
});

$(document).on('click', '*[data-remove-clone]', function(e) {
    e.preventDefault();
    var self = $(this);
    var target = self.attr('data-remove-clone');
    var container = self.closest('.data-clone-container');
    var count = container.find('*[data-target-clone]').length;
    if (count > 1) {
        self.closest('[data-target-clone]').remove();
    } else {
        alert('non è possibile cancellare');
    }
});

$(document).ready(function() {
    hideRemoveCloneButton()
});

function hideRemoveCloneButton() {
    var count = $('[data-target-clone]').length;
    if (count > 1) {
        $('[data-target-clone]').find('[data-remove-clone]').show();
    } else {
        $('[data-target-clone]').find('[data-remove-clone]').hide();
    }
}

$(document).on('change', 'select[name*=lato_facciate]', function(e) {
    e.preventDefault();
    return checkLatiAffacciFacciate(this, 'lato_facciate');
});

$(document).on('click', '.requiredJs', function(e) {
    e.preventDefault();
    var self = $(this);
    var form = self.closest('form');

    var emptyFields = form.find('*[required="1"], *[required="true"]').filter(function() {
        return ($(this).val() === "" || $(this).val().length == 0);
    }).length;
    if (emptyFields > 0) {
        alert('Completa tutti i campi obbligatori');
        return false;
    }

    form.submit();
});

$(document).ready(function() {
    $('.selectChange').each(function() {
        var valore = $(this).val();
        $('.selectChangeTarget').hide();
        $('#' + valore).show();
    });
});

$(document).on('change', '.selectChange', function() {
    var valore = $(this).val();
    $('.selectChangeTarget').hide();
    $('#' + valore).show();
});


// forse non serve più
$(document).ready(function() {

    $('.conditionalNext').each(function() {
        $(this).on('change', function() {
            var v = $(this).val();
            var next = $(this).attr('data-next');
            if (v == 'true') {
                $(this).attr('readonly', true);
                $('#' + next).removeClass('hidden').addClass('required').find('select').attr('required', true);

            } else {
                $(this).attr('readonly', false);
                $('#' + next).addClass('hidden').removeClass('required').find('select').attr('required', false);
            }
        });


    });
});

$(document).on('click', '#selfAssignedRespTecnico', function(e) {
    e.preventDefault();
    $('input[name=compatibile]').attr('disabled', true);
    $('input[name=sestesso]').removeAttr('disabled');
    $(this).closest('form').submit();
}).on('click', '#pubblicaAnnuncioRespTecnico', function(e) {
    e.preventDefault();
    $('input[name=compatibile]').removeAttr('disabled');
    $('input[name=sestesso]').attr('disabled', true);
    $(this).closest('form').submit();
}).on('click', '#scegliRespTecnico', function(e) {
    e.preventDefault();
    $('input[name=compatibile]').attr('disabled', true);
    $('input[name=sestesso]').attr('disabled', true);
    if (!$('select[name=internoaworkook]').val()) {
        alert('Seleziona un professionista!');
        return false;
    }
    $(this).closest('form').submit();
});


// serve nella verifica tecnica
$(document).ready(function() {
    $('.conditionalToggles').each(function() {

        $(this).find('.check_necessario').on('change', function() {
            if (this.checked) {
                $(this).closest('.conditionalToggles').find('.check_disponibile').prop('checked', false);
                $(this).closest('.conditionalToggles').find('.check_conforme').prop('checked', false);
            } else {
                $(this).closest('.conditionalToggles').find('.check_disponibile').prop('checked', false);
                $(this).closest('.conditionalToggles').find('.check_conforme').prop('checked', false);
            }
        });

        $(this).find('.check_disponibile').on('change', function() {
            if (this.checked) {
                $(this).closest('.conditionalToggles').find('.check_necessario').prop('checked', true);
                $(this).closest('.conditionalToggles').find('.check_conforme').prop('checked', false);
            } else {
                $(this).closest('.conditionalToggles').find('.check_necessario').val('checked', true);
                $(this).closest('.conditionalToggles').find('.check_conforme').prop('checked', false);
            }
        });

        $(this).find('.check_conforme').on('change', function() {
            if (this.checked) {
                $(this).closest('.conditionalToggles').find('.check_necessario').prop('checked', true);
                $(this).closest('.conditionalToggles').find('.check_disponibile').prop('checked', true);
            }
        });

    });
});


$('form:not(.sendOnEnter)').on('keyup keypress', function(e) {
    var keyCode = e.keyCode || e.which;
    if (keyCode === 13) {
        e.preventDefault();
        return false;
    }
});


$(document).on('change', '.collapse_panel', function() {
    $(this).closest('.panel').toggleClass('closed');
});


$(function() {
    $(".collapse_panel input:checked").change(function() {
        if (!this.checked) {
            $(this).closest('.panel').find('input[type="text"], input[type="number"]').each(function() {
                $(this).val('');
            });
            $(this).closest('.panel').find('input[type="checkbox"]').each(function() {
                $(this).prop("checked", false);
            });
        }
    });
});