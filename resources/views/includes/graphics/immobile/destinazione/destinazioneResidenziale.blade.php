<svg version="1.1" id="Livello_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                            viewBox="0 0 480 480" style="enable-background:new 0 0 480 480;" xml:space="preserve">
                        <style type="text/css">
                            .st0{fill:#484848;}
                            .st1{fill:#B1DDE1;}
                        </style>
                        <path class="st0" d="M28,444c-1.9,0-3.5-1.6-3.5-3.5s1.6-3.5,3.5-3.5h32.6V257.1c0-1.2,0.6-2.3,1.6-2.9l156.3-100v-29l-200,133.1
                            c-0.6,0.4-1.2,0.6-1.9,0.6c-1.2,0-2.3-0.6-2.9-1.6c-0.5-0.8-0.7-1.7-0.5-2.6c0.2-0.9,0.7-1.7,1.5-2.2l201.8-134.3l0.2-0.1l1.9-1.2
                            V94c0-1.3,0.7-2.4,1.8-3.1c0.5-0.3,1.1-0.4,1.7-0.4c0.6,0,1.3,0.2,1.8,0.5l193.1,118.4c1,0.6,1.7,1.8,1.7,3V437H460
                            c1.9,0,3.5,1.6,3.5,3.5s-1.6,3.5-3.5,3.5H28z M362.3,353.7c1.9,0,3.5,1.6,3.5,3.5V437h45.8V214.4L225.5,100.3v236.7
                            c0,1.9-1.6,3.5-3.5,3.5h-20V437h69.3v-79.8c0-1.9,1.6-3.5,3.5-3.5H362.3z M110,437h85v-96.6h-85V437z M67.6,259v178H103v-96.6H92.6
                            c-1.9,0-3.5-1.6-3.5-3.5s1.6-3.5,3.5-3.5h125.9v-171L67.6,259z M322.1,436h36.8v-75.2h-36.8V436z M278.3,436h36.8v-75.2h-36.8V436z"
                            />
                        <path class="st0" d="M462.6,214.1c-0.7,0-1.3-0.2-1.9-0.6l-240.6-154c-1.6-1-2.1-3.2-1.1-4.8c0.6-1,1.8-1.6,3-1.6
                            c0.7,0,1.3,0.2,1.9,0.6l240.6,154c1.6,1,2.1,3.2,1.1,4.8C464.9,213.5,463.8,214.1,462.6,214.1z"/>
                        <path class="st0" d="M258.5,308.2c-1.9,0-3.5-1.6-3.5-3.5v-43.6c0-1.9,1.6-3.5,3.5-3.5h43.8c1.9,0,3.5,1.6,3.5,3.5v43.6
                            c0,1.9-1.6,3.5-3.5,3.5H258.5z M262,301.2h36.8v-36.6H262V301.2z"/>
                        <path class="st0" d="M124,308.2c-1.9,0-3.5-1.6-3.5-3.5v-43.6c0-1.9,1.6-3.5,3.5-3.5h43.8c1.9,0,3.5,1.6,3.5,3.5v43.6
                            c0,1.9-1.6,3.5-3.5,3.5H124z M127.5,301.2h36.8v-36.6h-36.8V301.2z"/>
                        <path class="st0" d="M334.5,308.2c-1.9,0-3.5-1.6-3.5-3.5v-43.6c0-1.9,1.6-3.5,3.5-3.5h43.8c1.9,0,3.5,1.6,3.5,3.5v43.6
                            c0,1.9-1.6,3.5-3.5,3.5H334.5z M338,301.2h36.8v-36.6H338V301.2z"/>
                        <rect x="32.3" y="26.6" class="st1" width="6.8" height="6.8"/>
                        <rect x="54.9" y="26.6" class="st1" width="6.8" height="6.8"/>
                        <rect x="77.5" y="26.6" class="st1" width="6.8" height="6.8"/>
                        <rect x="100" y="26.6" class="st1" width="6.8" height="6.8"/>
                        <rect x="122.6" y="26.6" class="st1" width="6.8" height="6.8"/>
                        <rect x="145.2" y="26.6" class="st1" width="6.8" height="6.8"/>
                        <rect x="167.8" y="26.6" class="st1" width="6.8" height="6.8"/>
                        <rect x="190.4" y="26.6" class="st1" width="6.8" height="6.8"/>
                        <rect x="213" y="26.6" class="st1" width="6.8" height="6.8"/>
                        <rect x="235.5" y="26.6" class="st1" width="6.8" height="6.8"/>
                        <rect x="258.1" y="26.6" class="st1" width="6.8" height="6.8"/>
                        <rect x="51.5" y="45.8" class="st1" width="6.8" height="6.8"/>
                        <rect x="74" y="45.8" class="st1" width="6.8" height="6.8"/>
                        <rect x="96.6" y="45.8" class="st1" width="6.8" height="6.8"/>
                        <rect x="119.2" y="45.8" class="st1" width="6.8" height="6.8"/>
                        <rect x="141.8" y="45.8" class="st1" width="6.8" height="6.8"/>
                        <rect x="164.4" y="45.8" class="st1" width="6.8" height="6.8"/>
                        <rect x="187" y="45.8" class="st1" width="6.8" height="6.8"/>
                        <rect x="254.7" y="45.8" class="st1" width="6.8" height="6.8"/>
                        <rect x="277.3" y="45.8" class="st1" width="6.8" height="6.8"/>
                        <rect x="70.6" y="65" class="st1" width="6.8" height="6.8"/>
                        <rect x="93.2" y="65" class="st1" width="6.8" height="6.8"/>
                        <rect x="115.8" y="65" class="st1" width="6.8" height="6.8"/>
                        <rect x="138.4" y="65" class="st1" width="6.8" height="6.8"/>
                        <rect x="161" y="65" class="st1" width="6.8" height="6.8"/>
                        <rect x="183.6" y="65" class="st1" width="6.8" height="6.8"/>
                        <rect x="273.9" y="65" class="st1" width="6.8" height="6.8"/>
                        <rect x="296.5" y="65" class="st1" width="6.8" height="6.8"/>
                        </svg>