<meta charset="utf-8">
<meta name="viewport" content="user-scalable=yes, width=device-width, initial-scale=1">
<meta name="google" content="notranslate">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>@yield('title') {{ config('app.name') }}</title>
<meta property="og:locale" content="it_IT" />
<meta property="og:site_name" content="{{ config('app.name') }}" />
<meta property="og:url" content="{{ url()->current() }}" />
<meta property="og:type" content="@yield('og_type')" />
<meta property="og:title" content="@yield('og_title')" />
<meta property="og:description" content="@yield('og_description')" />
<meta property="og:image" content="@yield('og_image', '/images/ogimages/og_image_default.png')" />
@yield('meta')
@yield('ogtag')
<link rel="apple-touch-icon" sizes="180x180" href="/favicons/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/favicons/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicons/favicon-16x16.png">
<link rel="manifest" href="/favicons/site.webmanifest">
<link rel="mask-icon" href="/favicons/safari-pinned-tab.svg" color="#2f4cd7">
<meta name="msapplication-TileColor" content="#2f4cd7">
<meta name="theme-color" content="#2f4cd7">
<link href="{{ url()->current() }}" rel="canonical">