<form action="{{ route('annunci.search') }}" method="get" class="serchImmobili"> 

    Contratto
    <select name="contratto">
        <option value="vendita">Vendita</option>
        <option value="affitto">Affitto</option>
    </select>

    Comune
    <input type="text" name="comune"  value="@isset($request->comune){{ $request->comune }}@endisset">

    Categoria
    <select></select>

    Tipologia
    <select></select>

    Metri quadri minimi
    <input type="text" name="mq_min" value="@isset($request->mq_min){{ $request->mq_min }}@endisset">

    Metri quadri max
    <input type="text" name="mq_max" value="@isset($request->mq_max){{ $request->mq_max }}@endisset">

    Camere
    <input type="text" name="camere" value="@isset($request->camere){{ $request->camere }}@endisset">

    Bagni
    <input type="text" name="bagni" value="@isset($request->bagni){{ $request->bagni }}@endisset">

    Workook Score
    <select></select>

    <button class="btn btn-lg btn-primary">Cerca immobile</button>
</form>