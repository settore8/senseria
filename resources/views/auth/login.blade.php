@extends('layouts.auth')

@section('title') Login @endsection
@section('bodyclass')login @endsection

@section('titolo')
<h1 class="title">Login</h1>
@endsection

@section('content')

<form action="{!! route('auth.login') !!}" method="POST">
    @csrf

    <div class="form-group py5">
            <label>Email</label>
            <input type="email" name="email" class="form-input input-xl" value="{!! old('email') !!}">
    </div>

    <div class="form-group py5">
       <label>Password</label>
            <input type="password" class="form-input input-xl" name="password">
        </label>
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-primary btn-padding">Entra</button>
    </div>
    
    <small class="d-block mt10">Non sei registrato? <a href="{{ route('registrazione') }}"  title="Registrati a Workook" class="text-primary">Registrati adesso</a></small>
    <small class="d-block">Non ricordi la password? <a href="{!! route('auth.password.request.email') !!}"  title="Recupera la password" class="text-primary">Recuperala</a></small><br/>

</form>



@endsection
