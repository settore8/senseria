@extends('layouts.auth')

@section('title')Recupero password @endsection
@section('bodyclass') reset @endsection


@section('titolo')
<h1 class="title">Recupero password</h1>
@endsection

@section('content')

<form action="{!! route('auth.password.send.link') !!}" method="POST">
    @csrf
    <p>Inserisci il tuo indirizzo email per ricevere <br/>le istruzioni per il recupero della password.</p>
    <div class="form-group">
            <label class="form-label">Email</label>
            <input type="email" name="email" class="form-input input-xl" value="{!! old('email') !!}">
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-primary btn-padding">Recupera password</button>
    </div>

    <small class="d-block mt10">Vuoi riprovare? <a href="{{ route('login') }}"  title="Accedi a Workook" class="text-primary">Accedi</a></small>

</form>

@endsection
