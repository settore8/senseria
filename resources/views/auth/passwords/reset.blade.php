@extends('layouts.auth')

@section('title')Reset password @endsection
@section('bodyclass') @endsection

@section('content')

@if($message = Session::get('registrazione'))
    <div>
        <img src="{{ asset('images/visual-reset-password.svg') }}">
        <h1 class="title">Email inviata!</h1>
        <p class="subtitle">Ti abbiamo inviato una email con le istruzioni per il ripristino della tua password</p>
    </div>
@else

<h1 class="title">Inserisci la tua email</h1>
@if ($message = Session::get('msg-error'))
    {{ $message }}
@endif


<form action="{!! route('auth.password.store.reset') !!}" method="POST">
    @csrf
    <input type="hidden" name="token" value="{{ $token }}">
    <input type="hidden" name="user" value="{{ $user }}">
    <input type="hidden" name="type" value="{{ $type }}">

    <div class="form-group">
            <label>Email</label>
            <input type="email" name="email" class="form-input input-xl" value="{{ $email }}">
    </div>
    <div class="form-group">
            <label>Nuova password</label>
            <input type="password" name="password" class="form-input input-xl">
    </div>
    <div class="form-group">
            <label>Conferma password</label>
            <input type="password" name="password_confirmation" class="form-input input-xl">
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-primary btn-padding">Conferma</button>
    </div>
</form>

@endif



@endsection
