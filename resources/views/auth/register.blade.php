@extends('layouts.auth')

@section('title') Registrazione @endsection
@section('bodyclass')register {{ ($message = Session::get('registrazione') || $message = Session::get('emailconfermata')) ? 'success' : '' }} @endsection



@section('titolo')
@if($message = Session::get('registrazione'))
<div class="myauto">
    <img src="{{ asset('images/visual-registrazione-success.svg') }}" alt="Email inviata" class="max-width-200">
    <h1 class="title">Grazie!</h1>
    <p class="subtitle">Ti abbiamo inviato una email <br/>per confermare il tuo indirizzo email</p>
</div>

@elseif($message = Session::get('emailconfermata'))
<div class="myauto">
    <img src="{{ asset('images/visual-registrazione-success.svg') }}" alt="Email confermata" class="max-width-200">
    <h1 class="title">Email Confermata!</h1>
    <p class="subtitle">Ti aggiorneremo su quando potrai iniziare ad usare Workook!</p>
</div>
@else
<h1 class="title">Registrazione</h1>
@endif

@endsection 


@section('content')
@if(!$message = Session::get('registrazione') && !$message = Session::get('emailconfermata'))
<form method="POST" action="{!! route('auth.registrazione') !!}">
    @csrf

    @if($tipo == 'impresa')
        <input type="hidden" name="type" value="impresa">
        <div class="form-group">
                <label class="form-label">Ragione sociale <a href="{{ route('registrazione') }}" class="label-right text-primary">Sei un professionista?</a></label>
                <input name="ragione_sociale" class="form-input input-xl">
            </div>
        </div>

    @else
        <input type="hidden" name="type" value="professionista">
        <div class="form-group">
                <label class="form-label">Qualifica <a href="{{ route('registrazione', 'impresa') }}" class="label-right text-primary">Sei un'impresa?</a></label>
                <select name="qualifica" class="form-select select-xl">
                    @foreach ($qualifiche as $id_qualifica => $qualifica)   
                        <option @if(old('qualifica') == $id_qualifica) checked @endif value="{!! $id_qualifica !!}">{!! $qualifica !!}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group">
                <input type="text" class="form-input input-xl" placeholder="Nome" name="nome" required value="{!! old('nome') !!}">
            </div>
        
            <div class="form-group">
                <input type="text" class="form-input input-xl" placeholder="Cognome" name="cognome" required value="{!! old('cognome') !!}">
            </div>
    @endif


    <div class="form-group">
        <input type="email" class="form-input input-xl" placeholder="Email" name="email" required value="{!! old('email') !!}">
    </div>

    <div class="form-group py5">

        <label class="form-checkbox acceptance">
            <input type="checkbox" name="privacy" required>
            <i class="form-icon"></i> Confermo di aver letto e compreso la <a href="{{ route('legal.privacy') }}" target="_blank">privacy policy</a> ed autorizzo al trattamento dei miei dati.
        </label>

        <label class="form-checkbox acceptance">
            <input type="checkbox" name="condizioni" required>
            <i class="form-icon"></i> Accetto i <a href="{{ route('legal.condizioni') }}" target="_blank">termini e le condizioni</a> di utilizzo di Workook.
        </label>

    </div>

    <button class="btn btn-primary btn-padding">Registrati</button>

    <small class="d-block mt10">Sei già registrato? <a href="{{ route('login') }}" class="text-primary" title="Accedi a Workook">Accedi</a></small>
</form>

@endif

@endsection
