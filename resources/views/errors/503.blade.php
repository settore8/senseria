@extends(Auth::check() ? 'layouts.app' : 'layouts.error') 

@section('title')Error 503 @endsection

@section('bodyclass')error503 @endsection

@section('content')
<main>
<div>
    <div>
        <h1>Sito offline</h1>
        <h2>Ti invitiamo a riprovare più tardi</h2>
        <p>Stiamo migliorando Senseria <br/>.Ti invitiamo a riprovare più tardi.</p>
        <a href="{{ route('home') }}">Ritorna alla homepage</a>
        </div>

    </div>
</main>
@endsection