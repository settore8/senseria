@extends(Auth::check() ? 'layouts.app' : 'layouts.error') 

@section('title')Error 404 @endsection

@section('bodyclass')error404 @endsection

@section('content')
    <h1 class="error__title text-secondary mt20">Oops..</h1>
    <h2 class="error__subtitle text-secondary">Pagina non trovata</h2>
    <p>Ci dispiace, il contenuto che cerchi <br/>non esiste, è stato rimosso o è stato spostato.</p>
    <a href="{{ route('home') }}" class="btn btn-secondary btn-padding">Ritorna alla homepage</a>
@endsection