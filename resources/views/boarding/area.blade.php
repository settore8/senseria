
@extends('layouts.boarding')

@section('title') Boarding @endsection

@section('visual')
<img src="{{ asset('images/app/visual-onboarding-area.svg') }}" alt="Scelta competenze" class="max-width-200">
<h1 class="size-250 weight-100">Area operativa</h1>   
<p>Clicca sulla mappa per disegnare <strong>l'area operativa</strong> nella quale operi principalmente</p>
<button id="resetPolygon" class="btn btn-svg btn-outline-dark mb20">Disegna area <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 120 120">
<path d="M110.1,28.8l-18-18c-1.2-1.2-2.7-1.9-4-1.9c-0.8,0-1.4,0.2-1.9,0.7L24.4,71.6c-0.3,0.3-0.5,0.6-0.6,1l-8.6,27.9
   c-0.9,2.8,1.3,5.5,4,5.5c0.4,0,0.8-0.1,1.3-0.2l27.9-8.6c0.4-0.1,0.7-0.3,1-0.6l61.9-61.9C112.6,33.4,112.1,30.8,110.1,28.8z
    M27.9,97.4c-0.2-0.5-0.4-0.9-0.8-1.2l-2.5-2.5c-0.3-0.3-0.7-0.5-1-0.7L29,75.6c3,1.8,6.1,4.2,8.8,7c3,3,5.5,6.2,7.4,9.5L27.9,97.4z
    M49.7,88c-2-3.3-4.7-6.6-7.7-9.6c-2.8-2.8-5.8-5.2-8.9-7.2l55.5-55.5l16.7,16.7L49.7,88z"/>
</svg>
</button>
@endsection

@section('content')

 <div id="map" data-editable="true" data-draggable="true"></div>

<form action="{!! route('boarding.store.step3') !!}" method="POST">
    @csrf
    <input id="area" name="area" type="hidden" value="">
    <div id="fixed-action" class="form-group">
        <button type="submit" class="btn btn-padding btn-primary">Salva</button>
    </div>
</form>
@endsection
