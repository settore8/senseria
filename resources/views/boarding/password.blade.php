@extends('layouts.boarding')

@section('title') Boarding @endsection

@section('visual')
<img src="{{ asset('images/app/visual-onboarding-scelta-password.svg') }}" alt="Scelta password" class="max-width-200">
<h1 class="size-250 weight-100">Password</h1>

@endsection

@section('content')
        <div class="columns">
            <div class="column col-12">
            <p class="text-center text-left--md">Email <strong>{!! Auth::user()->email !!}</strong> verificata con successo, ancora pochi passi per entrare in Workook</p>
            <p class="text-center text-left--md">Scegli una passwrod che contenga almeno <strong>una lettera</strong>, <strong>un numero</strong> ed <strong>un carattere speciale</strong></p>
            <form action="{!! route('boarding.store.step1') !!}" method="POST" > 
                @csrf      
                <div class="form-group">
                    <label for="password" class="form-label">Password</label>
                    <input type="password" name="password" class="form-input input-lg">
                </div>
        
                <div class="form-group">
                    <label for="password_confirmation" class="form-label">Ripeti password</label>
                    <input type="password" name="password_confirmation" class="form-input input-lg">
                </div>
                
                <div id="fixed-action" class="form-group">
                    <button type="submit" class="btn btn-padding btn-primary">Avanti</button>
                </div>
            </form>
            </div>
        </div>
@endsection
