@extends('layouts.boarding')

@section('title') Boarding @endsection

@section('visual')
<img src="{{ asset('images/app/visual-onboarding-competenze.svg') }}" alt="Scelta competenze" class="max-width-200">
<h1 class="size-250 weight-100">Competenze</h1>   
<p>Scegli le <strong>competenze</strong> e seleziona la tua <strong>specializzazione</strong> cliccando sulla stella</p>
@endsection

@section('content')
<div class="item panel">
    <form action="{!! route('boarding.store.step2') !!}" method="POST">
        @csrf
        
        @include('includes.partials.editcompetenze', ['competenze' => $competenze])
            
        <div id="fixed-action" class="form-group">
        <button type="submit" class="btn btn-padding btn-primary">Salva</button>
        </div>

    </form>
</div>
@endsection

