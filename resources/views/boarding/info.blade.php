@extends('layouts.boarding')

@section('title') Boarding @endsection

@section('visual')
<img src="{{ asset('images/app/visual-onboarding-info.svg') }}" alt="Scelta competenze" class="max-width-200">
<h1 class="size-250 weight-100">Informazioni</h1>   
<p>Completa le ultime informazioni per entrare in Workook</p>
@endsection


@section('content')

<form action="{!! route('boarding.store.step4') !!}" method="POST">
    @csrf
    <input type="hidden" value="{!! Auth::user()->qualifica->albo->nome_pubblico !!}" readonly>
    <input type="hidden" value="{!! Auth::user()->qualifica->nome_pubblico !!}" readonly>
  
    <div class="form-group">
        <label for="collegio" class="form-label">Collegio</label>
        <select name="collegio" id="collegio" class="form-control select" aria-readonly="false">
            @foreach ($collegi as $collegio)
                <option @if(old('collegio') == $collegio->id) checked @endif value="{!! $collegio->id !!}"> {!! $collegio->nome_pubblico !!}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <label for="numero_iscrizione" class="form-label">Numero iscrizione all'albo</label>
        <input type="text" class="form-input input-lg" name="numero_iscrizione" class="form-control">
    </div>
    <div class="form-group">
        <label for="partita_iva" class="form-label">Partita iva</label>
        <input type="text" name="partita_iva" class="form-input input-lg showCounter" maxlength="11" >
    </div>

    <div id="fixed-action" class="form-group">
        <button type="submit" class="btn btn-padding btn-success">Concludi</button>
    </div>
</form>
@endsection