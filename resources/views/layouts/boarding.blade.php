<!DOCTYPE html>
<html lang="it">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="user-scalable=yes, width=device-width, initial-scale=1">
        <meta name="google" content="notranslate">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>{{ config('app.name') }} | @yield('title')</title>
        <link href="{{ asset('css/site.css') }}" rel="stylesheet">
    </head>
    <body class="auth @yield('bodyclass')">

            <section class="onboarding">
                    @yield('content')
            </section>

        <script src="{{ asset('js/app.js') }}"></script>
    </body>
</html>
