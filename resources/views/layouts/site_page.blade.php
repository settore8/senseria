<!DOCTYPE html>
<html lang="it">
  
<head>
    @include('includes.partials.head')
    <link href="{{ asset('css/site.css') }}" rel="stylesheet">
</head>

<body class="site senseria @yield('bodyclass')">
      
    @include('site.includes.navbar')

    <main>
      
      @yield('contentsidebar')
      </nav>

      <div id="maincontent">
        @yield('content')
      </div>

    </main>

    <footer class="footer py10">
        <div class="container text-center">
            @include('includes.partials.footer')
        </div>
    </footer>

  <div id="overlay"></div>
  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key={{ config('app.maps') }}&libraries=drawing&language=IT"></script>
  <script src="{{ asset('js/site_bundle.js') }}"></script>
  @yield('footerscript')

</body>

</html>