<!DOCTYPE html>
<html lang="it">

<head>
    @include('includes.partials.head')
    <link href="{{ asset('css/site.css') }}" rel="stylesheet">
</head>

<body class="site senseria @yield('bodyclass')">

        @include('site.includes.navbar')
      
    <main id="main" class="hero hero-lg">
        @yield('main')
    </main>
    
    @yield('content')

    <footer class="footer py10">
        <div class="container text-center">
          @include('includes.partials.footer')
        </div>
    </footer>

  <div id="overlay"></div>

  <script src="{{ asset('js/site.js') }}"></script>

</body>



</html>