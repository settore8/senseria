<!DOCTYPE html>
<html lang="it">
    
    <head>
        @include('includes.partials.head')
        <link href="{{ asset('css/site.css') }}" rel="stylesheet">
    </head>

    <body class="auth senseria @yield('bodyclass')">

            <div class="visual">
                <a href="{{ url(route('home')) }}" id="logo">@include('includes.graphics.logo')</a>
            </div>
            <section class="auth-form">
                    @yield('content')
                    <div class="footer">
                            @include('includes.partials.footer')
                    </div>

            </section>
        
        <script src="{{ asset('js/site.js') }}"></script>
    </body>
</html>
