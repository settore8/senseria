<!DOCTYPE html>
<html lang="it">

<head>
    @include('includes.partials.head')
    <link href="{{ asset('css/site.css') }}" rel="stylesheet">
</head>

<body class="error text-light @yield('bodyclass')">
        <main>
            <article>
                <a href="{{ url(route('home')) }}" id="logo" >@include('includes.graphics.logo')</a>
                @yield('content')
            </article>
        </main>
        <footer>
            @include('includes.partials.footer')
        </footer>
        <script src="{{ asset('js/site.js') }}"></script>
</body>

</html>