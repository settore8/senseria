@extends('layouts.site_page')
@section('title'){{ config('app.name') }}, il primo portale immobiliare di immobili certificati. @endsection
@section('bodyclass') has-fixed-nav @endsection

@section('content')

<section>


    @isset($request)
        @foreach($request->all() as $key => $req  )
           
            <span class="badge badge-primary">{{ $req }} <button data-remove-parameter="{{ $key }}">x</button></span>
            
        @endforeach
    @endisset


    <div id="searchForm" class="modal">
        <div class="container grid-lg">
        @include('includes.partials.searchform')
        </div>
    </div>
</section>

<section>
    <div class="container grid-lg">
        <div class="columns">
            @if($immobili->isNotEmpty())
                @foreach($immobili as $immobile)
                    <div class="column col-4 col-sm-6 col-xs-12">
                        <a href="" class="card card--immobile">
                            <div class="columns">
                                <figure class="column col-12 col-xs-5">
                                    <img src="https://picsum.photos/id/1/480/360"  class="img-responsive">
                                </figure>
                                <div class="column col-12 col-xs-5">
                                    <small>{{$immobile->codice}}</small>
                                    <h2>{{ $immobile->specifica->nome_pubblico }}</h2>

                                    {{ $immobile->prezzoVendita }}
                                    @if($immobile->informazione)
                                        {{ $immobile->informazione->comune }}
                                    @endif
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach

                {{ $immobili->links() }}
            @else
                nessun risultato 
            @endif
        </div>
    </div>
</section>

@endsection