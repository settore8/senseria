@extends('layouts.site_page')
@section('title'){{ config('app.name') }}, il primo portale immobiliare di immobili certificati. @endsection
@section('bodyclass') has-fixed-nav @endsection


@section('content')

{{dd($immobile)}}

<section class="hero hero-lg" id="come">
        <div class="container grid-xl">
        <div class="columns reverse">
        <div class="column  col-md-12">
            <h2>Cosa potrai fare con Senseria</h2>
            <p class="size-125">Con Senseria potrai <span class="highlight highlight-yellow">pianificare</span>, <span class="highlight highlight-yellow">organizzare</span>, <span class="highligh highligh-yellow">monitorare</span>, <span class="highlight highlight-yellow">collaborare</span> e
                    soprattutto trovare Opportunità di lavoro
                    direttamente dal mondo immobiliare</p>
            <a href="{{ route('registrazione') }}" class="btn btn-primary btn-padding">Registrati</a>
            <a href="#contact" class="btn btn-link">Vuoi saperne di più?</a>
        </div>
        <figure class="column  col-md-12">
           <img src="images/visual-video-senseria.svg">
        </figure>
    </div>
    </div>
</section>

@endsection