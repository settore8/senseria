@extends('layouts.site_landing')
@section('title')I Vantaggi di Workook @endsection
@section('bodyclass') has-fixed-nav @endsection


@section('content')
<section>
<div class="hero container grid-lg">
    <div class="columns benefit">
            <figure class="column col-3 col-mr-auto  col-sm-10">
                <img src="images/site/vantaggi-icon-intro.svg" alt="Vantaggi Workook">
            </figure>

            <div class="column col-7 col-mx-auto col-sm-10">
                <h1 class="weight-600">Vantaggi Workook</h1>
                <p class="size-100">Sappiamo quanto sia frequente riscontrare difformità urbanistiche e catastali, e quanto possa essere importante fornire in fase preventiva un quadro tecnico dell’immobile che metta in risalto le criticità. Già dalla fase del <strong class="highlight highlight-yellow">sopraluogo</strong> potrai quindi essere guidato passo passo nella raccolta di tutte le informazioni importanti direttamente dal tuo tablet, facilitando tutto il lavoro successivo ed evitandoti di dover ritornare. Mai più <strong class="highlight highlight-yellow">informazioni mancanti o incomplete</strong>.</p>
                <p class="size-100">Potrai controllare lo stato di avanzamento delle varie commesse relative agli immobili che stai gestendo, fino alla risoluzione completa delle problematiche riscontrate.</p>
                <p class="size-100">Potrai inoltre redigere, al momento opportuno e in maniera automatica, la <strong class="highlight highlight-yellow">relazione tecnica</strong> per l’eventuale vendita e, se lo vorrai, condividerla direttamente con il Notaio.</p>
            </div>
    </div>
</div>
</section>


<section class="bg-gray-light">
        <div class="hero container grid-lg">
                <div class="columns benefit">
                        <figure class="column col-3 col-mr-auto  col-sm-10">
                        <img src="images/site/vantaggi-icon-espandi.svg" alt="Espandi i tuoi Confini">
                        </figure>
                        <div class="column col-7 col-mx-auto col-sm-10">
                        <h2 class="weight-600">Espandi i tuoi confini</h2>
                        <p class="size-100">Per ogni progetto potrai creare una serie di compiti da assegnare a te stesso o ai tuoi collaboratori.
                                Ma Workook fa di più.
                                Ti permettrà di reclutare colleghi qualificati e certificati presenti all’interno dalla community che, solo dopo la tua approvazione, potranno accedere alle informazioni di cui hanno bisogno.</p>
                        <p class="size-100">In questo modo la tua operatività non avrà più limiti, troverai rapidamente uno specialista ad ogni necessità iin relazione alle tematiche dove ha dichiarato maggior esperienza.</p>
                                
                        <p class="size-100">Per ogni richiesta i colleghi potranno presentare la candidatura e sarai SOLO TU che valuterai e selezionerai la figura più adatta al compito proposto. Avrai quindi libero accesso a tutti i profili e potrai scegliere chi vuoi! Workook in questo caso farà da garante certificando i profili e ti aiuterà ad identificare la figura più qualificata.</p>
                        </div>
                </div>
        </div>
</section>

<section>
        <div class="hero container grid-lg">
                <div class="columns benefit">
                        <figure class="column col-3 col-mr-auto col-sm-10">
                        <img src="images/site/vantaggi-icon-vendita.svg" alt="Vendita Immobili Workook">
                        </figure>
                        <div class="column col-7 col-mx-auto col-sm-10">
                        <h2 class="weight-600">Una rete vendita a tua completa disposizione</h2>
                        <p class="size-100">La piattaforma Workook è integrata alla rete <a href="#">Senseria</a>, la rete di agenti immobiliari e agenzie sul terriotrio nazionale, che potranno aiutarti a vendere gli immobili che gestisci e che sono già verificati e pronti per andare sul mercato immobiliare.</p>
                        <p class="size-100">Sarai tu a decidere a chi dare l’incarico, in esclusiva o no, e stabilire le conidzioni sulla trattiva di vendita.
                                Viceversa gli agenti immobiliari potranno richiedere delle verifiche tecniche sui loro immobili
                                in modo da poterli vendere senza problemi grazie alla valutazione di professionisti come TE,
                                creando tante e reali opportunità di business!</p>
                        </div>
                </div>
        </div>
</section>

<section class="bg-gray-light">
        <p class="hero container grid-md size-125 text-center">Grazie alla collaborazione con alcune importanti realtà del settore immobiliare, all’interno del sistema sono disponibili tanti immobili e buona parte di questi stanno attendendo l’intervento di professionisti in modo che l’immobile abbia le garanzie per poter essere venduto senza problemi.</p>
        <div class="container grid-lg">
                <div class="columns benefit">
                        <figure class="column col-3 col-mr-auto  col-sm-10">
                        <img src="images/site/vantaggi-icon-info.svg" alt="Info Workook">
                        </figure>
                        <div class="column col-7 col-mx-auto col-sm-10">
                        <p class="size-100 text-center text-left--xs">I motivi per cui potresti non voler aderire potrebbero essere legati o ad una scarsa attitudine agli strumenti digitali (o magari la poca voglia di abbandonare i vecchi sistemi collaudati…) e in questo caso posso assicurati che la semplicità è stato il nostro punto focale, oppure pensi che il sistema potrebbe non soddisfare le tue reali necessità. Su questo punto posso solo dirti che l’intero progetto è stato ideato e portato avanti da architetti e geometri e che ha richiesto oltre 4 anni di sviluppo prima di venire alla luce e diventare uno strumento realmente utile e, a nostro avviso indispensabile, per gestire le complessità crescenti del nostro settore.</p>
                        </div>
                </div>
        </div>
        <p class="pt30 pb30 container grid-md size-125 text-center">Crediamo fortemente che Workook possa rivoluzionare il modo di operare nel nostro settore e speriamo di averti trasmesso il nostro entusiasmo. Augurandoci quindi che tu abbia voglia di raccogliere la sfida per affrontare assieme il nuovo mercato, ti aspettiamo su Workook.
        </p>
        <p class="text-center">
        <a href="{{ route('registrazione') }}" class="btn btn-primary btn-padding mb20">Ora non ti resta che provarlo!</a>
        </p>
</section>



@endsection