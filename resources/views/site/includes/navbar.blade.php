<header class="header--sticky">
    <div class="container grid-xl">
      <nav  class="navbar is-fixed py5" role="navigation" aria-label="main navigation">
        <div class="navbar-section">
            <a class="navbar-brand" href="{{ url(route('home')) }}" id="logo">@include('includes.graphics.logo')</a>
          </div>
          <button class="togglemenu hamburger" aria-controls="navbar">
             <span></span>
             <span></span>
             <span></span>
          </button>
        
          <div class="navbar-section" id="navbar">

            <div class="navbar-dropdown">
              <span class="navbar-dropdown-toggle">Panoramica</span>
              <div class="navbar-dropdown-menu">
                <a href="{{ route('caratteristiche') }}" class="btn btn-link btn-block text-dark {{ Custom::current('caratteristiche') }}">Caratteristiche</a>
                <a href="{{ route('vantaggi') }}" class="btn btn-link btn-block text-dark {{ Custom::current('vantaggi') }}">Vantaggi</a>
              </div>
            </div>
              
            <a href="{{ route('agenti') }}" class="btn btn-link text-dark hightlight-on-hover {{ Custom::current('agenti') }}">
                Agenti immobiliari
            </a>
            <a href="{{ route('senseria.annunci', ['contratto' => 'vendita']) }}" class="btn btn-link text-dark hightlight-on-hover {{ Custom::current('senseria.annunci') }}">
                Vendita
            </a>
            <a href="{{ route('senseria.annunci', ['contratto' => 'affitto']) }}" class="btn btn-link text-dark hightlight-on-hover {{ Custom::current('senseria.annunci') }}">
              Affitto
            </a>
            @if(Auth::check())
              <a class="btn btn-primary btn-padding ml5--md" href="{{ route('dashboard') }}">
                Dashboard
              </a>
            @else
            <a href="{{ route('login') }}" class="btn btn-link text-primary hightlight-on-hover">
                Accedi
            </a>
            <a class="btn btn-primary btn-padding ml5--md" href="{{ route('registrazione') }}">
              Registrati
            </a>
            @endif

        </div>
       </nav>
   </div>
</header>