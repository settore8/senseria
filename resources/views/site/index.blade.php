@extends('layouts.site_landing')
@section('title'){{ "Senseria, lo strumento di lavoro per il settore dell'edilizia" }}@endsection
@section('og_type'){{ "website" }}@endsection
@section('og_title'){{ "Senseria, lo strumento di lavoro per il settore dell'edilizia" }}@endsection
@section('bodyclass'){{ "has-fixed-nav" }}@endsection
@section('main')
    <div class="container grid-lg">
        <div class="columns reverse">
            <div class="column col-md-12">
                    <figure>
                            <img src="images/visual-INTRO.svg">
                    </figure>
            </div>
            <div class="column col-md-12">
                    <h1 class="size-300 text-600">Sei un <span class="highlight highlight-yellow">agente</span> <span class="highlight highlight-yellow">immobiliare</span>?</h1>
                    <h2 class="size-150 text-100">senseria è lo strumento <strong>che rivoluzionerà <br>il tuo modo di lavorare</strong>.</h2>
                    <a href="{{ route('registrazione') }}" class="btn btn-primary btn-padding mr-2">Registrati gratis!</a>
            </div>
        </div>
    </div>
@endsection


@section('content')
<section class="hero hero-lg bg-primary" id="cosa">
<div class="container grid-lg">
    <div class="columns reverse">
            <div class="column col-md-12">
                <h2>Cosa è senseria</h2>
                <p class="size-125">Una piattaforma di gestione del lavoro totale
                        tutto in uno spazio unico
                        dove Pianificare, Organizzare, Monitorare, Collaborare e
                        soprattutto trovare Opportunità di lavoro
                        direttamente dal mondo immobiliare.</p>
                <a herf="#" class="btn btn-padding btn-secondary">Registrati</a>
            </div>
            <figure class="column col-md-12">
                    <img src="images/visual-COSA.svg">
            </figure>
    </div>
</div>
</section>

<section>
        <div class="hero hero-lg container grid-lg">
        <div class="columns">
            <div class="column col-12 text-center">
                <h2 class="size-300 text-600">Cerca un immobile</h2>

                @include('includes.partials.searchform')
                
            </div>
        </div>
        </div>
</section>


<section>
    <div class="hero hero-lg container grid-lg">
    <div class="columns">
        <div class="column col-12">
            <h2>Ultimi immobili inseriti</h2>
            <div class="columns">

                <div class="column col-4 col-sm-6 col-md-12">
                    <a href="" class="card">

                        immobile 1
                    </a>
                    
                </div>

                <div class="column col-4 col-sm-6 col-md-12">
                    <a href="" class="card">

                        immobile 2
                    </a>
                </div>

                <div class="column col-4 col-sm-6 col-md-12">
                    <a href="" class="card">

                        immobile 3
                    </a>
                </div>

                <div class="column col-4 col-sm-6 col-md-12">
                    <a href="" class="card">

                        immobile 4
                    </a>
                </div>

                <div class="column col-4 col-sm-6 col-md-12">
                    <a href="" class="card">

                        immobile 5
                    </a>
                </div>

                <div class="column col-4 col-sm-6 col-md-12">
                    <a href="" class="card">

                        immobile 6
                    </a>
                </div>


            </div>

            <a href="{{ route('registrazione') }}" class="btn btn-secondary btn-padding mr-2">Vetrina immobili</a>
                    
        </div>

        
    </div>
</div>
</section>

@endsection