@extends('layouts.site_landing')
@section('title')La caratteristiche di Workook @endsection
@section('bodyclass') has-fixed-nav @endsection


@section('content')
<section>
<div class="hero container grid-lg">
    <div class="columns centered reverse">
            <div class="column col-5 col-mx-auto col-sm-12">
                <h1 class="weight-600">Caratteristiche Workook</h1>
                <p class="size-125">La piattaforma professionale più avanzata di sempre, tutto in uno spazio unico, dove potrai <strong class="highlight highlight-yellow">pianificare</strong>, <strong class="highlight highlight-yellow">organizzare</strong>, <strong class="highlight highlight-yellow">monitorare</strong>, <strong class="highlight highlight-yellow">collaborare</strong> e soprattutto <strong class="highlight highlight-yellow">trovare opportunità di lavoro</strong> direttamente dal mondo immobiliare.
                        Tanti strumenti a tua disposizione per costrurie il tuo spazio personale di lavoro e la tua rete di contatti specializzati.</p>
            </div>
            <figure class="column col-5 col-mx-auto col-sm-12">
                <img src="images/site/caratteristiche-intro.svg" alt="Caratteristiche Workook">
            </figure>
    </div>
</div>
</section>

<section>
    <div class="container grid-lg">
    <div class="columns">
        <div class="column col-4 col-sm-6 col-xs-12 py5 px10">
                <div class="feature p10 bg-gray-light">
                <figure>
                        <img src="images/site/caratteristiche-icon-procedure.svg" alt="Procedure Workook">
                </figure>
                <h2>Procedure guidate</h2>
                <p>Grazie poi ad una serie di procedure guidate, sarà per te estremamente semplice e veloce eseguire una verifica tecnica sull’immobile, riducendo da subito tempistiche e probabilità di commettere errori.</p>
                </div>
        </div>

 

        <div class="column  col-4 col-sm-6 col-xs-12 py5 px10">
                <div class="feature p10 bg-gray-light">
                <figure>
                        <img src="images/site/caratteristiche-icon-dashboard.svg" alt="Dashboard Workook">
                </figure>
                <h2>Dashboard di controllo</h2>
                <p>La tua bacheca personale raccoglie sinteticamente tutti i tuoi dati personali, dei tuoi progetti, degli articoli e dei professionisti che stai seguendo.</p>
                </div>
        </div>

        <div class="column  col-4 col-sm-6 col-xs-12 py5 px10">
                <div class="feature p10 bg-gray-light">
                <figure>
                        <img src="images/site/caratteristiche-icon-articoli.svg" alt="Articoli Workook">
                </figure>
                <h2>Articoli</h2>
                <p>In un ambiente in continua evoluzione avrai a disposizione uno strumento d’informazione utile, efficace e centrato sui tuoi interessi. Ti consentirà di condividere le tue esperienze e raccogliere quelle di altri professionisti.</p>
                </div>
        </div>

        <div class="column col-4 col-sm-6 col-xs-12 py5 px10">
                <div class="feature p10 bg-gray-light">
                <figure>
                        <img src="images/site/caratteristiche-icon-gallerie.svg" alt="Gallerie Workook">
                </figure>
                <h2>Gallerie</h2>
                <p>Grazie alle gallerie tematiche potrai mostrare alla rete workook i tuoi lavori. Potrai gestire e catalogare le tue gallerie e quelle preferite di altri professionisti.</p>
                </div>
        </div>

        <div class="column col-4 col-sm-6 col-xs-12 py5 px10">
                <div class="feature p10 bg-gray-light">
                <figure>
                        <img src="images/site/caratteristiche-icon-conversazioni.svg" alt="Conversazioni Workook">
                </figure>
                <h2>Conversazioni</h2>
                <p>All’interno di Workook troverai un canale di comunicazione diretto e dedicato, per dialogare privatamente con collaboratori e utenti della rete workook. Potrai sempre tenere ordinate e selezionate le chat che attiverai, senza dover ricorrere a strumenti esterni e senza perdere tempo.</p>
                </div>
        </div>

        <div class="column  col-4 col-sm-6 col-xs-12 py5 px10">
                <div class="feature p10 bg-gray-light">
                <figure>
                        <img src="images/site/caratteristiche-icon-statistiche.svg" alt="Statistiche Workook">
                </figure>
                <h2>Statistiche</h2>
                <p>I dati sulle tue attività saranno raccolti ed organizzati in maniera essenziale in grafici e statistiche, che ti consentiranno di visualizzare l’andamento delle tue attività, coi tuoi punti di forza e quelli da migliorare.</p>
                </div>  
        </div>

    </div>
    
</div>
</section>



@endsection