<nav id="headermenu" class="item item--auto item--center text-center text-right--sm">
    <ul class="inline-nav">
        {{ $slot }}
    </ul>
</nav>