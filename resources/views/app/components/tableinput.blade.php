<!--<div class="columns">
@isset($thead)
        @foreach ($thead as $th)
            <div class="column col-4">{{ $th }}</div>
        @endforeach
@endisset
</div>
-->
@isset($content)
    @foreach ($content as $item => $cnt)
            @formgrouph(['for' => $nameinput.'['.$item.']', 'name' => $nameinput.'['.$item.']', 'label' => $cnt, 'col1' => 'col-8', 'col2' => 'col-4'])
                @inputnumber(['attrs' => ['name' => $nameinput.'['.$item.']', 'id' => $nameinput.'['.$item.']', 'placeholder' => 'Km']]) @endinputnumber
            @endformgrouph
    @endforeach
@endisset
