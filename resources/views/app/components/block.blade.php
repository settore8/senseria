<div class="block @isset($class) {{ $class}}@endisset" @isset($id) id="{{ $id}}" @endisset>
    @isset($header)
        <div class="block__header">
            @isset($title) <h2 class="block__title">{!! $title !!}</h2>@endisset {!! $header !!}
        </div>
    @else

    @isset($title) <h2 class="block__title">{!! $title !!}</h2>@endisset

    @endisset
    {!! $slot !!}
</div>