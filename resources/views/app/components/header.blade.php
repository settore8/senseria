@section('header')
<header class="flex">

    @yield('headertitle')
    
    @yield('headermenu')

</header>
@show