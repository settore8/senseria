<div class="form-group form-group-submit">
    <input type="hidden" name="pubblicabile" value="0">
    <button data-pubblicabile="{{$attributo_bozza}}"  class="btn btn-outline btn-padding">{{$testo_bozza}}</button>
    <button data-pubblicabile="{{$attributo_pubblicazione}}" class="btn btn-primary btn-padding">{{$testo_pubblicazione}}</button>
</div>
