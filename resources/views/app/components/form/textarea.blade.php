<div class="form-group @if($errors->has($attrs['name'])) {{ 'has-error' }} @endif">
    @if (array_key_exists('label', $attrs))<label class="form-label" @if(array_key_exists('id', $attrs)) for="{{ $attrs['id'] }}" @endif >{{ $attrs['label'] }} @if(array_key_exists('required', $attrs)) <span class="text-error">*</span> @endif</label>@endif

    <textarea class="form-input {!! $errors->has($attrs['name']) ? 'is-invalid' : '' !!} {{ array_key_exists('class', $attrs) ? $attrs['class'] : '' }}" @foreach (Custom::array_keys_unset($attrs, ['value', 'label', 'class']) as $attr => $value) {{ $attr }}="{{ $value }}" @endforeach autocomplete="off">{{ array_key_exists('value', $attrs) ? $attrs['value'] : null }}</textarea>

    @if($errors->has($attrs['name']))
        <p id="val-username-error" class="form-input-hint">{{ $errors->first($attrs['name']) }}</p>
    @endif
</div>