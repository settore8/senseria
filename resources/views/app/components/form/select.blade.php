@if(array_key_exists('label', $attrs))  <label class="form-label"  @if(array_key_exists('id', $attrs))for="{{ $attrs['id'] }}"@endif>{{ $attrs['label'] }}@if(array_key_exists('required', $attrs) && $attrs['required']) <span class="text-error">*</span> @endif</label> @endif
    <select class="form-select {{ $errors->has($attrs['name']) ? ' is-error' : ''}} {{ array_key_exists('class', $attrs) ? $attrs['class'] : '' }}" @foreach (Custom::array_keys_unset($attrs, ['value', 'label']) as $attr => $value) {{ $attr }}="{{ $value }}" @endforeach>
        @if(array_key_exists('value', $attrs))
            @foreach ($attrs['value'] as $v)
                @if(array_key_exists('default', $attrs))<option value="{{ $v == '--' ? null : $v}}" {{$attrs['default'] == $v ? 'selected' : null}}>{{ $v }}</option>
                @else <option value="{{ $v == '--' ? null : $v}}">{{ $v }}</option>
                @endif
            @endforeach
        @endif
    </select>
    @if($errors->has($attrs['name']))
        <p class="form-input-hint">{{ $errors->first($attrs['name']) }}</p>
    @endif
