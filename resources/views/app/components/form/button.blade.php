<div class="form-group form-group-submit text-center text-right--sm mt20">
    <button type="submit" class="btn {{ isset($attrs['class']) ? $attrs['class'] : 'btn-primary btn-padding' }}" @foreach(\App\Custom\Custom::array_keys_unset($attrs, ['class', 'text']) as $attr => $value) {{$attr}} = {{$value}} @endforeach>{{ $attrs['text'] }}</button>
</div>