<div class="form-group">
    <label class="form-switch{{ $errors->has($attrs['name']) ? ' is-error' : ''}}">
      <input type="checkbox" @foreach (\App\Custom\Custom::array_keys_unset($attrs,['checked']) as $attr => $value) {{ $attr }}="{{ $value }}" @endforeach @if(array_key_exists('checked', $attrs) && $attrs['checked'] === true) checked @endif>
      <i class="form-icon"></i> @if(array_key_exists('label', $attrs)) {{ $attrs['label'] }} @endif
    </label>
    @if($errors->has($attrs['name']))
        <small><div id="val-username-error" class="invalid-feedback animated fadeIn">{{ $errors->first($attrs['name']) }}</div></small>
    @endif
</div>
