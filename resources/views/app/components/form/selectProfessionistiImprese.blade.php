
@if(array_key_exists('label', $attrs))  <label class="form-label" @if(array_key_exists('id', $attrs))for="{{ $attrs['id'] }}"@endif>{{ $attrs['label'] }}@if(array_key_exists('required', $attrs))<span class="text-error">*</span> @endif</label> @endif


<select class=" select toollist form-select js-select2-professionisti-imprese {{ $errors->has($attrs['name']) ? ' is-error' : ''}}" @foreach (Custom::array_keys_unset($attrs, ['value', 'label']) as $attr => $value) {{ $attr }}="{{ $value }}" @endforeach>
    @if(array_key_exists('default', $attrs))
        <option value="{{$attrs['default']['id']}}" selected>{{$attrs['default']['fullname']}}</option>
    @endif
    @if(array_key_exists('value', $attrs))
        <option value="{{$attrs['value']['id']}}" selected>{{$attrs['value']['value']}}</option>
    @endif
</select>


@if($errors->has($attrs['name']))
    <small>
        <div id="val-username-error" class="invalid-feedback animated fadeIn">{{ $errors->first($attrs['name']) }}</div>
    </small>
@endif

