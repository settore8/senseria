@if(array_key_exists('label', $attrs))  <label class="form-label"  @if(array_key_exists('id', $attrs))for="{{ $attrs['id'] }}"@endif>{{ $attrs['label'] }}@if(array_key_exists('required', $attrs) && $attrs['required']) <span class="text-error">*</span> @endif</label> @endif
    <select class="form-select js-select2 {{ $errors->has($attrs['name']) ? ' is-error' : ''}}" @foreach (Custom::array_keys_unset($attrs, ['value', 'label', 'default']) as $attr => $value) {{ $attr }}="{{ $value }}" @endforeach>
        @if(array_key_exists('value', $attrs))
            @foreach ($attrs['value'] as $v)
                @if(array_key_exists('default', $attrs))
                    @if(is_array($attrs['default']))
                        <option value="{{ $v == '--' ? null : $v}}" {{ in_array($v, $attrs['default']) ? 'selected' : null}}>{{ $v }}</option>
                    @else
                        <option value="{{ $v == '--' ? null : $v}}" {{$attrs['default'] == $v ? 'selected' : null}}>{{ $v }}</option>
                    @endif
                @else <option value="{{ $v == '--' ? null : $v}}">{{ $v }}</option>
                @endif
            @endforeach
        @endif
    </select>
    @if($errors->has($attrs['name']))
        <p class="form-input-hint">{{ $errors->first($attrs['name']) }}</p>
    @endif
