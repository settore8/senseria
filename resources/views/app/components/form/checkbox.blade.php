<div class="form-group">
    <label class="form-checkbox{{ $errors->has($attrs['name']) ? ' is-error' : ''}}">
      <input type="checkbox" @foreach ($attrs as $attr => $value) {{ $attr }}="{{ $value }}" @endforeach>
      <i class="form-icon"></i> @if(array_key_exists('label', $attrs)) {{ $attrs['label'] }} @endif
    </label>
    @if($errors->has($attrs['name']))
        <small><div id="val-username-error" class="invalid-feedback animated fadeIn">{{ $errors->first($attrs['name']) }}</div></small>
    @endif
</div>