<div @if(isset($id)) id="{{ $id }}" @endif  class="form-group @if(isset($class)) {{ $class }} @endif @if(isset($required)) required @endif @if(isset($name)) @if($errors->has($name)) has-error @endif @endif">
    <div class="@if(isset($col1)) {{ $col1 }} @else col-5 col-md-4 col-sm-12 @endif">
      <label class="form-label" for="{{ $for }}">{{ $label }} @if(isset($istruzioni))<small class="istruzioni"> {!! $istruzioni !!} </small>@endif </label>
      
    </div>
    <div class="@if(isset($col2)) {{ $col2 }} @else col-7 col-md-8 col-sm-12 @endif">
        {{ $slot }}
    </div>
</div>