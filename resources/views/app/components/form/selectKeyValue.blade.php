<div class="form-group">
    @if(array_key_exists('label', $attrs))  <label class="form-label"  @if(array_key_exists('id', $attrs))for="{{ $attrs['id'] }}"@endif>{{ $attrs['label'] }}@if(array_key_exists('required', $attrs)) <span class="text-error">*</span> @endif</label> @endif

    <select class="form-select{{ $errors->has($attrs['name']) ? ' is-error' : ''}}" @foreach (Custom::array_keys_unset($attrs, ['value', 'label']) as $attr => $value) {{ $attr }}="{{ $value }}" @endforeach>
        @if(array_key_exists('value', $attrs))
            @foreach ($attrs['value'] as $v => $l)
                @if(array_key_exists('default', $attrs) && $attrs['default'])
                    <option value="{{ $v == '--' ? null : $v}}" {{$attrs['default']->id == $v ? 'selected' : null}}>{{ $attrs['default']['nome'] }}</option>
                @else <option value="{{ $v == '--' ? null : $v}}">{{ $l }}</option>
                @endif
            @endforeach
        @endif
    </select>
    @if($errors->has($attrs['name']))
        <small><div id="val-username-error" class="invalid-feedback animated fadeIn">{{ $errors->first($attrs['name']) }}</div></small>
    @endif
  </div>
