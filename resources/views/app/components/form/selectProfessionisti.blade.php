@if(array_key_exists('label', $attrs))  <label class="form-label"
                                               @if(array_key_exists('id', $attrs))for="{{ $attrs['id'] }}"@endif>{{ $attrs['label'] }}@if(array_key_exists('required', $attrs))
        <span class="text-error">*</span> @endif</label> @endif
<select
    class="form-select js-select2-professionisti {{ $errors->has($attrs['name']) ? ' is-error' : ''}}" @foreach (Custom::array_keys_unset($attrs, ['value', 'label']) as $attr => $value) {{ $attr }}="{{ $value }}" @endforeach>
</select>
@if($errors->has($attrs['name']))
    <small>
        <div id="val-username-error" class="invalid-feedback animated fadeIn">{{ $errors->first($attrs['name']) }}</div>
    </small>
@endif
