<div class="form-group" @if($errors->has($attrs['name'])) {{ 'has-error' }} @endif>
    @if (array_key_exists('label', $attrs))<label class="form-label" @if(array_key_exists('id', $attrs)) for="{{ $attrs['id'] }}" @endif >{{ $attrs['label'] }} @if(array_key_exists('required', $attrs)) <span class="text-error">*</span> @endif</label>@endif
    <input class="form-input {{ $errors->has($attrs['name']) ? ' is-error' : ''}} {{ array_key_exists('class', $attrs) ? $attrs['class'] : '' }}" type="{{ isset($attrs['type']) ? $attrs['type'] : 'text' }}" @foreach (Custom::array_keys_unset($attrs, ['class', 'label', 'readonly']) as $attr => $value) {{ $attr }}="{{ $value }}" @endforeach @if(array_key_exists('readonly', $attrs) && $attrs['readonly'] === true) readonly @endif autocomplete="off">
    @if($errors->has($attrs['name']))
        <p id="val-username-error" class="form-input-hint">{{ $errors->first($attrs['name']) }}</p>
    @endif
</div>