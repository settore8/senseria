<table>
    <thead>
        <tr>
            <th>Lato</th>
            <th>Vista</th>
        </tr>
    </thead>
    <tbody>
        <tr class="row-affacci">
            <td>
                @select(['attrs' => ['name' => 'latoaffaccio[]',  'value' => Custom::addNull(config('constants.lati-affacci'))]]) @endselect
            </td>
            <td>
                @select(['attrs' => ['name' => 'affaccio[]',  'value' => Custom::addNull($fabbricato['affacci'])]]) @endselect
            </td>
        </tr>
    </tbody>
</table>
