@if(!isset($static))
<a data-toggle="collapse" data-tip-ajax="{{ $datatip }}" href="#{{ $datatip }}" class="tip_button {{ $visibile ? 'opened' : '' }}"> {{ $label ?? 'aiuto' }}</a>
@endif
<div class="collapse {{ $visibile ? 'show' : '' }} border radius p10 mb10" id="{{ $datatip }}" data-tips="{{ $datatip }}">
    <div class="columns align-items-center">
        @php $tip = App\Suggerimento::where('nome', $datatip)->first(); @endphp
        <div class="col-8 col-lg-7 col-md-6 col-sm-12">
            @if(!isset($text))
                @php /* se non è settata il text si prende quello del tip dal db */ @endphp
                {!! $tip->testo !!}
            @else
                {{ $text }}
            @endif
        </div>
        <div class="col-4 col-lg-5 col-md-6 col-sm-8 col-mx-auto">
            @if(!isset($immagine))
                @php /* se non è settata l'immagine si prende quella del tip dal db */ @endphp
                <img src="{{ asset('images/tips/'.$tip->filename) }}">
            @else
                <img src="{{ $immagine }}">
            @endif
        </div>
    </div>
    <button data-tip-ajax="{{ $datatip }}" class="btn-close collapse__close">chiudi</button>
</div>

