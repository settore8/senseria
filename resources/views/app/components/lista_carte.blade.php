@forelse(Auth::user()->carte as $card)
    <div class="card" data-fingerprint="{{$card->carta_id}}">
        <p>Intestatario Carta: {{$card->card_holder}}</p>
        <p>Tipo Carta: {{$card->card_brand}}</p>
        <p>Termina con: {{$card->last_four}}</p>
        <p>Scadenza: {{$card->exp_month}}{{$card->exp_year}}</p>
    </div>
@empty

@endforelse

