<header class="flex">
    <h1 class="item item--center item--auto">
        {{ $slot }}
    </h1>
</header>