<div @isset($id) id="{{ $id }}" @endisset class="modal @isset($class) {{ $class }} @endisset">
    <div class="modal__overlay"></div>
    <div class="modal__container radius">
        @isset($visual)<img src="{{ asset('images/app/'.$visual )}}" class="@isset($visual_size) modal--visual {{ $visual_size }} @endisset">@endisset
        @isset($title)<h4>{!! $title !!}</h4>@endisset
        {{ $slot }}
        <button class="modal__close"><svg><use xlink:href="#icon-remove"></use><title>Chiudi</title></svg></button>
    </div>
</div>