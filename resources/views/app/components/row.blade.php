<div class="columns row row--view-data">
    <div class="column col-6">
        {{ $col1 }}
    </div>
    <div class="column col-6">
        <strong>
            {{ $col2 }}
        </strong>
    </div>
</div>