<nav id="sidebar" class="item @isset($type) {{ $type }} @endisset">
    <ul>
        {{ $slot }}
    </ul>
</nav>