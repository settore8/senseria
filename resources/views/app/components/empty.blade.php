<div class="empty text-center col-8 col-mx-auto myauto">
    <div class="columns align-items-center">
        <div class="column col-6 col-sm-12">
            @isset($subtitle)
                <img src="{{ $visual }}" alt="{{ $title }}">
            @endisset
        </div>
        <div class="column col-6 col-sm-12">
            <p class="empty-title h3">{{ $title }}</p>
            @isset($subtitle)
            <p class="empty-subtitle">{{ $subtitle }}</p>
            @endisset
            @isset($action)
            <div class="empty-action">
                {{ $action }}
            </div>
            @endisset
        </div>
    </div>
</div>