<table>
    <thead>
        <tr>
            @foreach ($thead as $th)
                <th>{{ $th }}</th>
            @endforeach
        </tr>
    </thead>
    <tbody>
        @foreach ($content as $item => $cnt)
            <tr>
                <td>{{ $cnt }}</td>
                <td><input type="checkbox" name="{{ $nameinput }}[{{ $item }}]"></td>
            </tr>
        @endforeach
    </tbody>
</table>