@isset($back)
<a href="{{ $back }}" alt="Indietro" class="flex align-items-center mr5--md"><svg width="23" height="23"><use xlink:href="#icon-indietro"></use></svg></a>
@endisset
<h1 class="item item--auto item--center text-center text-left--xs focusitem--auto font-weight-normal mb10 mt10 mb0--md mt0--md">
    {{ $slot }}
</h1>