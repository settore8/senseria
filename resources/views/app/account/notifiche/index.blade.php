@extends('app.layouts.account')

@section('title', 'Notifiche')

@section('headertitle')
    @component('app.components.headertitle')
    <a href="{{ route('account.profilo') }}" alt="Riepilogo {{ Auth::user()->nomecognome }}">{{ Auth::user()->nomecognome }}</a> <span class="weight-300">/ Notifiche</span>
    @endcomponent
@endsection

@section('content')

        @include('app.partials.menunotifiche')
        
        @if(Auth::user()->notifiche)
            @foreach(Auth::user()->notifiche as $notifica)

                <a href="#" class="notifica">
                {!!  $notifica->messaggio !!}
                </a>

            @endforeach
        @endif


@endsection