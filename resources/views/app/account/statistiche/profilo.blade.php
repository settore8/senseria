@extends('app.layouts.account')

@section('title', 'Statistiche')

@section('headertitle')
    @component('app.components.headertitle')
    <a href="{{ route('account.profilo') }}" alt="Riepilogo {{ Auth::user()->nomecognome }}">{{ Auth::user()->nomecognome }}</a> <span class="weight-300">/ Statistiche</span>
    @endcomponent
@endsection

@section('headertopright')
    test
@endsection 

@section('content')

    @include('app.partials.menusettingsstatistiche')

    <h2>Statistiche profilo</h2>
    <ul> 
       <li> blocco generale profilo con grafico statistiche con vari filtri </li>
       <li> lista degli utenti che seguono l'utente </li>
    </ul>

@endsection