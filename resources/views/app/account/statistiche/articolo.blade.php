@extends('app.layouts.account')

@section('title', 'Statistiche')

@section('headertitle')
    @component('app.components.headertitle')
    <a href="{{ route('account.profilo') }}" alt="Riepilogo {{ Auth::user()->nomecognome }}">{{ Auth::user()->nomecognome }}</a> <span class="weight-300">/ Statistiche</span>
    @endcomponent
@endsection

@section('headertopright')
    test
@endsection 

@section('content')

    @include('app.partials.menusettingsstatistiche')


    <h2>Statistiche articolo</h2>
    <ul> 
       <li> Blocco delle statistiche dell'articolo singolo </li>
    </ul>
    
@endsection