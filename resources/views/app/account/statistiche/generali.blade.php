@extends('app.layouts.account')

@section('title', 'Statistiche')

@section('headertitle')
    @component('app.components.headertitle')
    <a href="{{ route('account.profilo') }}" alt="Riepilogo {{ Auth::user()->nomecognome }}">{{ Auth::user()->nomecognome }}</a> <span class="weight-300">/ Statistiche</span>
    @endcomponent
@endsection

@section('headertopright')
    test
@endsection 

@section('content')

    @include('app.partials.menusettingsstatistiche')

    
        @block(['class' => 'block--bordered', 'title' => 'Profilo'])
        <div class="columns">
            <div class="column col-4 col-xs-12 stat">
                <div class="stat-count-total">{{ Auth::user()->views()->count() }}</div>
                <div class="stat-count-total-unique">{{ views(Auth::user())->unique()->count() }}</div>
                <div class="stat-label">Visite profilo</div>
            </div>
            <div class="column col-4 col-xs-12 stat">
                <canvas id="visiteProfilo"></canvas>
            </div>
        </div>
        @endblock


        @block(['class' => 'block--bordered', 'title' => 'Articoli'])
        <div class="columns">
            <div class="column col-4 col-xs-12 stat">
                <div class="stat-count-total">{{ Auth::user()->views()->count() }}</div>
                <div class="stat-label">Visite articoli</div>
            </div>
            <div class="column col-4 col-xs-12 stat">
                <canvas id="visiteProfilo"></canvas>
            </div>
        </div>
        @endblock

   

   <ul>
   <h2>Dashboard Statistiche</h2>

   <li> 1) blocco con visite totali (+ torta utenti workook e senseria)</li>
   <li> 2) blocco con visite totali ultimo anno (+ torta utenti workook e senseria)</li>
   <li> 3) blocco con visite totali ultima settimana (+ torta utenti workook e senseria)</li>
   
   <li> 4) blocco con visite articoli totali (+ torta utenti workook e senseria)</li>
   <li> 4) blocco con visite gallerie totali(+ torta utenti workook e senseria)</li>



@endsection


@section('footerjs_after')
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.3/dist/Chart.min.js"></script>

<script>

$(function() {

    var ctx = document.getElementById('visiteProfilo');
    ctx.height = 150;
    var myChart = new Chart(ctx, {
        type: 'pie',
        data: {
            labels: ['Red', 'Blue', 'Yellow'],
            datasets: [{
                label: '# of Votes',
                data: [12, 19, 3],
                backgroundColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    display: false,
                    ticks: {
                        beginAtZero: true
                    }
                }],
                xAxes: [{
                    display: false 
                }
                ]

            }
        }
    });
});

    </script>

@endsection