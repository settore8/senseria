@extends('app.layouts.account')

@section('title', 'Statistiche')

@section('headertitle')
    @component('app.components.headertitle')
    <a href="{{ route('account.profilo') }}" alt="Riepilogo {{ Auth::user()->nomecognome }}">{{ Auth::user()->nomecognome }}</a> <span class="weight-300">/ Statistiche</span>
    @endcomponent
@endsection

@section('headertopright')
    test
@endsection 

@section('content')

    @include('app.partials.menusettingsstatistiche')


    <h2>Statistiche gallerie</h2>
    <ul> 
       <li> blocco generale gallerie con grafico statistiche con vari filtri </li>
       <li> classifica degli gallerie con più like</li>
       <li> classifica degli gallerie con più visite</li>
    </ul>
    
    @if($user->gallerie->where('pubblicata', 1)->isNotEmpty())

    @else

        <h2>Non esistono statistiche per le tue gallerie perchè non hai gallerie pubblicate</h2>

    @endif

@endsection