@extends('app.layouts.account')

@section('title', 'Statistiche')

@section('headertitle')
    @component('app.components.headertitle')
    <a href="{{ route('account.profilo') }}" alt="Riepilogo {{ Auth::user()->nomecognome }}">{{ Auth::user()->nomecognome }}</a> <span class="weight-300">/ Statistiche</span>
    @endcomponent
@endsection

@section('headertopright')
    test
@endsection 

@section('content')

    @include('app.partials.menusettingsstatistiche')

    <h2>Statistiche articoli</h2>

    <ul> 
       <li> blocco generale articoli con grafico statistiche con vari filtri</li>
       <li> classifica degli articoli con più like</li>
       <li> classifica degli articoli con più visite</li>
    </ul>
    
    @if($user->articoli->where('pubblicato', 1)->isNotEmpty())

    @else

        <h2>Non esistono statistiche per i tuoi articoli perchè non hai articoli pubblicati</h2>

    @endif
    
@endsection