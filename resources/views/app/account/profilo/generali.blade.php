@extends('app.layouts.account')

@section('title', 'Profilo generale utente')

@section('headertitle')
    @component('app.components.headertitle')
    <a href="{{ route('account.profilo') }}" alt="Riepilogo {{ Auth::user()->nomecognome }}">{{ Auth::user()->nomecognome }}</a> <span class="weight-300">/ Profilo</span>
    @endcomponent
@endsection


@section('content')

    @include('app.partials.menusettingsprofilo')

    @block()

        <form action="{!! route('account.store.profilo') !!}" method="POST" enctype="multipart/form-data">
            @csrf
        @if($user->isProfessionista())

                    <div class="columns">
                        <div class="column col-6 col-md-12">
                            @textarea(['attrs' => ['label' => 'Raccontati in breve', 'name' => 'descrizione', 'class' => 'showCounter','cols' => '30', 'rows' => '5', 'maxlength' => '200']])
                                {!! $user->meta->descrizione !!}
                            @endtextarea
                        </div>
                        <div class="column col-5 col-md-12 visible-md">
                            <img src="{{ asset('images/app/visual-descrizione.svg')}}">
                        </div>
                    </div>

                    <div class="columns">
                    <div class="column col-6">
                        @inputtext(['attrs' => ['name' => 'numero_iscrizione', 'label' => 'Numero iscrizione', 'class' => 'input-lg', 'value' =>  $user->meta->numero_iscrizione ]]) @endinputtext
                    </div>
                    <div class="column col-6">
                        @inputtext(['attrs' => ['name' => 'numero_albo', 'label' => 'Numero albo', 'class' => 'input-lg', 'value' =>  $user->meta->numero_albo ]]) @endinputtext
                    </div>
                    </div> 

                    <div class="form-group">
                        <label for="pec" class="form-label label-sm">Inizio attivita</label>
                        <input type="text" class="form-input input-lg" name="inizio_attivita" value="{{ $user->meta->inizio_attivita }}">
                    </div>
                    <div class="form-group">
                        <label for="pec" class="form-label label-sm">Inquadramento</label>
                        <select name="inquadramento" id="inquadramento" class="form-select select-lg">
                            @foreach(config('constants.inquadramento-professionista') as $key => $value)
                                <option value="{!! $key !!}" @if($user->meta->inquadramento == $key) checked @endif>{!! $value !!}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="pec" class="form-label label-sm">Partita iva</label>
                        <input type="text" class="form-input input-lg showCounter" name="partita_iva" maxlength="11" value="{{ $user->meta->partita_iva }}">
                    </div>
                    <div class="form-group">
                        <label for="pec" class="form-label label-sm">Cellulare</label>
                        <input type="text" class="form-input input-lg" name="cellulare" value="{{ $user->meta->cellulare }}">
                    </div>

                    <div class="form-group">
                        <label for="address" class="form-label label-sm">Indirizzo <span>*</span></label>
                        <input id="address" class="form-input input-lg {!! $errors->has('addressResult') ? 'is-error' : '' !!} " type="text" size="50" placeholder="Indirizzo">
                        @if ($errors->has('addressResult'))  
                            <p class="form-input-hint form-text text-muted">{!! $errors->first('addressResult') !!}</p>
                        @endif
                        <input type="hidden" id="addressResult" name="addressResult">

                        <input type="hidden" id="comune" name="comune" value="{!!  $user->meta->descrizione_comune !!}">
                        <input type="hidden" id="coordinate_comune" name="coordinate_comune" value="{!! $user->meta->lat !!},{!! $user->meta->lng !!}">
                        <input type="hidden" id="localita" name="localita" value="{!! $user->meta->localita !!}">
                        <input type="hidden" id="indirizzo" name="indirizzo" value="{!! $user->meta->indirizzo !!}">
                        <input type="hidden" id="provincia" name="provincia" value="{!! $user->meta->provincia !!}">
                    </div>
            @else

            <div class="form-group">
                <label for="email_pubblica">Email pubblica
                <input type="text" class="form-input input-lg" name="email_pubblica" value="{{ $user->meta->email_pubblica }}">
                </label>

                <label for="pec">Pec
                <input type="email" class="form-input input-lg" name="pec" value="{{ $user->meta->pec }}">
                </label>
            </div>

            
            <div class="form-group">
                <label for="pec">Descrizione</label>
                <textarea name="descrizione" id="" cols="30" rows="10">{!! $user->meta->descrizione !!}</textarea>
            </div>

            <div class="form-group">
                <label for="pec">Inizio attivita</label>
                <input type="text" class="form-input input-lg" name="inizio_attivita" value="{{ $user->meta->inizio_attivita }}">
            </div>

            <div class="form-group">
                <label for="pec">Tipologia</label>
                <select name="tipologia" id="tipologia">
                    @foreach(config('constants.tipologie-imprese') as $key => $value)
                        <option value="{!! $key !!}" @if($user->meta->tipologia == $key) checked @endif>{!! $value !!}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="pec">Partita iva</label>
                <input type="text" class="form-input input-lg" name="partita_iva" value="{{ $user->meta->partita_iva }}">
            </div>

            <div class="form-group">
                <label for="pec">Cellulare</label>
                <input type="text" class="form-input input-lg" name="cellulare" value="{{ $user->meta->cellulare }}">
            </div>

            <div class="form-group">
                <label for="rappresentante">Rappresentante</label>
                <input type="text" class="form-input input-lg" name="nome_rappresentante" multiple value="{{ $user->meta->nome_rappresentante }}">
                <input type="text" class="form-input input-lg" name="cognome_rappresentante" multiple value="{{ $user->meta->cognome_rappresentante }}">
                <input type="text" class="form-input input-lg" name="titolo_rappresentante" multiple value="{{ $user->meta->titolo_rappresentante }}">
            </div>

            <div class="form-group">
                <label for="address">Indirizzo <span>*</span></label>
                <input id="address" class="form-input input-xl {!! $errors->has('addressResult') ? 'is-error' : '' !!} " type="text" size="50" placeholder="Indirizzo">

                @if ($errors->has('addressResult'))  
                    <p class="form-input-hint form-text text-muted">{!! $errors->first('addressResult') !!}</p>
                @endif
                <input type="hidden" id="addressResult" name="addressResult">
                <input type="hidden" id="comune" name="comune" value="{!!  $user->meta->descrizione_comune !!}">
                <input type="hidden" id="coordinate_comune" name="coordinate_comune" value="{!! $user->meta->lat !!},{!! $user->meta->lng !!}">
                <input type="hidden" id="localita" name="localita" value="{!! $user->meta->localita !!}">
                <input type="hidden" id="indirizzo" name="indirizzo" value="{!! $user->meta->indirizzo !!}">
                <input type="hidden" id="provincia" name="provincia" value="{!! $user->meta->provincia !!}">
            </div>

            @endif

            @button(['attrs' => ['text' => 'Salva']])@endbutton
   
        </form>

   @endblock
@endsection