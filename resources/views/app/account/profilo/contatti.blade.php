@extends('app.layouts.account')

@section('title', 'Profilo contatti utente')

@section('headertitle')
    @component('app.components.headertitle')
    <a href="{{ route('account.profilo') }}" alt="Riepilogo {{ Auth::user()->nomecognome }}">{{ Auth::user()->nomecognome }}</a> <span class="weight-300">/ Profilo</span> <span class="weight-300">/ Contatti</span>
    @endcomponent
@endsection

@section('content')

    @include('app.partials.menusettingsprofilo')

    @block()

    <form action="{!! route('account.store.profilo') !!}" method="POST" enctype="multipart/form-data">
        @csrf
        
        <div class="form-group flex">
            <label for="email_pubblica" class="form-label label-sm item">Email pubblica
            <input type="text" class="form-input input-lg" name="email_pubblica" value="{{ $user->meta->email_pubblica }}">
        </label>

            <label for="pec" class="form-label label-sm item">Pec
            <input type="text" class="form-input input-lg" name="pec" value="{{ $user->meta->pec }}">
        </label>
        </div>
        
        <div class="form-group form-group-submit">
            <button type="submit" class="btn btn-primary">Salva</button>
        </div>

    </form>

    @endblock
@endsection