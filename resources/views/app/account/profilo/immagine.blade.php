@extends('app.layouts.account')

@section('title', 'Profilo immagine utente')

@section('headertitle')
    @component('app.components.headertitle')
    <a href="{{ route('account.profilo') }}" alt="Riepilogo {{ Auth::user()->nomecognome }}">{{ Auth::user()->nomecognome }}</a> <span class="weight-300">/ Profilo</span> <span class="weight-300">/ Immagine</span>
    @endcomponent
@endsection


@section('headertopright')
    test
@endsection 


@section('content')

    @include('app.partials.menusettingsprofilo')

    @block()
    <form action="{!! route('account.store.profilo') !!}" method="POST" enctype="multipart/form-data">
        @csrf

        <div class="form-group">
            <label for="image_profilo" class="form-label label-sm">Immagine profilo</label>
            <input type="file" class="form-input input-lg" name="image_profilo">
        </div>
        
        <div class="form-group form-group-submit">
            <button type="submit" class="btn btn-primary">Salva</button>
        </div>

    </form>
    @endblock

@endsection