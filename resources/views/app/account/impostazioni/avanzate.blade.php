@extends('app.layouts.account')

@section('title', 'Impostazioni')

@section('headertitle')
    @component('app.components.headertitle')
    <a href="{{ route('account.profilo') }}" alt="Riepilogo {{ Auth::user()->nomecognome }}">{{ Auth::user()->nomecognome }}</a> <span class="weight-300">/ Impostazioni</span> <span class="weight-300">/ Avanzate</span>
    @endcomponent
@endsection

@section('headertopright')
    test
@endsection


@section('content')

        @include('app.partials.menusettingsimpostazioni')

        @block(['title' => 'Modifica email', 'class' => 'block--bordered block--warning' ])
        <form class="">
            <div class="columns">
                <div class="column col-6 col-xs-12">
                    <p>Per cambiare l'indirizzo email di accesso a Workook, inserisci il nuovo indirizzo email che desideri utilizzare.
                        <br/><strong>Se disponibile riceverai un'email per confermare l'operazione</strong></p>
                </div>
                <div class="column col-5 col-ml-auto col-xs-12">
                    <div class="form-group">
                        <label>Nuovo indirizzo Email</label>
                        <input type="text" class="form-input input-lg" name="email">
                    </div>
                    <div class="form-group">
                        <label>La tua password</label>
                        <input type="text" class="form-input input-lg" name="email">
                    </div>
                    @button(['attrs' => ['text' => 'Modifica email', 'class' => 'btn-warning btn-padding']])@endbutton
                </div>
            </div>
        </form>
        @endblock

        @block(['title' => 'Cancellazione account', 'class' => 'block--bordered block--danger' ])
        <form action="{{route('account.cancella')}}" method="POST" class="">
            @csrf
            <div class="columns">
                <div class="column col-6 col-xs-12">
                    <p>Per cancellare l'account Workook, inserire la password di accesso e premere su Cancella account.<br/><strong>Riceverai un'email con un link per confermare l'operazione</strong>.</p>
                </div>
                <div class="column col-5 col-ml-auto col-xs-12">
                    <div class="form-group">
                        <label>La tua password</label>
                        <input type="password" class="form-input input-lg" name="password">
                    </div>
                    @button(['attrs' => ['text' => 'Cancella account', 'class' => 'btn-error btn-padding']])@endbutton
                </div>
            </div>
        </form>
        @endblock

@endsection
