@extends('app.layouts.account')

@section('title', 'Impostazioni')

@section('headertitle')
    @component('app.components.headertitle')
    <a href="{{ route('account.profilo') }}" alt="Riepilogo {{ Auth::user()->nomecognome }}">{{ Auth::user()->nomecognome }}</a> <span class="weight-300">/ Impostazioni</span> <span class="weight-300">/ Notifiche</span>
    @endcomponent
@endsection

@section('content')

        @include('app.partials.menusettingsimpostazioni')

        @block(['title' => 'Gestione notifiche', 'class' => 'block--bordered' ])

        <p>Da questa sezione puoi attivare o disattivare le notifiche Web e le notifiche Email</p>

        <form class="form-horizontal form-options">

            <fieldset class="fieldset">

                <legend>Intorno a te</legend>

                <div class="form-group">
                    <div class="col-8 col-lg-9">
                        <label  class="form-label">Desidero ricevere aggiornamenti su nuovi professionisti e imprese che si registrano nella mia area operativa</label>
                    </div>
                    <div class="col-2 col-lg-3 col-ml-auto">
                        @toggle(['attrs' => ['name' => 'email_nuovo_professionista']]) @endtoggle
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-8 col-lg-9">
                        <label  class="form-label">Desidero sapere quando un professionista o impresa che seguo pubblica un articolo</label>
                    </div>
                    <div class="col-2 col-lg-3 col-ml-auto">
                        @toggle(['attrs' => ['name' => 'email_nuovo_articolo']]) @endtoggle
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-8 col-lg-9">
                        <label  class="form-label">Desidero sapere quando un professionista o impresa che seguo pubblica una galleria</label>
                    </div>
                    <div class="col-2 col-lg-3 col-ml-auto">
                        @toggle(['attrs' => ['name' => 'email_nuovo_articolo']]) @endtoggle
                    </div>
                </div>

            </fieldset>


            <fieldset class="fieldset">
                <legend>Immobili</legend>

                <div class="form-group">
                    <div class="col-8 col-lg-9">
                        <label class="form-label">Avvisami se mi viene assegnato il ruolo di responsabile tecnico di un immobile</label>
                    </div>
                    <div class="col-2 col-lg-3 col-ml-auto">
                        @toggle(['attrs' => ['name' => 'email_responsabile_tecnico']]) @endtoggle
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-8 col-lg-9">
                        <label class="form-label">Avvisami quando è presente un annuncio per responsabile tecnico</label>
                    </div>
                    <div class="col-2 col-lg-3 col-ml-auto">
                        @toggle(['attrs' => ['name' => 'email_responsabile_tecnico']]) @endtoggle
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-8 col-lg-9">
                        <label  class="form-label">Quando scelgo o cerco un responsabile tecnico per un immobile tienimi aggiornato sulle candidature</label>
                    </div>
                    <div class="col-2 col-lg-3 col-ml-auto">
                        @toggle(['attrs' => ['name' => 'email_responsabile_tecnico']]) @endtoggle
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-8 col-lg-9">
                        <label  class="form-label">Avvisami quando è presente un annuncio di collaborazione</label>
                    </div>
                    <div class="col-2 col-lg-3 col-ml-auto">
                        @toggle(['attrs' => ['name' => 'email_responsabile_tecnico']]) @endtoggle
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-8 col-lg-9">
                        <label  class="form-label">Quando mi candito ad un annuncio di collaborazione tienimi aggiornato</label>
                    </div>
                    <div class="col-2 col-lg-3 col-ml-auto">
                        @toggle(['attrs' => ['name' => 'email_responsabile_tecnico']]) @endtoggle
                    </div>
                </div>

            </fieldset>

            @button(['attrs' => ['text' => 'Salva', 'class' => 'btn btn-primary btn-padding mlauto mrauto mr0--sm']])@endbutton
        </form>

        @endblock
        
    
@endsection