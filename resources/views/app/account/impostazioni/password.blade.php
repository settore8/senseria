@extends('app.layouts.account')

@section('title', 'Profilo')

@section('headertitle')
    @component('app.components.headertitle')
    <a href="{{ route('account.profilo') }}" alt="Riepilogo {{ Auth::user()->nomecognome }}">{{ Auth::user()->nomecognome }}</a> <span class="weight-300">/ Impostazioni</span>  <span class="weight-300">/ Password</span>
    @endcomponent
@endsection

@section('headertopright')
    test
@endsection 


@section('content')

        @include('app.partials.menusettingsimpostazioni')

        @block(['title' => 'Modifica password', 'class' => 'block--bordered' ])

        <form action="{!! route('account.store.password') !!}" method="POST">
            <div class="columns">
                <div class="column col-6 col-xs-12">
                    <p>Per cambiare la password di accesso a Workook, inserisci l'attuale password e scegline una nuova.
                    <br/><strong>Se non ricordi l'attuale password puoi cambiare password tramite la procedura di <a href="{{ route('auth.password.request.email')}}" target="_blank">recupero password</a>.</strong></p>
                </div>
                <div class="column col-5 col-ml-auto col-xs-12">
                    
                    <div class="form-group">
                        <label for="password" class="form-label">Password</label>
                        <input type="password" name="password" class="form-input" >
                    </div>
        
                    <div class="form-group">
                        <label for="password" class="form-label">Nuova Password</label>
                        <input type="password" name="password" class="form-input">
                    </div>
        
                    <div class="form-group">
                        <label for="password_confirmation" class="form-label">Conferma Nuova Password</label>
                        <input type="password" name="password_confirmation" class="form-input">
                    </div>


                    @button(['attrs' => ['text' => 'Modifica password']])@endbutton
                </div>
            </div>
        </form>
        @endblock


@endsection