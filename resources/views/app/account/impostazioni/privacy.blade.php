@extends('app.layouts.account')

@section('title', 'Impostazioni')

@section('headertitle')
    @component('app.components.headertitle')
    <a href="{{ route('account.profilo') }}" alt="Riepilogo {{ Auth::user()->nomecognome }}">{{ Auth::user()->nomecognome }}</a> <span class="weight-300">/ Impostazioni</span> <span class="weight-300">/ privacy</span>
    @endcomponent
@endsection

@section('headertopright')
    test
@endsection 


@section('content')

        @include('app.partials.menusettingsimpostazioni')


        @block(['title' => 'Privacy', 'class' => 'block--bordered' ])

        <form action="{!! route('account.store.password') !!}" method="POST">
            <div class="columns">
                <div class="column col-6 col-xs-12">
                    <p>QUi inseriamo le varie preferenze sulla privacy.
                        Nel dettaglio mostriamo readonly l'accettazione alla privacy ed ai termini e condizioni con la data di registrazione.
                        </p>
                </div>
                <div class="column col-5 col-ml-auto col-xs-12">
                    Facciamo un checkbox per attivare o disattivare le informazioni commerciali (sostanzialmente si attiva e disattiva )

                    @button(['attrs' => ['text' => 'Salva']])@endbutton
                </div>
            </div>
        </form>
        @endblock

@endsection