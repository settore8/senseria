@extends('app.layouts.account')

@section('title', 'Certificazione utente')


@section('headertitle')
    @component('app.components.headertitle')
    <a href="{{ route('account.profilo') }}" alt="Riepilogo {{ Auth::user()->nomecognome }}">{{ Auth::user()->nomecognome }}</a> <span class="weight-300">/ Certificazione</span>
    @endcomponent
@endsection


@section('content')

    @block(['class' => 'block--bordered' ])
    @switch($status)
        @case('attesa')
                <div class="columns">
                    <div class="column col-6 col-xs-12">
                    <h3>In lavorazione</h3>
                    <p>La tua richiesta è in lavorazione.<br/>Lo staff di Workook sta verificando i tuoi dati.</p>
                    </div>
                    <div class="column col-6 col-xs-12">
                    <img src="{{ asset('images/app/visual-certificazione-lavorazione.svg') }}" alt="In lavorazione" class="max-width-200">
                    </div>
                </div>
            @break
        @case('attiva')
            <div class="columns">
                    <div class="column col-6 col-xs-12">
                        <h3>Attiva</h3>
                        <p>Complimenti!<br/>Lo staff di Workook ha verificato i tuoi dati.</p>
                        <p>Il tuo account Workook è correttamente certificato!</p>
                    </div>
                    <div class="column col-6 col-xs-12">
                        <img src="{{ asset('images/app/visual-certificazione-attiva.svg') }}" alt="Attiva" class="max-width-200">
                    </div>
                </div>
            @break
        @case('annullata')
            
            @break
        @case('non_presente')
            <div class="columns">
                <div class="column col-6 col-xs-12">
                    <h3>Certifica il tuo profilo su Workook!</h3>
                    <p>Inviaci la visura camerale per poter verificare la tua impresa.</p>
                    <form method="POST" action="{!! route('account.store.certificazione') !!}" enctype="multipart/form-data">
                        @csrf
                        <input type="file" name="certificazione" accept="application/pdf">
                        @button(['attrs' => ['text' => 'Salva']])@endbutton
                    </form>
                </div>
                <div class="column col-6 col-xs-12">
                    <img src="{{ asset('images/app/visual-certificazione.svg') }}" alt="Richiesta" class="max-width-200">
                </div>
            </div>
            @break

        @default
    @endswitch

    @endblock
    
@endsection