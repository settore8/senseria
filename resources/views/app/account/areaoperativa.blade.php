@extends('app.layouts.account')

@section('title', 'Area operativa utente')

@section('headertitle')
    @component('app.components.headertitle')
    <a href="{{ route('account.profilo') }}" alt="Riepilogo {{ Auth::user()->nomecognome }}">{{ Auth::user()->nomecognome }}</a> <span class="weight-300">/ Area operativa</span>
    @endcomponent
@endsection


@section('content')
       
        @suggerimento
        @slot('label', '?')
        @slot('visibile', Auth::user()->viewSuggerimento('tip_user_areaoperativa'))
        @slot('datatip', 'tip_user_areaoperativa')
        @endsuggerimento

        <div class="flex">
            <div class="item panel">
                <div id="map" style="height:500px;" data-editable="true" data-draggable="true"></div>
            </div>
        </div>
        
        <div id="resetPolygon" class="btn btn-outline-primary">Nuova area</div>
        <form action="{!! route('account.store.area') !!}" method="POST">
            @csrf
            
            <input id="area" name="area" type="hidden" value="{!! Auth::user()->area_operativa !!}">
            @button(['attrs' => ['text' => 'Salva']])@endbutton
        </form>
@endsection
