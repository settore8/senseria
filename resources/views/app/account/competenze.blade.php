@extends('app.layouts.account')

@section('title', 'Competenze utente')


@section('headertitle')
    @component('app.components.headertitle')
        <a href="{{ route('account.profilo') }}" alt="Riepilogo {{ Auth::user()->nomecognome }}">{{ Auth::user()->nomecognome }}</a> <span class="weight-300">/ Competenze</span>
    @endcomponent
@endsection


@section('content')

        @suggerimento
            @slot('label', '?')
            @slot('visibile', Auth::user()->viewSuggerimento('tip_user_competenze'))
            @slot('datatip', 'tip_user_competenze')
        @endsuggerimento

        <div class="flex">
            <div class="item panel">
                    <form action="{!! route('account.store.competenze') !!}" method="POST">
                        @csrf
                       
                        @include('includes.partials.editcompetenze', ['competenze' => $competenze])

                        @button(['attrs' => ['text' => 'Salva', 'class' => 'btn btn-primary btn-padding nopreloader']])@endbutton

                    </form>
            </div>

        </div>

@endsection