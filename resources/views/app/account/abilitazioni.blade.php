@extends('app.layouts.account')

@section('title', 'Abilitazioni utente')


@section('headertitle')
    @component('app.components.headertitle')
    <a href="{{ route('account.profilo') }}" alt="Riepilogo {{ Auth::user()->nomecognome }}">{{ Auth::user()->nomecognome }}</a> <span class="weight-300">/ Abilitazioni</span>
    @endcomponent
@endsection


@section('content')
        @block(['title' => 'Abilitazioni', 'class' => 'block--bordered' ])
        <form method="POST" action="{!! route('account.store.abilitazioni') !!}">
            @csrf
            <div class="columns">
                <div class="column col-6 col-sm-12">
                    <p> Seleziona le tue abilitazioni</p>
                </div>
                <div class="column col-6 col-sm-12">
                @foreach ($abilitazioni as $abilitazione)
                    <div class="form-group">
                        <label for="check-{{ $abilitazione->id }}" class="form-switch">
                        <input type="checkbox" id="check-{{ $abilitazione->id }}" value="{!! $abilitazione->id !!}" name="abilitazioni[]" multiple {!! Auth::user()->abilitazioni->where('id',$abilitazione->id)->isNotEmpty() ? 'checked' : '' !!}>
                        <i class="form-icon"></i> {!! $abilitazione->nome_pubblico !!}
                    </label>
                    </div>
                @endforeach
                @button(['attrs' => ['text' => 'Salva']])@endbutton
                </div>
            </div>
        </form>
        @endblock

@endsection