@extends('app.layouts.account')

@section('title', 'Raccolte')
@section('bodyclass')@parent {{ 'raccolte' }}@endsection

@section('headertitle')
    @component('app.components.headertitle')
    <a href="{{ route('account.profilo') }}" alt="Riepilogo {{ Auth::user()->nomecognome }}">{{ Auth::user()->nomecognome }}</a> <span class="weight-300">/ Raccolte</span>
    @endcomponent
@endsection

@section('content')

    @include('app.partials.menuraccolte')

    @if($raccolte->isNotEmpty())
        <div class="columns">
        @foreach($raccolte as $raccolta)

            @if($raccolta->coverable_type == get_class(new App\User))
                 <div class="column col-12 p10 raccolta">
                        <a href="{{ route('profilo', $raccolta->coverable->slug) }}" class="card card--raccolta">
                        {{ $raccolta->coverable->fullname }}
                        <button type="button" class="tooltip hideRaccolta mlauto" data-tooltip="Rimuovi" data-raccolta="{{ encrypt( get_class(new App\User) ) }}" data-id="{{ encrypt($raccolta->coverable->id) }}" data-author="{{ encrypt($raccolta->coverable->id) }}">
                            rimuovi
                        </button>
                        </a>
                </div>
            @endif

            @if($raccolta->coverable_type == get_class(new App\Articolo))
                <div class="column col-12 p10 raccolta">
                    <a href="{{ route('articolo', ['slug' => $raccolta->coverable->slug, 'categoria' => $raccolta->coverable->categoria->slug]) }}"  class="card card--raccolta">
                        {{ $raccolta->coverable->titolo }}
                        {{ $raccolta->coverable->user->fullname }}
                        <button type="button" class="tooltip hideRaccolta mlauto" data-tooltip="Rimuovi" data-raccolta="{{ encrypt( get_class(new App\Articolo) ) }}" data-id="{{ encrypt($raccolta->coverable->id) }}" data-author="{{ encrypt($raccolta->coverable->user_id) }}">
                            rimuovi
                        </button>
                    </a>
                </div>
            @endif

            @if($raccolta->coverable_type == get_class(new App\Galleria))
                <div class="column col-12 p10 raccolta">
                    <a href="{{ route('galleria', $raccolta->coverable->slug) }}"  class="card card--raccolta">
                        {{ $raccolta->coverable->titolo }}
                        {{ $raccolta->coverable->user->fullname }}
                        <button type="button" class="tooltip hideRaccolta mlauto" data-tooltip="Rimuovi" data-raccolta="{{ encrypt( get_class(new App\Galleria) ) }}" data-id="{{ encrypt($raccolta->coverable->id) }}" data-author="{{ encrypt($raccolta->coverable->user_id) }}">
                            rimuovi
                        </button>
                    </a>
                </div>
            @endif

            @if($raccolta->coverable_type == get_class(new App\Annuncio))
                <div class="column col-12 p10 raccolta">
                    <a href="{{ route('annuncio', $raccolta->coverable) }}"  class="card card--raccolta">

                    {{ config('constants.oggetto-annunci')[$raccolta->coverable->type] }}

                    @if($raccolta->coverable->type == 'immobili')
                    {{ $raccolta->coverable->immobile->specifica->nome_pubblico }}
                    {{ $raccolta->coverable->immobile->informazione->comune }}
                    {{ $raccolta->coverable->immobile->informazione->metri_quadri }}
                    {{ $raccolta->coverable->creatore->fullname }}
                    
                    @elseif($raccolta->coverable->type == 'acquirenti')
                    {{ $raccolta->coverable->acquirente->specifica->nome_pubblico }}
                    @endif
                    <button type="button" class="tooltip hideRaccolta" data-tooltip="Rimuovi" data-raccolta="{{ encrypt( get_class(new App\Annuncio) ) }}" data-id="{{ encrypt($raccolta->coverable->id) }}" data-author="{{ encrypt($raccolta->coverable->creatore_id) }}">
                        rimuovi
                    </button>
                    </a>
                </div>
            @endif


        @endforeach

        </div>

        {{ $raccolte->links() }}
    @else

            @emptyblock
                @slot('visual'){{ asset('images/app/visual-nuovo-immobile.svg') }}@endslot
                @slot('title')Nessuna raccolta presente @isset($keyword) per il termine <strong>{{ $keyword }}</strong> @endisset @endslot
                @slot('subtitle')Aggiungi utenti, annunci e contenuti alle tue raccolte per averli sempre a disposizione in ogni istante!@endslot
            @endemptyblock

    @endif
    
@endsection
