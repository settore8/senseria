@extends('app.layouts.account')

@section('title', 'Impostazioni')

@section('bodyclass')@parent abbonamento @endsection


@section('headertitle')
    @component('app.components.headertitle')
        <a href="{{ route('account.profilo') }}"
           alt="Riepilogo {{ Auth::user()->nomecognome }}">{{ Auth::user()->nomecognome }}</a> <span class="weight-300">/ Abbonamento</span>
        <span class="weight-300">/ Modifica</span>
    @endcomponent
@endsection

@section('headertopright')

@endsection


@section('content')

    @include('app.partials.menusettingsabbonamento')

    <div class="abbonamenti">

        <div class="abbonamento titoli">

            <div class="abbonamento--body">
                <div class="riga titolo">
                    Profilo
                </div>
                <div class="riga sottotitolo">
                    Competenze
                </div>
                <div class="riga sottotitolo">
                    Abilitazioni
                </div>
                <div class="riga sottotitolo">
                    Area operativa
                </div>
                <div class="riga sottotitolo">
                    Certificazione
                </div>
                <div class="riga titolo">
                    Acquirenti
                </div>
                <div class="riga titolo">
                    Articoli
                </div>
                <div class="riga titolo">
                    Gallerie
                </div>
                <div class="riga titolo">
                    Preferiti
                </div>
                <div class="riga titolo">
                    Immobili
                </div>
                <div class="riga sottotitolo">
                    Sopralluoghi
                </div>
                <div class="riga sottotitolo">
                    Verifiche Tecniche
                </div>
                <div class="riga sottotitolo">
                    Schede Tecniche
                </div>
                <div class="riga sottotitolo">
                    Relazioni Tecniche
                </div>
                <div class="riga sottotitolo">
                    Stima
                </div>
                <div class="riga titolo">
                    Conversazioni
                </div>

            </div>

        </div>


        <div class="abbonamento @if(auth::user()->abbonamento_attuale()->plan == 'Free') active @endif">
            <div class="abbonamento--header">
                <h2>Free</h2>
            </div>

            <div class="abbonamento--body">
                <div class="riga">
                    <div class="title">Profilo</div>
                    <svg class="icon icon--25 icon--success">
                        <use xlink:href="#icon-check"></use>
                        <title>Si</title></svg>
                </div>
                <div class="riga">
                    <div class="title">Competenze</div>
                    <svg class="icon icon--25 icon--success">
                        <use xlink:href="#icon-check"></use>
                        <title>Si</title></svg>
                </div>
                <div class="riga">
                    <div class="title">Area operativa</div>
                    <svg class="icon icon--25 icon--success">
                        <use xlink:href="#icon-check"></use>
                        <title>Si</title></svg>
                </div>
                <div class="riga">
                    <div class="title">Abilitazioni</div>
                    <svg class="icon icon--25 icon--success">
                        <use xlink:href="#icon-check"></use>
                        <title>Si</title></svg>
                </div>
                <div class="riga">
                    <div class="title">Certificazione</div>
                    <svg class="icon icon--25 icon--success">
                        <use xlink:href="#icon-check"></use>
                        <title>Si</title></svg>
                </div>
                <div class="riga">
                    <div class="title">Acquirenti</div>
                    <svg class="icon icon--25 icon--success">
                        <use xlink:href="#icon-check"></use>
                        <title>Si</title></svg>
                </div>
                <div class="riga">
                    <div class="title">Articoli</div>
                    <svg class="icon icon--25 icon--success">
                        <use xlink:href="#icon-check"></use>
                        <title>Si</title></svg>
                </div>
                <div class="riga">
                    <div class="title">Gallerie</div>
                    <svg class="icon icon--25 icon--success">
                        <use xlink:href="#icon-check"></use>
                        <title>Si</title></svg>
                </div>
                <div class="riga">
                    <div class="title">Preferiti</div>
                    <svg class="icon icon--25 icon--success">
                        <use xlink:href="#icon-check"></use>
                        <title>Si</title></svg>
                </div>
                <div class="riga">
                    <div class="title">Immobili</div>
                    <svg class="icon icon--25 icon--success">
                        <use xlink:href="#icon-check"></use>
                        <title>Si</title></svg>
                </div>
                <div class="riga">
                    <div class="title">Sopralluogo</div>
                    <svg class="icon icon--25 icon--success">
                        <use xlink:href="#icon-check"></use>
                        <title>Si</title></svg>
                </div>
                <div class="riga">
                    <div class="title">Verifiche tecniche</div>
                    <svg class="icon icon--25 icon--success">
                        <use xlink:href="#icon-check"></use>
                        <title>Si</title></svg>
                </div>
                <div class="riga riga-x3">
                    <div class="title">Schede tecniche, relazioni tecniche o stime</div>
                    <div class="text">5</div>
                </div>
                <div class="riga">
                    <div class="title">Conversazioni</div>
                    <svg class="icon icon--25 icon--success">
                        <use xlink:href="#icon-check"></use>
                        <title>Si</title></svg>
                </div>

            </div>

            <div class="abbonamento--footer">
                <div class="price">
                    Gratis
                </div>
            </div>
        </div>

        <div class="abbonamento @if(auth::user()->abbonamento_attuale()->plan == 'Premium') active @endif">

            <div class="abbonamento--header">
                <h2>{{$plans->where('slug', 'premium')->first()->name}}</h2>
            </div>

            <div class="abbonamento--body">
                <div class="riga">
                    <div class="title">Profilo</div>
                    <svg class="icon icon--25 icon--success">
                        <use xlink:href="#icon-check"></use>
                        <title>Si</title></svg>
                </div>
                <div class="riga">
                    <div class="title">Competenze</div>
                    <svg class="icon icon--25 icon--success">
                        <use xlink:href="#icon-check"></use>
                        <title>Si</title></svg>
                </div>
                <div class="riga">
                    <div class="title">Area operativa</div>
                    <svg class="icon icon--25 icon--success">
                        <use xlink:href="#icon-check"></use>
                        <title>Si</title></svg>
                </div>
                <div class="riga">
                    <div class="title">Abilitazioni</div>
                    <svg class="icon icon--25 icon--success">
                        <use xlink:href="#icon-check"></use>
                        <title>Si</title></svg>
                </div>
                <div class="riga">
                    <div class="title">Certificazione</div>
                    <svg class="icon icon--25 icon--success">
                        <use xlink:href="#icon-check"></use>
                        <title>Si</title></svg>
                </div>
                <div class="riga">
                    <div class="title">Acquirenti</div>
                    <svg class="icon icon--25 icon--success">
                        <use xlink:href="#icon-check"></use>
                        <title>Si</title></svg>
                </div>
                <div class="riga">
                    <div class="title">Articoli</div>
                    <svg class="icon icon--25 icon--success">
                        <use xlink:href="#icon-check"></use>
                        <title>Si</title></svg>
                </div>
                <div class="riga">
                    <div class="title">Gallerie</div>
                    <svg class="icon icon--25 icon--success">
                        <use xlink:href="#icon-check"></use>
                        <title>Si</title></svg>
                </div>
                <div class="riga">
                    <div class="title">Preferiti</div>
                    <svg class="icon icon--25 icon--success">
                        <use xlink:href="#icon-check"></use>
                        <title>Si</title></svg>
                </div>
                <div class="riga">
                    <div class="title">Immobili</div>
                    <svg class="icon icon--25 icon--success">
                        <use xlink:href="#icon-check"></use>
                        <title>Si</title></svg>
                </div>
                <div class="riga">
                    <div class="title">Sopralluogo</div>
                    <svg class="icon icon--25 icon--success">
                        <use xlink:href="#icon-check"></use>
                        <title>Si</title></svg>
                </div>
                <div class="riga">
                    <div class="title">Verifiche tecniche</div>
                    <svg class="icon icon--25 icon--success">
                        <use xlink:href="#icon-check"></use>
                        <title>Si</title></svg>
                </div>
                <div class="riga riga-x3">
                    <div class="title">Schede tecniche, relazioni tecniche o stime</div>
                    <div class="text">25</div>
                </div>
                <div class="riga titolo">
                    <div class="title">Conversazioni</div>
                    <svg class="icon icon--25 icon--success">
                        <use xlink:href="#icon-check"></use>
                        <title>Si</title></svg>
                </div>

            </div>

            <div class="abbonamento--footer">
                <div class="price">
                    {{$plans->where('slug', 'premium')->first()->cost}}<span class="valuta">€</span><span class="anno">/anno</span>
                </div>
                @if(auth::user()->abbonamento_attuale()->plan == 'Premium')
                    <a>Il Tuo Piano Attuale</a>
                @else
                    <a href="{{route('account.abbonamento.show', $plans->where('slug', 'premium')->first())}}"
                       class="btn btn-primary">
                        Acquista</a>
                @endif
            </div>

        </div>

        <div class="abbonamento @if(auth::user()->abbonamento_attuale()->plan == 'Unlimited') active @endif">
            <div class="abbonamento--header">
                <h2>{{$plans->where('slug', 'unlimited')->first()->name}}</h2>
            </div>

            <div class="abbonamento--body">
                <div class="riga">
                    <div class="title">Profilo</div>
                    <svg class="icon icon--25 icon--success">
                        <use xlink:href="#icon-check"></use>
                        <title>Si</title></svg>
                </div>
                <div class="riga">
                    <div class="title">Competenze</div>
                    <svg class="icon icon--25 icon--success">
                        <use xlink:href="#icon-check"></use>
                        <title>Si</title></svg>
                </div>
                <div class="riga">
                    <div class="title">Area operativa</div>
                    <svg class="icon icon--25 icon--success">
                        <use xlink:href="#icon-check"></use>
                        <title>Si</title></svg>
                </div>
                <div class="riga">
                    <div class="title">Abilitazioni</div>
                    <svg class="icon icon--25 icon--success">
                        <use xlink:href="#icon-check"></use>
                        <title>Si</title></svg>
                </div>
                <div class="riga">
                    <div class="title">Certificazione</div>
                    <svg class="icon icon--25 icon--success">
                        <use xlink:href="#icon-check"></use>
                        <title>Si</title></svg>
                </div>
                <div class="riga">
                    <div class="title">Acquirenti</div>
                    <svg class="icon icon--25 icon--success">
                        <use xlink:href="#icon-check"></use>
                        <title>Si</title></svg>
                </div>
                <div class="riga">
                    <div class="title">Articoli</div>
                    <svg class="icon icon--25 icon--success">
                        <use xlink:href="#icon-check"></use>
                        <title>Si</title></svg>
                </div>
                <div class="riga">
                    <div class="title">Gallerie</div>
                    <svg class="icon icon--25 icon--success">
                        <use xlink:href="#icon-check"></use>
                        <title>Si</title></svg>
                </div>
                <div class="riga">
                    <div class="title">Preferiti</div>
                    <svg class="icon icon--25 icon--success">
                        <use xlink:href="#icon-check"></use>
                        <title>Si</title></svg>
                </div>
                <div class="riga">
                    <div class="title">Immobili</div>
                    <svg class="icon icon--25 icon--success">
                        <use xlink:href="#icon-check"></use>
                        <title>Si</title></svg>
                </div>
                <div class="riga">
                    <div class="title">Sopralluogo</div>
                    <svg class="icon icon--25 icon--success">
                        <use xlink:href="#icon-check"></use>
                        <title>Si</title></svg>
                </div>
                <div class="riga">
                    <div class="title">Verifiche tecniche</div>
                    <svg class="icon icon--25 icon--success">
                        <use xlink:href="#icon-check"></use>
                        <title>Si</title></svg>
                </div>
                <div class="riga riga-x3">
                    <div class="title">Schede tecniche, relazioni tecniche o stime</div>
                    <div class="text">illimitate</div>
                </div>
                <div class="riga titolo">
                    <div class="title">Conversazioni</div>
                    <svg class="icon icon--25 icon--success">
                        <use xlink:href="#icon-check"></use>
                        <title>Si</title></svg>
                </div>

            </div>

            <div class="abbonamento--footer">
                <div class="price">
                    {{$plans->where('slug', 'unlimited')->first()->cost}}<span class="valuta">€</span><span
                        class="anno">/anno</span>
                </div>
                @if(auth::user()->abbonamento_attuale()->plan == 'Unlimited')
                    <a>Il Tuo Piano Attuale</a>
                @else
                    <a href="{{route('account.abbonamento.show', $plans->where('slug', 'unlimited')->first())}}"
                       class="btn btn-primary">
                        Acquista</a>
                @endif

            </div>

        </div>

    </div>

@endsection
