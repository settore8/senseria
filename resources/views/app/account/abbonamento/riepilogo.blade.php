@extends('app.layouts.account')

@section('title', 'Abbonamento utente')

@section('bodyclass')@parent abbonamento @endsection


@section('headertitle')
    @component('app.components.headertitle')
    <a href="{{ route('account.profilo') }}" alt="Riepilogo {{ Auth::user()->nomecognome }}">{{ Auth::user()->nomecognome }}</a> <span class="weight-300">/ Abbonamento</span>
    @endcomponent
@endsection

@section('headertopright')

@endsection

@section('content')

    @include('app.partials.menusettingsabbonamento')

    <div class="item item-2">
        <div class="dashboard-block">
            <div class="dashboard-block__header">Il tuo attuale abbonamento</div>
            <label class="">Il tuo abbonamento attuale è: {{auth::user()->abbonamento_attuale()->plan}}</label>
            <label class="">Il prossimo rinnovo sarà: {{auth::user()->abbonamento_attuale()->end_at}}</label>
        </div>
    </div>

@endsection
