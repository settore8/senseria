@extends('app.layouts.master')

@section('title', 'Gestione immobili')

@section('headertitle')
    @component('app.components.headertitle')
        Immobili
        <a href="{{ url(route('immobile.create')) }}" class="btn-add tooltip tooltip-right" data-tooltip="Nuovo immobile">+</a>
    @endcomponent
@endsection

@section('headersearch')
<!--
<form class="item item--center">
<div class="form-group form-group-search">
    <input type="text" class="form-input input-lg input-search input-rounded" placeholder="Cerca immobile">
    <svg><use xlink:href="#icon-search"></use></svg>
</div>
</form>
-->
@endsection


@section('headermenu')
    @component('app.components.headermenu')
    @include('app.partials.headermenuimmobili')
    @endcomponent
@endsection


@section('content')
@if($immobili->isNotEmpty())

<section class="columns">
@foreach($immobili as $immobile)
        <div class="column col-4 col-xs-12 col-md-6 pb8">
            <article class="card card--immobile">
                <a href="{{ route('immobile', $immobile) }}">
                    
                    <svg><use xlink:href="#destinazione-{{ strtolower($immobile->destinazione->nome) }}"></use></svg>

                    <div class="c100 perc70">
                        <span>70%</span>
                        <div class="slice">
                            <div class="bar"></div>
                            <div class="fill"></div>
                        </div>
                    </div>
                    
                    <h2>
                    {{ $immobile->nome }}
                    </h2>

                    <p>{{ $immobile->specifica->nome_pubblico }} </p>
                    @if($immobile->informazione)
                        {{ $immobile->informazione->metri_quadri }} Mq
                        <small>{{ $immobile->informazione->comune }}</small>
                    @endif

                    @if($immobile->tecnico)
                        Resp tecnico: {{ $immobile->tecnico->nomecognome }}
                    @endif

                    @if($immobile->agente)
                        Resp agente: {{ $immobile->agente->nomecognome }}
                    @endif
                </a>
            </article>
        </div>
@endforeach
</section>

{{ $immobili->links() }}

@else
        <div class="empty text-center col-8 col-mx-auto myauto">
            <div class="columns align-items-center">
                <div class="column col-6 col-sm-12">
                    <img src="{{ asset('images/app/visual-nuovo-immobile.svg') }}" alt="Crea immobile">
                </div>
                <div class="column col-6 col-sm-12">
                    <p class="empty-title h3">Inserisci il tuo primo immobile!</p>
                    <p class="empty-subtitle">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    <div class="empty-action">
                    <a href="{{ route('immobile.create') }}" class="btn btn-lg btn-secondary"><span class="new">+</span> Nuovo immobile</a>
                    <div><small><a href="#" data-offcanvas="helpimmobili">Per saperne di più</a></small></div>
                </div>
            </div>
            </div>
        </div>
    
@endif

@endsection

