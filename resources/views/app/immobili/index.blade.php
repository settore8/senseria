@extends('app.layouts.master')

@section('title', 'Immobili')

@section('headertitle')
    @component('app.components.headertitle')
        <a href="{{ url(route('immobili')) }}" alt="Immobili">Immobili</a>
        <a href="{{ url(route('immobile.create')) }}" class="btn-add tooltip tooltip-right" data-tooltip="Nuovo immobile">+</a>
    @endcomponent
@endsection

@section('headersearch')
    @include('app.partials.searchformimmobili')
@endsection


@section('headermenu')
    @component('app.components.headermenu')
    @include('app.partials.headermenuimmobili')
    @endcomponent
@endsection


@section('content')
@if($immobili->isNotEmpty())
    @block()
    <div class="columns">
        @foreach($immobili as $immobile)
                <div class="column col-4 col-xs-12 col-md-6 pb8">
                        @include('app.partials.previewimmobile', ['immobile' => $immobile])
                </div>
        @endforeach
    </div>

    {{ $immobili->links() }}

    @endblock
@else
        @emptyblock
            @slot('visual'){{ asset('images/app/visual-nuovo-immobile.svg') }}@endslot
            @slot('title')Inserisci il tuo primo immobile!@endslot
            @slot('subtitle')Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua!@endslot
            @slot('action')
                    <a href="{{ route('immobile.create') }}" class="btn btn-lg btn-secondary">Nuovo immobile</a>
                    <div><small><a href="#" data-offcanvas="helpimmobili">Per saperne di più</a></small></div>
            @endslot
        @endemptyblock
@endif

@endsection

@section('footerjs_after')
<script src="https://cdnjs.cloudflare.com/ajax/libs/easy-pie-chart/2.1.6/jquery.easypiechart.min.js"></script>

<script>
$(function() {
    
    $('.chart').easyPieChart({
        size: 45,
        scaleLength: 0,
        barColor: function (percent) {
            return (percent < 20 ? '#d20b0b' : percent < 50 ? '#f38910' : percent < 70 ? '#eedf14' : percent < 85 ? '#c5d212' : '#42d212');
        },
        lineWidth: 7,
        trackColor: "#e0e0e0",
        lineCap: "circle",
        animate: 2000,
    });
    $('.chart--lg').easyPieChart({
        size: 90,
        lineWidth: 13,
    });
});
</script>
@endsection