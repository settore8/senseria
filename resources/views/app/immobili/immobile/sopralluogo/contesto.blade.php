@extends('app.layouts.immobileservizitecnici')

@section('title', 'Sopralluogo')


@section('headertitle')
    @component('app.components.headertitle')
        @include('app.partials.titlesopralluogo', ['childs' => ['Contesto']])
    @endcomponent
@endsection

@section('content')





    @if(Auth::id() != $immobile->tecnico_id)
        @include('app.partials.sopralluogo.showSopralluogoContesto', ['immobile' => $immobile])
    @else
        @include('app.partials.sopralluogo.formSopralluogoContesto', ['immobile' => $immobile])
    @endif


@endsection
