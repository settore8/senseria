@extends('app.layouts.immobileservizitecnici')

@section('title', 'Sopralluogo')

@section('headertitle')
    @component('app.components.headertitle')
        @include('app.partials.titlesopralluogo', ['childs' => ['Foto e video']])
    @endcomponent
@endsection


@section('content')
    <span>Documentazione Fotografica</span>
    <form action="{!! route('immobile.sopralluogo.upload.fotovideo', $immobile) !!}" id="dropzoneimg2" class="dropzone" method="POST"
          enctype="multipart/form-data">
        @csrf
        <div class="fallback">
            <input type="file" name="file" multiple>
        </div>
    </form>

@endsection

@section('footerjs_after')
    <script type="text/javascript" src={{ asset('js/dropzone.js') }}></script>
@endsection


