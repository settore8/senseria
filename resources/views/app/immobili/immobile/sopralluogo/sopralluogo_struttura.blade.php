@extends('app.layouts.immobileservizitecnici')

@section('title', 'Sopralluogo')


@section('headertitle')
    @component('app.components.headertitle')
        @include('app.partials.titleimmobile', ['childs' => ['Sopralluogo', 'Fabbricato', ' Struttura']])
    @endcomponent
@endsection


@section('content')



    @if(Auth::id() != $immobile->tecnico_id)
        @include('app.partials.sopralluogo.showSopralluogoFabbricato', ['immobile' => $immobile])
    @else
        @include('app.partials.menusopralluogofabbricato')
        @include('app.partials.sopralluogo.formSopralluogoFabbricatoStruttura', ['immobile' => $immobile, 'fabbricato' => $fabbricato])
    @endif

@endsection

@section('footerjs_after')
    <script>
        function showOption(element, $elementToShowOrhide) {
            var value = $(element).val();
            if (value != 1) {
                $('#' + $elementToShowOrhide).removeClass('visible').addClass('hidden');
                return false;
            }
            $('#' + $elementToShowOrhide).removeClass('hidden').addClass('visible');
        }
    </script>
@endsection
