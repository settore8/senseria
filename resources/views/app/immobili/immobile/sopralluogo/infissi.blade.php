@extends('app.layouts.immobileservizitecnici')

@section('title', 'Sopralluogo')


@section('headertitle')
    @component('app.components.headertitle')
    @include('app.partials.titlesopralluogo', ['childs' => ['Unità immobiliare', 'Infissi']])
    @endcomponent
@endsection


@section('content')



    @if(Auth::id() != $immobile->tecnico_id)
        @include('app.partials.sopralluogo.showSopralluogoUnitaImmobiliare', ['immobile' => $immobile])
    @else
        @include('app.partials.menusopralluogounitaimmobiliare')
        @include('app.partials.sopralluogo.formSopralluogoUnitaImmobiliareInfissi', ['immobile' => $immobile])
    @endif

@endsection

@section('footerjs_after')
    <script>
        $(document).on('change', "*[data-moltiplicatore=true]", function (e) {
            var molt = 1;
            $(this).closest("tr").find('*[data-moltiplicatore]').each(function () {
                molt *= parseFloat(this.value);
                if(!isNaN(molt)){
                    $(this).closest("tr").find('*[data-moltiplicazione]').val(molt);
                }

            });
        });

        $(document).on('change', "*[data-moltiplicatore=true]", function (e) {
            var molt = 1;
            $(this).closest("tr").find('*[data-moltiplicatore]').each(function () {
                molt *= parseFloat(this.value);
                console.log(this.value, molt);
                $(this).closest("tr").find('*[data-moltiplicazione]').val(molt);
            });
        });
    </script>
@endsection
