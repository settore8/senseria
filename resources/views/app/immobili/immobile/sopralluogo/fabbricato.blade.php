@extends('app.layouts.immobileservizitecnici')

@section('title', 'Sopralluogo')


@section('headertitle')
    @component('app.components.headertitle')
        @include('app.partials.titlesopralluogo', ['childs' => ['Fabbricato']])
    @endcomponent
@endsection


@section('content')


    @if(Auth::id() != $immobile->tecnico_id)
        @include('app.partials.sopralluogo.showSopralluogoFabbricato', ['immobile' => $immobile])
    @else
        @include('app.partials.menusopralluogofabbricato')
        @include('app.partials.sopralluogo.formSopralluogoFabbricato', ['immobile' => $immobile])
    @endif

@endsection

@section('footerjs_after')
    <script>
        function showOption(element, $elementToShowOrhide) {
            var value = $(element).val();
            if (value !== 'Si') {
                $('#' + $elementToShowOrhide).removeClass('visible').addClass('hidden');
                return false;
            } else {
                $('#' + $elementToShowOrhide).removeClass('hidden').addClass('visible');
            }
        }
    </script>
@endsection
