@extends('app.layouts.immobileservizitecnici')

@section('title', 'Sopralluogo')

@section('headertitle')
    @component('app.components.headertitle')
        @include('app.partials.titlesopralluogo', ['childs' => ['Unità immobiliare', 'impianti']])
    @endcomponent
@endsection


@section('content')



    @if(Auth::id() != $immobile->tecnico_id)
        @include('app.partials.sopralluogo.showSopralluogoUnitaImmobiliare', ['immobile' => $immobile])
    @else
        @include('app.partials.menusopralluogounitaimmobiliare')
        @include('app.partials.sopralluogo.formSopralluogoUnitaImmobiliareImpianti', ['immobile' => $immobile])
    @endif

@endsection

@section('footerjs_after')
    <script>
        $(document).on('change', "*[data-moltiplicatore=true]", function (e) {
            var molt = 1;
            $(this).closest("tr").find('*[data-moltiplicatore]').each(function () {
                molt *= parseFloat(this.value);
                if (!isNaN(molt)) {
                    $(this).closest("tr").find('*[data-moltiplicazione]').val(molt);
                }

            });
        });
        /*
        function showOption(element, $elementToShowOrhide) {

            $($elementToShowOrhide).removeClass('hidden');

            var value = $(element).val();
            if (value !== 'Si') {
                $('#' + $elementToShowOrhide).removeClass('visible').addClass('hidden');
                return false;
            } else {
                $('#' + $elementToShowOrhide).removeClass('hidden').addClass('visible');
            }

        }
        */
    </script>
@endsection

