@extends('app.layouts.immobileservizitecnici')

@section('title', 'Sopralluogo')

@section('headertitle')
    @component('app.components.headertitle')
    
            @include('app.partials.titlesopralluogo')
            
    @endcomponent

@endsection

@section('content')

        @block(['title' => 'Stato', 'class' => 'block--bordered'])

                <div class="columns align-items-center">
                <div class="column col-5 col-lg-5 col-md-12">
                <p>La <strong>scheda di sopralluogo</strong> ha lo scopo agevolare e velocizzare  la raccolta delle informazioni tecniche utili alla descrizione dell’edificio e dell’unità immobiliare, nonché delle parti condominiali. Tali informazioni possono essere utilizzate nella successiva fase di verifica dell’immobile e  condivise con l’agente immobiliare che si occupa della vendita.</p>        
                </div>

                <div class="column col-3 col-md-6 mlauto text-center">

                <div class="chart-container">
                        <div class="chart-title">Sopralluogo</div>
                        <canvas id="chart-sopralluogo" width="200px" height="200px" ></canvas>
                </div>
                </div>

                <div class="column col-3 col-md-6 text-center">

                <div class="chart-container">
                        <div class="chart-title">Immobile</div>
                        <canvas id="chart-completamento" width="200px" height="200px"></canvas>
                </div>
                </div>
        </div>

        @endblock

        @if($immobile->sopralluogo && $immobile->sopralluogo->contesto)

                @block(['title' => 'Contesto', 'class' => 'block--bordered block--success'])

                        @slot('header')<a href="{{ route('immobile.sopralluogo.contesto', $immobile)}}" class="ml5"><svg width="23" height="23"><use xlink:href="#icon-pen"></use></svg></a> <span class="badge badge--success mlauto">Completato</span>@endslot

                        Contesto paesaggistico:
                        {{ $immobile->sopralluogo->contesto['paesaggistico'] }}
                        Contesto territoriale:
                        {{ $immobile->sopralluogo->contesto['territoriale'] }}
                        
                        <h4>Viabilità:</h4>
                        @if($immobile->sopralluogo->contesto['viabilita'])
                                @foreach($immobile->sopralluogo->contesto['viabilita'] as $viabilita => $valore)
                                        {{ __('messages.'.$viabilita)}}: {{$valore}}<br/>
                                @endforeach
                        @endif

                        <h4>Trasporti:</h4>
                        @if($immobile->sopralluogo->contesto['trasporti'])
                                @foreach($immobile->sopralluogo->contesto['trasporti'] as $trasporto => $distanza)
                                        {{ $trasporto }}: {{$distanza}}
                                @endforeach
                        @endif
                        <h4>Servizi</h4>
                        @if($immobile->sopralluogo->contesto['servizi'])
                                @foreach($immobile->sopralluogo->contesto['servizi'] as $servizio)
                                        {{ $servizio }}
                                @endforeach
                        @endif
                @endblock
        @else
                @block(['title' => 'Contesto', 'class' => 'block--bordered'])

                @slot('header')<a href="{{ route('immobile.sopralluogo.contesto', $immobile)}}" class="ml5"><svg width="23" height="23"><use xlink:href="#icon-pen"></use></svg></a> <span class="badge mlauto">Assente</span>@endslot

                @endblock

        @endif

        @if($immobile->sopralluogo && $immobile->sopralluogo->fabbricato)
                @block(['title' => 'Fabbricato', 'class' => 'block--bordered block--success'])
                        @slot('header')<a href="{{ route('immobile.sopralluogo.fabbricato', $immobile)}}" class="ml5"><svg width="23" height="23"><use xlink:href="#icon-pen"></use></svg></a> <span class="badge badge--success mlauto">Completato</span>@endslot
                        Anno costruzione: {{ $immobile->sopralluogo->fabbricato['annocostruzione'] }}
                @endblock
        @else
                @block(['title' => 'Fabbricato', 'class' => 'block--bordered'])
                        @slot('header')<a href="{{ route('immobile.sopralluogo.fabbricato', $immobile)}}" class="ml5"><svg width="23" height="23"><use xlink:href="#icon-pen"></use></svg></a> <span class="badge mlauto">Assente</span>@endslot
                @endblock
        @endif

        @if($immobile->sopralluogo && $immobile->sopralluogo->unitaimmobiliare)
        @block(['title' => 'Unità immobiliare', 'class' => 'block--bordered block--success'])
                        @slot('header')<a href="{{ route('immobile.sopralluogo.unitaimmobiliare', $immobile)}}" class="ml5"><svg width="23" height="23"><use xlink:href="#icon-pen"></use></svg></a> <span class="badge badge--success mlauto">Completato</span>@endslot
        @endblock
        @else
        @block(['title' => 'Unità immobiliare', 'class' => 'block--bordered'])
                @slot('header')<a href="{{ route('immobile.sopralluogo.unitaimmobiliare', $immobile)}}" class="ml5"><svg width="23" height="23"><use xlink:href="#icon-pen"></use></svg></a> <span class="badge mlauto">Assente</span>@endslot
        @endblock
        @endif


        @if($immobile->sopralluogo && $immobile->sopralluogo->particondominiali)
        @block(['title' => 'Parti condominiali', 'class' => 'block--bordered block--success'])
                @slot('header')<a href="{{ route('immobile.sopralluogo.particondominiali', $immobile)}}" class="ml5"><svg width="23" height="23"><use xlink:href="#icon-pen"></use></svg></a> <span class="badge badge--success mlauto">Completato</span>@endslot
        @endblock
        @else
        @block(['title' => 'Parti condominiali', 'class' => 'block--bordered'])
                @slot('header')<a href="{{ route('immobile.sopralluogo.particondominiali', $immobile)}}" class="ml5"><svg width="23" height="23"><use xlink:href="#icon-pen"></use></svg></a> <span class="badge mlauto">Assente</span>@endslot
        @endblock
        @endif


        @if($immobile->sopralluogo && $immobile->sopralluogo->fotovideo)
        @block(['title' => 'Foto e video', 'class' => 'block--bordered block--success'])
                @slot('header')<a href="{{ route('immobile.sopralluogo.fotovideo', $immobile)}}" class="ml5"><svg width="23" height="23"><use xlink:href="#icon-pen"></use></svg></a> <span class="badge badge--success mlauto">Completato</span>@endslot
        @endblock
        @else
        @block(['title' => 'Foto e video', 'class' => 'block--bordered'])
                @slot('header')<a href="{{ route('immobile.sopralluogo.fotovideo', $immobile)}}" class="ml5"><svg width="23" height="23"><use xlink:href="#icon-pen"></use></svg></a> <span class="badge mlauto">Assente</span>@endslot
        @endblock
        @endif



@endsection


@section('footerjs_after')
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.3/dist/Chart.min.js"></script>

<script>

        var config_doughnut = {
			type: 'doughnut',
			data: {
				datasets: [{
					data: [
                                                6,
						94,
					],
					backgroundColor: [
                        "#d20b0b",
                        "#f3f3f3",
                        "#f3f3f3",
                    ],
                    borderWidth: 1,
					label: false
				}],
				labels: [
					'Sopralluogo',
                    'Da completare',
				]
			},
			options: {
                weight: 3,
                cutoutPercentage: 65,
				responsive: true,
				legend: false,
                title: false,
                tooltips: {
                        mode: 'dataset'
                },
				animation: {
					animateScale: false,
					animateRotate: true
                }
            },
            plugins: [{
                    beforeDraw: function(chart, options) {
                        var width = chart.chart.width,
                            height = chart.chart.height,
                            ctx = chart.chart.ctx;

                        ctx.restore();
                        var fontSize = (height / 114).toFixed(2);
                        ctx.font = fontSize + "em sans-serif";
                        ctx.textBaseline = "middle";

                        var text = "6%",
                            textX = Math.round((width - ctx.measureText(text).width) / 2),
                            textY = height / 2;

                        ctx.fillText(text, textX, textY);
                        ctx.save();
                    }
                }],
		};


       var config_pie = {
			type: 'pie',
			data: {
				datasets: [{
					data: [
						3,
						3,
                                                3,
                                                3,
                                                3,
					],
					backgroundColor: [
                        "#ef476f",
                        "#ffd166",
                        "#f3f3f3",
                        "#f3f3f3",
                        "#f3f3f3"
              
                    ],
                    borderWidth: 0,

					label: 'Dataset 1'
				}],
				labels: [
					'Contesto',
					'Fabbricato',
					'Unità immobiliare',
                    'Parti condominiali',
                    'Foto e video'
		        ]
			},
			options: {
                weight: 3,
                cutoutPercentage: 0,
				responsive: true,
				legend: false,
				title: false,
				animation: {
					animateScale: false,
					animateRotate: true
				}
			}
		};

		window.onload = function() {
            var verificatecnica = document.getElementById('chart-sopralluogo');
            var completamento = document.getElementById('chart-completamento');
            window.verificatecnica = new Chart(verificatecnica, config_pie);
            window.completamento = new Chart(completamento, config_doughnut);
        };



    </script>

@endsection