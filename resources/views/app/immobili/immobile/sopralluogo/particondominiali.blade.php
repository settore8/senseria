@extends('app.layouts.immobileservizitecnici')

@section('title', 'Sopralluogo')

@section('headertitle')
    @component('app.components.headertitle')
        @include('app.partials.titlesopralluogo', ['childs' => ['Parti condominiali']])
    @endcomponent
@endsection


@section('content')

    @if(Auth::id() != $immobile->tecnico_id)
        @include('app.partials.sopralluogo.showSopralluogoPartiCondominiali', ['immobile' => $immobile])
    @else
        @include('app.partials.sopralluogo.formSopralluogoPartiCondominiali', ['immobile' => $immobile])
    @endif

@endsection

@section('footerjs_after')

@endsection

