@extends('app.layouts.immobile')

@section('title', 'Impostazioni')
@section('bodyclass'){{ 'app immobile impostazioni' }}@endsection


@section('headertitle')
    @component('app.components.headertitle')
        <a href="{{ route('immobile', $immobile) }}" alt="Allegati {{ $immobile->nome }}">{{ $immobile->nome }}</a> <span class="weight-300">/ Impostazioni</span> 
    @endcomponent
@endsection


@section('content')
<style>
.select2-dropdown.select2-dropdown--below{
    width: 500px !important;
}
</style>
        @if (session('message'))
            <div class="alert alert-success">
                <strong>{{ session('message')['paginarichiesta'] }}</strong>
            </div>
        @else

            <div class="flex">
                <div class="item item--center">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim <strong>veniam, quis nostrud exercitation ullamco</strong> laboris nisi ut aliquip ex
                        ea commodo. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut</p>
                </div>
                <div class="item item--center">
                    <img src="{{ asset('/images/visual-immobile.svg') }}/">
                </div>
            </div>
            <div class="flex">
                <ul>
                    @foreach ($candidature as $candidato => $candidatura)
                        <li>{{ $candidato }}</li> 
                        @if(is_null($immobile->tecnico_id))
                            <button data-immobile="{!! $immobile->id_criptato !!}" data-candidato="{!! $candidatura->first()->user->id_criptato !!}" data-event="scelta_candidato">Scegli candidato</button>
                        @endif
                        <a href="javascript:void(0)" data-conversazione="true" data-recipient-id-conversazione="{{ $candidatura->first()->user_id }}" data-object-id-conversazione={{ $immobile->annuncioRespTecnico->id }} data-object-class-conversazione={{ get_class($immobile->annuncioRespTecnico) }}>Apri conversazione</a>
                        <ul>
                            @foreach ($candidatura as $dc)
                                <li>{!! $dc->dettaglio->coverable->nome_pubblico !!}: {{ $dc->quotazione }}: {{ ($dc->note ? $dc->note->testo : '') }}</li>
                            @endforeach
                        </ul>
                    @endforeach
                </ul>
            </div>
        @endif
       

@endsection