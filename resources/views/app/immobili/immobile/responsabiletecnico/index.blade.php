@extends('app.layouts.immobile')

@section('title', 'Impostazioni')
@section('bodyclass'){{ 'app immobile' }}@endsection

@section('headertitle')
    @component('app.components.headertitle')
        @include('app.partials.titleimmobile', ['childs' => ['Responsabile tecnico']])
    @endcomponent
@endsection

@section('content')
<style>
/*
.select2-dropdown.select2-dropdown--below{
    width: 500px !important;
}
*/
</style>
        @block(['class' => 'block--bordered'])
            <div class="columns">
                <div class="column col-7 col-lg-6 col-xs-12">
                    <h2>Gestione responsabile tecnico</h2>
                    <p>Ad ogni scheda immobile creata è possibile abbinare un <strong>Responsabile tecnico</strong> che provveda all’acquisizione dei documenti, al sopralluogo ed alla verifica dell’immobile con l’ausilio delle funzioni dedicate messe a disposizione da Workook. Se sei un professionista puoi <strong>nominare te stesso</strong> come responsabile tecnico, <strong>selezionare un professionista</strong> di fiducia dalla rete Workook oppure <strong>pubblicare un annuncio</strong> per trovarne uno.</p>
                </div>
                <div class="column col-ml-auto col-4 col-lg-5 col-xs-8 col-mx-auto">
                    <img src="{{ asset('/images/app/visual-assegna-tecnico.svg') }}/">
                </div>
            </div>

            <hr class="hr hr--separator">
        
            @if($immobile->moduli)
                @if($immobile->tecnico_id)
                        <div class="columns">
                            <div class="column col-5 col-sm-12">
                                <h2>Servizi tecnici abilitati</h2>
                                
                                @foreach(\App\Modulo::orderBy('order', 'ASC')->get() as $modulo)
                                    <div class="columns">
                                        <div class="column">
                                        {{ $modulo->nome_pubblico }}
                                        </div> 
                                        <div class="column py3">
                                        @if(in_array($modulo->nome, $immobile->moduli)) 
                                            <button class="btn btn-success btn-sm"  data-destinatario="{{ encrypt($immobile->tecnico_id)  }}" data-modulo="{{ $modulo->nome }}" data-immobile="{{ encrypt($immobile->id) }}" data-attiva-modulo="true"> Abilitato </button>
                                        @else 
                                            <button class="btn btn-sm"  data-destinatario="{{ encrypt($immobile->tecnico_id)  }}" data-modulo="{{ $modulo->nome }}" data-immobile="{{ encrypt($immobile->id) }}" data-attiva-modulo="true"> Disabilitato </button>
                                        @endif 
                                        </div>
                                    </div>
                                @endforeach
                                
                            </div>
                            <div class="column col-5 col-ml-auto  col-sm-12">
                            <h2>Responsabile tecnico</h2>
                                <div>
                                <img src="{{ Custom::getImageProfile($immobile->tecnico, 's') }}">
                                <a href="{{ route('profilo', $immobile->tecnico->slug) }}">{!! $immobile->tecnico->display_name !!}</a>
                                </div>
                                <button class="btn btn-link-error" data-toggle="modal" data-target="#rimuoviResposanbile">Rimuovi incarico</button>
                            </div>

                            @modal(['id' =>'rimuoviResposanbile', 'class' => 'modal--sm', 'visual' => 'message-attention.svg', 'visual_size' => 'small'])
                                <form action="{{ route('delete.responsabiletecnico', $immobile) }}" method="post">
                                    @csrf

                                    <p>Rimuovi <strong>{!! $immobile->tecnico->fullname !!}</strong> <br/>dal ruolo di responsabile tecnico</p>
                                   
                                    <div class="form-group">
                                        <label class="form-label">Digita RIMUOVI nel campo sottostante</label>
                                        <input name="action" class="form-input input-lg text-center input-width-150 mlauto mrauto" type="text">
                                    </div>
                                 
                                    @if($immobile->moduli)
                                        <hr class="hr hr--separator">
                                        <p>Vuori cancellare anche i dati <br/>dei servizi tecnici?</p>
                                        <div>
                                            <select class="form-select select-width-150" name="dati">
                                                <option value="mantieni" selected>Mantieni i dati</option>
                                                <option value="cancella">Cancella i dati</option>
                                            </select>
                                        </div>
                                    @endif

                                    @button(['attrs' => ['text' => 'Procedi', 'class' => 'btn btn-error btn-padding nopreloader']])@endbutton

                                </form>
                            @endmodal



                            
                        </div>
                @elseif($immobile->annuncioRespTecnico)

                        
                @else
                        
                        @if ($immobile->annuncioRespTecnico->candidature->where('candidato', NULL)->isNotEmpty())
                            @php  /* se ancora non é stato scelto nessuno o se non ci sono candidature attive perché non riconfermate */  @endphp
                            <form action="{!! route('store.responsabiletecnico', $immobile) !!}" method="POST">
                                @csrf
                                @selectProfessionisti(['attrs' => ['name' => 'internoaworkook']]) @selectProfessionisti
                                @foreach ($moduli as $key => $value)
                                    @php /* Faccio un ciclo; ogni checkbox ha il nome della relazione che ha il model Immobile */ @endphp
                                    <input @if (in_array($value->nome, $immobile->moduli)) checked @endif type="checkbox" value="{!! $value->id !!}" name="moduli[]" multiple> {!! $value->nome_pubblico !!}
                                @endforeach
                                @button(['attrs' => ['text' => 'Scegli']])@endbutton
                            </form>
                            <h1>Cerca tramite territorio compatibile</h1>
                            <form action="{!! route('store.responsabiletecnico', $immobile) !!}" method="POST">
                                @csrf
                                <input type="hidden" name="compatibile" value="1">
                                @foreach ($moduli as $key => $value)
                                    @php /* Faccio un ciclo; ogni checkbox ha il nome della relazione che ha il model Immobile */ @endphp
                                    <input @if (in_array($value->nome, $immobile->moduli)) checked @endif type="checkbox" value="{!! $value->id !!}" name="moduli[]" multiple> {!! $value->nome_pubblico !!}
                                @endforeach
                                @button(['attrs' => ['text' => 'Pubblica annuncio']])@endbutton
                            </form>
                        @endif

                        @forelse ($immobile->annuncioRespTecnico->candidature->groupBy('user.nome_cognome')->take(20) as $candidato => $candidatura)
                            @if($loop->first)
                                <a href="{{ route('immobile.responsabiletecnico.candidature', $immobile) }}" class="btn btn-primary btn-sm">Visualizza tutte</a>
                            @endif
                            {{ $candidato }}
                        @empty
                            @php  /* se non sono presenti candidature o non sono attive */  @endphp
                            <form action="{!! route('store.responsabiletecnico', $immobile) !!}" method="POST">
                                @csrf
                                @selectProfessionisti(['attrs' => ['name' => 'internoaworkook']]) @selectProfessionisti
                                @foreach ($moduli as $key => $value)
                                    @php /* Faccio un ciclo; ogni checkbox ha il nome della relazione che ha il model Immobile */ @endphp
                                    <input @if (in_array($value->nome, $immobile->moduli)) checked @endif type="checkbox" value="{!! $value->id !!}" name="moduli[]" multiple> {!! $value->nome_pubblico !!}
                                @endforeach
                                <button type="submit">SALVA</button>
                            </form>

                            <form action="{!! route('store.responsabiletecnico', $immobile) !!}" method="POST">
                                @csrf
                                <input type="hidden" name="compatibile" value="1">
                                @foreach ($moduli as $key => $value)
                                    @php /* Faccio un ciclo; ogni checkbox ha il nome della relazione che ha il model Immobile */ @endphp
                                    <input @if (in_array($value->nome, $immobile->moduli)) checked @endif type="checkbox" value="{!! $value->id !!}" name="moduli[]" multiple> {!! $value->nome_pubblico !!}
                                @endforeach
                                <button type="submit">TERRITORIO COMPATIBILE</button>
                            </form>
                        @endforelse
                @endif
            @else
                <form action="{!! route('store.responsabiletecnico', $immobile) !!}" method="POST" id="formResponsabileTecnico">
                    @csrf

                    <div class="columns">
                        
                        <div class="column col-5 col-sm-12">
                            <h2>Servizi tecnici</h2>
                            <p>Seleziona i servizi tecnici che vuoi che vengano gestiti dal responsabile tecnico e successivamente scegli un metodo di assegnazione del ruolo di responsabile tecnico.</p>
                            @foreach ($moduli->sortBy('order') as $key => $value)
                                @php /* Faccio un ciclo; ogni checkbox ha il nome della relazione che ha il model Immobile */ @endphp
                                <div class="columns py3">
                                    <div class="column col-6">
                                        {!! $value->nome_pubblico !!}
                                    </div>
                                    <div class="column col-6">
                                        <input type="checkbox"  value="{!! $value->nome !!}" name="moduli[]" multiple>
                                    </div>
                                </div>
                            @endforeach
                            <input type="hidden" name="compatibile" value="1" disabled>
                            <input type="hidden" name="sestesso" value="1" disabled>
                        </div>

                        <div class="column col-5 col-ml-auto  col-sm-12">
                            <h2>Responsabile tecnico</h2>
                            <p>Assegna il ruolo di Responsabile tecnico con uno dei seguenti metodi.</p>
                            @if(Auth::user()->tipo != 'agente')
                            <div>
                                <button class="btn btn-outline-primary bnt-sm mb10" data-submit-respTecnico="true" id="selfAssignedRespTecnico">Assegna a me stesso</button>
                            </div>
                            @endif
                            <div>
                                <button class="btn btn-outline-primary bnt-sm mb10" data-toggle="modal" data-target="#assegnaProfessionista">Assegna ad un professionista</button>
                            </div>
                            <p>Oppure pubblica un annuncio. <br/>I professionisti che operano nell'area dell'immobile riceveranno una notifica.</p>
                            <div>
                                <button class="btn btn-outline-primary bnt-sm" data-submit-respTecnico="true" id="pubblicaAnnuncioRespTecnico">Pubblica annuncio</button>
                            </div>
                                @modal(['id' =>'assegnaProfessionista', 'title' => 'Assegna ad un professionista', 'class' => 'modal--sm emptyOnClose'])
                                    @slot('title')Assegna ad un professionista @endslot
                                    <p>Seleziona un professionista digitando il suo nome. Clicca su conferma per assegnare il ruolo di responsabile tecnico al professionista selezionato. Potrai in ogni momento sostituirlo o rimuoverlo.</p>
                                    <form action="" method="POST">
                                        @csrf
                                        @selectprofessionisti(['attrs' => ['name' => 'internoaworkook']]) @endselectprofessionisti
                                        <button class="btn btn-primary bnt-sm btn-block mt20" data-submit-respTecnico="true" id="scegliRespTecnico">Conferma</button>
                                    </form>
                                @endmodal

                            </div>
                        </div>

                    </div>

                </form>

               

            @endif

        @endblock

@endsection