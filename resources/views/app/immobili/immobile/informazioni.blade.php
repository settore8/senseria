@extends('app.layouts.immobile')

@section('title', 'Informazioni immobile')

@section('headertitle')
    @component('app.components.headertitle')
        @include('app.partials.titleimmobile', ['childs' => ['Informazioni']])
    @endcomponent
@endsection

@section('content')

        @php /*
        @suggerimento
            @slot('label', '?')
            @slot('visibile', Auth::user()->viewSuggerimento('tip_immobile_info'))
            @slot('datatip', 'tip_immobile_info')
        @endsuggerimento

        */ @endphp
        
        @if(Auth::id() == $immobile->user_id && Auth::id() != $immobile->tecnico_id && $immobile->sopralluogo && $immobile->sopralluogo->unitaimmobiliare)
                @include('app.partials.immobile.showInformazioni', ['immobile' => $immobile]) 
        @else
                @include('app.partials.immobile.formInformazioni', ['immobile' => $immobile])
        @endif


@endsection
