
@extends('layouts.boarding')

@section('content')
<form action="{!! route('action.declina', $immobile) !!}"  method="POST">
    @csrf
    <input type="hidden" name="token" value="{!! $token !!}">
    <input type="hidden" name="type" value="{!! $type !!}">
    <button type="submit"> RIFIUTA ASSEGNAZIONE </button>
</form>
@endsection
