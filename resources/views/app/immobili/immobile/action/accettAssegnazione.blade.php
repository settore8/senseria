
@extends('layouts.boarding')

@section('content')
<form action="{!! route('action.confermAssegnazioneAgenteImmobiliare', $immobile) !!}"  method="POST">
    @csrf
    <input type="hidden" name="token" value="{!! $token !!}">
    <input type="hidden" name="type" value="{!! $type !!}">
    <button type="submit"> CONFERMA ASSEGNAZIONE </button>
</form>
@endsection
