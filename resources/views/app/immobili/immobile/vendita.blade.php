@extends('app.layouts.immobile')

@section('title', 'Vendita')

@section('headertitle')
    @component('app.components.headertitle')
    @include('app.partials.titleimmobile', ['childs' => ['Vendita']])
    @endcomponent
@endsection


@section('content')
        
    <div class="columns align-item-center">
        <div class="column col-8">
            <p>Puoi in qualsiasi momento decidere di mettere in pubblicità immobiliare una scheda immobile precedentemente creata. Puoi farlo semplicemente pubblicando un <strong>annuncio immobiliare sul portale Workook</strong>, che sarà visibile ai professionisti ed alle imprese registrate. Puoi inoltre affidare la vendita dell’immobile ad un agente immobiliare selezionato dalla rete “Senseria”.</p>
        </div>
        <div class="column col-4">
            <img src="{{ asset('images/app/immobile-vendita.svg') }}/" alt="Immobile Vendita">
        </div>
    </div> 

    @block(['class' => 'block--bordered'])

        @if (!$immobile->vendita)
        <p>Compilare i campi richiesti</p>
        @endif
        
        <form action="{!! route('immobile.store.vendita', $immobile) !!}" method="POST" class="form-horizontal"> 
            @csrf
            @formgrouph(['for' => 'proprietario', 'label' => 'Attuale proprietario', 'col1' => 'col-6', 'col2' => 'col-6', 'required' => true])
                <select name="proprietario" id="proprietario" class="form-select">
                    @foreach (config('constants.vendita.proprietari') as $proprietario)
                        <option value="{!! $proprietario !!}">{!! $proprietario !!}</option>
                    @endforeach
                </select>
            @endformgrouph

            @if(Auth::user()->tipo != 'agente')
                @formgrouph(['for' => 'interventi', 'label' => 'Interventi necessari', 'col1' => 'col-6', 'col2' => 'col-6', 'istruzioni' => 'Selezionare quale intervento è necessario'])
                    <select name="intervento" id="interventi" class="form-select">
                        @foreach ($interventi as $id =>  $nome)
                            <option value="{!! $id !!}">{!! $nome !!}</option>
                        @endforeach
                    </select>
                @endformgrouph
            @endif
            @if (!$immobile->vendita)
            @button(['attrs' => ['text' => 'Avanti', 'class' => 'btn btn-outline-primary btn-padding']])@endbutton
            @else
            @button(['attrs' => ['text' => 'Aggiorna', 'class' => 'btn btn-outline-primary btn-padding']])@endbutton
            @endif
        </form>

        @endblock

        @if ($immobile->vendita)

            @if(Auth::user()->tipo != 'agente')
                @if($immobile->agente_id)
                    @block(['class' => 'block--bordered', 'title' => 'Agente immobiliare'])
                            {!! $immobile->agente->nome_cognome !!}
                            <button class="">Rimuovi incarico</button>
                    @endblock
                @else
                    @block(['class' => 'block--bordered', 'title' => 'Affida l\'immobile a un agente immobiliare'])
                        <div class="columns">
                            <div class="column col-6 col-xs-12">
                                <p>Puoi selezionare un agente immobiliare della rete Senseria, oppure pubblicare un annuncio per trovarne uno.</p>
                            </div>
                            <div class="column col-6 col-ml-auto col-xs-12">
                                <form action="{!! route('immobile.store.vendita', $immobile) !!}" method="POST">
                                    @csrf
                                    <div class="form-group">
                                        <select name="agente_immobiliare" id="selectagentiimmobiliari" class="form-select select"></select>
                                    </div>
                                    @button(['attrs' => ['text' => 'Pubblica aunnuncio']])@endbutton
                                    @button(['attrs' => ['text' => 'Conferma']])@endbutton
                                </form>
                            </div>
                        </div>
                    @endblock
                @endif
            @endif

            @if($immobile->annuncioVendita)
                @block(['class' => 'block--bordered', 'title' => 'Annuncio'])
                    <a href="{{ route('annuncio', $immobile->annuncioVendita->codice) }}">Rif. <strong>{{ $immobile->annuncioVendita->codice }}</strong></a>
                    <button>Modifica</button>
                    <a href="">Statistiche</a>
                    <button>Metti in pausa l'annuncio</button>
                    <button>Rimuovi annuncio</button>
                @endblock
            @else
            @block(['class' => 'block--bordered', 'title' => 'Metti in pubblicità'])

                <div class="columns">
                    <div class="column col-6 col-xs-12">
                        <p>Pubblicando un annuncio l'immobile sarà visibile a professionisti e imprese su Workook e nell'area riservata del agenti immobiliari su Senseria</p>
                    </div>
                    <div class="column col-5 col-ml-auto col-xs-12">
                        <form action="{!! route('immobile.store.vendita', $immobile) !!}" method="POST">
                            @csrf
                            @formgrouph(['for' => 'prezzo_vendita', 'label' => 'Prezzo vendita', 'col1' => 'col-6', 'col2' => 'col-6'])
                                @inputtext(['attrs' => ['name' => 'prezzo_vendita', 'id' => '', 'placeholder' => '200.000', 'class' => 'priceFormat']]) @endinputtext
                            @endformgrouph
            
                            @formgrouph(['for' => 'prezzo_locazione', 'label' => 'Prezzo locazione', 'col1' => 'col-6', 'col2' => 'col-6'])
                                    @inputtext(['attrs' => ['name' => 'prezzo_locazione', 'id' => '', 'placeholder' => '800', 'class' => 'priceFormat']]) @endinputtext
                            @endformgrouph
                    
                            @formgrouph(['for' => 'testo', 'label' => 'Testo annuncio', 'col1' => 'col-6 col-xs-12', 'col2' => 'col-6 col-xs-12'])
                                    @textarea(['attrs' => ['name' => 'testo', 'rows' => '6', 'maxlength' => '1000', 'class' => 'showCounter']])
                                    
                                    @endtextarea
                            @endformgrouph
                            @button(['attrs' => ['text' => 'Pubblica annuncio']])@endbutton
                        </form>
                    </div>
                </div>
                
            @endblock

            @endif

        @endif
@endsection