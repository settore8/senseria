@extends('app.layouts.immobileservizitecnici')

@section('title', 'Verifica tecnica')


@section('headertitle')
    @component('app.components.headertitle')
    @include('app.partials.titleverificatecnica')
    @endcomponent
@endsection


@section('content')

<form action="{{ route('immobile.verificatecnica.store', $immobile) }}" method="POST">
    @csrf

    @block(['class' => 'block--bordered', 'title' => 'Stato'])

            <div class="columns align-items-center">

                <div class="column col-5 col-lg-5 col-md-12">
                    <p>Attraverso il <strong>form di verifica</strong> il responsabile tecnico, effettuato il sopralluogo e verificata la documentazione tecnica acquisita, può definire il grado di conformità e vendibilità di un immobile ed emettere un report, evidenziando eventuali criticità. La verifica è condivisibile con l’agente immobiliare che si occupa della vendita.</p>
                </div>

                <div class="column col-3 col-md-6 mlauto text-center">

                    <div class="chart-container">
                        <div class="chart-title">Verifica tecnica</div>
                        <canvas id="chart-verificatecnica" width="200px" height="200px" data-percentuale=""></canvas>
                    </div>
                </div>

                <div class="column col-3 col-md-6 text-center">

                    <div class="chart-container">
                        <div class="chart-title">Immobile</div>
                        <canvas id="chart-completamento" width="200px" height="200px"></canvas>

                    </div>
                </div>
            </div>

    @endblock
    @block(['class' => 'block--bordered', 'title' => 'Dichiarazioni'])

        @php



        $valori = [
            'datiurbanistici' => 'Dati urbanistici',
            'catasto' => 'Catasto',
            'provenienza' => 'Provenienza',
            'agibilita' => 'Agibilità',
            'impianti' => 'Impianti',
            'caldaia' => 'Caldaia',
            'ape' => 'APE',
            'cdu' => 'CDU',
            'postuma' => 'Postuma'
        ]


        @endphp
        <table class="table table--verificatecnica">
            <thead>
                    <tr>
                        <th></th>
                        <th>Verifica necessaria</th>
                        <th>Documenti disponibili</th>
                        <th>Conformità</th>
                    </tr>
            </thead>

            <tbody>
                

                    @if($valori)

                            @foreach ($valori as $key => $val)

                                <tr class="conditionalToggles">
                                    
                                    <td>{{ $val }}</td>

                                    <td class="col_check">
                                        <div class="form-group">
                                            <label class="form-checkbox form-checkbox-success">
                                                <input type="checkbox" class="check_necessario" name="verificatecnica[{{ $key }}][]" value="Necessario" @if(!$immobile->verifica_tecnica || ($immobile->verifica_tecnica->$key && $immobile->verifica_tecnica->$key['dichiarazioni']['Necessario'])) {{'checked'}} @endif>
                                                <i class="form-icon"></i>
                                             </label>
                                        </div>

                                    </td>

                                    <td class="col_check">
                                        <div class="form-group">
                                        <label class="form-checkbox form-checkbox-success">
                                            <input type="checkbox" class="check_disponibile" name="verificatecnica[{{ $key }}][]" value="Disponibile" @if($immobile->verifica_tecnica && $immobile->verifica_tecnica->$key && $immobile->verifica_tecnica->$key['dichiarazioni']['Disponibile'])  {{'checked'}} @endif>
                                            <i class="form-icon"></i>
                                         </label>
                                        </div>

                                    </td>

                                    <td class="col_check">
                                        <div class="form-group">

                                            <label class="form-checkbox  form-checkbox-error form-checkbox-inline tooltip tooltip-error"  data-tooltip="Non conforme">
                                                <input type="checkbox" class="check_conforme fakeradio" data-radio="radio{{ $key }}" name="verificatecnica[{{ $key }}][]" value="false" @if($immobile->verifica_tecnica && $immobile->verifica_tecnica->$key && $immobile->verifica_tecnica->$key['dichiarazioni']['Conforme'] == 'false')  {{'checked'}} @endif>
                                                <i class="form-icon"></i>
                                            </label>

                                            <label class="form-checkbox form-checkbox-success form-checkbox-inline tooltip tooltip-success" data-tooltip="Conforme">
                                                <input type="checkbox" class="check_conforme fakeradio" data-radio="radio{{ $key }}" name="verificatecnica[{{ $key }}][]" value="true" @if($immobile->verifica_tecnica && $immobile->verifica_tecnica->$key &&  $immobile->verifica_tecnica->$key['dichiarazioni']['Conforme'] == 'true')  {{'checked'}} @endif>
                                                <i class="form-icon"></i>
                                            </label>
             
                                        </div>

                                    </td>
                                </tr>
                            @endforeach
                            <input type="hidden" name="valori" value="{{ json_encode($valori) }}">

                    @endif


            </tbody>


        </table>



        @button(['attrs' => ['text' => 'Salva']])@endbutton

    @endblock

</form>

@endsection



@section('footerjs_after')
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.3/dist/Chart.min.js"></script>

<script>

        var config_doughnut = {
			type: 'doughnut',
			data: {
				datasets: [{
					data: [
                        15,
						30,
						65,
					],
					backgroundColor: [
                        "#ffed00",
                        "#ffed00",
                        "#f7f7f7",
                    ],
                    borderWidth: 1,
					label: false
				}],
				labels: [
					'Sopralluogo',
                    'Verifica tecnica',
                    'Da completare'
				]
			},
			options: {
                weight: 3,
                cutoutPercentage: 65,
				responsive: true,
				legend: false,
                title: false,
                tooltips: {
                        mode: 'dataset'
                },
				animation: {
					animateScale: false,
					animateRotate: true
                }
            },
            plugins: [{
                    beforeDraw: function(chart, options) {
                        var width = chart.chart.width,
                            height = chart.chart.height,
                            ctx = chart.chart.ctx;

                        ctx.restore();
                        var fontSize = (height / 114).toFixed(2);
                        ctx.font = fontSize + "em sans-serif";
                        ctx.textBaseline = "middle";

                        var text = "45%",
                            textX = Math.round((width - ctx.measureText(text).width) / 2),
                            textY = height / 2;

                        ctx.fillText(text, textX, textY);
                        ctx.save();
                    }
                }],
		};


       var config_pie = {
			type: 'pie',
			data: {
				datasets: [{
					data: [
						30,
						10,
                        10,
                        10,
                        5,
                        5,
                        5,
                        5,
                        5
					],
					backgroundColor: [
                        "#ef476f",
                        "#ffd166",
                        "#06d6a0",
                        "#f7f7f7",
                        "#f7f7f7",
                        "#f7f7f7",
                        "#f7f7f7",
                        "#f7f7f7",
                        "#f7f7f7"
                    ],
                    borderWidth: 0,

					label: 'Dataset 1'
				}],
				labels: [
					'Dati urbanistici',
					'Catasto',
					'Provenienza',
                    'Agibilità',
                    'Impianti',
                    'Caldaia',
                    'APE',
                    'CDU',
                    'Postuma'
				]
			},
			options: {
                weight: 3,
                cutoutPercentage: 0,
				responsive: true,
				legend: false,
				title: false,
				animation: {
					animateScale: false,
					animateRotate: true
				}
			}
		};

		window.onload = function() {
            var verificatecnica = document.getElementById('chart-verificatecnica');
            var completamento = document.getElementById('chart-completamento');
            window.verificatecnica = new Chart(verificatecnica, config_pie);
            window.completamento = new Chart(completamento, config_doughnut);
        };



    </script>

@endsection
