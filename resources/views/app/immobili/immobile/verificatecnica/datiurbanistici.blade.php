@extends('app.layouts.immobileservizitecnici')

@section('title', 'Verifica tecnica')


@section('headertitle')
    @component('app.components.headertitle')
    @include('app.partials.titleverificatecnica', ['childs' => ['Dati urbanistici']])
    @endcomponent
@endsection


@section('content')
       
<form action="" method="POST" class="form-horizontal">
    @csrf

    @block(['class' => 'block--bordered'])

    <div class="conditionalGroup">

        @formgrouph(['for' => 'dichiarazioni', 'name' => 'dichiarazioni', 'label' => 'Dichiarazioni', 'required' => true, 'col1' => 'col-8', 'col2' => 'col-4'])
            <select class="form-select conditionalNext" data-next="disponibile">
                <option name="necessario">--</option>
                <option value="true">Necessario</option>
                <option value="false">Non necessario</option>
            </select>
        @endformgrouph

        @formgrouph(['for' => 'dichiarazioni', 'name' => 'dichiarazioni', 'label' => 'Disponibilità', 'id' => 'disponibile', 'class' => 'hidden', 'col1' => 'col-8', 'col2' => 'col-4'])
            <select class="form-select conditionalNext" data-next="conforme">
                <option name="disponibile">--</option>
                <option value="true">Disponibile</option>
                <option value="false">Non disponibile</option>
            </select>
        @endformgrouph

        @formgrouph(['for' => 'conformita', 'name' => 'conformita', 'label' => 'Conformità', 'id' => 'conforme', 'class' => 'hidden', 'col1' => 'col-8', 'col2' => 'col-4'])
        <select class="form-select conditionalNext">
            <option name="conformita">--</option>
            <option value="true">Conforme</option>
            <option value="false">Non conforme</option>
        </select>
        @endformgrouph
        
    </div>

    @endblock

</form>
@endsection