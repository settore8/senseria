@extends('app.layouts.immobile')

@section('title', 'Allegati')

@section('headertitle')
    @component('app.components.headertitle')
        @include('app.partials.titleimmobile', ['childs' => ['Allegati']])
    @endcomponent
@endsection


@section('content')

    @if(Auth::id() != $immobile->tecnico_id)
        @block(['title' => 'Upload foto e allegati
        <button type="button" data-toggle="collapse" data-target="#tip_immobile_allegati"
                data-tip="tip_immobile_allegati" class="mlauto">?
        </button>','class' => 'block--bordered'])

        <div id="tip_immobile_allegati" class="collapse">
            <div class="columns align-items-center">
                <div class="column col-8 col-sm-7 col-sm-6 col-xs-12">
                    <p>Puoi inserire foto relative all’identificazione dall’esterno, nonché foto degli interni utili a
                        descrivere al meglio l’immobile; puoi inoltre allegare planimetrie, documenti e schede dotazioni
                        relative all’immobile in formato pdf.</p>
                </div>
                <div class="column col-4 col-sm-5 col-sm-6 col-xs-12">
                    <img src="{{ asset('/images/tips/tip-immobile.svg') }}/">
                </div>
            </div>
        </div>

        <form action="{!! route('immobile.carica.allegati', $immobile) !!}" id="dropzoneallegati" class="dropzone item"
              enctype="multipart/form-data">
            @csrf
            <div class="fallback">
                <input name="file" type="file" multiple/>
            </div>
        </form>
        @endblock
    @endif

    @block(['title' => 'File caricati','class' => 'block--bordered'])

            @foreach($immobile->allegati as $immagine)
                <p>
                    <img width="100px" height="100px"
                        src="{{Storage::url('gallerie/'.$galleria->user_id.'/'.$galleria->codice.'/'.$galleria->immagine_evidenza->filename)}}">
                    <button data-remove-immagine="{{$immagine->id}}">Elimina immagine</button>
                </p>
            @endforeach

    @endblock
@endsection

@section('footerjs_after')
    <script type="text/javascript" src={{ asset('js/dropzone.js') }}></script>
@endsection
