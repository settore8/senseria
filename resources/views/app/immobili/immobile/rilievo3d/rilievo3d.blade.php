@extends('app.layouts.immobile')

@section('title', 'Rilievo 3d')


@section('headertitle')
    @component('app.components.headertitle')
        <a href="{{ route('immobile', $immobile) }}" alt="Allegati {{ $immobile->nome }}">{{ $immobile->nome }}</a>
        <span class="weight-300">/ Rilievo 3d</span>
    @endcomponent
@endsection


@section('content')



    @if($immobile->rilievo)

        @include('app.partials.menuimmobilerilievo')

        @block(['class' => 'block--bordered', 'title'  => $immobile->rilievo->titolo])

            prova

        @endblock

    @else

        <div class="empty">
            <div class="empty-icon">
                <i class="icon icon-people"></i>
            </div>
            <p class="empty-title h5">Richiedi rilievo 3D</p>
            <p class="empty-subtitle">Inserisci il titolo del rilievo per iniziare.</p>
            <form action="{{ route('immobile.rilievo.store', $immobile) }}" method="post">
                @csrf

                <div class="empty-action input-group input-inline @error('titolo') {{ 'has-error' }} @enderror">
                    <button class="btn btn-primary input-group-btn">Inizia</button>
                </div>

            </form>
        </div>

    @endif

@endsection
