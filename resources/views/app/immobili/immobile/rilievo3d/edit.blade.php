@extends('app.layouts.immobile')

@section('title', 'Rilievo 3d')


@section('headertitle')
    @component('app.components.headertitle')
        <a href="{{ route('immobile', $immobile) }}" alt="Allegati {{ $immobile->nome }}">{{ $immobile->nome }}</a>
        <span class="weight-300">/ Rilievo 3d</span>
    @endcomponent
@endsection


@section('content')

@block(['class' => 'block--bordered'])
@if($immobile->rilievo->stato == 'Bozza') {!! '<span class="label  label-rounded">Bozza</span>' !!} @endif
        <small>Rif. {{ $immobile->rilievo->id }}</small>       

@if($immobile->rilievo->stato == 'Bozza')
<form action="{{ route('immobile.rilievo.destroy', $immobile->rilievo->id) }}" method="post">
  @csrf
  @method('delete')
  <button type="submit" class="btn btn-error btn-link">elimina</button>
</form>
@endif

<div class="columns">

  <form id="formRichiesta" action="{{ route('immobile.rilievo.update', $immobile ) }}" method="post" class="column">
      @csrf
  
      <div class="form-group  @error('titolo') {{ 'has-error' }} @enderror">
        <input type="hidden" name="titolo" value="{{ $immobile->rilievo->titolo }}">  
      </div>
  
      @php
      /*
      STANDARD ###########
      ###################
      */
      @endphp

      <fieldset class="panel bg-white">
        <div class="panel-header">
          <div class="panel-section-title"><strong>Standard</strong></div>
        </div>
  
        <div class="panel-body">
  
          <div class="columns">
  
            <div class="form-group column col-4">
              <label class="form-label" for="mq">Mq</label>
              <input class="form-input input-lg" type="number" id="mq" name="mq" value="{{ $immobile->informazione->metri_quadri }}">
            </div>
  
            <div class="form-group column col-4">
              <label class="form-label" for="piani">N° piani</label>
              <input class="form-input input-lg" type="number" id="piani" name="piani" value="{{old('piani') ? old('piani') : $immobile->rilievo->standard['piani']}}">
            </div>
  
            <div class="form-group column col-4">
              <label class="form-label" for="vani">Vani</label>
              <input class="form-input input-lg" type="number" id="vani" name="vani" value="{{old('vani') ? old('vani') : $immobile->rilievo->standard['vani']}}">
            </div>
  
          </div>
  
          <div class="columns">
            
            @php /* si recuperano in automatico le distanze. se la distanza è inferiore a 400km si caolcola il preventivo in automatico inviando una email dopo 1 ora circa.
            Se la distanza dall'immobile è superiore a 400km si fa il preventivo manuale */ @endphp
            <input class="form-input input-lg" type="hidden" id="address" name="address" value="{{ $immobile->informazione->address }}">
            <input type="hidden" id="indirizzo" name="indirizzo"  value="">
            <input type="hidden" id="distanza" name="distanza"  value="">
            <input type="hidden" id="durata" name="durata"  value="">
  
          </div>
  
          <div class="panel-section"><strong>Prestazioni richieste</strong></div>
  
          <div class="columns">
  
            <div class="form-group column col-4 col-sm-12">
  
      <label class="form-switch">
          <input type="checkbox" name="prestazioni[]" value="trueview"
          @if(!is_null($immobile->rilievo->standard['prestazioni']))
          @if( in_array('trueview', $immobile->rilievo->standard['prestazioni'] ) || ( is_array(old('prestazioni')) && in_array('trueview', old('prestazioni') ) ) ) checked  @endif-
            @else
          @if( is_array(old('prestazioni')) && in_array('trueview', old('prestazioni') ) ) checked @endif
          @endif ><i class="form-icon"></i> True View (cad Nuvola a Colori) </label>
              </div>
  
              <div class="form-group column col-4 col-sm-12">
                <label class="form-switch">
                  <input type="checkbox" name="prestazioni[]" value="restituzione"
                    @if(!is_null($immobile->rilievo->standard['prestazioni']))
                      @if( in_array('restituzione', $immobile->rilievo->standard['prestazioni'] ) || ( is_array(old('prestazioni')) && in_array('restituzione', old('prestazioni') ) ) ) checked  @endif
                    @else
                      @if( is_array(old('prestazioni')) && in_array('restituzione', old('prestazioni') ) ) checked @endif
                  @endif
                  ><i class="form-icon"></i> Restituzione (pianta 1Sez 1:100)
                </label>
              </div>
  
              <div class="form-group column col-4 col-sm-12">
                <label class="form-switch">
                  <input type="checkbox" name="prestazioni[]" value="cadbim"
                    @if(!is_null($immobile->rilievo->standard['prestazioni']))
                      @if( in_array('cadbim', $immobile->rilievo->standard['prestazioni'] ) || ( is_array(old('prestazioni')) && in_array('cadbim', old('prestazioni') ) ) ) checked  @endif
                    @else
                      @if( is_array(old('prestazioni')) && in_array('cadbim', old('prestazioni') ) ) checked @endif
                  @endif
                  ><i class="form-icon"></i> CAD/BIM (Modello 3D)
                </label>
              </div>
  
          </div>
  
        </div>
  
      </fieldset>
  
      @php
      /*
      PERSONALIZZA ###########
      ###################
      */
      @endphp
  
      <fieldset class="panel bg-white  @if($immobile->rilievo->personalizza == null) {{ 'closed' }} @endif">
            <div class="panel-header">
              <div class="panel-section-title"><strong>Personalizza <small>Opzionale</small></strong></div>
              <div class="panel-options"><label class="form-switch collapse_panel"><input type="checkbox" autocomplete="off" name="panel_personalizza" value="1" @if( $immobile->rilievo->personalizza != null || old('panel_personalizza') != null) {{ 'checked'}} @endif ><i class="form-icon"></i></label></div>
            </div>
            <div class="panel-body">
              <div class="columns">
                <div class="column col-6 col-sm-12">
  
                  <div class="form-group">
                    <label class="form-switch">
                      <input type="checkbox" name="restituzione" value="restituzione"><i class="form-icon"></i> Restituzione di sezioni Extra
                    </label>
                  </div>
  
                  <div class="panel-section"><strong>N° fotopiani</strong></div>
                  <div class="columns">
  
                    <div class="form-group column col-4 col-sm-12">
                      <label class="form-label" for="fotopiani">Pareti</label>
                      <input class="form-input input-lg" type="number" id="fotopiani_pareti" name="fotopiani_pareti" value="{{old('fotopiani') ? old('fotopiani') : $immobile->rilievo->personalizza['fotopiani']}}">
                    </div>
  
                    <div class="form-group column col-4 col-sm-12">
                      <label class="form-label" for="fotopiani">Pavimenti</label>
                      <input class="form-input input-lg" type="number" id="fotopiani_pavimenti" name="fotopiani_pavimenti" value="{{old('fotopiani') ? old('fotopiani') : $immobile->rilievo->personalizza['fotopiani']}}">
                    </div>
  
                    <div class="form-group column col-4 col-sm-12">
                      <label class="form-label" for="fotopiani">Soffitti</label>
                      <input class="form-input input-lg" type="number" id="fotopiani_soffitti" name="fotopiani_soffitti" value="{{old('fotopiani') ? old('fotopiani') : $immobile->rilievo->personalizza['fotopiani']}}">
                    </div>
  
                  </div>
  
                  <div class="panel-section"><strong>Rilievi</strong></div>
  
                  <div class="form-group">
                    <label class="form-switch">
                      <input type="checkbox" name="rilievo_arredamento" value="rilievo_arredamento"><i class="form-icon"></i> Rilievo arredamento
                    </label>
                  </div>
  
                  <div class="form-group">
                    <label class="form-switch">
                      <input type="checkbox" name="rilievo_abachifinestre" value="rilievo_abachifinestre"><i class="form-icon"></i> Rilievo Abachi finestre
                    </label>
                  </div>
  
                  <div class="form-group">
                    <label class="form-switch">
                      <input type="checkbox" name="rilievo_impiantoelettrico" value="rilievo_impiantoelettrico"><i class="form-icon"></i> Rilievo impianto elettrico
                    </label>
                  </div>
  
                  <div class="form-group">
                    <label class="form-switch">
                      <input type="checkbox" name="rilievo_impiantoriscaldamento" value="rilievo_impiantoriscaldamento"><i class="form-icon"></i> Rilievo impianto riscaldamento
                    </label>
                  </div>
  
                  <div class="form-group">
                    <label class="form-switch">
                      <input type="checkbox" name="rilievo_quadrofessurativo" value="rilievo_quadrofessurativo"><i class="form-icon"></i> Rilievo quadro fessurativo
                    </label>
                  </div>
  
                  <div class="form-group">
                    <label class="form-switch">
                      <input type="checkbox" name="rilievo_materico" value="rilievo_materico"><i class="form-icon"></i> Rilievo materico
                    </label>
                  </div>
  
                  <div class="form-group">
                    <label class="form-switch">
                      <input type="checkbox" name="rilievo_termigrafico" value="rilievo_termigrafico"><i class="form-icon"></i> Rilievo termografico
                    </label>
                  </div>
  
  
  
                </div>
  
                <div class="column col-6 col-sm-12">
                  foto
                </div>
  
              </div>
            </div>
      </fieldset>
  
      @php
      /*
      ESTERNI ###########
      ###################
      */
      @endphp
  
      <fieldset class="panel bg-white @if($immobile->rilievo->esterni == null || old('panel_esterni') != null) {{ 'closed' }} @endif">
            <div class="panel-header">
              <div class="panel-section-title"><strong>Esterni <small>Opzionale</small></strong></div>
            <div class="panel-options"><label class="form-switch collapse_panel"><input type="checkbox" autocomplete="off" name="panel_esterni" @if(old('panel_esterni') != null) {{ 'checked' }}@endif><i class="form-icon"></i></label></div>
            </div>
  
            <div class="panel-body closed">
              <div class="columns">
                <div class="column col-6 col-sm-12">
  
                  <div class="form-group">
                    <label class="form-label" for="n_prospetti">N° prospetti</label>
                      <input class="form-input input-lg" type="number" id="n_prospetti" name="n_prospetti" value="{{old('n_prospetti') ? old('n_prospetti') : $immobile->rilievo->esterni['n_prospetti']}}">
                  </div>
  
                  <div class="form-group">
                    <label class="form-switch">
                      <input type="checkbox" name="copertura" value="copertura"><i class="form-icon"></i> Copertura
                    </label>
                  </div>
  
                  <div class="form-group">
                    <label class="form-label" for="n_fotopiani">N° fotopiani</label>
                      <input class="form-input input-lg" type="number" id="n_fotopiani" name="n_fotopiani" value="{{old('n_fotopiani') ? old('n_fotopiani') : $immobile->rilievo->esterni['n_fotopiani']}}">
                  </div>
  
                  <div class="form-group">
                    <label class="form-switch">
                      <input type="checkbox" name="pilievo_materico" value="pilievo_materico"><i class="form-icon"></i> Rilievo materico
                    </label>
                  </div>
  
                  <div class="form-group">
                    <label class="form-switch">
                      <input type="checkbox" name="analisi_degrado" value="analisi_degrado"><i class="form-icon"></i> Analisi del degrado
                    </label>
                  </div>
  
                </div>
  
              </div>
          </div>
      </fieldset>
  
      @php
      /*
      AREA ###########
      ###################
      */
      @endphp
  
      <fieldset class="panel bg-white @if($immobile->rilievo->area == null) {{ 'closed' }} @endif">
  
          <div class="panel-header">
            <div class="panel-section-title"><strong>Area <small>Opzionale</small></strong></div>
            <div class="panel-options"><label class="form-switch collapse_panel"><input type="checkbox" autocomplete="off" name="panel_area"><i class="form-icon"></i></label></div>
          </div>
  
          <div class="panel-body">
        </div>
      </fieldset>
  
      @php
      /*
      NOTE ###########
      ###################
      */
      @endphp
  
  
  
      <div class="form-group">
          <textarea class="form-input input-xl"  name="note" placeholder="Scrivi qui le tue note">{{old('note') ? old('note') : $immobile->rilievo->note}}</textarea>
      </div>
  
      @if($immobile->rilievo->stato == 'Bozza')
      <button class="btn btn-padding btn-outline"  name="action" value="save">Salva bozza</button>
      <button class="btn btn-padding btn-primary" name="action" value="send">Invia richiesta</button>
      @endif 
  
  </form>
  
  </div>


  @endblock
  
@endsection



@section('footerjs_after')

<script>


  /* ***********************************
  **************************************
  GOOGLE MAPS
  **************************************
  ************************************** */
  
  
  function createAddressResult(lat, lng, address) {
      var locationInfo = {
          geo: null,
          paese: null,
          regione: null,
          provincia: null,
          comune: null,
          localita: null,
          cap: null,
          indirizzo: null,
          numerocivico: null,
          reset: function() {
              this.geo = null;
              this.paese = null;
              this.regione = null;
              this.provincia = null;
              this.comune = null;
              this.localita = null;
              this.cap = null;
              this.indirizzo = null;
              this.numerocivico = null;
          }
      };
  
      locationInfo.reset();
  
      locationInfo.geo = [lat, lng];
      for (var i = 0; i < address.length; i++) {
          var component = address[i].types[0];
          switch (component) {
              case "country":
                  locationInfo.paese = address[i]["long_name"];
                  break;
              case "administrative_area_level_1":
                  locationInfo.regione = address[i]["long_name"];
                  break;
              case "administrative_area_level_2":
                  locationInfo.provincia = address[i]["short_name"];
                  break;
              case "administrative_area_level_3":
                  locationInfo.comune = address[i]["long_name"];
                  break;
              case "locality":
                  locationInfo.localita = address[i]["long_name"];
                  break;
              case "postal_code":
                  locationInfo.cap = address[i]["long_name"];
                  break;
              case "route":
                  locationInfo.indirizzo = address[i]["long_name"];
                  break;
              case "street_number":
                  locationInfo.numerocivico = address[i]["long_name"];
                  break;
              default:
                  break;
          }
      }
  
      var result = JSON.stringify(
          locationInfo,
          null,
          4
      );
  
      if (!result) {
          alert('nooooo');
      }
      document.getElementById("indirizzo").value = result;
  
  }
  
  
  $('#address').focusin(function() {
  
      var input = document.getElementById('address');
  
      var options = {
          types: ['geocode'],
          componentRestrictions: { country: 'it' }
      };
  
      var autocomplete = new google.maps.places.Autocomplete(input, options);
  
      google.maps.event.addListener(autocomplete, "place_changed", function() {
          var place = autocomplete.getPlace(),
              address = place.address_components,
              lat = place.geometry.location.lat(),
              lng = place.geometry.location.lng();
  
          createAddressResult(lat, lng, address);
          calculateDistance(lat, lng);
      });
  
  });
  
  $(document).ready(function() {
  
      var address = document.getElementById('address').value;
      var geocoder = new google.maps.Geocoder();
  
      if (!address) {
          return;
      }
  
      geocoder.geocode({ 'address': address }, function(results, status) {
          if (status === 'OK') {
  
              address = results[0].address_components;
              lat = results[0].geometry.location.lat();
              lng = results[0].geometry.location.lng();
  
              createAddressResult(lat, lng, address);
              calculateDistance(lat, lng);
  
          } else {
              alert('Geocode was not successful for the following reason: ' + status);
          }
      });
  
  
  });
  
  
  function calculateDistance(lat, lng) {
      var origin = new google.maps.LatLng(43.6257445, 11.4840228);
      var destination = new google.maps.LatLng(lat, lng);
  
      var service = new google.maps.DistanceMatrixService();
      service.getDistanceMatrix({
          origins: [origin],
          destinations: [destination],
          travelMode: 'DRIVING',
          //transitOptions: TransitOptions,
          //drivingOptions: DrivingOptions,
          //unitSystem: UnitSystem,
          //avoidHighways: Boolean,
          //avoidTolls: Boolean,
      }, callback);
  
      function callback(response, status) {
  
          var outputDiv = document.getElementById('output');
  
          var results = response.rows[0].elements;
          var distance = results[0].distance.text;
          var duration = results[0].duration.text;
  
          document.getElementById("distanza").value = btoa(parseFloat(distance.replace(/\./g, "")));
          document.getElementById("durata").value = btoa(duration);
  
          /*
          if (parseFloat(distance.replace(/\./g, "")) > 400) {
              console.log('maggiore');
          }
          */
  
      }
  
  }
  
  
  </script>


@endsection
