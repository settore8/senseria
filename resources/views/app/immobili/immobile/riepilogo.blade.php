@extends('app.layouts.immobile')

@section('title', 'Riepilogo immobile')
@section('bodyclass')@parent {{ 'riepilogo' }}@endsection

@section('headertitle')
    @component('app.components.headertitle')
        @include('app.partials.titleimmobile')
    @endcomponent
@endsection


@section('content')
        <div class="flex">

            <div class="item">

                <div class="dashboard-block">
                    <div class="dashboard-block__header">
                        <span class="immobile-codice">Rif <strong>{{ $immobile->codice }}</strong></span>
                        <nav class="dashboard-block__nav">
                            <a href="{{ route('immobile.informazioni', $immobile) }}" class="tooltip" data-tooltip="Modifica"><svg width="15" height="15"><use xlink:href="#icon-pen"></use></svg></a> 
                        </nav>
                    </div>
                    @if($immobile->informazione)
                        <div class="dashboard-block__body">
                            <div class="columns">
                                <div class="column col-8">
                                    <div><strong>{{ $immobile->specifica->nome_pubblico }}</strong> {{ $immobile->informazione->metri_quadri }} Mq</div>
                                    <div>
                                    <svg class="icon--15 mr1"><use xlink:href="#icon-marker"></use></svg> 

                                    {{ $immobile->informazione->comune }} @if($immobile->informazione->provincia) {{ $immobile->informazione->provincia }} @endif
                                    </div>
                                    <div>
                                    {{ $immobile->informazione->indirizzo }} 
                                    </div>
                                    @php 
                                        /*
                                        @if($immobile->informazione->locali)
                                            @foreach($immobile->informazione->locali as $locali)
                                                @foreach ( $locali as $k => $c)
                                                        {{ $k }} {{ $c }}
                                                @endforeach
                                            @endforeach
                                        @endif
                                        */
                                    @endphp
                                </div>
                                <div class="column col-4">
                                    <svg class="destinazione icon--50"><use xlink:href="#destinazione-{{ strtolower($immobile->destinazione->nome) }}"></use></svg>
                                </div>
                            </div>  
                        </div>
                    @endif
                </div>
            </div>
                
            <div class="item">
                <div class="dashboard-block">
                    <div class="dashboard-block__header">
                        Stato di verifica
                    </div>
                    <div class="dashboard-block__body">
                        @php 
                            $rand = rand(0, 100);
                        @endphp
                        <div class="chart chart--lg" data-percent="{{ $rand  }}" data-scale-color="#42d212"><span class="percentuale"><span class="perc_value">{{ $rand  }}</span><span class="perc_sign">%</span></span></div>
                    </div>
                </div>
            </div>
            <!--
            <div class="item ">
                <div class="dashboard-block">
                    <div class="dashboard-block__header">
                        Vendita
                        <nav class="dashboard-block__nav">
                            <a href="{{ route('immobile.vendita', $immobile) }}" class="tooltip" data-tooltip="Modifica"><svg width="15" height="15"><use xlink:href="#icon-pen"></use></svg></a> 
                        </nav>
                    </div>
                    <div class="dashboard-block__body">
                        <div class="chart-container">
                            <canvas id="myChart"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        -->
        </div>

        <div class="flex">
            <div class="item">
                <div class="dashboard-block">
                    <div class="dashboard-block__header">
                        Servizi tecnici
                        <nav class="dashboard-block__nav">
                            <a href="{{ route('immobile.responsabiletecnico', $immobile) }}" class="tooltip" data-tooltip="Modifica"><svg width="15" height="15"><use xlink:href="#icon-pen"></use></svg></a> 
                        </nav>
                    </div>
                    <div class="dashboard-block__body">

                        @if($immobile->tecnico)
                            <div class="columns">
                                
                                <div class="column col-4 col-xs-12">
                                    <div><strong>Resposanbile tecnico</strong></div>
                                    <div class="">{{ $immobile->tecnico->qualifica->nome_pubblico }}</div>
                                    {{  $immobile->tecnico->fullname }}
                                </div>

                                <div class="column col-8 col-xs-12">
                                    <h3>Servizi abilitati</h3>
                                    @if($immobile->moduli)
                                        @foreach($immobile->moduli as $modulo)
                                        {{ $modulo }}
                                        @endforeach
                                    @endif
                                </div>

                            </div>
                        @elseif($immobile->annuncioRespTecnico)

                            {{ $immobile->annuncioRespTecnico->codice }}

                            @if($immobile->annuncioRespTecnico->candidature)
                                <h3>Candidati</h3>
                                @foreach ($immobile->annuncioRespTecnico->candidature->groupBy('user.nome_cognome') as $candidato => $candidature)
                                        {{ $candidato }} <br>
                                @endforeach
                            @else
                                Nessun canditato
                            @endif

                            
                            @if($immobile->moduli)
                                Servizi tecnici richiesti
                                @foreach($immobile->moduli as $modulo)
                                {{ $modulo }}
                                @endforeach
                             @endif

                            @php 
                            /*
                            @forelse ($immobile->annuncioRespTecnico->dettagli as $dettaglio)
                                {{ $dettaglio->coverable->nome_pubblico }};
                            @empty
                               Nessuno
                            @endforelse
                            */ 
                            @endphp
                        @else
                            Nessun responsabile tecnico
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="flex">

            <div class="item">
                @if(Auth::user()->id == $immobile->user_id)
                <div class="dashboard-block">
                    <div class="dashboard-block__header">
                        Vendita
                        <nav class="dashboard-block__nav">
                            <a href="" class="current tooltip" data-tooltip="Vendita" data-tab="#tab_vendita">Gestisci</a>
                            <a href="" class="tooltip" data-tooltip="Statistiche annuncio di vendita" data-tab="#tab_vendita_statistiche">Statistiche</a>
                        </nav>
                    </div>
                    <div class="dashboard-block__body flex">

                        <div class="tabs">

                            <div class="tab show" id="tab_vendita">

                                @if($immobile->agente)
                                    Mettiamo la scheda dell'agente immobiliare
                                @else
                                    Immmobile non in vendita
                                @endif

                            </div>
    
                            <div class="tab" id="tab_vendita_statistiche">
                                <div class="chart-container">
                                    <canvas id="myChart"></canvas>
                                </div>
                            </div>
    
                        </div>
                    </div>
                </div>
                @else                
                    si mostra solo al  responsabile tecnico


                @endif
            </div>

        </div>

@endsection



@section('footerjs_after')
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.3/dist/Chart.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/easy-pie-chart/2.1.6/jquery.easypiechart.min.js"></script>

<script>

$(function() {

    $('.chart').easyPieChart({
            size: 90,
            scaleLength: 0,
            barColor: function (percent) {
                return (percent < 20 ? '#d20b0b' : percent < 50 ? '#f38910' : percent < 70 ? '#eedf14' : percent < 85 ? '#c5d212' : '#42d212');
            },
            lineWidth: 12,
            trackColor: "#e0e0e0",
            lineCap: "circle",
            animate: 2000,
        });
    });

    var ctx = document.getElementById('myChart');
    ctx.height = 150;
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
            datasets: [{
                label: '# of Votes',
                data: [12, 19, 3, 5, 2, 3],
                backgroundColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    display: false,
                    ticks: {
                        beginAtZero: true
                    }
                }],
                xAxes: [{
                    display: false 
                }
                ]

            }
        }
    });


    </script>

@endsection