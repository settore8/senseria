@extends('app.layouts.immobile')

@section('title', 'Statistiche')
@section('bodyclass'){{ 'app immobile statistiche' }}@endsection


@section('headertitle')
    @component('app.components.headertitle')
        @include('app.partials.titleimmobile', ['childs' => ['Statistiche']])
    @endcomponent
@endsection


@section('content')
   

<div class="flex">
    <div class="item item-2">
        <div class="dashboard-block">
            <div class="dashboard-block__header">Vendita</div>
            <div class="dashboard-block__body">
                <div class="chart-container">
                    Se l'immobile è in vendita mettiamo le statistiche sulle visite ricevute dall'annuncio dell'immobile. Se presente anche su senseria mettiamo anche quelle
                    <!--<canvas id="myChart" width="400" height="400"></canvas>-->
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

@section('footerjs_after')
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.3/dist/Chart.min.js"></script>

<script>
    var ctx = document.getElementById('myChart');
    ctx.height = 300;
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: ['Gen', 'Feb', 'Mar', 'Aprile', 'Maggio', 'Giugno', 'luglio', 'Agosto', 'Settembre', 'Ottobre', 'Novembre', 'Dicembre'],
            datasets: [{
                label: 'Visite',
                data: [135, 19, 3, 5, 90, 3, 70, 19, 3, 67, 90, 3,],
                borderColor: '#2f4cd7',
                pointBackgroundColor: '#2f4cd7',
                pointRadius: 5,
                pointHoverRadius: 7,
                backgroundColor: 'transparent'
            },
            {
                label: 'Visite uniche',
                data: [120, 170, 25, 5, 10, 30, 23, 120, 3, 100, 90, 3,],
                borderColor: '#ffed00',
                pointBackgroundColor: '#ffed00',
                pointRadius: 5,
                pointHoverRadius: 7,
                backgroundColor: 'transparent'
            },
        ]
        },
        maintainAspectRatio: false,
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            },
            tooltips: {
            callbacks: {
                labelColor: function(tooltipItem, chart) {
                    return {
                        borderColor: 'rgb(255, 0, 0)',
                        backgroundColor: 'rgb(255, 0, 0)'
                    };
                },
                labelTextColor: function(tooltipItem, chart) {
                    return '#543453';
                }
            }
        }
        }
    });
</script>

@endsection