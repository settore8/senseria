@extends('app.layouts.immobile')

@section('title', 'Condivisione')

@section('headertitle')
    @component('app.components.headertitle')
    @include('app.partials.titleimmobile', ['childs' => ['Condivisione']])
    @endcomponent
@endsection


@section('content')

        @block(['class' => 'block--bordered'])
        
            <form action="{!! route('action.condividi', $immobile) !!}" method="POST">
                <div class="columns">
                    <div class="column col-8 col-sm-12">
                        <p>La condivisione dell'immobile <strong>ti permette di far visionare l'immobile e le sue caratteristiche ad uno o più professionisti e imprese</strong>. Inserisci l'email del destinatario e seleziona i contenuti da condividere. Il destinatario riceverà una email con un <strong>link riservato</strong> per la consultazione dell'immobile</p>        
                        @csrf
                    </div>
                    <div class="column col-4 col-sm-12">
                        <img src="{{ asset('images/tips/tip-condivisione-immobile.svg') }}" alt="Condivisione immobile">
                    </div>
                </div>

                <div class="columns">
                    <div class="column col-12">
                        <h3 class="mt20">Destinatari</h3>
                        <label>
                            <input type="email" class="form-input input-lg" name="email">
                            <p class="form-input-hint">Inserisci una o più email separate da virgola</p>
                        </label>
                    </div>
                </div>
            
                <div class="columns">
                        <div class="column col-6 col-sm-12">
                            <h3 class="mt20">Cosa vuoi condividere?</h3>
                            <div class="form-group">
                                <label class="form-switch">
                                    <input type="checkbox" name="dettagli[]" value="informazioni">
                                    <i class="form-icon"></i>
                                    Informazioni sull'immobile
                                </label>
                            </div>

                            <div class="form-group">
                                <label class="form-switch">
                                    <input type="checkbox" name="dettagli[]" value="foto">
                                    <i class="form-icon"></i>
                                    Foto
                                </label>
                            </div>

                            <div class="form-group">
                                <label class="form-switch">
                                    <input type="checkbox" name="dettagli[]" value="foto">
                                    <i class="form-icon"></i>
                                    Documenti
                                </label>
                            </div>

                            <div class="form-group">
                                <label class="form-switch">
                                    <input type="checkbox" name="dettagli[]" value="vendita">
                                    <i class="form-icon"></i>
                                    Informazioni vendita
                                </label>
                            </div>

                            @if($immobile->moduli)
                                @foreach($immobile->moduli as $modulo)
                                <div class="form-group">
                                    <label class="form-switch">
                                        @php 
                                        $mod = App\Modulo::where('nome', $modulo)->first();
                                        @endphp 
                                        <input type="checkbox" name="moduli[]" value="{!! $mod->id !!}">
                                        <i class="form-icon"></i>
                                        {!! $mod->nome_pubblico !!}
                                    </label>
                                </div>
                                @endforeach
                            @endif

                        </div>

                        <div class="column col-6 col-sm-12">
                            <h3 class="mt20">Scadenza condivisione</h3>
                            <input class="form-input input-lg" id="scadenza" type="date" name="scadenza" min="{{ $today->format('Y-m-d') }}" value="{{ $today->addDays(30)->format('Y-m-d') }}" required>
                            <p class="form-input-hint">Scegli una data di scadenza della condivisione</p>
                        </div>

                </div>
           
                @button(['attrs' => ['id' => 'condividi-immobile', 'text' => 'Condividi']])@endbutton

        </form>
        
        @endblock

       

        @if($immobile->condivisioni->isNotEmpty())
        @block(['title' => 'Storico condivisioni', 'class' => 'block--bordered'])
            <ul>
                @foreach($immobile->condivisioni as $condivisione)
                    <li>
                    @foreach (json_decode($condivisione->email) as $email)
                            {{ $email}}
                    @endforeach
                        {{ $condivisione->scadenza}}
                        <form action="" method="post">
                            @csrf
                            <button class="text-error">elimina</button>
                        </form>
                    </li>
                @endforeach
            </ul>
        @endblock
        @else
        @block(['class' => 'block--bordered text-center'])
            <p>Nessuna condivisione per questo immobile</p>
        @endblock
        @endif

       

       
@endsection