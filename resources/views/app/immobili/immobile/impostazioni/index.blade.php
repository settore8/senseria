@extends('app.layouts.immobile')

@section('title', 'Impostazioni')
@section('bodyclass'){{ 'app immobile' }}@endsection


@section('headertitle')
    @component('app.components.headertitle')
    @include('app.partials.titleimmobile', ['childs' => ['Impostazioni']])
    @endcomponent
@endsection


@section('content')

        @if($immobile->informazione)

        @block(['title' => 'Responsabile tecnico', 'class' => 'block--bordered block--primary' ])
            <div class="columns">
                <div class="column col-12">
                    <p>Per gestire le impostazioni del responsabile tecnico ed i servizi tecnici abilitati, visita l'<a href="{!! route('immobile.responsabiletecnico', $immobile) !!}">apposita sezione</a>.</p>
                </div>
            </div>
        @endblock

        @block(['title' => 'Archivia immobile', 'class' => 'block--bordered block--warning' ])
        <form action="{{ route('immobile.archivia', $immobile) }}" method="post">
            @csrf
            <div class="columns">
                <div class="column col-6 col-xs-12">
                    <p>Archiviando l'immobile lo troverai nell'apposita sezione. <br/><strong>Potrai sempre recuperarlo in futuro</strong></p>
                </div>
                <div class="column col-5 col-ml-auto col-xs-12">
                    <div class="form-group">
                        <label>Digita ARCHIVIA nel campo sottostante</label>
                        <input type="text" class="form-input input-lg" name="action">
                    </div>
                    @button(['attrs' => ['text' => 'Archivia immobile', 'class' => 'btn-warning btn-padding']])@endbutton
                </div>
            </div>
        </form>
        @endblock

        @endif

        @block(['title' => 'Cancella immobile', 'class' => 'block--bordered block--danger' ])
        <form action="{{ route('immobile.destroy', $immobile) }}" method="post">
            @csrf
            <div class="columns">
                <div class="column col-6 col-xs-12">
                    <p>La cancellazione dell'immobile è un'azione irreversibile. <br/><strong>Sei sicuro di voler provcedere?</strong></p>
                    @if($immobile->informazioni)
                        <p>Puoi salvare l'immobile sul tuo computer, effettuando la procedura di Download</p>
                    @endif
                </div>
                <div class="column col-5 col-ml-auto col-xs-12">
                    <div class="form-group">
                        <label>Digita CANCELLA nel campo sottostante</label>
                        <input type="text" class="form-input input-lg" name="action">
                    </div>
                    @button(['attrs' => ['text' => 'Cancella immobile', 'class' => 'btn-error btn-padding']])@endbutton
                </div>
            </div>
        </form>
        @endblock

@endsection