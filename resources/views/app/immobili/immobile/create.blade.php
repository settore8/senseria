@extends('app.layouts.master')

@section('title', 'Nuovo immobile')

@section('headertitle')
    @component('app.components.headertitle')
    @slot('back'){{ route('immobili') }}@endslot
    Nuovo immobile
    @endcomponent
@endsection

@section('content')

<form action="{!! route('immobile.store') !!}" method="POST">
    @csrf

    @block(['title' => 'Tipologia dell\'immobile', 'class' => 'block--bordered' ])
        @include('app.partials.selettoreimmobile')

        <hr class="hr mt20 mb20"/>

        <div class="columns">
            <div class="column col-6 col-md-12">
                <div class="form-group form-group-xl">
                        <label>Specifica</label>
                        <input id="specificaold" type="hidden" value="{{ old('specifica')}}">
                        <select id="specifica" name="specifica" class="form-select select select-xl" disabled></select>
                </div>
            </div>

            <div class="column col-6 col-md-12">
                <label>Nome <small class="badge">Visibile solo a te</small></label> 
                <div class="form-group">
                    <input type="text" name="nome" class="form-input input-xl" value="{{ old('nome')}}">
                </div>
            </div>

        </div>

    @button(['attrs' => ['text' => 'Crea immobile']])@endbutton

    @endblock

    
</form>      

@endsection

