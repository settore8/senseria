<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="robots" content="noindex">
    <title>Workook viewer</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="apple-touch-icon" sizes="180x180" href="/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicons/favicon-16x16.png">
    <link rel="manifest" href="/favicons/site.webmanifest">
    <link rel="mask-icon" href="/favicons/safari-pinned-tab.svg" color="#2f4cd7">
    <meta name="msapplication-TileColor" content="#2f4cd7">
    <meta name="theme-color" content="#2f4cd7">

</head>
<body>
    
    <div class="container grid-md">

    <h1>Workook Viewer</h1>
    {{ $immobile->codice }}

    @if(isset($ic->options['dettagli']))
        @foreach(json_decode($ic->options['dettagli']) as $dettaglio)
            <h2>{{$dettaglio}}</h2>
        @endforeach
    @endif

    @if(isset($ic->options['moduli']))
       @if($ic->options['moduli'] != 'null')
        @foreach(json_decode($ic->options['moduli']) as $modulo)
            @php 
            $mod = App\Modulo::where('nome', $modulo)->first();
            @endphp
            <h2>{{ $modulo }}</h2>

            @if($modulo == 'sopralluogo')

            @endif
            
        @endforeach
       @endif
    @endif

    </div>
        
</body>
</html>


