@extends('app.layouts.master')

@php
/******************
 * HEADER
*/
@endphp

@section('headermenu')
    @component('app.components.headermenu')
        @if(Auth::user()->id == $immobile->user_id)
        <li>
            <a href="{{ route('immobile.statistiche', $immobile) }}" class="has-notify tooltip tooltip-bottom @if(Route::currentRouteName() == 'immobile.statistiche') {{ 'current' }} @endif" data-tooltip="Statistiche"><svg width="23" height="23"><use xlink:href="#icon-statistiche"></use></svg></a>
        </li>
        <li>
            <a href="{{ route('immobile.condivisione', $immobile) }}" class="tooltip tooltip-bottom @if(Route::currentRouteName() == 'immobile.condivisione') {{ 'current' }} @endif" data-tooltip="Condividi immobile"><svg width="23" height="23"><use xlink:href="#icon-share"></use></svg></a>
        </li>
        <li>
            <a href="{{ route('immobile.download', $immobile) }}" class="tooltip tooltip-bottom @if(Route::currentRouteName() == 'immobile.download') {{ 'current' }} @endif" data-tooltip="Download immobile @if(Route::currentRouteName() == 'immobile.download') {{ 'current' }} @endif"><svg width="23" height="23"><use xlink:href="#icon-download"></use></svg></a>
        </li>
        <li>
            <a href="{{ route('immobile.impostazioni', $immobile) }}" class="has-notify tooltip tooltip-bottom  @if(Route::currentRouteName() == 'immobile.impostazioni') {{ 'current' }} @endif" data-tooltip="Impostazioni"><svg width="23" height="23"><use xlink:href="#icon-settings"></use></svg></a>
        </li>
        @endif
    @endcomponent
@endsection

@php
/******************
 * SIDEBAR
*/
@endphp

@section('sidebar')

    @component('app.components.sidebar', ['type' => 'scrollable'])
            <li><a href="{{ route('immobile', $immobile) }}" @if(Route::currentRouteName() == 'immobile') class="current" @endif>Riepilogo</a></li>
            <li><a href="{{ route('immobile.informazioni', $immobile) }}" @if(Route::currentRouteName() == 'immobile.informazioni') class="current" @endif>Informazioni</a></li>
            <li><a href="{{ route('immobile.allegati', $immobile) }}" @if(Route::currentRouteName() == 'immobile.allegati') class="current" @endif >Foto e allegati</a></li>
            @if(Auth::user()->id == $immobile->user_id)
            <li><a href="{{ route('immobile.vendita', $immobile) }}"  @if(Route::currentRouteName() == 'immobile.vendita') class="current" @endif>Vendita @if($immobile->annuncioVendita)
                <span class="badge badge--success badge--xs ml3">In pubblicità</span>
        @endif</a></li>
            <li><a href="{{ route('immobile.responsabiletecnico', $immobile) }}" @if(Route::currentRouteName() == 'immobile.responsabiletecnico') class="current" @endif>Responsabile tecnico</a></li>
            @endif
            <li class="divider">Servizi tecnici</li>
            @foreach(\App\Modulo::orderBy('order', 'ASC')->get() as $modulo)
                        @if(!$immobile->moduli || !in_array($modulo->nome, $immobile->moduli))
                        <li><a href="{{ route($modulo->route_name, $immobile) }}" class="disabled" data-modulo-menu="{{ $modulo->nome }}">
                            {!! $modulo->nome_pubblico !!} <svg><use xlink:href="#icon-lock" width="14" height="14"></use></svg>
                            </a>
                        </li>
                        @else
                        <li>
                            <a href="{{ route($modulo->route_name, $immobile) }}" data-modulo-menu="{{ $modulo->nome }}" @if(Route::currentRouteName() == $modulo->route_name) class="current" @endif>
                            {!! $modulo->nome_pubblico !!} <svg class="hidden"><use xlink:href="#icon-lock" width="14" height="14"></use></svg>
                            </a>
                        </li>
                        @endif
            @endforeach
            <li class="divider">
                Servizi Workook
            </li>
            <li><a href="{{ route('immobile.visuraipotecaria', $immobile) }}" @if(Route::currentRouteName() == 'immobile.visuraipotecaria') class="current" @endif>Visura ipotecaria</a></li>
            <li><a href="{{ route('immobile.rilievo', $immobile) }}" @if(Request::is('*/rilievo') || Request::is('*/rilievo/*')) class="current" @endif>Rilievo 3d</a></li>
        @endcomponent
@endsection


@php
/******************
 * FOOTER
*/
@endphp
@section('footer')

@endsection
