@extends('app.layouts.master')

@php
/******************
 * HEADER
*/
@endphp

@section('headermenu')
    @component('app.components.headermenu')
    @include('app.partials.headermenuimmobile')
    @endcomponent
@endsection

@php
/******************
 * SIDEBAR
*/
@endphp

@section('sidebar')

        @component('app.components.sidebar', ['type' => 'scrollable'])

        @if(Request::is('*/sopralluogo') || Request::is('*/sopralluogo/*'))
        <li><a href="{{ route('immobile.sopralluogo', $immobile) }}" @if(Request::is('*/sopralluogo')) class="current" @endif>Sopralluogo</a></li>
        <li><a href="{{ route('immobile.sopralluogo.contesto', $immobile) }}" @if(Request::is('*/sopralluogo/contesto')) class="current" @endif>Contesto @if($immobile->sopralluogo && $immobile->sopralluogo->contesto) <span class="status completed"></span> @endif</a></li>
        <li><a href="{{ route('immobile.sopralluogo.fabbricato', $immobile) }}" @if(Request::is('*/sopralluogo/fabbricato') || Request::is('*/sopralluogo/fabbricato/*')) class="current" @endif>Fabbricato @if($immobile->sopralluogo && $immobile->sopralluogo->fabbricato) <span class="status completed"></span> @endif</a>
        </li>
        <li><a href="{{ route('immobile.sopralluogo.unitaimmobiliare', $immobile) }}" @if(Request::is('*/sopralluogo/unita-immobiliare') || Request::is('*/sopralluogo/unita-immobiliare/*')) class="current" @endif>Unità immobiliare @if($immobile->sopralluogo && $immobile->sopralluogo->unitaimmobiliare) <span class="status completed"></span> @endif</a>
        </li>
        <li><a href="{{ route('immobile.sopralluogo.particondominiali', $immobile) }}" @if(Request::is('*/sopralluogo/parti-condominiali')) class="current" @endif>Parti condominiali @if($immobile->sopralluogo && $immobile->sopralluogo->particondominiali) <span class="status completed"></span> @endif</a></li>
        <li><a href="{{ route('immobile.sopralluogo.fotovideo', $immobile) }}" @if(Request::is('*/sopralluogo/fotovideo')) class="current" @endif>Foto e video @if($immobile->sopralluogo && $immobile->sopralluogo->fotovideo) <span class="status completed"></span> @endif</a></li>
        @endif

        @if(Request::is('*/verifica-tecnica') || Request::is('*/verifica-tecnica/*'))
        <li><a href="{{ route('immobile.verificatecnica', $immobile) }}" @if(Request::is('*/verifica-tecnica')) class="current" @endif>Verifica tecnica</a></li>
        <li><a href="{{ route('immobile.verificatecnica.datiurbanistici', $immobile) }}" @if(Request::is('*/verifica-tecnica/datiurbanistici') || Request::is('*/verifica-tecnica/datiurbanistici/*') ) class="current" @endif>Dati urbanistici</a></li>
        <li><a href="{{ route('immobile.verificatecnica.catasto', $immobile) }}" @if(Request::is('*/verifica-tecnica/catasto') || Request::is('*/verifica-tecnica/catasto/*') ) class="current" @endif>Catasto</a></li>
        <li><a href="{{ route('immobile.verificatecnica.provenienza', $immobile) }}" @if(Request::is('*/verifica-tecnica/provenienza') || Request::is('*/verifica-tecnica/provenienza/*') ) class="current" @endif>Provenienza </a></li>
        <li><a href="{{ route('immobile.verificatecnica.agibilita', $immobile) }}" @if(Request::is('*/verifica-tecnica/agibilita') || Request::is('*/verifica-tecnica/agibilita/*') ) class="current" @endif>Agibilità</a></li>
        <li><a href="{{ route('immobile.verificatecnica.impianti', $immobile) }}" @if(Request::is('*/verifica-tecnica/impianti') || Request::is('*/verifica-tecnica/impianti/*') ) class="current" @endif>Impianti</a></li>
        <li><a href="{{ route('immobile.verificatecnica.caldaia', $immobile) }}" @if(Request::is('*/verifica-tecnica/caldaia') || Request::is('*/verifica-tecnica/caldaia/*') ) class="current" @endif>Caldaia</a></li>
        <li><a href="{{ route('immobile.verificatecnica.ape', $immobile) }}" @if(Request::is('*/verifica-tecnica/ape') || Request::is('*/verifica-tecnica/ape/*') ) class="current" @endif>APE</a></li>
        <li><a href="{{ route('immobile.verificatecnica.cdu', $immobile) }}" @if(Request::is('*/verifica-tecnica/cdu') || Request::is('*/verifica-tecnica/cdu/*') ) class="current" @endif>CDU</a></li>
        <li><a href="{{ route('immobile.verificatecnica.postuma', $immobile) }}" @if(Request::is('*/verifica-tecnica/postuma') || Request::is('*/verifica-tecnica/postuma/*') ) class="current" @endif>Postuma</a></li>
        <li><a href="{{ route('immobile.verificatecnica.altridocumenti', $immobile) }}" @if(Request::is('*/verifica-tecnica/altriimmobili') || Request::is('*/verifica-tecnica/altriimmobili/*') ) class="current" @endif>Altri documenti</a></li>
        @endif
        
        @endcomponent
       
@endsection


@php
/******************
 * FOOTER
*/
@endphp
@section('footer')

        @if(Auth::id() == $immobile->tecnico_id )
            @if(Request::is('*/sopralluogo') || Request::is('*/sopralluogo/*'))
                @php
                $class = '';
                $data = '';
                if($immobile->sopralluogo && !$immobile->sopralluogo->data) {
                    $class = 'show';
                } elseif($immobile->sopralluogo && $immobile->sopralluogo->data) {
                    $data = $immobile->sopralluogo->data->format('Y-m-d');
                } elseif(!$immobile->sopralluogo) {
                    $class = 'show';
                    $data = '';
                }
                @endphp
                
                @modal(['id' =>'dataSopralluogoModal', 'class' => "modal--dialog text-center $class", 'title' => 'Data del sopralluogo'])
                        <p>Selezionare i giorno in cui è stato fatto il sopralluogo</p>
                        <form action="{{ route('immobile.sopralluogo.store.data', $immobile) }}" method="post">
                            @csrf
                            @inputtext(['attrs' => ['name' => 'datasopralluogo', 'required' => true, 'class' => 'input-lg text-center','type' => 'date', 'max' => Carbon\Carbon::now()->format('Y-m-d'), 'value' => $data ]])@endinputtext
                            @button(['attrs' => ['text' => 'Salva']])@endbutton
                        </form>
                @endmodal

            @endif
        @endif

@endsection
