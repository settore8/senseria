@extends('app.layouts.master')

@php
/******************
 * HEADER
*/
@endphp

@section('headermenu')
    @component('app.components.headermenu')
    <li>
        <a href="{{ route('profilo', Auth::user()->slug) }}" class="tooltip tooltip-bottom" data-tooltip="Vedi profilo"><svg width="23" height="23"><use xlink:href="#icon-visite"></use></svg></a>
    </li>
    <li>
        <a href="{{ route('account.raccolte') }}" class="tooltip tooltip-bottom @if(Request::is('account/raccolte') || Request::is('account/raccolte/*')) {{ 'current' }} @endif" data-tooltip="Raccolte"><svg width="23" height="23"><use xlink:href="#icon-raccolta-empty"></use></svg></a>
    </li>
    <li>
        <a href="{{ route('account.notifiche') }}" class="tooltip tooltip-bottom @if(Request::is('account/notifiche') || Request::is('account/notifiche/*')) {{ 'current' }} @endif" data-tooltip="Notifiche"><svg width="23" height="23"><use xlink:href="#icon-notifica"></use></svg></a>
    </li>
    <li>
        <a href="{{ route('account.statistiche') }}" class="has-notify tooltip tooltip-bottom @if(Request::is('account/statistiche') || Request::is('account/statistiche/*')) {{ 'current' }} @endif" data-tooltip="Statistiche user"><svg width="23" height="23"><use xlink:href="#icon-statistiche"></use></svg></a>
    </li>
    <li>
        <a href="#shareAccount" data-toggle="modal" class="tooltip tooltip-bottom" data-tooltip="Condividi profilo"><svg width="23" height="23"><use xlink:href="#icon-share"></use></svg></a>
    </li>
    <li>
        <a href="#downloadAccount" data-toggle="modal" class="tooltip tooltip-bottom" data-tooltip="Download presentazione"><svg width="23" height="23"><use xlink:href="#icon-download"></use></svg></a>
    </li>
    @endcomponent
@endsection

@php
/******************
 * SIDEBAR
*/
@endphp
@section('sidebar')
    @component('app.components.sidebar', ['type' => 'scrollable'])
        <li><a href="{{ route('account.profilo') }}" @if(Request::is('account/profilo') || Request::is('account/profilo/*')) class="current" @endif>Profilo</a></li>
        <li><a href="{{ route('account.competenze') }}" @if(Route::currentRouteName() == 'account.competenze') class="current" @endif>Competenze</a></li>
        @if(Auth::user()->collegio->albo->abilitazioni->isNotEmpty())
            <li><a href="{{ route('account.abilitazioni') }}" @if(Route::currentRouteName() == 'account.abilitazioni') class="current" @endif>Abilitazioni</a></li>
        @endif

        <li><a href="{{ route('account.area') }}" @if(Route::currentRouteName() == 'account.area') class="current" @endif>Area operativa</a></li>
        <li><a href="{{ route('account.certificazione') }}" @if(Route::currentRouteName() == 'account.certificazione') class="current" @endif>Certificazione</a></li>
        <li><a href="{{ route('account.abbonamento') }}" @if(Request::is('account/abbonamento') || Request::is('account/abbonamento/*')) class="current" @endif>Abbonamento <span class="status"></span></a></li>
        <li><a href="{{ route('account.impostazioni') }}" @if(Request::is('account/impostazioni') || Request::is('account/impostazioni/*')) class="current" @endif>Impostazioni</a> </li>
    @endcomponent
@endsection


@section('footer')

    @modal(['id' =>'shareAccount', 'title' => 'Condividi il tuo profilo', 'visual' => 'visual-condivisione-profilo.svg', 'class' => 'modal--dialog text-center'])
    <form action="{{ route('account.share') }}" method="POST">
        @csrf
            <p>Inserisci l'email per condividere il tuo profilo con altre persone</p>
            <div class="form-group">
                @inputtext(['attrs' => ['name' => 'email', 'class' => 'input-lg text-center', 'placeholder' => 'Email']])@endinputtext
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-lg btn-primary">Condividi profilo</button>
            </div>
    </form>
    @endmodal

    @modal(['id' =>'downloadAccount', 'title' => 'Crea presentazione profilo',  'visual' => 'visual-download-presentazione.svg', 'class' => 'modal--dialog text-center'])
    <form action="{{ route('account.download') }}" method="POST">
        @csrf
            <p>Workook ti mette a disposizione uno strumento per creare una presentazione con un click!</p>
            <div class="form-group">
                <button type="submit" class="btn btn-lg btn-primary nopreloader">Scarica pdf</button>
            </div>
    </form>
    @endmodal

@endsection