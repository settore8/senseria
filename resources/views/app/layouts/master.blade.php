<!DOCTYPE html>
<html lang="it" class="app">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="user-scalable=yes, width=device-width, initial-scale=1">
        <meta name="google" content="notranslate">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="csrf-token" content="{!! csrf_token() !!}">

        <meta name="user-token" content="{!! sha1(Auth::id()) !!}">

        <title>@yield('title') {{ config('app.name') }}</title>
        <meta property="og:locale" content="it_IT" />
        <meta property="og:site_name" content="{{ config('app.name') }}" />
        <meta property="og:url" content="{{ url()->current() }}" />
        <meta property="og:type" content="@yield('og_type')" />
        <meta property="og:title" content="@yield('og_title')" />
        <meta property="og:description" content="@yield('og_description')" />
        <meta property="og:image" content="@yield('og_image', '/images/ogimages/og_image_default.png')" />
        @yield('meta')
        @yield('ogtag')
        @yield('style')
        <link href="{{ url()->current() }}" rel="canonical">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link rel="apple-touch-icon" sizes="180x180" href="/favicons/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/favicons/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/favicons/favicon-16x16.png">
        <link rel="manifest" href="/favicons/site.webmanifest">
        <link rel="mask-icon" href="/favicons/safari-pinned-tab.svg" color="#2f4cd7">
        <meta name="msapplication-TileColor" content="#2f4cd7">
        <meta name="theme-color" content="#2f4cd7">
    </head>

    <body class="@section('bodyclass')app  @show @if(Auth::user()->preferences && Auth::user()->preferences['fullscreen'] && Auth::user()->preferences['fullscreen'] == 'true') {{ 'fullscreen'}} @endif" itemscope itemtype="http://schema.org/WebPage">
         
        <div id="app">
            <header id="header">
                    <a href="#" id="nav_toggle" class="hamburger">
                        <span></span>
                        <span></span>
                        <span></span>
                    </a>
                    <a href="#" id="nav_collapse">
                        <span></span>
                    </a>
                    <a href="{{ url(route('dashboard')) }}" id="logo">@include('includes.graphics.logo')</a>
                    <a href="" id="user_toggle"><span class="user-name">{{ Auth::user()->email }}</span><span class="user-placeholder">{{ Auth::user()->iniziali }}</span></a>
                    <nav id="user_menu">
                        <a href="{{ route('account.profilo') }}" @if(Route::currentRouteName() == 'account.profilo') class="current" @endif><svg><use xlink:href="#icon-profilo"></use></svg> Profilo</a>
                        @if(Auth::user()->tipo != 'agente')
                        <a href="{{ route('account.competenze') }}" @if(Route::currentRouteName() == 'account.competenze') class="current" @endif><svg><use xlink:href="#icon-competenze"></use></svg> Competenze</a>
                        <a href="{{ route('account.abilitazioni') }}" @if(Route::currentRouteName() == 'account.abilitazioni') class="current" @endif><svg><use xlink:href="#icon-abilitazioni"></use></svg> Abilitazioni</a>
                        @endif
                        <a href="{{ route('account.area') }}" @if(Route::currentRouteName() == 'account.area') class="current" @endif><svg><use xlink:href="#icon-areaoperativa"></use></svg> Area operativa</a>
                        @if(Auth::user()->tipo != 'agente')
                        <a href="{{ route('account.certificazione') }}" @if(Route::currentRouteName() == 'account.certificazione') class="current" @endif><svg><use xlink:href="#icon-certificazione"></use></svg> Certificazione</a>
                        @endif
                        <a href="{{ route('account.abbonamento') }}"><svg><use xlink:href="#icon-abbonamento"></use></svg> Abbonamento</a>
                        <a href="{{ route('account.impostazioni') }}"><svg><use xlink:href="#icon-impostazioni"></use></svg> Impostazioni</a>
                        <hr/>
                        <a href="{{ route('account.notifiche') }}" @if(Route::currentRouteName() == 'account.notifiche') class="current" @endif><svg><use xlink:href="#icon-notifica"></use></svg> Notifiche</a>
                        <a href="{{ route('account.statistiche') }}" @if(Route::currentRouteName() == 'account.statistiche') class="current" @endif><svg><use xlink:href="#icon-statistiche"></use></svg> Statistiche</a>
                        @if(Auth::user()->tipo != 'agente')
                        <a href="{{ route('account.raccolte') }}" @if(Route::currentRouteName() == 'account.raccolte') class="current" @endif><svg><use xlink:href="#icon-raccolta-empty"></use></svg> Raccolte</a>
                        @endif
                        <hr/>
                        <a href="{{ route('logout') }}" onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();"><svg><use xlink:href="#icon-esci"></use></svg> Esci
                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                        </a>
                    </nav>
            </header>


            <nav id="nav">
                <ul>
                    <li>
                        <a href="{{ route('dashboard') }}" @if(Route::currentRouteName() == 'dashboard') class="current" @endif> <svg><use xlink:href="#icon-dashboard"></use></svg> <span>Bacheca</span></a>
                    </li>
                    
                    <li>
                        <a href="{{ route('annunci') }}" @if(Request::is('annunci') || Request::is('annunci/*')) class="current" @endif> <svg><use xlink:href="#icon-annunci"></use></svg> <span>Annunci</span></a>
                    </li>

                    <li class="divider"></li>

                    <li>
                        <a href="{{ route('immobili') }}"  @if(Request::is('immobili') || Request::is('immobile/*')) class="current" @endif> <svg><use xlink:href="#icon-immobili"></use></svg> <span>Immobili</span> </a><a href="{{ route('immobile.create') }}" class="link tooltip tooltip-left" data-tooltip="Nuovo immobile"><svg><use xlink:href="#icon-add"></use></svg></a>
                    </li>

                    @if(Auth::user()->tipo != 'agente')
                    <li>
                        <a href="{{ route('acquirenti') }}" @if(Request::is('acquirenti') || Request::is('acquirente/*')) class="current" @endif> <svg><use xlink:href="#icon-acquirenti"></use></svg> <span>Acquirenti</span></a><a href="{{ route('acquirente.create') }}" class="link tooltip tooltip-left" data-tooltip="Nuovo acquirente"><svg><use xlink:href="#icon-add"></use></svg></a>
                    </li>

                    <li class="divider"></li>

                    <li>
                        <a href="{{ route('articoli') }}" @if(Request::is('articoli') || Request::is('articoli/*')) class="current" @endif> <svg><use xlink:href="#icon-articoli"></use></svg> <span>Articoli</span></a><a href="#nuovoArticolo" class="link tooltip tooltip-left" data-tooltip="Nuovo articolo" data-toggle="modal"><svg><use xlink:href="#icon-add"></use></svg></a>
                    </li>
                    <li>
                        <a href="{{ route('gallerie') }}" @if(Request::is('gallerie') || Request::is('gallerie/*')) class="current" @endif> <svg><use xlink:href="#icon-gallerie"></use></svg> <span>Gallerie</span></a><a href="#nuovaGalleria" class="link tooltip tooltip-left" data-tooltip="Nuova galleria" data-toggle="modal"><svg><use xlink:href="#icon-add"></use></svg></a>
                    </li>

                    <li class="divider"></li>

                    <li>
                        <a href="{{ route('professionisti') }}" @if(Request::is('professionisti')) class="current" @endif><svg><use xlink:href="#icon-professionisti"></use></svg> <span>Professionisti</span></a><a href="{{ route('immobile.create') }}" class="link"><svg><use xlink:href="#icon-search"></use></svg></a>
                    </li>
                    <li>
                        <a href="{{ route('imprese') }}" @if(Request::is('imprese')) class="current" @endif> <svg><use xlink:href="#icon-imprese"></use></svg> <span>Imprese</span></a>
                        <a href="{{ route('immobile.create') }}" class="link"><svg><use xlink:href="#icon-search"></use></svg></a>
                    </li>
                    @endif

                   
                    @if(Auth::user()->tipo == 'agente')
                    <li >
                        <a href="{{ route('calendario') }}" @if(Request::is('calendario') || Request::is('calendario/*')) class="current" @endif> <svg><use xlink:href="#icon-messaggi"></use></svg> <span class="has-notify">Calendario</span></a>
                    </li>
                    <li >
                        <a href="{{ route('calendario') }}" @if(Request::is('calendario') || Request::is('calendario/*')) class="current" @endif> <svg><use xlink:href="#icon-messaggi"></use></svg> <span class="has-notify">Rubrica</span></a>
                    </li>
                    @endif
                    <li>
                        <a href="{{ route('conversazioni') }}" @if(Request::is('conversazioni') || Request::is('conversazioni/*')) class="current" @endif> <svg><use xlink:href="#icon-messaggi"></use></svg> <span class="has-notify">Conversazioni<span class="notify">2</span></span></a>
                    </li>

                </ul>
            </nav>
        @show

        <main id="main">

            @php 
            // questo va tolto
            @endphp

            @if($errors->any())
                <div class="toast toast-error text-center fixed animate">
                    <button class="btn btn-clear float-right"></button>
                    {!! implode('', $errors->all('<div>:message</div>')) !!}
                </div>
            @endif

            @if($session = \Session::get('message'))
                <div class="toast toast-{{ $session['type'] }} text-center fixed animate">
                    <button class="btn btn-clear float-right"></button>
                    @if(isset($session['title'])) {!! '<h4>'.$session['title'].'</h4>' !!} @endif
                    @if(isset($session['text'])) {!! $session['text'] !!} @endif
                </div>
            @endif

            @if($session = \Session::get('modal'))
                @modal(['id' =>'sessionModal', 'class' => 'modal--dialog text-center show'])
                    @if(isset($session['visual'])) 
                        <img src="{{ asset('images/app/'.$session['visual']) }}" class="modal--visual @if(isset($session['visual_size'])) {{ $session['visual_size'] }} @endif">
                    @endif
                    @if(isset($session['title'])) <h4>{!! $session['title'] !!}</h4> @endif
                    @if(isset($session['text'])) {!! $session['text'] !!} @endif
                    @if(isset($session['close_button']))
                        <div><button class="button__close btn btn-lg btn-primary btn-padding mt10">{!! $session['close_button'] !!}</button></div>
                    @endif
                @endmodal
            @endif

            @yield('breadcrumb')

            @if(View::hasSection('headertitle') || View::hasSection('headersearch') || View::hasSection('headermenu'))
                <header class="flex">
                    @yield('headertitle')
                    @yield('headersearch')
                    @yield('headermenu')
                </header>
            @endif

            <div class="flex flex-1 flex--reverse">
                @yield('sidebar')
                <div id="maincontent" class="item pt10">
                    @yield('content')
                </div>
            </div>

        </main>

        
        @section('footerlegal')
        <footer id="footer">
             @include('includes.partials.footer')
        </footer>
        @show

        @yield('footer')

        @include('includes.graphics.icons')

        <div id="notification_area">

            <div id="notification_area_header">
            <h3><a href="{{ route('account.notifiche')}}">Notifiche</a></h3>
                <button id="close_notification_area" class="btn btn-close mlauto">Chiudi</button>
            </div>
            
            @if($notifiche && $notifiche->isNotEmpty())
                <div id="notification_wrapper">
                @foreach($notifiche as $notifica) 
                        @include('app.partials.previewNotifica', ['notifica' => $notifica])
                @endforeach
                </div>
            @else
            <div class="text-center mt20 p20">
                <img src="{{ asset('images/app/visual-niente-notifiche.svg') }}" alt="Nessuna notifica" alt="">
                <p class="mt10 mb10">Nessuna notifica <br/>da leggere</p>
                <a href="{{ route('account.notifiche')}}" class="btn btn-sm btn-secondary" title="Vai alle notifiche">Vai alle notifiche</a>
            </div>
            @endif
           
        </div>

        <button id="toggle_notification_area" class="fire">
            <svg width="26" height="26"><use xlink:href="#icon-notifica"></use></svg>
            @if($unreaded > 0)
                <span class="counter">{{ $unreaded }}</span>
            @endif
        </button>

        </div> @php /* end app div */ @endphp

        @if(Auth::user()->tipo != 'agente')

        @modal(['id' =>'nuovaGalleria', 'title' => 'Crea galleri', 'class' => 'modal--sm', 'visual' => 'visual-nuova-galleria.svg'])
            @slot('title')Crea galleria @endslot
            <form action="{{ route('galleria.store') }}" method="POST">
                @csrf
                @inputtext(['attrs' => ['name' => 'titolo', 'class' => 'input-xl text-center', 'placeholder' => 'Titolo della galleria']])@endinputtext
                @button(['attrs' => ['text' => 'Inizia', 'class' => 'btn btn-primary btn-padding nopreloader']])@endbutton
            </form>
        @endmodal

        @modal(['id' =>'nuovoArticolo', 'title' => 'Crea articolo', 'class' => 'modal--sm', 'visual' => 'visual-nuovo-articolo.svg'])
            @slot('title')Scrivi articolo @endslot
            <form action="{{ route('articoli.store') }}" method="POST">
                @csrf
                <?php $categorie = App\Categoria::whereIn('id', Auth::user()->prestazioni->pluck('categoria_id')->unique()->values()->all())->pluck('nome_pubblico', 'id') ?>
                @if(count($categorie) > 1)
        {{--            #TO_REVIEW#--}}
        {{--            #non funziona il componente dentro questo form#--}}
        {{--            @select([ 'attrs' => ['name' => 'categoria', 'id' => 'categoria_articolo', 'label' => 'Seleziona la Categoria', 'value' => Custom::addNull($categorie)]])@endselect--}}
                    <select class="form-select" name="categoria" id="categoria_articolo">
                        @foreach (Custom::addNull($categorie) as $id => $value)
                            <option value="{!! $id !!}">{!! $value !!}</option>
                        @endforeach
                    </select>
                @else
                    <input type="hidden" name="{{ array_key_first(head($categorie)) }}" value="">
                @endif
                @button(['attrs' => ['text' => 'Inizia', 'class' => 'btn btn-primary btn-padding nopreloader']])@endbutton
            </form>
        @endmodal

        @endif

        <script src="https://js.pusher.com/5.0/pusher.min.js"></script>
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key={{ config('app.maps') }}&libraries=places,drawing,geometry&types=address&region=IT&language=IT"></script>
        @yield('footerjs_before')
            <script src="{{ asset('js/app_bundle.js?'.date('s')) }}"></script>
        @yield('footerjs_after')
    </body>
</html>
