@extends('app.layouts.master')

@php
/******************
 * HEADER
*/
@endphp

@section('headermenu')
    @component('app.components.headermenu')

    <li>
        <a href="{{ route('acquirente.edit', $acquirente) }}" class="tooltip tooltip-bottom @if(Route::currentRouteName() == 'acquirente.edit') {{ 'current' }} @endif" data-tooltip="Edit acquirente"><svg width="23" height="23"><use xlink:href="#icon-edit"></use></svg></a>
    </li>

    <li>
        <a href="{{ route('acquirente.impostazioni', $acquirente) }}" class="has-notify tooltip  tooltip-bottom   @if(Route::currentRouteName() == 'acquirente.impostazioni') {{ 'current' }} @endif" data-tooltip="Impostazioni"><svg width="23" height="23"><use xlink:href="#icon-settings"></use></svg></a>
    </li>

    @endcomponent
@endsection

@php
/******************
 * SIDEBAR
*/
@endphp


@php
/******************
 * FOOTER
*/
@endphp
@section('footer')

@endsection
