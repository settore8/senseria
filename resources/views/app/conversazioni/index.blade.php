@extends('app.layouts.master')

@section('title', 'Conversazioni')
@section('bodyclass')@parent {{ 'conversazioni' }}@endsection

@section('headertitle')
    @component('app.components.headertitle')
        Conversazioni
        <a href="{{ url(route('articoli.nuovo')) }}" class="btn-add tooltip tooltip-right" data-tooltip="Nuova conversazione">+</a>
    @endcomponent
@endsection


@section('headersearch')
<form class="item item--center ">
<div class="form-group form-group-search">
    <input type="text" class="form-input input-lg input-search input-rounded" placeholder="Cerca conversazione">
    <svg><use xlink:href="#icon-search"></use></svg>
</div>
</form>
@endsection

@section('headermenu')
    @component('app.components.headermenu')
    <li>
        <a href="{{ route('conversazioni.archivio') }}" class="tooltip  @if(Route::currentRouteName() == 'conversazioni.archivio') {{ 'current' }} @endif" data-tooltip="Archivio conversazioni">Archivio</a>
    </li>
    @endcomponent
@endsection

@section('content')
        @if($conversazioni)
            <div class="columns">
                @foreach ($conversazioni as $conversazione)

                    <div class="column col-12 p10">
                        <a href="{{ route('conversazione', $conversazione)}}">
                            <!--<small>{{  $conversazione->messaggi()->latest('data')->first() }}</small>-->
                            @if($conversazione->coverable_type == get_class(new App\User))
                                <h4>Contatto con {{ $conversazione->coverable->fullname }}</h4>
                            @endif
                            
                            @if($conversazione->coverable->type == 'immobili') 
                            <h4>{{ config('constants.oggetto-annunci')[$conversazione->coverable->type] }}</h4>
                            {{ $conversazione->coverable->immobile->codice}}
                            @endif

                            @if($conversazione->coverable->type == 'acquirenti') 
                            <h4>{{ config('constants.oggetto-annunci')[$conversazione->coverable->type] }}</h4>
                           {{ $conversazione->coverable->immobile->codice}}
                            @endif
                            
                            @foreach ($conversazione->users as $user)
                                <small>@ {!! $user->display_name !!}</small><br>
                            @endforeach
                        </a>
                    </div>
                @endforeach
            </div>
        @else

                    nessuna conversazione attiva

        @endif
@endsection
