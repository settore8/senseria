@extends('app.layouts.master')

@section('title', 'Conversazioni')
@section('bodyclass')@parent {{ 'conversazioni' }}@endsection

@section('headertitle')
    @component('app.components.headertitle')
        Conversazioni
    @endcomponent 
@endsection


@section('headersearch')
<form class="item item--center ">
<div class="form-group form-group-search">
    <input type="text" class="form-input input-lg input-search input-rounded" placeholder="Cerca conversazione">
    <svg><use xlink:href="#icon-search"></use></svg>
</div>
</form>
@endsection



@section('headermenu')
    @component('app.components.headermenu')
    <li>
        <a href="{{ route('conversazioni.archivio') }}" class="tooltip  @if(Route::currentRouteName() == 'conversazioni.archivio') {{ 'current' }} @endif" data-tooltip="Archivio conversazioni">Archivio</a>
    </li>
    @endcomponent
@endsection


@section('content')
            <ul class="list">
            {{-- @for ($i = 0; $i < 24; $i++)
                <li>
                    <a href="{{ route('conversazione')}}">
                        <small>13-01-2020</small>
                        <h4>Oggetto del messaggio {{ $i }}</h4>
                        <small>{{ '@' }}Geometra Andrea Ferri</small>
                    </a>
                </li>
                
            @endfor --}}
            @foreach ($user->sonopartecipe as $conversazione)
                <li>
                    <a href="{{ route('conversazione', $conversazione)}}">
                        <small>13-01-2020</small>
                        <h4>{{ config('constants.oggetto-annunci')[$conversazione->coverable->type] }} </h4>
                        @foreach ($conversazione->users as $user)
                            <small>@ {!! $user->display_name !!}</small><br>
                        @endforeach
                        
                    </a>
                </li>
            @endforeach
        </ul>
@endsection
