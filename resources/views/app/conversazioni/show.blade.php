@extends('app.layouts.master')

@section('title', 'Conversazioni')
@section('bodyclass')@parent {{ 'conversazione' }}@endsection

@section('header')
@endsection

@section('content')
    <div class="conversazione_sidebar">
            <div class="conversazione_sidebar_header">
            <small><a href="{{ route('conversazioni')}}"><svg><use xlink:href="#icon-left"></use></svg> Conversazioni</a></small>
            <button data-toggle="#conversazionesidebar">•••</button>
            </div>
            <div id="conversazionesidebar" class="conversazione_sidebar_body">
            <small>Oggetto conversazione</small>
            <a href="{{ route('annuncio', $conversazione->coverable->codice) }}"><h1>{{ $conversazione->coverable->immobile->codice}}</h1></a>

            @if($conversazione->users)
            <small>Partecipanti</small>
                @foreach($conversazione->users as $user)
                <h3>{{ $user->fullname}}</h3>
                @endforeach
            @endif
            
            <div class="form-group">
            <label class="form-label">Aggiungi utente</label>
            <select class="select"></select>
            </div>
            </div>
            <div class="conversazione_sidebar_footer">
            <small><a href="">Archivia conversazione</a></small><br/>
            <small><a href="">Esporta conversazione</a></small>
            </div>
    </div>
    <div class="conversazione_content" id="conversazione_content" data-sender="{{ Auth::id() }}">

        <div class="overlay"></div>

        <div class="messaggi">
            @foreach ($conversazione->messaggi as $messaggio)

            <div class="messaggio messaggio--{{ $messaggio->from == Auth::id() ? 'right' : 'left' }}" data-messaggio="{{ $messaggio->id }}">
                <div class="messaggio__content">
                <div class="messaggio__header">{!! $messaggio->user->display_name !!} <div class="dropdown mlauto"><a href="" class="dropdown-toggle">•••</a>
                                                                    <ul class="dropdown-menu">
                                                                        <li><a href="#">Elimina per me</a></li>
                                                                    </ul></div>
                </div>
                    {!! $messaggio->messaggio !!}
                <time>{!! $messaggio->data->format('H:i') !!}</time>
                </div>
            </div>
            
            @endforeach
        </div>
    </div>
    <div class="conversazione_input">
        <textarea id="corpo"></textarea>
        <input type="submit" id="send-message" data-conversazione="{!! $conversazione->id !!}" class="btn btn-primary nopreloader" value="invia">
    </div>
@endsection


@section('footerlegal')
@endsection

@section('footerjs_after')

<script>

    // Enable pusher logging - don't include this in production
    // Pusher.logToConsole = true;
    var input = document.getElementById("corpo");
        input.addEventListener("keyup", function(event) {
        if (event.keyCode === 13) {
            event.preventDefault();
            document.getElementById("send-message").click();
            return false;
        }
    });

  </script>
@endsection