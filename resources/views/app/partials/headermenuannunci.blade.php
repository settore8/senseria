<li class="has-submenu"><a href="#" @if(Request::is('annunci')) class="current" @endif><svg><use xlink:href="#icon-annunci"></use></svg> <span class="label">Suggeriti</span></a>
    <ul class="submenu submenu-right">
        <li>
            <a href="{!! route('annunci') !!}" @if(Request::is('immobili')) class="current" @endif>Suggeriti</a>
        </li>
        <li>
            <a href="{!! route('annunci', ['tipo' => 'immobili']) !!}" title="Annunci immobili in vendita" data-tooltip="Immobili in vendita" class="tooltip @if(Request::is('annunci/immobili')) {{ 'current' }} @endif"><span class="tipo-annuncio tipo-annuncio-immobile"></span>Immobili</a>
        </li>
        <li>
            <a href="{{ route('annunci', ['tipo' => 'acquirenti']) }}" title="Annunci di acquirenti in cerca di immobili" data-tooltip="Acquirenti in cerca di un immobile"  class="tooltip @if(Request::is('annunci/acquirenti')) {{ 'current' }} @endif"><span class="tipo-annuncio tipo-annuncio-acquirente"></span>Acquirenti</a>
        </li>
        @if(Auth::user()->isProfessionista())
        <li>
            <a href="{{ route('annunci', ['tipo' => 'responsabiletecnico']) }}" title="Annunci per responsabile tecnico" data-tooltip="Annunci per Responsabile tecnico" class="tooltip @if(Request::is('annunci/responsabiletecnico')) {{ 'current' }} @endif"><span class="tipo-annuncio tipo-annuncio-responsabiletecnico"></span>Responsabile tecnico</a>
        </li>
        @endif
        <li>
            <a href="{{ route('annunci', ['tipo' => 'collaborazioni']) }}" title="Annunci di collaborazione" data-tooltip="Annunci di collaborazione" class="tooltip @if(Request::is('annunci/collaborazioni')) {{ 'current' }} @endif"><span class="tipo-annuncio tipo-annuncio-collaborazione"></span>Collaborazioni</a>
        </li>

        <li class="divider"></li>

        <li>
            <a href="{{ route('annunci.candidature') }}" title="Le mie candidature" data-tooltip="Le mie candidature" class="tooltip @if(Request::is('annunci/candidature')) {{ 'current' }} @endif">Le mie candidature</a>
        </li>

    </ul>
</li>