<div class="column col-4 col-xs-12 col-md-6 pb8">

    <a href="{{ route('annuncio', $annuncio->codice) }}" class="card card--annuncio preview--annuncio {{ $annuncio->type }}">

    <div class="card--header">
        {{ config('constants.oggetto-annunci')[$annuncio->type] }}
        <span class="annuncio-code mlauto">{{ $annuncio->codice }}</span>
    </div>

    
    @if($annuncio->type == 'immobili')

            <div class="card--body">
                <div class="columns">
                    <div class="column col-7">
                        <div class="card--title">
                            <strong>{{ $annuncio->immobile->specifica->nome_pubblico }}</strong>
                        </div>
                        <div class="card--location">
                            <svg class="icon--15 mr1"><use xlink:href="#icon-marker"></use></svg>
                            {{ $annuncio->immobile->informazione->comune }} <span class="card--location--pr">{{ $annuncio->immobile->informazione->provincia }}</span>
                            {{ $annuncio->immobile->indirizzo }}
                        </div>
                    </div>
                    <div class="column col-5">
                        <img src="https://picsum.photos/300/300" class="img-responsive img">
                        <svg class="icon--50"><use xlink:href="#destinazione-{{ strtolower($annuncio->immobile->destinazione->nome) }}"></use></svg>
                    </div>
                </div>
            </div>
            
        
            <div class="card--footer">

                @if($annuncio->immobile->tecnico)
                    <svg><use xlink:href="#icon-responsabile"></use></svg>
                    {{ $annuncio->immobile->tecnico->qualifica->nome_pubblico }} {{ $annuncio->immobile->tecnico->fullname }}
                @endif

                @if($annuncio->immobile->agente)
                    <svg><use xlink:href="#icon-agente"></use></svg>
                    {{ $annuncio->immobile->agente->fullname }}
                @else
                    <svg><use xlink:href="#icon-user"></use></svg>
                    {{ $annuncio->immobile->user->fullname }}
                @endif
                    
                @if(Auth::user()->isInRaccolta($annuncio->id, get_class(new App\Annuncio)))
                <button type="button" class="btn btn-link tooltip active" data-tooltip="Salva" data-raccolta="{{ encrypt('App\Annuncio') }}" data-id="{{ encrypt($annuncio->id) }}" data-author="{{ encrypt($annuncio->creatore_id) }}">
                    <svg><use xlink:href="#icon-raccolta"></use></svg>
                </button>
                @else
                <button type="button" class="btn btn-link tooltip" data-tooltip="Salva" data-raccolta="{{ encrypt('App\Annuncio') }}" data-id="{{ encrypt($annuncio->id) }}" data-author="{{ encrypt($annuncio->creatore_id) }}">
                    <svg><use xlink:href="#icon-raccolta-empty"></use></svg>
                </button>
                @endif

            </div>

    @elseif($annuncio->type == 'responsabiletecnico')
            
        <div class="card--body">
            <div class="columns">
                <div class="column col-7">
                    <div class="card--title">
                        <strong>{{ $annuncio->immobile->specifica->nome_pubblico }}</strong>
                    </div>
                    <div class="card--location">
                        <svg class="icon--15 mr1"><use xlink:href="#icon-marker"></use></svg>
                        {{ $annuncio->immobile->informazione->comune }} <span class="card--location--pr">{{ $annuncio->immobile->informazione->provincia }}</span>
                        {{ $annuncio->immobile->indirizzo }}
                    </div>
                </div>
                <div class="column col-5">
                    <img src="https://picsum.photos/300/240" class="img-responsive">
                    <svg class="icon--50"><use xlink:href="#destinazione-{{ strtolower($annuncio->immobile->destinazione->nome) }}"></use></svg>
                </div>
            </div>
        </div>
    

        <div class="card--footer">

            {{ $annuncio->immobile->user->fullname }}
                
            @if(Auth::user()->isInRaccolta($annuncio->id, get_class(new App\Annuncio)))
            <button type="button" class="btn btn-link tooltip active" data-tooltip="Salva" data-raccolta="{{ encrypt('App\Annuncio') }}" data-id="{{ encrypt($annuncio->id) }}" data-author="{{ encrypt($annuncio->creatore_id) }}">
                <svg><use xlink:href="#icon-raccolta"></use></svg>
            </button>
            @else
            <button type="button" class="btn btn-link tooltip" data-tooltip="Salva" data-raccolta="{{ encrypt('App\Annuncio') }}" data-id="{{ encrypt($annuncio->id) }}" data-author="{{ encrypt($annuncio->creatore_id) }}">
                <svg><use xlink:href="#icon-raccolta-empty"></use></svg>
            </button>
            @endif

        </div>


    @elseif($annuncio->type == 'agentiimmobiliari')


    @elseif($annuncio->type == 'collaborazioni')
        @if($annuncio->dettagli->isNotEmpty())
            
            <div>{{ $annuncio->dettagli->first()->coverable->categoria->nome_pubblico }}</div>
            {{ $annuncio->dettagli->first()->coverable->nome_pubblico }}
              
        @endif
    @elseif($annuncio->type == 'acquirenti')

        <div class="card--body">
            <div class="columns">
                <div class="column col-7">
                    <div class="card--title">
                        <strong>{{ $annuncio->acquirente->specifica->nome_pubblico }}</strong>
                    </div>
                    <div class="card--location">
                        <svg class="icon--15 mr1"><use xlink:href="#icon-marker"></use></svg>
                        Recuperare Comune al centro della poligonale
                    </div>
                    camere: {{ $annuncio->acquirente->dettagli['camere'] }}
                    bagni: {{ $annuncio->acquirente->dettagli['bagni'] }}
                    budget: {{ $annuncio->acquirente->budget }}

                    descrizione: {{ $annuncio->acquirente->dettagli['descrizione'] }}

                </div>
                <div class="column col-5">
                    <svg class="icon--50"><use xlink:href="#destinazione-{{ strtolower($annuncio->acquirente->destinazione->nome) }}"></use></svg>
                </div>
            </div>
        </div>

        <div class="card--footer">

            {{ $annuncio->acquirente->user->fullname }}
                
            @if(Auth::user()->isInRaccolta($annuncio->id, get_class(new App\Annuncio)))
            <button type="button" class="btn btn-link tooltip active" data-tooltip="Salva" data-raccolta="{{ encrypt('App\Annuncio') }}" data-id="{{ encrypt($annuncio->id) }}" data-author="{{ encrypt($annuncio->creatore_id) }}">
                <svg><use xlink:href="#icon-raccolta"></use></svg>
            </button>
            @else
            <button type="button" class="btn btn-link tooltip" data-tooltip="Salva" data-raccolta="{{ encrypt('App\Annuncio') }}" data-id="{{ encrypt($annuncio->id) }}" data-author="{{ encrypt($annuncio->creatore_id) }}">
                <svg><use xlink:href="#icon-raccolta-empty"></use></svg>
            </button>
            @endif

        </div>



    

    @endif

    
    </a>
    
</div>