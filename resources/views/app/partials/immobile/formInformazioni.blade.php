<form autocomplete="off" action="{!! route('immobile.store.informazioni', $immobile) !!}" method="POST">
    @csrf

    @block(['title' => 'Generali <button type="button" data-toggle="collapse" data-target="#tip_immobile_informazioni" data-tip="tip_immobile_informazioni" class="mlauto">?</button>', 'class' => 'block--bordered'])

    <div id="tip_immobile_informazioni" class="collapse">
        <div class="columns align-items-center">
            <div class="column col-8 col-sm-7 col-sm-6 col-xs-12">
                <p>Completa la <strong>scheda immobile</strong> inserendo i dati necessari ad identificare l’immobile: localizzazione, metri quadri indicativi, consistenza. Una volta creata la scheda potrai gestire l’immobile utilizzando le funzioni messe a disposizione da Workook.</p>
            </div>
            <div class="column col-4 col-sm-5 col-sm-6 col-xs-12">
                <img src="{{ asset('/images/tips/tip-immobile.svg') }}/">
            </div>
            <hr class="hr hr--separator"/>
        </div>
    </div>

    <div class="columns">
        <div class="column col-9 col-xs-12">
        <div class="form-group">
            <label for="address" class="form-label">Indirizzo <span class="text-error">*</span> <!--<a href="#" data-offcanvas="addressMapOffcanvas" class="float-right">Seleziona sulla mappa</a>--></label>
            <input id="address" name="address" class="form-input input-xl {!! $errors->has('addressResult') ? 'is-error' : '' !!} " type="text" size="50" placeholder="Indirizzo" value="@if(old('address')) {{ old('address') }} @elseif($immobile->informazione && $immobile->informazione->address) {{ $immobile->informazione->address }} @endif">
            @if ($errors->has('addressResult'))  
                <p class="form-input-hint form-text text-muted">{!! $errors->first('addressResult') !!}</p>
            @endif
            <input type="hidden" id="addressResult" name="addressResult" value="@if(old('addressResult')){{ old('addressResult') }}@elseif($immobile->informazione && $immobile->informazione->addressResult){{ $immobile->informazione->addressResult }}@endif">
        </div>
        </div>

        <div class="column col-3 col-xs-12">
            <div class="form-group">
                <label class="form-label" for="metri_quadri">Mq indicativi <span class="text-error">*</span></label>
                <input type="number" class="form-input input-xl {!! $errors->has('metri_quadri') ? 'is-error' : '' !!}" id="metri_quadri" name="metri_quadri" placeholder="90" min="0" value="@if(old('metri_quadri')){{ old('metri_quadri') }}@elseif($immobile->informazione &&  $immobile->informazione->metri_quadri){{ $immobile->informazione->metri_quadri }}@endif">
                @if ($errors->has('metri_quadri'))
                    <p class="form-input-hint">{!! $errors->first('metri_quadri') !!}</p>
                @endif
            </div>
        </div>

        <!--
        <div class="offcanvas offcanvas--right" id="addressMapOffcanvas">
            <div id="addressMap" class="map"></div>

            <div class="offcanvas__footer">
                <button class="offcanvas_conferma btn btn-primary">Conferma</button>
                <button class="offcanvas_annulla btn btn-danger">Annulla</button>
            </div>
        </div>
        -->
    </div>
    <div class="columns">

        <div class="column col-12">
            <div class="form-group">
                <label class="form-label" for="descrizione">Descrizione <span class="text-error">*</span></label>
                <textarea class="form-input {!! $errors->has('descrizione') ? 'is-error' : '' !!}" id="descrizione" name="descrizione" placeholder="Inserisci una descrizione dell'immobile" rows="3">@if(old('descrizione')){{ old('descrizione')}}@elseif($immobile->informazione && $immobile->informazione->descrizione){{ $immobile->informazione->descrizione }}@endif</textarea>
                @if ($errors->has('descrizione'))
                    <p class="form-input-hint">{!! $errors->first('descrizione') !!}</p>
                @endif
            </div>

        </div>

    </div>
    
    <h2>Consistenza <span class="text-error">*</span></h2>
    @foreach ($consistenze as $consistenza => $valori)
        <fieldset>
        <legend>{{ __('messages.'.$consistenza) }}</legend>
        @foreach($valori as $chiave => $valore)
            <div class="columns py3 consistenza consistenza_{{ $chiave }}">
                <div class="column col-6">
                    {!! $valore !!}
                </div>
                <!-- TO_REVIEW non recupera le consistenze salvate nel db -->
                <div class="column col-3">
                    @toggle(['attrs' => ['name' => "consistenze[$consistenza][]", "value" => $valore, "checked" => $immobile->informazione && array_key_exists($consistenza, $immobile->informazione->locali) && in_array($valore, $immobile->informazione->locali[$consistenza]) ? true : false ]]) @endtoggle
                </div>
                <div class="column col-3">
                    @inputnumber(['attrs' => ['name' => "valore_consistenza[$consistenza][]", 'placeholder' => 'Quantità', 'value' => 1, 'class' => 'hidden', 'min' => '1','disabled' => true]]) @endinputnumber
                </div>
            </div>
        @endforeach
        </fieldset>
    @endforeach

    @button(['attrs' => ['text' => 'Salva']])@endbutton


    @endblock
   




</form>

@if(!$immobile->informazioni)
<small><a href="{{ route('immobile.impostazioni', $immobile) }}" class="text-error">Elimina immobile</button></a>
@endif
