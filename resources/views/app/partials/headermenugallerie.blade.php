<li class="has-submenu"><a href="#" @if(Request::is('gallerie') || Request::is('gallerie/*')) class="current" @endif><svg><use xlink:href="#icon-gallerie"></use></svg> <span class="label">Gallerie</span></a>
    <ul class="submenu">
        <li><a href="{{ route('gallerie') }}" title="Gallerie recenti" @if(Request::is('gallerie')) class="current" @endif>Recenti</a></li>
        <li><a href="{{ route('gallerie.popolari') }}" title="Gallerie popolari" @if(Request::is('gallerie/popolari')) class="current" @endif>Popolari</a></li>
    </ul>
</li>

@if(Auth::check())
<li class="has-submenu gestione"><a href="#"><svg><use xlink:href="#icon-edit"></use></svg> <span class="label">Gestione</span></a>
    <ul class="submenu submenu-right">
        <li><a href="{!! route('gallerie.gestione') !!}" title="Le mie gallerie" @if(Request::is('gallerie/gestione')) class="current" @endif>Le mie gallerie</a></li>
        <li><a href="{{ route('gallerie.gestione.tag') }}" title="Gallerie dove sono taggato" @if(Request::is('gallerie/gestione/tag')) class="current" @endif>Dove sono taggato</a></li>
    </ul>
</li>
@endif
