<ul class="inline-nav scrollmenu text-center text-left--md">
    <li><a href="{!! route('account.impostazioni') !!}" title="Impostazione Notifiche" @if(Route::currentRouteName() == 'account.impostazioni') class="current" @endif>Notifiche</a></li>
    <li><a href="{!! route('account.impostazioni.password') !!}" title="Impostazione Password" @if(Route::currentRouteName() == 'account.impostazioni.password') class="current" @endif>Password</a></li>
    <li><a href="{!! route('account.impostazioni.privacy') !!}" title="Impostazione Privacy" @if(Route::currentRouteName() == 'account.impostazioni.privacy') class="current" @endif>Privacy</a></li>
    <li><a href="{!! route('account.impostazioni.avanzate') !!}" title="Impostazione Avanzate" @if(Route::currentRouteName() == 'account.impostazioni.avanzate') class="current" @endif>Avanzate</a></li>
</ul>