<form action="{{ route('annunci.search') }}" id="searchform" class="item item--center sendOnEnter canLeavePage" method="get">
    <div class="form-group form-group-search">
        <input type="text" name="s" class="form-input input-lg input-search input-rounded" placeholder="Es 094F5T">
        <svg><use xlink:href="#icon-search"></use></svg>
        <div class="toggle-searchadvanced"><svg><use xlink:href="#icon-filtro"></use></svg></div>
        <div id="searchadvanced">
            <div class="form-group">
                <label class="form-label">Tipologia di annuncio</label>
                <select type="text" class="form-select select-lg">
                    <option value="">--</option>
                    <option value="immobile">Immobili</option>
                    <option value="acquirenti">Acquirenti</option>
                    <option value="responsabile">Annunci per responsabile tecnico</option>
                    <option value="collaborazione">Collaborazioni</option>
                </select>
                <!-- #TO_REVIEW# stampare destinazioni e tipologie di immobili-->
            </div>

            <div class="form-group">
                <!--<div id="map"></map>-->
            </div>

            <div class="form-group">
                <label class="form-switch">
                    <input type="checkbox" name="area" checked><i class="form-icon"></i> Nella mia area operativa <svg class="icon icon--15 icon--tertiary" aria-hidden="true"><use xlink:href="#icon-areaoperativa"></use></svg>
                </label>
            </div>

            @button(['attrs' => ['text' => 'Cerca']])@endbutton
        </div>
    </div>
    </form>
<div id="searchoverlay"></div>