<ul class="inline-nav scrollmenu text-center text-left--md">
    <li><a href="{!! route('account.notifiche') !!}" title="Tutte le notifiche" @if(Route::currentRouteName() == 'account.notifiche') class="current" @endif>Tutte</a></li>
    <li><a href="{!! route('account.notifiche') !!}" title="Tutte le notifiche" @if(Route::currentRouteName() == 'account.notifiche.profilo') class="current" @endif>Profilo</a></li>
    <li><a href="{!! route('account.notifiche') !!}" title="Tutte le notifiche" @if(Route::currentRouteName() == 'account.notifiche.immobili') class="current" @endif>Immobili</a></li>
    <li><a href="{!! route('account.notifiche') !!}" title="Tutte le notifiche" @if(Route::currentRouteName() == 'account.notifiche.annunci') class="current" @endif>Annunci</a></li>
    <li><a href="{!! route('account.notifiche') !!}" title="Tutte le notifiche" @if(Route::currentRouteName() == 'account.notifiche.contenuti') class="current" @endif>Contenuti</a></li>
    <li class="right">
        <form>
            <div class="form-group form-group-search">
            <input type="text" class="form-input input-lg input-rounded input-search" placeholder="Cerca notifiche">
            <svg><use xlink:href="#icon-search"></use></svg>
            </div>
        </form>
    </li>
</ul>
