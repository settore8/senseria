@if(Auth::check())
<li class="has-submenu gestione"><a href="#"><svg><use xlink:href="#icon-edit"></use></svg> <span class="label">Gestione</span></a>
    <ul class="submenu">
        <li><a href="{{ route('articoli.gestione.raccolte') }}" title="Raccolta articoli preferiti" @if(Request::is('articoli/gestione/raccolte')) class="current" @endif>Le mie raccolte</a></li>
    </ul>
</li>
@endif
