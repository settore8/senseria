<li class="has-submenu gestione"><a href="#" @if(Route::currentRouteName() == 'immobili.gestione') class="current" @endif><svg><use xlink:href="#icon-edit"></use></svg> <span class="label">Gestione</span></a>
    <ul class="submenu submenu-right">
        <li>
            <a href="{!! route('immobili') !!}" @if(Request::is('immobili')) class="current" @endif>Immobili</a>
        </li>
        <li>
            <a href="{!! route('immobili.gestione') !!}" title="Immobili creati da me" @if(Request::is('immobili/gestione')) class="current" @endif>Creati da me</a>
        </li>
        @if(Auth::user()->isProfessionista())
        <li>
            <a href="{{ route('immobili.gestione.assegnati') }}" title="Immobili assegnati a me" @if(Request::is('immobili/gestione/assegnati')) class="current" @endif>Assegnati a me</a>
        </li>
        @endif
        <li>
            <a href="{{ route('immobili.gestione.archiviati') }}" title="Immobili archiviati" @if(Request::is('immobili/gestione/archiviati')) class="current" @endif>Archiviati</a>
        </li>
    </ul>
</li>