<form class="item item--center sendOnEnter canLeavePage">
    <div class="form-group form-group-search">
        <input type="text" class="form-input input-lg input-search input-rounded" placeholder="Cerca galleria">
        <svg><use xlink:href="#icon-search"></use></svg>
    </div>
</form>