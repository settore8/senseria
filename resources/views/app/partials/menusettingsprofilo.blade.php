<ul class="inline-nav scrollmenu text-center text-left--md scrollmenu">
    <li><a href="{!! route('account.profilo') !!}" title="Informazioni generali" @if(Route::currentRouteName() == 'account.profilo') class="current" @endif>Generali</a></li>
    <li><a href="{!! route('account.profilo.immagine') !!}" title="Immagine Profilo" @if(Route::currentRouteName() == 'account.profilo.immagine') class="current" @endif>Immagine profilo</a></li>
    <li><a href="{!! route('account.profilo.contatti') !!}" title="Contatti" @if(Route::currentRouteName() == 'account.profilo.contatti') class="current" @endif>Contatti</a></li>
</ul>