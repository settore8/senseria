<a class="card card--immobile" href="{{ route('immobile', $immobile) }}">

    <div class="card--header">
        {{ $immobile->specifica->nome_pubblico }}  @if($immobile->informazione) <span class="immobile-mq"> {{ $immobile->informazione->metri_quadri }}<span class="mq">m<sup>2</sup></span> </span> @endif
        @if($immobile->annuncioVendita)
                <span class="badge badge--success badge--xs mlauto">In pubblicità</span>
        @endif
        
    </div>

    <div class="columns">

        <div class="column col-9 text-left">
            <div class="card--title">
                <strong>{{ $immobile->nome }}</strong> @if(!$immobile->informazione) <svg class="icon--25"><use xlink:href="#icon-color-attention"></use></svg>@endif
            </div>
            @if($immobile->informazione && $immobile->informazione->comune)
            <div class="card--location">
                <svg class="icon--15 mr1"><use xlink:href="#icon-marker"></use></svg> {{ $immobile->informazione->comune }} {{ $immobile->informazione->provincia }}
            </div>
            @endif

            <div class="immobile-agente">
                @if($immobile->agente)
                <span class="badge badge--xs"><svg><use xlink:href="#icon-agente"></use></svg> {{ $immobile->agente->nomecognome }}</span>
                @endif
            </div>

        </div>

        <div class="column col-3 text-right">
            <svg class="destinazione"><use xlink:href="#destinazione-{{ strtolower($immobile->destinazione->nome) }}"></use></svg>
        </div>

    </div>

    <div class="card--footer">
            @if($immobile->tecnico)
                <div class="immobile-responsabiletecnico">

                    <div class="responsabiletecnico-name">
                        <svg><use xlink:href="#icon-responsabile"></use></svg>
                        <span class="qualifica">{{ $immobile->tecnico->qualifica->nome_pubblico }} </span>
                        <span class="nome">{{ $immobile->tecnico->nomecognome }}</span>
                    </div>

                    <div class="mlauto pr3">
                        @php
                            $rand = rand(0, 100);
                        @endphp
                        <div class="chart-container tooltip" data-tooltip="Stato completamento">
                            <div class="chart chart--lg" data-percent="{{ $rand }}" data-scale-color="#42d212"></div>
                            <span class="chart-percentage"><span class="perc_value">{{ $rand }}</span><span class="perc_sign">%</span></span>
                        </div>
                    </div>

                </div>

            @elseif($immobile->annuncioRespTecnico)
                <div class="immobile-annuncio">
                    Annuncio responsabile tecnico
                </div>
            @else
                <div class="immobile-noresult">
                    Nessun Responsabile tecnico
                </div>
            @endif

    </div>

</a>
