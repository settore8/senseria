<div class="articolo__preview card">
    <a href="{{ route('articolo', ['categoria' => $articolo->categoria->slug, 'slug' => $articolo->slug ]) }}" title="Apri {{ $articolo->titolo }}" class="img-container">
    <img src="https://picsum.photos/360/220" alt="{{ $articolo->titolo }}">
    </a>
    <h4><a href="{{ route('articolo', ['categoria' => $articolo->categoria->slug, 'slug' => $articolo->slug ]) }}" title="Apri {{ $articolo->titolo }}">{{ Custom::formatTitle($articolo->titolo) }}</a></h4>
    
    <footer>
        @include('includes.partials.userpopover', ['user' => $articolo->user, 'linkable' => true])
       
        @include('includes.partials.counterviews', ['data' => $articolo])

        @include('includes.partials.counterraccolte', ['data' => $articolo, 'class' => get_class(new App\Articolo)])

        @include('includes.partials.counterlikes', ['data' => $articolo, 'class' => get_class(new App\Articolo)])

    </footer>
</div>