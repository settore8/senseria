<ul class="inline-nav scrollmenu text-center text-left--md">
    <li><a href="{!! route('account.raccolte') !!}" title="Tutte le raccolte" @if(Request::is('*/raccolte')) class="current" @endif>Tutte</a></li>
    <li><a href="{{ route('account.raccolte', 'utenti') }}" title="Tutte le raccolte" @if(Request::is('*/utenti')) class="current" @endif>Utenti</a></li>
    <li><a href="{!! route('account.raccolte', 'annunci') !!}" title="Tutte le raccolte" @if(Request::is('*/annunci')) class="current" @endif>Annunci</a></li>
    <li><a href="{!! route('account.raccolte', 'contenuti') !!}" title="Tutte le raccolte" @if(Request::is('*/contenuti')) class="current" @endif>Contenuti</a></li>
    <li class="right">
    <form action="{{ route('account.raccolte.search') }}" class="sendOnEnter canLeavePage" method="get">
            <div class="form-group form-group-search">
            <input type="text" name="s" class="form-input input-lg input-rounded input-search @isset($keyword) {{ 'focused' }} @endisset" placeholder="Cerca raccolte" value="@isset($keyword) {{ $keyword }} @endisset">
                <svg><use xlink:href="#icon-search"></use></svg>
            </div>
        </form>
    </li>
</ul>
