<form action="{{ route('immobile.sopralluogo.store.step2.fabbricato', $immobile) }}" method="POST">
    @csrf
    @php $editing = $immobile->sopralluogo->fabbricato; @endphp
    @php $fabbricato_json = $immobile->sopralluogo->fabbricato; @endphp

    @block(['class' => 'block--bordered'])
    <div class="columns">
        <div class="column col-6 col-xs-12">
                @row(['col1' => 'Anno costruzione', 'col2' => $fabbricato_json['annocostruzione']])@endrow
                @row(['col1' => 'Anno ristrutturazione', 'col2' => $fabbricato_json['annoristrutturazione']])@endrow
                @row(['col1' => 'Piani fuori terra', 'col2' => $fabbricato_json['pianifuoriterra']])@endrow
                @row(['col1' => 'Piani entro terra', 'col2' => $fabbricato_json['pianientroterra']])@endrow
        </div>

        <div class="column col-6 col-xs-12">
            @if($fabbricato_json['complessoimmobiliare'] != 'No')
                @foreach ($fabbricato_json['complessoimmobiliare'] as $specifica => $quantita)
                    @if($quantita != '0')
                        @row(['col1' => __('messages.'.$specifica), 'col2' => $quantita])@endrow
                    @endif
                @endforeach
            @endif
        </div>
    </div>
    @endblock
       
    @block(['class' => 'block--bordered', 'title' => 'Affacci'])
    @if($fabbricato_json['affacci'])
     
                    @php $index = 0; @endphp
                    @foreach ($fabbricato_json['affacci'] as $lato => $vista)
                        @if($editing && array_key_exists('struttura', $editing) && array_key_exists('facciate', $editing['struttura']) && is_array($editing['struttura']['facciate']) && $editing['struttura']['facciate'])
                            @foreach($editing['struttura']['facciate'][$index] as $key => $value)
                             
                                        <h4>{{ __('messages.'.$lato) }} - {{ __('messages.'.$vista) }}</h4>
                                        
                                                    <label>Tipologia</label>
                                                    @select(['attrs' => ['name' => 'facciata_tipologia[]', 'value' =>
                                                    Custom::addNull($fabbricato['struttura']['facciate']['tipologia']),
                                                    'default' => $key ? $key : null
                                                    ]])
                                                    @endselect
                                               
                                                    <label>Stato Conservazione</label>
                                                    @select(['attrs' => ['name' => 'facciata_stato_conservazione[]',
                                                    'value' =>
                                                    Custom::addNull($fabbricato['struttura']['facciate']['statoConservazione']),
                                                    'default' => $value ? $value : null
                                                    ]])
                                                    @endselect
                         
                            @endforeach
                            @php $index++; @endphp
                        @else
                            <tr>
                                <td>
                                    <h4>{{ __('messages.'.$lato) }} - {{ __('messages.'.$vista) }}</h4>
                                    <table>
                                        <tbody>
                                        <tr>
                                            <td>
                                                <label>Tipologia</label>
                                                @select(['attrs' => ['name' => 'facciata_tipologia[]', 'value' =>
                                                Custom::addNull($fabbricato['struttura']['facciate']['tipologia']),
                                                ]])
                                                @endselect
                                            </td>
                                            <td>
                                                <label>Stato Conservazione</label>
                                                @select(['attrs' => ['name' => 'facciata_stato_conservazione[]',
                                                'value' =>
                                                Custom::addNull($fabbricato['struttura']['facciate']['statoConservazione']),
                                                ]])
                                                @endselect
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        @endif
                    @endforeach

    @endif
    @endblock
    @block(['class' => 'block--bordered'])
    <div class="flex">
        <div class="item">

            <div class="flex">
                <div class="item">
                    <div class="form-group">
                        <span>Struttura Portante Verticale</span>
                        <span><a href="#" data-clone="row-struttura_portante_verticale"
                                 class="btn btn-primary">+</a></span>
                        <table>
                            <tbody>

                            @if($editing && array_key_exists('struttura', $editing) && array_key_exists('struttura_portante_verticale', $editing['struttura']) && is_array($editing['struttura']['struttura_portante_verticale']) && $editing['struttura']['struttura_portante_verticale'])
                                @foreach($editing['struttura']['struttura_portante_verticale'] as $value)
                                    @foreach($value as $value_key => $value_name)
                                        <tr data-target-clone="row-struttura_portante_verticale">
                                            <td>
                                                <label>Materiale</label>
                                                @select(['attrs' => ['name' =>
                                                'struttura_portante_verticale_materiale[]',
                                                'value' =>
                                                Custom::addNull($fabbricato['struttura']['portanteVerticale']['materiale']),
                                                'default' => $value_key ? $value_key : null
                                                ]])
                                                @endselect
                                            </td>
                                            <td>
                                                <label>Stato Conservazione</label>
                                                @select(['attrs' => ['name' =>
                                                'struttura_portante_verticale_stato_conservazione[]', 'value' =>
                                                Custom::addNull($fabbricato['struttura']['portanteVerticale']['statoConservazione']),
                                                'default' => $value_name ? $value_name : null
                                                ]])
                                                @endselect
                                            </td>
                                        </tr>
                                    @endforeach
                                @endforeach
                            @else
                                <tr data-target-clone="row-struttura_portante_verticale">
                                    <td>
                                        <label>Materiale</label>
                                        @select(['attrs' => ['name' => 'struttura_portante_verticale_materiale[]',
                                        'value' =>
                                        Custom::addNull($fabbricato['struttura']['portanteVerticale']['materiale']),
                                        ]])
                                        @endselect
                                    </td>
                                    <td>
                                        <label>Stato Conservazione</label>
                                        @select(['attrs' => ['name' =>
                                        'struttura_portante_verticale_stato_conservazione[]', 'value' =>
                                        Custom::addNull($fabbricato['struttura']['portanteVerticale']['statoConservazione']),
                                        ]])
                                        @endselect
                                    </td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <br><br>
        </div>
    </div>
    @endblock
    @block(['class' => 'block--bordered'])
    <div class="flex">
        <div class="item">

            <div class="flex">
                <div class="item">
                    <div class="form-group">
                        <span>Copertura</span>
                        <span><a href="#" data-clone="row-copertura" class="btn btn-primary">+</a></span>
                        <table>
                            <tbody>
                            @if($editing && array_key_exists('struttura', $editing) && array_key_exists('copertura', $editing['struttura']) && is_array($editing['struttura']['copertura']) && $editing['struttura']['copertura'])
                                @foreach($editing['struttura']['copertura'] as $copertura_key => $copertura_value)
                                    @foreach($copertura_value as $key => $value)
                                        <tr data-target-clone="row-copertura">
                                            <td>
                                                <label>Tipologia</label>
                                                @select(['attrs' => ['name' => 'copertura_tipologia[]', 'value' =>
                                                Custom::addNull($fabbricato['struttura']['copertura']['tipologia']),
                                                'default' => $key ? $key : null
                                                ]])
                                                @endselect
                                            </td>
                                            <td>
                                                <label>Finitura</label>
                                                @select(['attrs' => ['name' => 'copertura_finitura[]', 'value' =>
                                                Custom::addNull($fabbricato['struttura']['copertura']['finitura']),
                                                'default' => $value['Finitura'] ? $value['Finitura'] : null
                                                ]])
                                                @endselect
                                            </td>
                                            <td>
                                                <label>Stato Conservazione</label>
                                                @select(['attrs' => ['name' => 'copertura_stato_conservazione[]',
                                                'value' =>
                                                Custom::addNull($fabbricato['struttura']['copertura']['statoConservazione']),
                                                'default' => $value['Stato conservazione'] ?
                                                $value['Stato conservazione'] : null
                                                ]])
                                                @endselect
                                            </td>
                                        </tr>
                                    @endforeach
                                @endforeach
                            @else
                                <tr data-target-clone="row-copertura">
                                    <td>
                                        <label>Tipologia</label>
                                        @select(['attrs' => ['name' => 'copertura_tipologia[]', 'value' =>
                                        Custom::addNull($fabbricato['struttura']['copertura']['tipologia']),

                                        ]])
                                        @endselect
                                    </td>
                                    <td>
                                        <label>Finitura</label>
                                        @select(['attrs' => ['name' => 'copertura_finitura[]', 'value' =>
                                        Custom::addNull($fabbricato['struttura']['copertura']['finitura']),
                                        ]])
                                        @endselect
                                    </td>
                                    <td>
                                        <label>Stato Conservazione</label>
                                        @select(['attrs' => ['name' => 'copertura_stato_conservazione[]', 'value' =>
                                        Custom::addNull($fabbricato['struttura']['copertura']['statoConservazione']),
                                        ]])
                                        @endselect
                                    </td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <br><br>
        </div>
    </div>
    @endblock
    @block(['class' => 'block--bordered'])
    <div class="flex">
        <div class="item">

            <div class="flex">
                <div class="item">
                    <div class="form-group">
                        <span>Struttura Portante Orizzontale (Solai)</span>
                        <span><a href="#" data-clone="row-struttura_portante_orizzontale"
                                 class="btn btn-primary">+</a></span>
                        <table>
                            <tbody>
                            @if($editing && array_key_exists('struttura', $editing) && array_key_exists('struttura_portante_orizzontale', $editing['struttura']) && is_array($editing['struttura']['struttura_portante_orizzontale']) && $editing['struttura']['struttura_portante_orizzontale'])
                                @foreach($editing['struttura']['struttura_portante_orizzontale'] as $value)
                                    @foreach($value as $value_key => $value_name)
                                        <tr data-target-clone="row-struttura_portante_orizzontale">
                                            <td>
                                                <label>Materiale</label>
                                                @select(['attrs' => ['name' =>
                                                'struttura_portante_orizzontale_materiale[]',
                                                'value' =>
                                                Custom::addNull($fabbricato['struttura']['portanteOrizzonale']['materiale']),
                                                'default' => $value_key ? $value_key : null
                                                ]])
                                                @endselect
                                            </td>
                                            <td>
                                                <label>Stato Conservazione</label>
                                                @select(['attrs' => ['name' =>
                                                'struttura_portante_orizzontale_stato_conservazione[]', 'value' =>
                                                Custom::addNull($fabbricato['struttura']['portanteOrizzonale']['statoConservazione']),
                                                'default' => $value_name ? $value_name : null
                                                ]])
                                                @endselect
                                            </td>
                                        </tr>
                                    @endforeach
                                @endforeach
                            @else
                                <tr data-target-clone="row-struttura_portante_orizzontale">
                                    <td>
                                        <label>Materiale</label>
                                        @select(['attrs' => ['name' => 'struttura_portante_orizzontale_materiale[]',
                                        'value' =>
                                        Custom::addNull($fabbricato['struttura']['portanteOrizzonale']['materiale']),
                                        ]])
                                        @endselect
                                    </td>
                                    <td>
                                        <label>Stato Conservazione</label>
                                        @select(['attrs' => ['name' =>
                                        'struttura_portante_orizzontale_stato_conservazione[]', 'value' =>
                                        Custom::addNull($fabbricato['struttura']['portanteOrizzonale']['statoConservazione']),
                                        ]])
                                        @endselect
                                    </td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <br><br>
        </div>
    </div>
    @endblock

    @button(['attrs' => ['text' => 'Completa fabbricato']])@endbutton

</form>
