@php $parti_condominiali = $immobile->sopralluogo->particondominiali @endphp
{{--{{dd($parti_condominiali)}}--}}
@if($parti_condominiali)
    @foreach($parti_condominiali as $key => $value)
        @if(!is_numeric($key))<h2>{{__('messages.'.$key)}}</h2>@endif
        @if(is_array($value))
            @foreach($value as $subkey => $subvalue)
                @if(is_array($subvalue) && $subvalue)
                    @if(!is_numeric($subkey))<h3>{{__('messages.'.$subkey)}}</h3>@endif
                    @foreach($subvalue as $sub2key => $sub2value)
                        @if(is_array($sub2value))
                            @if(!is_numeric($sub2key))<h4>{{__('messages.'.$sub2key)}}</h4>@endif
                            @foreach($sub2value as $sub3key => $sub3value)
                                @if(is_array($sub3value))
                                    @if(!is_numeric($sub3value))<h5>{{__('messages.'.$sub3value)}}</h5>@endif
                                    @foreach($sub3value as $sub4key => $sub4value)
                                        <div>
                                            {{__('messages.'.$sub4key).': '.$sub4value}}
                                        </div>
                                    @endforeach
                                @else
                                    @if(is_numeric($sub3key))
                                        <div>
                                            {{__('messages.'.$sub2key).': '.$sub3value}}
                                        </div>
                                    @else
                                        <div>
                                            {{__('messages.'.$sub3key).': '.$sub3value}}
                                        </div>
                                    @endif
                                @endif
                            @endforeach
                        @else
                            <div>
                                {{$subkey.': '.$sub2value }}
                            </div>
                        @endif
                    @endforeach
                @elseif(!$subvalue)
                    <div>
                        {{__('messages.'.$subkey).': ' }}
                    </div>
                @elseif(is_numeric($subkey))
                    <div>
                        {{__('messages.'.$key).': '.$subvalue }}
                    </div>
                @else
                    <div>
                        {{__('messages.'.$subkey).': '.$subvalue }}
                    </div>
                @endif
            @endforeach
        @else
            <div>
                {{__('messages.'.$key).': '.$value}}
            </div>
        @endif
    @endforeach
@else
    <div>
        Le parti condominiali non sono state ancora inserite
    </div>
@endif

