@php $contesto = $immobile->sopralluogo->contesto @endphp

@if($contesto)
    @block(['class' => 'block--bordered'])
    @foreach($contesto as $key => $value)
        @if(is_array($value))
            @if(!is_numeric($key))<h2>{{__('messages.'.$key)}}</h2>@endif
            @foreach($value as $subkey => $subvalue)
                @if(is_numeric($subkey))
                    @if(!is_numeric($subkey))<h3>{{__('messages.'.$subkey)}}</h3>@endif
                        {{$subvalue}}@if(!$loop->last){{ ', ' }}@endif
                @else
                    <div>
                        @row(['col1' => __('messages.'.$subkey), 'col2' => $subvalue])@endrow
                    </div>
                @endif
            @endforeach
        @else
                @row(['col1' => __('messages.'.$key), 'col2' => $value])@endrow
        @endif

    @endforeach
    @endblock
   
@else
    <div>
        Il contesto non è ancora stato creato
    </div>
@endif
