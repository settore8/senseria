<form action="{{ route('immobile.sopralluogo.store.impianti.unitaimmobiliare', $immobile) }}" method="POST">
    @csrf
    @php $editing = $immobile->sopralluogo->unitaimmobiliare @endphp
    @foreach($unita_immobiliare['finiture'] as $finiture => $finitura)
{{--        {{dd($editing)}}--}}
        @block(['class' => 'block--bordered'])
        <div class="flex">
            <div class="item">
                <div class="form-group">
                    <span>{{__('messages.'.$finiture)}}</span>
                    <table>
                        <tbody>
                        @if($editing && array_key_exists('finiture', $editing) && $editing['finiture'][$finiture])
                            <tr>
                                @php $index = 0 @endphp
                                @foreach($finitura as $tipologia => $valore)
                                    <td>
                                        <label>{{__('messages.'.$tipologia)}}</label>
                                        @select(['attrs' => ['name' => "finiture[".$finiture."][".$tipologia."]",
                                        'value' => Custom::addNull($valore),
                                        'default' => $editing['finiture'][$finiture] &&
                                        array_key_exists($index, $editing['finiture'][$finiture]) ?
                                        $editing['finiture'][$finiture][$index] : null
                                        ]])
                                        @endselect

                                    </td>
                                    @php $index++ @endphp
                                @endforeach
                                <td>

                                    <span>Spessore</span>
                                    @inputnumber(['attrs' => ['name' => "finiture[".$finiture."][spessore]",
                                    'placeholder' => 'Spessore',
                                    'value' => $editing['finiture'][$finiture] &&
                                    array_key_exists($index, $editing['finiture'][$finiture]) ?
                                    $editing['finiture'][$finiture][$index] : null
                                    ]])
                                    @endinputnumber
                                </td>
                            </tr>
                        @else
                            <tr>
                                @foreach($finitura as $tipologia => $valore)
                                    <td>
                                        <label>{{__('messages.'.$tipologia)}}</label>
                                        @select(['attrs' => ['name' => "finiture[".$finiture."][".$tipologia."]",
                                        'value' =>
                                        Custom::addNull($valore) ]])
                                        @endselect
                                    </td>
                                @endforeach
                                <td>
                                    <span>Spessore</span>
                                    @inputnumber(['attrs' => ['name' => "finiture[".$finiture."][spessore]",
                                    'placeholder' =>
                                    'Spessore']]) @endinputnumber
                                </td>
                            </tr>
                        @endif
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
        <br><br>
        @endblock
    @endforeach
    @block(['class' => 'block--bordered'])
    <div class="flex">
        <div class="item">
            <div class="form-group">
                <span>Impianto Riscaldamento</span>
                <table>
                    <tbody>

                    @foreach($unita_immobiliare['impiantoRiscaldamento'] as $nome => $tipo)
                        <tr>
                            @php $index = 0; @endphp
                            @if($nome == 'tipologia')
                                <td>

                                    <label>{{__('messages.'.$nome)}}</label>
                                    @select(['attrs' => ['name' => "impiantoriscaldamento[".$nome."][]", 'value' =>
                                    Custom::addNull($tipo),
                                    'default' => array_key_exists('impiantoriscaldamento', $editing) &&
                                    $editing['impiantoriscaldamento'][$nome] &&
                                    array_key_exists($index, $editing['impiantoriscaldamento'][$nome]) ?
                                    $editing['impiantoriscaldamento'][$nome][$index] : null ]])
                                    @endselect
                                </td>
                            @elseif($nome == 'caldaia')

                                @foreach($tipo as $sottotipo_nome => $sottotipo_value)
                                    <td>
                                        @if($loop->first)
                                            <span>{{__('messages.'.$nome)}} </span>
                                        @endif
                                        <label>{{ $sottotipo_nome }}</label>
                                        @select(['attrs' => ['name' =>
                                        "impiantoriscaldamento[".$nome."][".$sottotipo_nome."][]",
                                        'value' => Custom::addNull($sottotipo_value),
                                        'default' => array_key_exists('impiantoriscaldamento', $editing) &&
                                        $editing['impiantoriscaldamento'][$nome] &&
                                        array_key_exists($index, $editing['impiantoriscaldamento'][$nome]) ?
                                        $editing['impiantoriscaldamento'][$nome][$index][0] : null ]])
                                        @endselect
                                    </td>

                                    @if($sottotipo_nome == 'potenza')
                                        <td>

                                            @inputnumber(['attrs' => ['name' =>
                                            "impiantoriscaldamento[".$nome."][".$sottotipo_nome."][]", 'placeholder' =>
                                            'Potenza',
                                            'value' => array_key_exists('impiantoriscaldamento', $editing) &&
                                            $editing['impiantoriscaldamento'][$nome] &&
                                            array_key_exists($index, $editing['impiantoriscaldamento'][$nome]) ?
                                            $editing['impiantoriscaldamento'][$nome][$index][1] : null ]])
                                            @endinputnumber
                                        </td>
                                    @endif
                                    @php $index++; @endphp
                                @endforeach
                                <td>
                                    {{--                                    {{dd($editing['impiantoriscaldamento'][$nome][$index])}}--}}
                                    <span>Marca</span>
                                    @inputtext(['attrs' => ['name' => "impiantoriscaldamento[".$nome."][marca]",
                                    'placeholder' => 'Marca Caldaia',
                                    'value' => array_key_exists('impiantoriscaldamento', $editing) &&
                                    $editing['impiantoriscaldamento'][$nome] &&
                                    array_key_exists($index, $editing['impiantoriscaldamento'][$nome]) ?
                                    $editing['impiantoriscaldamento'][$nome][$index++] : null ]])
                                    @endinputtext
                                    <span>Modello</span>
                                    @inputtext(['attrs' => ['name' => "impiantoriscaldamento[".$nome."][modello]",
                                    'placeholder' => 'Modello Caldaia',
                                    'value' => array_key_exists('impiantoriscaldamento', $editing) &&
                                    $editing['impiantoriscaldamento'][$nome] &&
                                    array_key_exists($index, $editing['impiantoriscaldamento'][$nome]) ?
                                    $editing['impiantoriscaldamento'][$nome][$index++] : null ]])
                                    @endinputtext
                                    <span>Anno Installazione</span>
                                    @inputnumber(['attrs' => ['name' =>
                                    "impiantoriscaldamento[".$nome."][anno_installazione]", 'placeholder' => 'Anno
                                    Installazione',
                                    'value' => array_key_exists('impiantoriscaldamento', $editing) &&
                                    $editing['impiantoriscaldamento'][$nome] &&
                                    array_key_exists($index, $editing['impiantoriscaldamento'][$nome]) ?
                                    $editing['impiantoriscaldamento'][$nome][$index++] : null ]])
                                    @endinputnumber
                                </td>
                            @elseif($nome == 'terminali')
                                @php $index = 0; @endphp
                                <td>
                                    <label>{{__('messages.'.$nome)}}</label>
                                    @select(['attrs' => ['name' => "impiantoriscaldamento[".$nome."][]", 'value' =>
                                    Custom::addNull($tipo),
                                    'default' => array_key_exists('impiantoriscaldamento', $editing) &&
                                    $editing['impiantoriscaldamento'][$nome] &&
                                    !is_array(array_key_exists($index, $editing['impiantoriscaldamento'][$nome])) &&
                                    !is_array($editing['impiantoriscaldamento'][$nome][$index]) ?
                                    $editing['impiantoriscaldamento'][$nome][$index++] : null
                                    ]])
                                    @endselect
                                </td>
                                <td id="Aria" class="selectChangeTarget hidden">
                                    Aria
                                </td>


                                {{--#TO_REVIEW# Deve esistere solo se è selezionato radiatori e termo arredo--}}
                                <td id="Radiatorietermoarredo" class="selectChangeTarget hidden">
                                @foreach($immobile->sopralluogo->unitaimmobiliare['consistenza'] as $locali => $locale)
                                    @foreach($immobile->sopralluogo->unitaimmobiliare['consistenza'][$locali] as $sublocale)
                                        <td>
                                            <span>{{__('messages.'.$sublocale['locali'])}}</span><br>
                                            <span>Numero colonne</span>
                                            @inputnumber(['attrs' => ['name' =>
                                            "impiantoriscaldamento[".$nome."][".$sublocale['locali']."][numerocolonne]",
                                            'placeholder' => 'Numero Colonne',
                                            'value' => array_key_exists('impiantoriscaldamento', $editing) &&
                                            array_key_exists($nome, $editing['impiantoriscaldamento']) &&
                                            array_key_exists($index, $editing['impiantoriscaldamento'][$nome]) &&
                                            $editing['impiantoriscaldamento'][$nome] &&
                                            $editing['impiantoriscaldamento'][$nome][$index]['numerocolonne'] ?
                                            $editing['impiantoriscaldamento'][$nome][$index]['numerocolonne'] : null ]])
                                            @endinputnumber

                                            <span>Altezza Radiatore</span>
                                            @inputnumber(['attrs' => ['name' =>
                                            "impiantoriscaldamento[".$nome."][".$sublocale['locali']."][altezzaradiatore]",
                                            'placeholder' => 'Altezza Radiatore',
                                            'value' => array_key_exists('impiantoriscaldamento', $editing) &&
                                            array_key_exists($nome, $editing['impiantoriscaldamento']) &&
                                            array_key_exists($index, $editing['impiantoriscaldamento'][$nome]) &&
                                            $editing['impiantoriscaldamento'][$nome] &&
                                            $editing['impiantoriscaldamento'][$nome][$index]['altezzaradiatore'] ?
                                            $editing['impiantoriscaldamento'][$nome][$index]['altezzaradiatore'] : null
                                            ]])
                                            @endinputnumber
                                            <span>Materiale</span>
                                            @select(['attrs' => ['name' =>
                                            "impiantoriscaldamento[".$nome."][".$sublocale['locali']."][materiale]",
                                            'value' => config('constants.materiale-radiatore'),
                                            'default' => array_key_exists('impiantoriscaldamento', $editing) &&
                                            array_key_exists($nome, $editing['impiantoriscaldamento']) &&
                                            array_key_exists($index, $editing['impiantoriscaldamento'][$nome]) &&
                                            $editing['impiantoriscaldamento'][$nome] &&
                                            $editing['impiantoriscaldamento'][$nome][$index]['materiale'] ?
                                            $editing['impiantoriscaldamento'][$nome][$index++]['materiale'] : null ]])
                                            @endselect
                                        </td>
                                        @endforeach
                                        @endforeach
                                        </td>
                                        @elseif($nome == 'produzioneAcquaCaldaSanitaria')
                                            @php $index = 0; @endphp
                                            <td>
                                                <span>Produzione Acqua Calda Sanitaria</span>
                                                @select(['attrs' => ['name' => "impiantoriscaldamento[".$nome."][]",
                                                'value' => config('constants.produzione-acqua-calda-sanitaria'),
                                                'default' => array_key_exists('impiantoriscaldamento', $editing) &&
                                                $editing['impiantoriscaldamento'][$nome] &&
                                                $editing['impiantoriscaldamento'][$nome][$index] ?
                                                $editing['impiantoriscaldamento'][$nome][$index] : null ]])
                                                @endselect
                                            </td>
                                            {{--#TO_REVIEW# Deve esistere solo se è selezionato produzioneAcquaCaldaSanitaria--}}
                                            @foreach($tipo as $sottotipo_nome => $sottotipo_value)
                                                @if($sottotipo_value != 'Integrato con il generatore di calore')
                                                    @php $index++; @endphp
                                                    @foreach($sottotipo_value as $cat_name_value => $cat_value)
                                                        <td>
                                                            @if($loop->first)
                                                                <span>{{__('messages.'.$nome)}} </span>
                                                            @endif
                                                            <label>{{ __('messages.'.$cat_name_value) }}</label>
                                                            @select(['attrs' => ['name' =>
                                                            "impiantoriscaldamento[".$nome."][".$sottotipo_nome."][".$cat_name_value."]",
                                                            'value' => Custom::addNull($cat_value),
                                                            'default' => array_key_exists('impiantoriscaldamento',
                                                            $editing) &&
                                                            $editing['impiantoriscaldamento'][$nome] &&
                                                            $editing['impiantoriscaldamento'][$nome][$index][$cat_name_value]
                                                            ?
                                                            $editing['impiantoriscaldamento'][$nome][$index][$cat_name_value]
                                                            : null ]])
                                                            @endselect
                                                        </td>
                                                    @endforeach
                                                    <td>
                                                        <span>Marca</span>
                                                        @inputtext(['attrs' => ['name' =>
                                                        "impiantoriscaldamento[".$nome."][".$sottotipo_nome."][marca]",
                                                        'placeholder' => 'Marca Caldaia',
                                                        'value' => array_key_exists('impiantoriscaldamento', $editing)
                                                        &&
                                                        $editing['impiantoriscaldamento'][$nome] &&
                                                        $editing['impiantoriscaldamento'][$nome][$index]['marca'] ?
                                                        $editing['impiantoriscaldamento'][$nome][$index]['marca'] : null
                                                        ]])
                                                        ]])
                                                        @endinputtext
                                                        <span>Modello</span>
                                                        @inputtext(['attrs' => ['name' =>
                                                        "impiantoriscaldamento[".$nome."][".$sottotipo_nome."][modello]",
                                                        'placeholder' => 'Modello Caldaia',
                                                        'value' => array_key_exists('impiantoriscaldamento', $editing)
                                                        &&
                                                        $editing['impiantoriscaldamento'][$nome] &&
                                                        $editing['impiantoriscaldamento'][$nome][$index]['modello'] ?
                                                        $editing['impiantoriscaldamento'][$nome][$index]['modello'] :
                                                        null ]])
                                                        ]])
                                                        @endinputtext
                                                        <span>Anno Installazione</span>
                                                        @inputtext(['attrs' => ['name' =>
                                                        "impiantoriscaldamento[".$nome."][".$sottotipo_nome."][anno_installazione]",
                                                        'placeholder' => 'Anno Installazione',
                                                        'value' => array_key_exists('impiantoriscaldamento', $editing)
                                                        &&
                                                        $editing['impiantoriscaldamento'][$nome] &&
                                                        $editing['impiantoriscaldamento'][$nome][$index]['anno_installazione']
                                                        ?
                                                        $editing['impiantoriscaldamento'][$nome][$index]['anno_installazione']
                                                        : null ]])
                                                        ]])
                                                        @endinputtext
                                                    </td>
                                                @endif
                                            @endforeach
                                        @elseif($nome == 'elementiIntegrativiProduzioneCalore')
                                            @php $index = 0; @endphp
                                            <td>
                                                <label>elementi Integrativi Produzione Calore</label>
                                                @select(['attrs' => ['name' => "impiantoriscaldamento[".$nome."][]",
                                                'value' => Custom::addNull($tipo),
                                                'default' => array_key_exists('impiantoriscaldamento', $editing) &&
                                                $editing['impiantoriscaldamento'][$nome] &&
                                                $editing['impiantoriscaldamento'][$nome][$index] ?
                                                $editing['impiantoriscaldamento'][$nome][$index] : null
                                                ]])
                                                @endselect
                                            </td>
                                        @elseif($nome == 'statoConservazione')
                                            <td>
                                                <label>{{ __('messages.'.$nome) }}</label>
                                                @select(['attrs' => ['name' => "impiantoriscaldamento[".$nome."][]",
                                                'value' => Custom::addNull($tipo),
                                                'default' => array_key_exists('impiantoriscaldamento', $editing) &&
                                                $editing['impiantoriscaldamento'][$nome] &&
                                                $editing['impiantoriscaldamento'][$nome][$index] ?
                                                $editing['impiantoriscaldamento'][$nome][$index] : null
                                                ]])
                                                @endselect
                                            </td>
                                        @endif
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>
    <br><br>
    @endblock
    @block(['class' => 'block--bordered'])
    <div class="flex">
        <div class="item">
            <div class="form-group">
                <span>Fonti Rinnovabili</span>
                <table>
                    <tbody>
                    @foreach($unita_immobiliare['fontiRinnovabili'] as $nome => $tipo)

                        @if($nome == 'impiantoFotovoltaico' || $nome == 'impiantoSolareTermico')
                            @if(array_key_exists('fontirinnovabili', $editing) && array_key_exists($nome, $editing['fontirinnovabili']))
                                @foreach($editing['fontirinnovabili'][$nome] as $editing_key => $editing_value)
                                    @php $index = 0; @endphp
                                    <tr data-target-clone="row-{{$nome}}">
                                        @foreach($tipo as $sottotipo_nome => $sottotipo_value)
                                            <td>
                                                @if($loop->first)
                                                    <span><a href="#" data-clone="row-{{$nome}}"
                                                             class="btn btn-primary">+</a></span>
                                                    <span>{{__('messages.'.$nome)}} </span>
                                                @endif
                                                <label>{{ __('messages.'.$sottotipo_nome) }}</label>
                                                @select(['attrs' => ['name' =>
                                                "fontirinnovabili[".$nome."][".$sottotipo_nome."][]", 'value' =>
                                                Custom::addNull($sottotipo_value),
                                                'default' =>
                                                array_key_exists('fontirinnovabili', $editing) &&
                                                $editing['fontirinnovabili'][$nome] &&
                                                $editing['fontirinnovabili'][$nome][$editing_key][$index] ?
                                                $editing['fontirinnovabili'][$nome][$editing_key][$index] : null
                                                ]])
                                                @endselect
                                                @php $index++; @endphp
                                                @endforeach
                                            </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr data-target-clone="row-{{$nome}}">
                                    @foreach($tipo as $sottotipo_nome => $sottotipo_value)
                                        <td>
                                            @if($loop->first)
                                                <span><a href="#" data-clone="row-{{$nome}}"
                                                         class="btn btn-primary">+</a></span>
                                                <span>{{__('messages.'.$nome)}} </span>
                                            @endif
                                            <label>{{ __('messages.'.$sottotipo_nome) }}</label>
                                            @select(['attrs' => ['name' =>
                                            "fontirinnovabili[".$nome."][".$sottotipo_nome."][]", 'value' =>
                                            Custom::addNull($sottotipo_value),

                                            ]])
                                            @endselect
                                            @endforeach
                                        </td>
                                </tr>
                            @endif
                        @elseif($nome == 'impiantoEolico')
                            @if(array_key_exists('fontirinnovabili', $editing) && array_key_exists($nome, $editing['fontirinnovabili']))
                                @foreach($editing['fontirinnovabili'][$nome] as $editing_key => $editing_value)
                                    @foreach($editing_value as $k => $v)
                                        <tr data-target-clone="row-{{$nome}}">
                                            <td>
                                        <span><a href="#" data-clone="row-{{$nome}}"
                                                 class="btn btn-primary">+</a></span>
                                                <span>{{$nome}} </span>
                                                <span>Potenza</span>
                                                @inputtext(['attrs' => ['name' =>
                                                "fontirinnovabili[".$nome."][][potenza]",
                                                'placeholder' => 'Potenza',
                                                'value' =>
                                                array_key_exists('fontirinnovabili', $editing) &&
                                                $editing['fontirinnovabili'][$nome] &&
                                                $editing['fontirinnovabili'][$nome][$editing_key][$k] ?
                                                $editing['fontirinnovabili'][$nome][$editing_key][$k] : null
                                                ]])
                                                @endinputtext
                                            </td>
                                            @endforeach
                                        </tr>
                                    @endforeach
                                    @else
                                        <tr data-target-clone="row-{{$nome}}">
                                            <td>
                                        <span><a href="#" data-clone="row-{{$nome}}"
                                                 class="btn btn-primary">+</a></span>
                                                <span>{{$nome}} </span>
                                                <span>Potenza</span>
                                                @inputtext(['attrs' => ['name' =>
                                                "fontirinnovabili[".$nome."][][potenza]",
                                                'placeholder' => 'Potenza',
                                                'value' =>
                                                array_key_exists('fontirinnovabili', $editing) &&
                                                $editing['fontirinnovabili'][$nome] &&
                                                $editing['fontirinnovabili'][$nome][$editing_key][$k] ?
                                                $editing['fontirinnovabili'][$nome][$editing_key][$k] : null
                                                ]])
                                                @endinputtext
                                            </td>
                                        </tr>
                                    @endif
                                    @endif
                                @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>
    <br><br>
    @endblock
    @block(['class' => 'block--bordered'])
    <div class="flex">
        <div class="item">
            <div class="form-group">
                <span>Impianto idrico e scarichi</span>
                <table>
                    <tbody>
                    @foreach($unita_immobiliare['Impiantoidricoescarichi'] as $nome => $tipo)
                        <tr>
                            <td>
                                <label>{{__('messages.'.$nome)}}</label>
                                @if($nome == 'statoConservazione')
{{--                                                                                                            {{dd($editing)}}--}}
{{--                                                                                                            {{dd($editing['impiantoidricoescarichi'][$nome])}}--}}
                                    @select(['attrs' => ['name' => "impiantoidricoescarichi[".$nome."][]", 'value' =>
                                    Custom::addNull($tipo),
                                    'default' => array_key_exists('impiantoidricoescarichi', $editing) &&
                                    $editing['impiantoidricoescarichi'][$nome] ?
                                    $editing['impiantoidricoescarichi'][$nome][0] : null
                                    ]])
                                    @endselect
                                @else
                                    {{--                                    #TO_REVIEW# da impostare il default per le select 2--}}
                                    @selectmultiple(['attrs' => ['name' => "impiantoidricoescarichi[".$nome."][]",
                                    'value' => Custom::addNull($tipo),
                                    'multiple' => 'multiple',
                                    'default' => array_key_exists('impiantoidricoescarichi', $editing) &&
                                    array_key_exists($nome, $editing['impiantoidricoescarichi']) &&
                                    $editing['impiantoidricoescarichi'][$nome] ?
                                    $editing['impiantoidricoescarichi'][$nome] : null
                                    ]])
                                    @endselectmultiple
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>
    <br><br>
    @endblock
    @block(['class' => 'block--bordered'])
    <div class="flex">
        <div class="item">
            <div class="form-group">
                <span>Impianti elettrici e tecnologici</span>
                <table>
                    <tbody>
                    {{--                        {{dd($unita_immobiliare['impiantiElettriciTecnologici'])}}--}}
                    @foreach($unita_immobiliare['impiantiElettriciTecnologici'] as $nome => $tipo)
                        <tr>
                            <td>
                                <label>{{__('messages.'.$nome)}}</label>
                                @if($nome == 'statoConservazione')
                                    @select(['attrs' => ['name' => "impiantielettricitecnologici[".$nome."][]", 'value'
                                    => Custom::addNull($tipo),
                                    'default' => array_key_exists('impiantielettricitecnologici', $editing) &&
                                    $editing['impiantielettricitecnologici'][$nome] ?
                                    $editing['impiantielettricitecnologici'][$nome][0] : null
                                    ]])
                                    @endselect
                                @else
{{--                                                                        {{dd($editing)}}--}}
{{--                                                                        {{dd($editing['impiantielettricitecnologici'][$nome])}}--}}
                                    {{--                                    #TO_REVIEW# da impostare il default per le select 2--}}
                                    @selectmultiple(['attrs' => ['name' => "impiantielettricitecnologici[".$nome."][]",
                                    'value' => Custom::addNull($tipo),
                                    'multiple' => 'multiple',
                                    'default' => array_key_exists('impiantielettricitecnologici', $editing) &&
                                    array_key_exists($nome, $editing['impiantielettricitecnologici']) &&
                                    $editing['impiantielettricitecnologici'][$nome] ?
                                    $editing['impiantielettricitecnologici'][$nome] : null
                                    ]])
                                    @endselectmultiple
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>
    <br><br>
    @endblock
    <button class="btn btn-primary" type="submit">Continua</button>
</form>
