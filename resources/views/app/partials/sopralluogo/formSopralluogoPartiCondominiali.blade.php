<form action="{{ route('immobile.sopralluogo.store.particondominiali', $immobile) }}" method="POST">
    @csrf
    @php $editing = $immobile->sopralluogo->particondominiali @endphp
    @foreach($parti_condominiali['tipologia'] as $key => $value)

        @block(['class' => 'block--bordered'])
        <div class="flex">
            <div class="item">
                <div class="form-group">
                    <span>{{__('messages.'.$key)}}</span>
                    <table>
                        <tbody>
                            <span><a href="#" data-clone="row-{{$key}}"
                                     class="btn btn-primary">+</a></span>

                            @foreach($editing['tipologia'][$key] as $editing_key => $editing_value)
                                <tr data-target-clone="row-{{$key}}" data-max-clone="{{count($value)}}">
                                    {{--                                    {{dd($editing['tipologia'][$key],$editing_key,$editing_value)}}--}}
                                    <td>
                                        @select(['attrs' => ['name' => "tipologia[".$key."][]", 'value' =>
                                        Custom::addNull($value),
                                        'default' => $editing_value
                                        ]])
                                        @endselect
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
        <br><br>
        @endblock
    @endforeach
    @block(['class' => 'block--bordered'])
    <div class="flex">
        <div class="item">
            <div class="form-group">
                <span>Numero Condomini</span>
                <table>
                    <tbody>
                    <tr>
                        <td>
                            @inputnumber(['attrs' => ['name' => "ncondomini[numerocondomini]", 'placeholder' => 'Numero
                            Condomini',
                            'value' => $editing['ncondomini']['numerocondomini']
                            ]])
                            @endinputnumber
                        </td>
                    </tr>
                    </tbody>
                </table>

            </div>
        </div>
    </div>
    <br><br>
    @endblock
    @block(['class' => 'block--bordered'])
    <div class="flex">
        <div class="item">
            <div class="form-group">
                <span>Riaprtizioni Costi</span>
                <table>
                    <tbody>
                    <tr>
{{--                        {{dd($editing['ripartizionicosti']['ripartizionicosti'])}}--}}
                        <td>
                            @select(['attrs' => ['name' => "ripartizionicosti[ripartizionicosti]", 'value' =>
                            Custom::addNull($value),
                            'default' => $editing['ripartizionicosti']['ripartizionicosti']
                            ]])
                            @endselect
                        </td>
                        <td>
                            <label>Costi fissi annuali manutenzione ordinaria</label>
                            @inputnumber(['attrs' => ['name' => "ripartizionicosti[costifissi]", 'placeholder' => 'Costi
                            fissi annuali manutenzione ordinaria',
                            'value' => $editing['ripartizionicosti']['costifissi']
                            ]])
                            @endinputnumber
                        </td>
                        <td>
                            <label>Stima Costi annuali manutenzione straordinaria</label>
                            @inputnumber(['attrs' => ['name' => "ripartizionicosti[costimanutenzione]", 'placeholder' =>
                            'Stima Costi annuali manutenzione straordinaria',
                            'value' => $editing['ripartizionicosti']['costimanutenzione']
                            ]])
                            @endinputnumber
                        </td>
                    </tr>

                    </tbody>
                </table>

            </div>
        </div>
    </div>
    <br><br>
    @endblock
    <button class="btn btn-primary" type="submit">Continua</button>
</form>
