<form action="{{ route('immobile.sopralluogo.store.unitaimmobiliare', $immobile) }}" method="POST">
    @csrf
    @block(['class' => 'block--bordered'])
    <div class="form-group">
        <div class="columns">
            <div class="column col-8">
                @php $unitaimmobiliare = $immobile->specifica; @endphp
                <label>Destinazione: {{$unitaimmobiliare->destinazione->nome}}</label><br>
                <label>Nome Pubblico: {{$unitaimmobiliare->nome_pubblico}}</label>
            </div>
        </div>
    </div>
    @endblock

    @php $unita_immobiliare['consistenza'][$immobile->destinazione->nome]['altreSuperfici'][] = ''; @endphp
    @foreach($unita_immobiliare['consistenza'][$immobile->destinazione->nome] as $locali => $valori)
        @block(['class' => 'block--bordered'])
        <div class="flex">
            <div class="item">
                <div class="form-group">
                    <span>{{__('messages.'.$locali)}}</span>
                    <span><a href="#" data-clone="row-{{$locali}}"
                             class="btn btn-primary">+</a></span>
                    <table>
                        <tbody>
                        @if($immobile->sopralluogo->unitaimmobiliare && $immobile->sopralluogo->unitaimmobiliare['consistenza'] && array_key_exists($locali, $immobile->sopralluogo->unitaimmobiliare['consistenza']))
                            @foreach($immobile->sopralluogo->unitaimmobiliare['consistenza'][$locali] as $index => $listalocali)
{{--                                {{dd($immobile->sopralluogo->unitaimmobiliare['consistenza'],$index , $listalocali )}}--}}
                                <tr data-target-clone="row-{{$locali}}">
                                    <td>
                                        @if($locali != 'altreSuperfici')
                                            @select(['attrs' => ['name' => 'locali['.$locali.'][]', 'value' =>
                                            Custom::addNull($valori), 'required' => $loop->first , 'label' => 'Locale',
                                            'default' => array_key_exists('locali', $listalocali) &&
                                            $listalocali['locali'] ?
                                            $listalocali['locali'] : null
                                            ]])
                                            @endselect
                                        @else
                                            @inputtext(['attrs' => ['name' => "locali[".$locali."][]", 'placeholder' =>
                                            'Altro Locale', 'required' => $loop->first ,
                                            'value' => array_key_exists('locali', $listalocali) &&
                                            $listalocali['locali'] ?
                                            $listalocali['locali'] : null
                                            ]])
                                            @endinputtext
                                        @endif
                                    </td>
                                    <td>
                                        @inputnumber(['attrs' => ['name' => "latoa[".$locali."][]", 'placeholder' =>
                                        'Lato A (Metri)', 'label' => 'Lunghezza lato A', 'required' => $loop->first,
                                        'data-moltiplicatore' => 'true', 'data-moltiplicatore-locale' => $locali ,
                                        'value' => array_key_exists('latoa', $listalocali) &&
                                        $listalocali['latoa'] ?
                                        $listalocali['latoa'] : null
                                        ]])
                                        @endinputnumber
                                    </td>
                                    <td>
                                        @inputnumber(['attrs' => ['name' => "latob[".$locali."][]", 'placeholder' =>
                                        'Lato B (Metri)', 'label' => 'Lunghezza lato B', 'required' => $loop->first,
                                        'data-moltiplicatore' => 'true', 'data-moltiplicatore-locale' => $locali,
                                        'value' => array_key_exists('latob', $listalocali) &&
                                        $listalocali['latob'] ?
                                        $listalocali['latob'] : null
                                        ]])
                                        @endinputnumber
                                    </td>
                                    <td>
                                        <label>Risultato: </label>
                                        @inputnumber(['attrs' => ['name' => "latoab[".$locali."][]", 'placeholder' =>
                                        'Lato A * B', 'required' => $loop->first, 'data-moltiplicazione' => 'true',
                                        'data-moltiplicazione-locale' => $locali, 'readonly' => 'true',
                                        'value' => array_key_exists('latoab', $listalocali) &&
                                        $listalocali['latoab'] ?
                                        $listalocali['latoab'] : null
                                        ]])
                                        @endinputnumber
                                    </td>
                                    <td>
                                        @inputnumber(['attrs' => ['name' => "altezza[".$locali."][]", 'placeholder' =>
                                        'Altezza', 'label' => 'Altezza locale','required' => $loop->first,
                                        'value' => array_key_exists('altezza', $listalocali) &&
                                        $listalocali['altezza'] ?
                                        $listalocali['altezza'] : null
                                        ]])
                                        @endinputnumber
                                    </td>
                                    <td>
                                        <label>Piano</label>
                                        @select(['attrs' => ['name' => 'piani['.$locali.'][]', 'required' =>
                                        $loop->first, 'label' => 'Seleziona piano', 'value' =>
                                        ['--','S1','S2','S3','S4','T1','T2','T3','T4','T5','T6','T7','T8','T9','T10','T11','T12','T13','T14','T15','T16','T17','T18','T19','T20','T21','T22','T23','T24','T25','T26','T27','T28','T29','T30','T31','T32','T33','T34','T35','T36','T37','T38','T39','T40','T41','T42','T43','T44','T45','T46','T47','T48','T49','T50','T51','T52','T53','T54','T55','T56','T57','T58','T59','T60','T61','T62','T63','T64','T65','T66','T67','T68','T69','T70','T71','T72','T73','T74','T75','T76','T77','T78','T79','T80','T81','T82','T83','T84','T85','T86','T87','T88','T89','T90','T91','T92','T93','T94','T95','T96','T97','T98','T99','T100'],
                                        'default' => array_key_exists('piani', $listalocali) &&
                                        $listalocali['piani'] ?
                                        $listalocali['piani'] : null
                                        ]])
                                        @endselect
                                    </td>
                                    <td>
                                        @textarea(['attrs' => ['name' => "descrizione[".$locali."][]", 'placeholder' =>
                                        'Descrizione',
                                        'value' => array_key_exists('descrizione', $listalocali) &&
                                        $listalocali['descrizione'] ?
                                        $listalocali['descrizione'] : null
                                        ]])
                                        @endtextarea
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr data-target-clone="row-{{$locali}}">
                                <td>
                                    @if($locali != 'altreSuperfici')
                                        @select(['attrs' => ['name' => 'locali['.$locali.'][]', 'value' =>
                                        Custom::addNull($valori), 'required' => $loop->first , 'label' => 'Locale'
                                        ]])
                                        @endselect
                                    @else
                                        @inputtext(['attrs' => ['name' => "locali[".$locali."][]", 'placeholder' =>
                                        'Altro Locale', 'required' => $loop->first]]) @endinputtext
                                    @endif
                                </td>
                                <td>
                                    @inputnumber(['attrs' => ['name' => "latoa[".$locali."][]", 'placeholder' =>
                                    'Lato A (Metri)', 'label' => 'Lunghezza lato A', 'required' => $loop->first,
                                    'data-moltiplicatore' => 'true', 'data-moltiplicatore-locale' => $locali ]])
                                    @endinputnumber
                                </td>
                                <td>
                                    @inputnumber(['attrs' => ['name' => "latob[".$locali."][]", 'placeholder' =>
                                    'Lato B (Metri)', 'label' => 'Lunghezza lato B', 'required' => $loop->first,
                                    'data-moltiplicatore' => 'true', 'data-moltiplicatore-locale' => $locali ]])
                                    @endinputnumber
                                </td>
                                <td>
                                    <label>Risultato: </label>
                                    @inputnumber(['attrs' => ['name' => "latoab[".$locali."][]", 'placeholder' =>
                                    'Lato A * B', 'required' => $loop->first, 'data-moltiplicazione' => 'true',
                                    'data-moltiplicazione-locale' => $locali, 'readonly' => 'true' ]])
                                    @endinputnumber
                                </td>
                                <td>
                                    @inputnumber(['attrs' => ['name' => "altezza[".$locali."][]", 'placeholder' =>
                                    'Altezza', 'label' => 'Altezza locale','required' => $loop->first]])
                                    @endinputnumber
                                </td>
                                <td>
                                    <label>Piano</label>
                                    @select(['attrs' => ['name' => 'piani['.$locali.'][]', 'required' =>
                                    $loop->first, 'label' => 'Seleziona piano', 'value' =>
                                    ['--','S1','S2','S3','S4','T1','T2','T3','T4','T5','T6','T7','T8','T9','T10','T11','T12','T13','T14','T15','T16','T17','T18','T19','T20','T21','T22','T23','T24','T25','T26','T27','T28','T29','T30','T31','T32','T33','T34','T35','T36','T37','T38','T39','T40','T41','T42','T43','T44','T45','T46','T47','T48','T49','T50','T51','T52','T53','T54','T55','T56','T57','T58','T59','T60','T61','T62','T63','T64','T65','T66','T67','T68','T69','T70','T71','T72','T73','T74','T75','T76','T77','T78','T79','T80','T81','T82','T83','T84','T85','T86','T87','T88','T89','T90','T91','T92','T93','T94','T95','T96','T97','T98','T99','T100']]])
                                    @endselect
                                </td>
                                <td>
                                    @textarea(['attrs' => ['name' => "descrizione[".$locali."][]", 'placeholder' =>
                                    'Descrizione' ]]) @endtextarea
                                </td>
                            </tr>
                        @endif
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
        <br><br>
        @endblock
    @endforeach
    @block(['class' => 'block--bordered'])
    <div class="flex">
        <div class="item">
            <div class="form-group">
                <table>
                    <tbody>
                    <tr>
                        <td>
                            <label>Accessi</label>
                            @selectmultiple(['attrs' => ['name' => 'accessi[]', 'value' =>
                            Custom::addNull($unita_immobiliare['accessi']), 'multiple' => 'multiple', 'label' =>
                            'Accessi', 'required' => 'true',
                            'default' => $immobile->sopralluogo->unitaimmobiliare &&
                            $immobile->sopralluogo->unitaimmobiliare['accessi'] ?
                            $immobile->sopralluogo->unitaimmobiliare['accessi'] : null ]])
                            @endselectmultiple
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <br><br>
    @endblock
    <h3>Tipologia Confine</h3>
    @block(['class' => 'block--bordered'])
    <div class="flex">
        <div class="item">
            <div class="form-group">
                <span>Confini</span>
                <table>
                    <tbody>
                    <tr>
                        <td>
                            <label>Confine Superiore</label>
                            @select(['attrs' => ['name' => 'confine[superiore][]', 'value' =>
                            Custom::addNull($unita_immobiliare['confini']['superiore']),
                            'default' => $immobile->sopralluogo->unitaimmobiliare &&
                            $immobile->sopralluogo->unitaimmobiliare['confini'] &&
                            $immobile->sopralluogo->unitaimmobiliare['confini']['superiore'] ?
                            $immobile->sopralluogo->unitaimmobiliare['confini']['superiore'][0] : null ]])
                            @endselect
                        </td>
                        <td>
                            <label>Confine Inferiore</label>
                            @select(['attrs' => ['name' => 'confine[inferiore][]', 'value' =>
                            Custom::addNull($unita_immobiliare['confini']['inferiore']),
                            'default' => $immobile->sopralluogo->unitaimmobiliare &&
                            $immobile->sopralluogo->unitaimmobiliare['confini'] &&
                            $immobile->sopralluogo->unitaimmobiliare['confini']['inferiore'] ?
                            $immobile->sopralluogo->unitaimmobiliare['confini']['inferiore'][0] : null ]])
                            @endselect
                        </td>
                    </tr>
                    <span>Confini Laterali</span>
                    @if($immobile->sopralluogo['fabbricato'] && $immobile->sopralluogo['fabbricato']['affacci'])
                        @foreach($immobile->sopralluogo['fabbricato']['affacci'] as $lato => $affaccio)

                            <tr>
                                <td>
                                    <label>{{__('messages.'.$lato)}}: {{__('messages.'.$affaccio)}}</label>
                                    @select(['attrs' => ['name' => 'confine[laterale][]', 'value' =>
                                    Custom::addNull($unita_immobiliare['confini']['laterale']),
                                    'default' => $immobile->sopralluogo->unitaimmobiliare &&
                                    $immobile->sopralluogo->unitaimmobiliare['confini'] &&
                                    $immobile->sopralluogo->unitaimmobiliare['confini']['laterale'] &&
                                    array_key_exists($affaccio,
                                    $immobile->sopralluogo->unitaimmobiliare['confini']['laterale']) &&
                                    $immobile->sopralluogo->unitaimmobiliare['confini']['laterale'][$affaccio] ?
                                    $immobile->sopralluogo->unitaimmobiliare['confini']['laterale'][$affaccio] : null
                                    ]]) @endselect
                                </td>
                            </tr>
                        @endforeach
                    @endif

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <br><br>
    @endblock
    <button class="btn btn-primary requiredJs" type="button">Continua con gli Infissi</button>
</form>
