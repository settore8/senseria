<form action="{{ route('immobile.sopralluogo.store.infissi.unitaimmobiliare', $immobile) }}" method="POST">
    @csrf
    @foreach($immobile->sopralluogo->unitaimmobiliare['consistenza'] as $locali => $locale)
        @foreach($immobile->sopralluogo->unitaimmobiliare['consistenza'][$locali] as $sublocale_key => $sublocale)
            @block(['class' => 'block--bordered'])
            <div class="flex">
                <div class="item">
                    <div class="form-group">
                        <span>{{__('messages.'.$sublocale['locali'])}}</span>
                        <span><a href="#" data-clone="row-{{Str::slug($sublocale['locali'], ' ')}}-{{$sublocale_key}}"
                                 class="btn btn-primary">+</a></span>
                        <table>
                            <tbody>
                            @if($immobile->sopralluogo->unitaimmobiliare && array_key_exists('infissi', $immobile->sopralluogo->unitaimmobiliare) && $immobile->sopralluogo->unitaimmobiliare['infissi'] &&$immobile->sopralluogo->unitaimmobiliare['infissi'][$sublocale['locali']])
                                @foreach($immobile->sopralluogo->unitaimmobiliare['infissi'][$sublocale['locali']] as $index => $listalocali)
                                <tr data-target-clone="row-{{Str::slug($sublocale['locali'], ' ')}}-{{$sublocale_key}}">
                                    @foreach($unita_immobiliare['infissi'] as $tipologia => $valore)
                                        <td>
                                            <label>{{__('messages.'.$tipologia)}}</label>
                                            @select(['attrs' => ['name' => $tipologia."[".$sublocale['locali']."][]",
                                            'value' => Custom::addNull($valore),
                                            'default' => $immobile->sopralluogo->unitaimmobiliare['infissi'][$sublocale['locali']][$index][$tipologia]
                                            ]])
                                            @endselect
                                        </td>
                                    @endforeach
                                    <td>
                                        @inputnumber(['attrs' => ['name' => "larghezza[".$sublocale['locali']."][]",
                                        'placeholder' =>
                                        'Larghezza Infisso', 'data-moltiplicatore' => 'true',
                                        'data-moltiplicatore-locale' => $sublocale['locali'],
                                        'value' => array_key_exists('larghezza', $immobile->sopralluogo->unitaimmobiliare['infissi'][$sublocale['locali']][$index]) ?
                                        $immobile->sopralluogo->unitaimmobiliare['infissi'][$sublocale['locali']][$index]['larghezza'] : null
                                        ]])
                                        @endinputnumber

                                        @inputnumber(['attrs' => ['name' => "altezza[".$sublocale['locali']."][]",
                                        'placeholder' =>
                                        'Altezza Infisso', 'data-moltiplicatore' => 'true',
                                        'data-moltiplicatore-locale' => $sublocale['locali'],
                                        'value' => array_key_exists('altezza', $immobile->sopralluogo->unitaimmobiliare['infissi'][$sublocale['locali']][$index]) ?
                                        $immobile->sopralluogo->unitaimmobiliare['infissi'][$sublocale['locali']][$index]['altezza'] : null
                                        ]])
                                        @endinputnumber

                                        @inputnumber(['attrs' => ['name' => "supla[".$sublocale['locali']."][]",
                                        'placeholder' =>
                                        'Superficie L * H', 'required' => $loop->first, 'data-moltiplicazione' =>
                                        'true',
                                        'data-moltiplicazione-locale' => $locali, 'readonly' => 'true',
                                        'value' => array_key_exists('supla', $immobile->sopralluogo->unitaimmobiliare['infissi'][$sublocale['locali']][$index]) ?
                                        $immobile->sopralluogo->unitaimmobiliare['infissi'][$sublocale['locali']][$index]['supla'] : null
                                        ]])
                                        @endinputnumber

                                    </td>
                                    <td>
                                        @inputnumber(['attrs' => ['name' => "ante[".$sublocale['locali']."][]",
                                        'placeholder' => 'Ante',
                                        'value' => array_key_exists('ante' ,$immobile->sopralluogo->unitaimmobiliare['infissi'][$sublocale['locali']][$index]) ?
                                        $immobile->sopralluogo->unitaimmobiliare['infissi'][$sublocale['locali']][$index]['ante'] : null
                                        ]])
                                        @endinputnumber
                                        @inputnumber(['attrs' => ['name' => "altezzaterra[".$sublocale['locali']."][]",
                                        'placeholder' => 'Altezza da terra',
                                        'value' => array_key_exists('altezzaterra' ,$immobile->sopralluogo->unitaimmobiliare['infissi'][$sublocale['locali']][$index]) ?
                                        $immobile->sopralluogo->unitaimmobiliare['infissi'][$sublocale['locali']][$index]['altezzaterra'] : null
                                        ]])
                                        @endinputnumber

                                    </td>
                                </tr>
                                @endforeach
                            @else

                                <tr data-target-clone="row-{{Str::slug($sublocale['locali'], ' ')}}-{{$sublocale_key}}" >
                                    @foreach($unita_immobiliare['infissi'] as $tipologia => $valore)
                                        {{--                                    {{dd($unita_immobiliare['infissi'] , $tipologia, $valore)}}--}}
                                        <td>
                                            <label>{{__('messages.'.$tipologia)}}</label>
                                            @select(['attrs' => ['name' => $tipologia."[".$sublocale['locali']."][]",
                                            'value' => Custom::addNull($valore)]])
                                            @endselect
                                        </td>
                                    @endforeach
                                    <td>
                                        @inputnumber(['attrs' => ['name' => "larghezza[".$sublocale['locali']."][]",
                                        'placeholder' =>
                                        'Larghezza Infisso', 'data-moltiplicatore' => 'true',
                                        'data-moltiplicatore-locale' => $sublocale['locali'],
                                        ]])
                                        @endinputnumber

                                        @inputnumber(['attrs' => ['name' => "altezza[".$sublocale['locali']."][]",
                                        'placeholder' =>
                                        'Altezza Infisso', 'data-moltiplicatore' => 'true',
                                        'data-moltiplicatore-locale' => $sublocale['locali'],
                                        ]])
                                        @endinputnumber

                                        @inputnumber(['attrs' => ['name' => "supla[".$sublocale['locali']."][]",
                                        'placeholder' =>
                                        'Superficie L * H', 'required' => $loop->first, 'data-moltiplicazione' =>
                                        'true',
                                        'data-moltiplicazione-locale' => $locali, 'readonly' => 'true',
                                        ]])
                                        @endinputnumber

                                    </td>
                                    <td>
                                        @inputnumber(['attrs' => ['name' => "ante[".$sublocale['locali']."][]",
                                        'placeholder' => 'Ante'
                                        ]])
                                        @endinputnumber
                                        @inputnumber(['attrs' => ['name' => "altezzaterra[".$sublocale['locali']."][]",
                                        'placeholder' => 'Altezza da terra',
                                        ]])
                                        @endinputnumber

                                    </td>
                                </tr>
                            @endif

                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
            <br><br>
            @endblock
        @endforeach
    @endforeach
    <button class="btn btn-primary" type="submit">Continua</button>
</form>
