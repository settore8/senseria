@php $unita_immobiliare = $immobile->sopralluogo->unitaimmobiliare @endphp
{{--{{dd($unita_immobiliare)}}--}}
@if($unita_immobiliare)
    @foreach($unita_immobiliare as $key => $value)
        @if(!is_numeric($key))<a data-toggle="collapse"><h2>{{__('messages.'.$key)}}</h2></a>@endif
        <div class="collapse show">
            @if(is_array($value))
                @foreach($value as $subkey => $subvalue)
                    @if(is_array($subvalue) && $subvalue)
                        @if(!is_numeric($subkey))<h3>{{__('messages.'.$subkey)}}</h3>@endif
                        @foreach($subvalue as $sub2key => $sub2value)
                            @if(is_array($sub2value))
                                @if(!is_numeric($sub2key))<h4>{{__('messages.'.$sub2key)}}</h4>@endif
                                @foreach($sub2value as $sub3key => $sub3value)
                                    @if(is_array($sub3value))
                                        @if(!is_numeric($sub3value))<h5>{{__('messages.'.$sub3value)}}</h5>@endif
                                        @foreach($sub3value as $sub4key => $sub4value)
                                            <div>
                                                {{__('messages.'.$sub4key).': '.$sub4value}}
                                            </div>
                                        @endforeach
                                    @else
                                        @if(is_numeric($sub3key) && !is_numeric($sub2key))
                                            <div>
                                                {{__('messages.'.$sub2key).': '.$sub3value}}
                                            </div>
                                        @elseif(is_numeric($sub3key) && is_numeric($sub2key))
                                            @if($sub3key == 0)
                                                <div>
                                                    Tipologia: {{$sub3value}}
                                                </div>
                                            @elseif($sub3key == 1)
                                                <div>
                                                    Orientamento: {{$sub3value}}
                                                </div>
                                            @elseif($sub3key == 2)
                                                <div>
                                                    Posizionamento: {{$sub3value}}
                                                </div>
                                            @endif
                                        @else
                                            <div>
                                                {{__('messages.'.$sub3key).': '.$sub3value}}
                                            </div>
                                        @endif
                                    @endif
                                @endforeach
                            @else
                                <div>
                                    {{$subkey.': '.$sub2value }}
                                </div>
                            @endif
                        @endforeach
                    @elseif(!$subvalue)
                        <div>
                            {{__('messages.'.$subkey).': ' }}
                        </div>
                    @elseif(is_numeric($subkey))
                        <div>
                            {{__('messages.'.$key).': '.$subvalue }}
                        </div>
                    @else
                        <div>
                            {{__('messages.'.$subkey).': '.$subvalue }}
                        </div>
                    @endif
                @endforeach
            @else
                <div>
                    {{__('messages.'.$key).': '.$value}}
                </div>
            @endif
        </div>
    @endforeach
@else
    <div>
        L'unità immobiliare non è stata ancora inserita
    </div>
@endif

