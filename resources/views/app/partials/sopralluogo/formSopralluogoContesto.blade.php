<form action="{{ route('immobile.sopralluogo.store.contesto', $immobile) }}" method="POST" class="form-horizontal">
    @csrf
    {{--{{dd($immobile->sopralluogo)}}--}}
    @block(['class' => 'block--bordered'])

    @formgrouph(['for' => 'contesto_paesaggistico', 'name' => 'paesaggistico', 'label' => 'Contesto paesaggistico',
    'required' => true, 'responsive' => true])

    @select(['attrs' => ['name' => 'paesaggistico', 'id' => 'contesto_paesaggistico', 'value' =>
    Custom::addNull($contesto['contestoPaesaggistico']),
    'default' => old('paesaggistico') ?
    old('paesaggistico') :
    ($immobile->sopralluogo &&
    $immobile->sopralluogo->contesto ?
    $immobile->sopralluogo['contesto']['paesaggistico'] : null)
    ]])
    @endselect
    @endformgrouph

    @formgrouph(['for' => 'contesto_territoriale', 'name' => 'territoriale', 'label' => 'Contesto territoriale' ,
    'required' => true, 'responsive' => true])

    @select(['attrs' => ['name' => 'territoriale', 'id' => 'contesto_territoriale','value' =>
    Custom::addNull($contesto['contestoTerritoriale']),
    'default' => old('territoriale') ?
    old('territoriale') :
    ($immobile->sopralluogo &&
    $immobile->sopralluogo->contesto ?
    $immobile->sopralluogo['contesto']['territoriale'] : null)
    ]])
    @endselect
    @endformgrouph

    @endblock

    @block(['title' => 'Viabilità di accesso','class' => 'block--bordered'])

    @formgrouph(['for' => 'tipologia_viabilita', 'name' => 'tipologia_viabilita', 'label' => 'Tipologia', 'required' =>
    true])
    @select(['attrs' => ['name' => 'tipologia_viabilita', 'id' => 'tipologia_viabilita', 'value' =>
    Custom::addNull($contesto['viabilitaAccesso']['tipologia']),
    'default' => old('tipologia_viabilita') ?
    old('tipologia_viabilita') :
    ($immobile->sopralluogo &&
    $immobile->sopralluogo->contesto ?
    $immobile->sopralluogo['contesto']['viabilita']['tipologia'] : null)
    ]])
    @endselect
    @endformgrouph

    @formgrouph(['for' => 'finitura_viabilita', 'name' => 'finitura_viabilita', 'label' => 'Finitura', 'required' =>
    true])
    @select(['attrs' => ['name' => 'finitura_viabilita', 'id' => 'finitura_viabilita', 'value' =>
    Custom::addNull($contesto['viabilitaAccesso']['finitura']),
    'default' => old('finitura_viabilita') ?
    old('finitura_viabilita') :
    ($immobile->sopralluogo &&
    $immobile->sopralluogo->contesto ?
    $immobile->sopralluogo['contesto']['viabilita']['finitura'] : null)
    ]])
    @endselect
    @endformgrouph

    @formgrouph(['for' => 'pendenza_viabilita', 'name' => 'pendenza_viabilita', 'label' => 'Pendenza', 'required' =>
    true])
    @select(['attrs' => ['name' => 'pendenza_viabilita', 'id' => 'pendenza_viabilita', 'value' =>
    Custom::addNull($contesto['viabilitaAccesso']['pendenza']),
    'default' => old('pendenza_viabilita') ?
    old('pendenza_viabilita') :
    ($immobile->sopralluogo &&
    $immobile->sopralluogo->contesto ?
    $immobile->sopralluogo['contesto']['viabilita']['pendenza'] : null)
    ]])
    @endselect
    @endformgrouph

    @formgrouph(['for' => 'stato_conservazione', 'name' => 'stato_conservazione', 'label' => 'Stato conservazione',
    'required' => true])
    @select(['attrs' => ['name' => 'stato_conservazione', 'id' => 'stato_conservazione', 'value' =>
    Custom::addNull($contesto['viabilitaAccesso']['statoConservazione']),
    'default' => old('stato_conservazione') ?
    old('stato_conservazione') :
    ($immobile->sopralluogo &&
    $immobile->sopralluogo->contesto ?
    $immobile->sopralluogo['contesto']['viabilita']['stato_conservazione'] : null )
    ]])
    @endselect
    @endformgrouph

    @endblock
    @block(['title' => 'Trasporti','class' => 'block--bordered'])
    <div class="m-col m-col-1 m-col-2--xs m-col-3--lg">

        @foreach ($contesto['trasporti'] as $item => $cnt)
            @formgrouph(['for' => 'trasporti'.$item, 'name' => 'trasporti['.$item.']', 'label' => $cnt, 'col1' =>
            'col-8', 'col2' => 'col-4'])
            @inputnumber(['attrs' => ['name' => 'trasporti['.$item.']', 'id' => 'trasporti'.$item, 'placeholder' =>
            'Km', 'value' => $immobile->sopralluogo->contesto && array_key_exists($cnt,
            $immobile->sopralluogo->contesto['trasporti']) ? $immobile->sopralluogo->contesto['trasporti'][$cnt] : null
            ]]) @endinputnumber
            @endformgrouph
        @endforeach
    </div>
    @endblock

    @block(['title' => 'Servizi nelle vicinanze','class' => 'block--bordered'])
    <div class="m-col m-col-1 m-col-2--xs m-col-3--lg">
        @foreach ($contesto['serviziVicinanze'] as $item => $cnt)
            @formgrouph(['for' => 'servizi_vicinanze'.$item, 'name' => 'servizi_vicinanze['.$item.']', 'label' => $cnt,
            'col1' => 'col-8', 'col2' => 'col-4'])
            @toggle(['attrs' => ['name' => 'servizi_vicinanze['.$item.']', 'id' => 'servizi_vicinanze'.$item,
            'placeholder' => 'Km', 'checked' => $immobile->sopralluogo->contesto && in_array($cnt,
            $immobile->sopralluogo->contesto['servizi']) ? true : false]]) @endtoggle
            @endformgrouph
        @endforeach
    </div>
    @php /*@tablecheckbox(['thead' => ['Mezzi', ''], 'content' => $contesto['serviziVicinanze'], 'nameinput' => 'servizi_vicinanze']) @endtablecheckbox */ @endphp
    @endblock


    @button(['attrs' => ['text' => 'Salva']])@endbutton


</form>
