<form action="{{ route('immobile.sopralluogo.store.fabbricato', $immobile) }}" method="POST" class="form-horizontal">
    @csrf
    @php $editing = $immobile->sopralluogo->fabbricato; @endphp
    @block(['class' => 'block--bordered'])

    @formgrouph(['for' => 'anno_costruzione', 'name' => 'anno_costruzione', 'label' => 'Anno di costruzione',
    'responsive' => true, 'required' => true])
        
        <div class="form-group">
            <div class="columns">
                <div class="column col-4">
                    @inputtext(['attrs' => ['name' => 'anno_costruzione', 'id' => 'anno_costruzione', 'class' => 'input-width-100', 'maxlength' => 4, 'minlength' => 4,
                    'value' => old('anno_costruzione') ?
                    old('anno_costruzione') :
                    ($editing &&
                    $editing['annocostruzione'] ?
                    $editing['annocostruzione'] : null) ,
            
                    'readonly' => old('anno_costruzione') ?
                    old('anno_costruzione') :
                    ($editing &&
                    $editing['annocostruzione'] == 'Ante 42/1943-2018')
                    ]])
                    @endinputtext
                </div>
                <div class="column col-8">
                    @toggle(['attrs' => ['name' => 'checkbox_costruzione', 'id' => 'checkbox_costruzione',
                    'value' => 'Ante 42/1943-2018', 'data-target-disabled' => 'anno_costruzione', 'label' => 'Ante 42/1943-2018',
                    'checked' => old('checkbox_costruzione') ? old('checkbox_costruzione') :
                    ($editing &&
                    $editing['annocostruzione'] == 'Ante 42/1943-2018')
                    ]])
                    @endtoggle
                </div>
            </div>
        </div>

     @endformgrouph

     @formgrouph(['for' => 'anno_ristrutturazione', 'name' => 'anno_ristrutturazione', 'label' => 'Anno di ristrutturazione',
    'responsive' => true])
        
        <div class="form-group">
            <div class="columns">
                <div class="column col-4">
                    @inputtext(['attrs' => ['name' => 'anno_ristrutturazione', 'id' => 'anno_ristrutturazione', 'class' => 'input-width-100', 'maxlength' => 4, 'minlength' => 4,
                    'value' => old('anno_ristrutturazione') ? old('anno_ristrutturazione') :
                    ($editing &&
                    $editing['annoristrutturazione'] ?
                    $editing['annoristrutturazione'] : null),

                    'readonly' => (old('anno_ristrutturazione') ? old('anno_ristrutturazione') :
                    $editing &&
                    $editing['annoristrutturazione'] == 'Ante 42/1943-2018')
                    ]])
                    @endinputtext
                </div>
                <div class="column col-8">
                    @toggle(['attrs' => ['name' => 'checkbox_ristrutturazione', 'id' => 'checkbox_ristrutturazione',
                    'value' => 'Ante 42/1943-2018', 'data-target-disabled' => 'anno_ristrutturazione', 'label' => 'Ante
                    42/1943-2018',
                    'checked' => old('checkbox_costruzione') ? old('checkbox_costruzione') :
                    ($editing &&
                    $editing['annoristrutturazione'] == 'Ante 42/1943-2018')
                    ]])
                    @endtoggle
                </div>
            </div>
        </div>

     @endformgrouph

    
     @formgrouph(['for' => 'piani_fuori_terra', 'name' => 'piani_fuori_terra', 'label' => 'Piani fuori terra',
     'responsive' => true, 'required' => true])

        @inputnumber(['attrs' => ['name' => 'piani_fuori_terra',
        'value' => old('piani_fuori_terra') ? old('piani_fuori_terra') :
        ($editing &&
        $editing['pianifuoriterra'] ?
        $editing['pianifuoriterra'] : null),
        ]])
        @endinputnumber

     @endformgrouph

     @formgrouph(['for' => 'piani_entro_terra', 'name' => 'piani_entro_terra', 'label' => 'Piani entro terra',
     'responsive' => true, 'required' => true])

        @inputnumber(['attrs' => ['name' => 'piani_entro_terra',
        'value' => old('piani_entro_terra') ? old('piani_entro_terra') :
        ($editing &&
        $editing['pianientroterra'] ?
        $editing['pianientroterra'] : null) ,
        ]])
        @endinputnumber

     @endformgrouph


    @endblock

    @block(['class' => 'block--bordered'])

    @formgrouph(['for' => 'complesso_immobiliare', 'name' => 'complesso_immobiliare', 'label' => 'Parte di un complesso immobiliare'])

        @select(['attrs' => ['name' => 'complesso_immobiliare',
            'value' => config('constants.sino'),
            'onchange' => 'return showOption(this,"listaComplessiImmobiliare")',
            'id' => 'complesso_immobiliare',
            'class' => 'select-width-100',
            'default' => (($editing &&
            !is_array($editing['complessoimmobiliare']) &&
            $editing['complessoimmobiliare']) == 'No') ? 'No' : 'Si'
            ]])
        @endselect

    @endformgrouph

    

    <div id="listaComplessiImmobiliare"
         class="{{$immobile->sopralluogo->fabbricato && $immobile->sopralluogo->fabbricato['complessoimmobiliare'] == 'No' ? 'hidden' : 'visible'}}">

        @foreach ($fabbricato['complessoImmobiliare'] as $item => $cnt)
            @formgrouph(['for' => 'complessoImmobiliare'.$item, 'name' => 'complessoImmobiliare['.$item.']', 'label' =>
            $cnt, 'col1' => 'col-5 col-md-8', 'col2' => 'col-7 col-md-4'])

            @inputnumber(['attrs' => ['name' => 'complessoImmobiliare['.$item.']', 'id' => 'complessoImmobiliare'.$item,
            'placeholder' => 'N',
            'value' => old('complessoImmobiliare') ? old('complessoImmobiliare')[$item] :
            ($editing &&
            $editing['complessoimmobiliare'] != 'No' &&
            $editing['complessoimmobiliare'][$cnt] ?
            $editing['complessoimmobiliare'][$cnt] : null)
            ]])
            @endinputnumber

            @endformgrouph
        @endforeach

        @php /* @tableinput(['thead' => ['Specifica', 'Quantitá'], 'content' => $fabbricato['complessoImmobiliare'], 'nameinput' => 'complessoimmobiliare']) @endtableinput */ @endphp
    </div>

    @endblock

    @block(['title' => 'Affacci','class' => 'block--bordered'])
           
                    <div class="columns">
                        <div class="column col-5">Lato</div>
                        <div class="column col-5">Vista</div>
                        <div class="column col-2 text-center"><a href="#" data-clone="row-affacci" class="btn btn-sm">Aggiungi</a></div>
                    </div>
                    <div class="data-clone-container">
                    @if($editing && array_key_exists('affacci', $editing))
                        @foreach($editing['affacci'] as $key => $value)
                            <div data-target-clone="row-affacci"
                                data-max-clone="{{ count(config('constants.lati-affacci')) }}" class="columns align-items-center">
                                <div class="column col-5 p10">
                                    @select(['attrs' => ['name' => 'latoaffaccio[]', 'value' =>
                                    Custom::addNull(config('constants.lati-affacci')),
                                    'default' => $key ?
                                    $key : null
                                    ]])
                                    @endselect
                                </div>
                                <div class="column col-5 p10">
                                    @select(['attrs' => ['name' => 'affaccio[]', 'value' =>
                                    Custom::addNull($fabbricato['affacci']),
                                    'default' => $value ?
                                    $value : null
                                    ]])
                                    @endselect
                                </div>
                                <div class="column col-1 text-center">
                                    <button data-remove-clone="" class="btn btn-link-error">x</button>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <div data-target-clone="row-affacci"
                            data-max-clone="{{ count(config('constants.lati-affacci')) }}" class="columns align-items-center">
                            <div class="column col-5 p10">
                                @select(['attrs' => ['name' => 'latoaffaccio[]', 'value' =>
                                Custom::addNull(config('constants.lati-affacci')),
                                ]])
                                @endselect
                            </div>
                            <div class="column col-5 p10">
                                @select(['attrs' => ['name' => 'affaccio[]', 'value' =>
                                Custom::addNull($fabbricato['affacci']),
                                ]])
                                @endselect
                            </div>
                            <div class="column col-2 text-center">
                                <button data-remove-clone="" class="btn btn-link-error">x</button>
                            </div>
                        </div>
                        
                    @endif
                    </div>

    @endblock

    @button(['attrs' => ['text' => 'Continua']])@endbutton

</form>
