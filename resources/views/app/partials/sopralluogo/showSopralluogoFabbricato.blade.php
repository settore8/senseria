@php $fabbricato = $immobile->sopralluogo->fabbricato @endphp
{{--{{dd($fabbricato)}}--}}
@if($fabbricato)
    @foreach($fabbricato as $key => $value)
        @if(is_array($value))
            <h2>{{__('messages.'.$key)}}</h2>
            @foreach($value as $subkey => $subvalue)
                @if($subkey)
                    @if(is_array($subvalue) && $subvalue)
                        <h3>{{__('messages.'.$subkey)}}</h3>
                        @foreach($subvalue as $sub2key => $sub2value)
                            @if(is_array($sub2value))
                                @if(!is_numeric($sub2key))<h4>{{__('messages.'.$sub2key)}}</h4>@endif
                                @foreach($sub2value as $sub3key => $sub3value)
                                    @if(is_array($sub3value))
                                        <h5>{{__('messages.'.$sub3key)}}</h5>
                                        @foreach($sub3value as $sub4key => $sub4value)
                                            <div>
                                                {{__('messages.'.$sub4key).': '.$sub4value}}
                                            </div>
                                        @endforeach
                                    @else
                                        <div>
                                            {{__('messages.'.$sub3key).': '.$sub3value}}
                                        </div>
                                    @endif
                                @endforeach
                            @else
                                <div>
                                    {{__('messages.'.$key).': '.$subkey }}
                                </div>
                            @endif
                        @endforeach
                    @elseif(!$subvalue)
                        <div>
                            {{__('messages.'.$subkey).': ' }}
                        </div>
                    @else
                        <div>
                            {{__('messages.'.$subkey).': '.$subvalue }}
                        </div>
                    @endif
                @else
                    <div>
                        {{__('messages.'.$key).': '.$subkey }}
                    </div>
                @endif
            @endforeach
        @else
            <div>
                {{__('messages.'.$key).': '.$value}}
            </div>
        @endif

    @endforeach
@else
    <div>
        Il Fabbricato non è stato ancora inserito
    </div>
@endif
