<form action="{{ route('immobili.search') }}" class="item item--center sendOnEnter canLeavePage" method="get">
    <div class="form-group form-group-search">
        <input type="text" name="s" class="form-input input-lg input-search input-rounded" placeholder="Cerca tra i tuoi immobili">
        <svg><use xlink:href="#icon-search"></use></svg>
    </div>
</form>