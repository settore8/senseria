<ul class="inline-nav scrollmenu text-center text-left--md">
    <li><a href="{!! route('account.abbonamento') !!}" title="Riepilogo abbonamento" @if(Route::currentRouteName() == 'account.abbonamento') class="current" @endif>Riepilogo</a></li>
    <li><a href="{!! route('account.abbonamento.piani') !!}" title="Modifica abbonamento" @if(Route::currentRouteName() == 'account.abbonamento.piani') class="current" @endif>Modifica</a></li>
</ul>
