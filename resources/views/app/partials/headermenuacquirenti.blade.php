
<li class="has-submenu gestione"><a href="#"><svg><use xlink:href="#icon-edit"></use></svg> <span class="label">Gestione</span></a>
    <ul class="submenu submenu-right">
        <li>
            <a href="{!! route('acquirenti') !!}" @if(Request::is('acquirenti')) class="current" @endif>Gestione</a>
        </li>
        <li>
            <a href="{{ route('acquirenti.gestione.archiviati') }}" title="Acquirenti archiviati" @if(Request::is('acquirenti/gestione/archiviati')) class="current" @endif>Archiviati</a>
        </li>
    </ul>

</li>
