@if(Request::is('*/sopralluogo') || Request::is('*/sopralluogo/*'))
    @if($immobile->sopralluogo && $immobile->sopralluogo->data)
    <a href="#dataSopralluogoModal" class="ml1" data-toggle="modal"> {{ $immobile->sopralluogo->data->format('d/m/Y') }}<svg width="15" height="15"><use xlink:href="#icon-pen"></use></svg></a>
    @endif
@endif 

<li class="has-submenu gestione"><a href="#" @if(Route::currentRouteName() == 'immobili.gestione') class="current" @endif><span class="label">{{ $immobile->nome }}</span></a>
    <ul class="submenu submenu-right">
        <li>
            <a href="{!! route('immobile', $immobile) !!}" @if(Request::is('immobili')) class="current" @endif>Immobile</a>
        </li>
        <li class="divider"></li>
        <li>
            <a href="{!! route('immobile.sopralluogo', $immobile) !!}">Sopralluogo</a>
        </li>
        <li>
            <a href="{{ route('immobile.verificatecnica', $immobile) }}">Verifica Tecnica</a>
        </li>
    </ul>
</li>