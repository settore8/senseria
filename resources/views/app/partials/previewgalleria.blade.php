<div class="articolo__preview card">
    <a href="{{route('galleria', $galleria->slug)}}" class="img-container">
        <span class="img-counter">{{ count($galleria->immagini)}}</span>

        <img
            src="{{Storage::url('gallerie/'.$galleria->user_id.'/'.$galleria->codice.'/crop/'.$galleria->immagine_evidenza->filename)}}"
            alt="{{ $galleria->titolo }}">
    </a>
    <h4><a href="{{route('galleria', $galleria->slug)}}">{{ $galleria->titolo }}</a></h4>
    <footer>
        @include('includes.partials.userpopover', ['user' => $galleria->user, 'linkable' => true])

        @include('includes.partials.counterviews', ['data' => $galleria])

        @include('includes.partials.counterraccolte', ['data' => $galleria, 'class' => get_class(new App\Galleria)])

        @include('includes.partials.counterlikes', ['data' => $galleria, 'class' => get_class(new App\Galleria)])
    </footer>
</div>
