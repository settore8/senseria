<li class="has-submenu"><a href="#" @if(Request::is('articoli') || Request::is('articoli/*')) class="current" @endif><svg><use xlink:href="#icon-articoli"></use></svg> <span class="label">Articoli</span></a>
    <ul class="submenu">
        <li><a href="{{ route('articoli') }}" title="Articoli recenti" @if(Request::is('articoli')) class="current" @endif>Recenti</a></li>
        @if(Auth::check())
        <li><a href="{{ route('articoli.suggeriti') }}" title="Articoli suggeriti" @if(Request::is('articoli/suggeriti')) class="current" @endif>Suggeriti</a></li>
        @endif
        <li><a href="{{ route('articoli.popolari') }}" title="Articoli popolari" @if(Request::is('articoli/popolari')) class="current" @endif>Popolari</a></li>
    </ul>
</li>

@if(Auth::check())
<li class="has-submenu gestione"><a href="#"><svg><use xlink:href="#icon-edit"></use></svg> <span class="label">Gestione</span></a>
    <ul class="submenu submenu-right">
        <li><a href="{!! route('articoli.gestione') !!}" title="I miei articoli" @if(Request::is('articoli/gestione')) class="current" @endif>I miei articoli</a></li>
        <li><a href="{{ route('articoli.gestione.tag') }}" title="Articoli dove sono taggato" @if(Request::is('articoli/gestione/tag')) class="current" @endif>Dove sono taggato</a></li>
    </ul>
</li>
@endif
