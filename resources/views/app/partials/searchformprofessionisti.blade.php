<form id="searchform" action="{{ route('professionisti.search') }}" class="item item--center sendOnEnter canLeavePage searchFormLarge" method="get">
    <div class="form-group form-group-search">
        <input type="text" class="form-input input-lg input-search input-rounded" placeholder="Cerca professinista" name="s" value="@isset($keyword) {{ $keyword }} @endisset">
        <svg><use xlink:href="#icon-search"></use></svg>
        <div class="toggle-searchadvanced"><svg><use xlink:href="#icon-filtro"></use></svg></div>
        <div id="searchadvanced">

            <div class="form-group">
                <label class="form-label">Operatività</label>
                <input type="text" class="form-input input-lg">
            </div>

            <div class="form-group">
                <label class="form-label">Competenza</label>
                <select type="text" class="form-select select-lg">
                    <option>ddddd</option>
                </select>
            </div>

            <div class="form-group">
                <label class="form-label">Specifica</label>
                <select type="text" class="form-select select-lg">
                    <option>ddddd</option>
                </select>
            </div>

            <div class="form-group">
                <label class="form-label">Abilitazioni</label>
                <select type="text" class="form-select select-lg">
                    <option>ddddd</option>
                </select>
            </div>

            <div class="form-group">
                <label class="form-switch">
                    <input type="checkbox"><i class="form-icon"></i> Solo verificati <svg class="icon icon--15 icon--tertiary" aria-hidden="true"><use xlink:href="#icon-certificazione-fill"></use></svg>
                </label>
            </div>

            @button(['attrs' => ['text' => 'Cerca']])@endbutton
        </div>
    </div>
    </form>
<div id="searchoverlay"></div>