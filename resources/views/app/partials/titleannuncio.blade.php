@slot('back'){{ route('annunci') }}@endslot
<a href="{{ route('annuncio', $annuncio->codice) }}" title="Annuncio {{ $annuncio->codice }}">Annuncio {{ $annuncio->codice }}</a>
@isset($childs) @foreach($childs as $child) <span class="weight-300">/ {{ $child }}</span> @endforeach @endif