<ul class="inline-nav scrollmenu text-center text-left--md">
    <li><a href="{!! route('immobile.sopralluogo.fabbricato', $immobile) !!}" title="Fabbricato" @if(Request::is('*/sopralluogo/fabbricato')) class="current" @endif>Fabbricato</a></li>
    <li><a href="{!! route('immobile.sopralluogo.fabbricato.struttura', $immobile) !!}" title="Struttura" @if(Route::currentRouteName() == 'immobile.sopralluogo.fabbricato.struttura') class="current" @endif>Struttura</a></li>
</ul>
