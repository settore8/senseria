@php /* @slot('back'){{ route('immobili') }} @endslot  */ @endphp
<a href="{{ route('immobile', $immobile) }}" title="Riepilogo {{ $immobile->nome }}" class="immobile-title">{{ $immobile->nome }}</a>
@isset($childs) @foreach($childs as $child) <span class="weight-300">/ {{ $child }}</span> @endforeach @endif