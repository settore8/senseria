<div class="grid">
    <div class="radio-img">
    <input type="radio" name="destinazione" id="destinazioneResidenziale" value="1" data-id-destinazione="1" {{ old('destinazione') == 1 ? 'checked' : '' }}>
    <label for="destinazioneResidenziale">
        <svg><use xlink:href="#destinazione-residenziale"></use></svg>
        <h4>Residenziale</h4>
    </label>
    </div>

    <div class="radio-img">
        <input type="radio" name="destinazione" id="destinazioneProduttivo" value="2" data-id-destinazione="2" {{ old('destinazione') == 2 ? 'checked' : '' }}>
        <label for="destinazioneProduttivo">
            <svg><use xlink:href="#destinazione-produttivo"></use></svg>
            <h4>Produttivo</h4>
        </label>
    </div>

    <div class="radio-img">
        <input type="radio" name="destinazione" id="destinazioneDirezionale" value="3" data-id-destinazione="3" {{ old('destinazione') == 3 ? 'checked' : '' }}>
        <label for="destinazioneDirezionale">
            <svg><use xlink:href="#destinazione-direzionale"></use></svg>
            <h4>Direzionale</h4>
        </label>
    </div>

    <div class="radio-img">
        <input type="radio" name="destinazione" id="destinazioneTuristicoRicettiva" value="4" data-id-destinazione="4" {{ old('destinazione') == 4 ? 'checked' : '' }}>
        <label for="destinazioneTuristicoRicettiva">
        <svg><use xlink:href="#destinazione-turisticoricettivo"></use></svg>
        <h4>Turistico ricettivo</h4>
        </label>
    </div>

    <div class="radio-img">
        <input type="radio" name="destinazione" id="destinazioneCultura" value="5" data-id-destinazione="5" {{ old('destinazione') == 5 ? 'checked' : '' }}>
        <label for="destinazioneCultura">
            <svg><use xlink:href="#destinazione-culturatempolibero"></use></svg>
        <h4>Cultura e tempo libero</h4>
        </label>
    </div>

    <div class="radio-img">
        <input type="radio" name="destinazione" id="destinazioneAgricola" value="6" data-id-destinazione="6" {{ old('destinazione') == 6 ? 'checked' : '' }}>
        <label for="destinazioneAgricola">
            <svg><use xlink:href="#destinazione-agricola"></use></svg>
        <h4>Agricola</h4>
        </label>
    </div>

    <div class="radio-img">
        <input type="radio" name="destinazione" id="destinazioneSanita" value="7" data-id-destinazione="7" {{ old('destinazione') == 7 ? 'checked' : '' }}>
        <label for="destinazioneSanita">
            <svg><use xlink:href="#destinazione-sanitaistruzione"></use></svg>
        <h4>Sanità e istruzione</h4>
        </label>
    </div>

    <div class="radio-img">
        <input type="radio" name="destinazione" id="destinazioneParcheggi" value="8" data-id-destinazione="8" {{ old('destinazione') == 8 ? 'checked' : '' }}>
        <label for="destinazioneParcheggi">
            <svg><use xlink:href="#destinazione-parcheggiservizimobilita"></use></svg>
        <h4>Parcheggi e servizi mobilità</h4>
        </label>
    </div>

</div>
