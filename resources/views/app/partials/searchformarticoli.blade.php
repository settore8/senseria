<form action="{{ route('articoli.search') }}" id="searchform" class="item item--center sendOnEnter canLeavePage" method="get">
    <div class="form-group form-group-search">
        <input type="text" name="s" class="form-input input-lg input-search input-rounded" placeholder="Cerca articolo">
        <svg><use xlink:href="#icon-search"></use></svg>
        <div class="toggle-searchadvanced"><svg><use xlink:href="#icon-filtro"></use></svg></div>
        <div id="searchadvanced">
            <div class="form-group">
                <label class="form-label">Categoria</label>
                <!-- #TO_REVIEW# stampare qui tutte le categorie filtrabili con select2-->
                <select type="text" class="form-select select-lg" name="categoria">
                    <option value="dddd">ddddd</option>
                    <option value="weeee">ddddd</option>
                </select>
            </div>
            @button(['attrs' => ['text' => 'Cerca']])@endbutton
        </div>
    </div>
    </form>
<div id="searchoverlay"></div>