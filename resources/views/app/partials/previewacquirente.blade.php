<a class="card card--acquirente" href="{{ route('acquirente.edit', $acquirente) }}">

    <div class="card--header">
        {{ $acquirente->specifica->nome_pubblico }}  @if($acquirente->informazione) <span class="acquirente-mq"> {{ $acquirente->informazione->metri_quadri }}<span class="mq">m<sup>2</sup></span> </span> @endif
        @if($acquirente->annuncio)
                <span class="badge badge--success badge--xs mlauto">In pubblicità</span>
        @endif
    </div>

    <div class="columns">

        <div class="column col-9 text-left">
            <div class="card--title">
                <strong>{{ $acquirente->nome }}</strong>

                {{ $acquirente->dettagli['camere'] }}
                {{ $acquirente->dettagli['bagni'] }}
                
            </div>
        </div>

        <div class="column col-3 text-right">
            <svg class="destinazione"><use xlink:href="#destinazione-{{ strtolower($acquirente->destinazione->nome) }}"></use></svg>
        </div>

    </div>

    <div class="card--footer">

    </div>

</a>
