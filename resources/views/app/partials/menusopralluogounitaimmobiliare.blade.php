<ul class="inline-nav scrollmenu text-center text-left--md">
    <li><a href="{{ route('immobile.sopralluogo.unitaimmobiliare', $immobile) }}" @if(Route::currentRouteName() == 'immobile.sopralluogo.unitaimmobiliare') class="current" @endif>Unità immobiliare</a></li>
    <li><a href="{{ route('immobile.sopralluogo.unitaimmobiliare.infissi', $immobile) }}" @if(Route::currentRouteName() == 'immobile.sopralluogo.unitaimmobiliare.infissi') class="current" @endif>Infissi</a></li>
    <li><a href="{{ route('immobile.sopralluogo.unitaimmobiliare.impianti', $immobile) }}" @if(Route::currentRouteName() == 'immobile.sopralluogo.unitaimmobiliare.impianti') class="current" @endif>Impianti</a></li>
</ul>
