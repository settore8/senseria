<ul class="inline-nav scrollmenu text-center text-left--md">
    <li><a href="{!! route('immobile.rilievo', $immobile) !!}" title="Riepilogo abbonamento" @if(Route::currentRouteName() == 'immobile.rilievo') class="current" @endif>Rilievo 3d</a></li>
    <li><a href="{!! route('immobile.rilievo.preventivo', $immobile) !!}" title="Modifica abbonamento" @if(Route::currentRouteName() == 'immobile.rilievo.preventivo') class="current" @endif>Preventivo</a></li>
    <li><a href="{!! route('immobile.rilievo.messaggi', $immobile) !!}" title="Modifica abbonamento" @if(Route::currentRouteName() == 'immobile.rilievo.messaggi') class="current" @endif>Messaggi</a></li>
    <li><a href="{!! route('immobile.rilievo.cloud', $immobile) !!}" title="Modifica abbonamento" @if(Route::currentRouteName() == 'immobile.rilievo.cloud') class="current" @endif>Cloud</a></li>
</ul>