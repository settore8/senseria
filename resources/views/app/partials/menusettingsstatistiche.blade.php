<ul class="inline-nav scrollmenu text-center text-left--md">
    <li><a href="{!! route('account.statistiche') !!}" title="Statistiche generali" @if(Route::currentRouteName() == 'account.statistiche') class="current" @endif>Generali</a></li>
    <li><a href="{!! route('account.statistiche.profilo') !!}" title="Statistiche profilo" @if(Route::currentRouteName() == 'account.statistiche.profilo') class="current" @endif>Profilo</a></li>
    <li><a href="{!! route('account.statistiche.articoli') !!}" title="Statistiche articoli" @if(Route::currentRouteName() == 'account.statistiche.articoli') class="current" @endif>Articoli</a></li>
    <li><a href="{!! route('account.statistiche.gallerie') !!}" title="Statistiche gallerie" @if(Route::currentRouteName() == 'account.statistiche.gallerie') class="current" @endif>Gallerie</a></li>
</ul>