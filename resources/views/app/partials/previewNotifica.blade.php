<div class="notifica {{ $notifica->readed ? 'readed' : '' }}">
    <div class="notifica-action">
        @if(!$notifica->readed)
            <button class="unreaded" data-action="signNotificationAsReaded" data-id="{{ encrypt($notifica->id) }}"></button>
        @endif
   </div>
    <a href="{{ url($notifica->route)}}">
        <div class="notifica-header">
            <div class="date">
            {{ Custom::createDate($notifica->created_at) }}
            </div>
            <div class="action">
                <button class="delete {{  $notifica->readed ? 'show' : '' }}" data-action="deleteNotification" data-id="{{ encrypt($notifica->id) }}">elimina</button>
            </div>
        </div>
        <div class="notifica-body">
        {!! $notifica->messaggio !!}
        </div>
    </a>
</div>