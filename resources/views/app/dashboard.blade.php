@extends('app.layouts.master')

@section('title'){{ "Bacheca" }}@endsection
@section('bodyclass')dashboard @endsection
@section('header')
@endsection

@section('content')

    @if(Auth::user())
        <!-- se l'utente non ha certificazione attiva, foto del profilo, descrizione, indirizzo attività -->
        <div class="tip tip--profilo radius p10 flex">
            <div>
                <h5 class="tip__title">Completa il tuo profilo!</h5>
                <p class="tip__text">
                    Gli utenti con una foto profilo appaiono come più professionali.<br/>
                    Cosa aspetti? <a href="{{ route('account.profilo') }}">aggiungi una foto profilo adesso!</a>
                </p>
            </div>
            <nav class="flex">
                @if(auth::user()->certificazione)
                    @if(Auth::user()->certificazione->status == 'attesa')
                        <a href="{{ route('account.certificazione') }}">
                            <img src="{{ asset('images/app/visual-certificazione-lavorazione.svg')}}"
                                 alt="Foto profilo">
                            <div>Certificazione</div>
                        </a>
                    @endif
                @endif
                <a href="{{ route('account.profilo.immagine')}}">
                    <img src="{{ asset('images/app/visual-avatar-professionista.svg')}}" alt="Foto profilo">
                    <div>Foto profilo</div>
                </a>

                <a href="{{ route('account.profilo')}}">
                    <img src="{{ asset('images/app/visual-descrizione.svg')}}" alt="Foto profilo">
                    <div>Descrizione</div>
                </a>
            </nav>
        </div>
    @endif

    <div class="flex">
        <div class="item flex">
            <div class="dashboard-block">
                <div class="dashboard-block__header">
                    Workook
                </div>
                <div class="dashboard-block__body">
                    <div class="chart-container">
                    <canvas id="chart_utenti" data-professionisti="{{ count($professionisti) }}, 670, 200" data-imprese="{{ count($imprese) }}, 500, 330"></canvas>
                    </div>
                </div>
            </div>
        </div>

        <div class="item flex">
            <div class="dashboard-block">
                <div class="dashboard-block__header">
                    Annunci
                </div>
                <div class="dashboard-block__body">
                    <div class="chart-container">
                    <canvas id="chart_annunci"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="flex">
        <div class="item flex">
            <div class="dashboard-block">
                <div class="dashboard-block__header">
                    Immobili <a href="{{ url(route('immobile.create')) }}" class="btn-add btn-add-sm">+</a>
                    <nav class="dashboard-block__nav">
                        <div class="dropdown">
                            <a href="" class="dropdown-toggle">•••</a>
                            <ul class="dropdown-menu">
                                <li><a href="{{ route('immobili.gestione') }}">Creati da me</a></li>
                                <li><a href="{{ route('immobili.gestione.assegnati') }}">Assegnati a me</a></li>
                            </ul>
                        </div>
                    </nav>
                </div>
                <div class="dashboard-block__body">

                    @if($immobili)
                        <ul class="list hover">
                            @foreach($immobili as $immobile)
                                <li>
                                    <a href="{{ route('immobile', $immobile) }}">
                                        <svg><use xlink:href="#destinazione-{{ strtolower($immobile->destinazione->nome) }}"></use></svg>
                                        <strong>{{ $immobile->nome }}</strong>
                                        <div>{{$immobile->specifica->nome_pubblico}}</div>
                                        <small class="mlauto hidden--sm">@if($immobile->informazione && $immobile->informazione->metri_quadri) {{ $immobile->informazione->metri_quadri }}Mq </small>@endif
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    @else
                        <div class="block_placeholder text-center">
                            <img src="{{  asset('images/app/visual-crea-nuovo-immobile.svg')}}"
                                 alt="Crea nuovo immobile">
                            <div>
                                <a href="{{ route('immobile.create')}}"
                                   class="btn btn-sm btn-secondary p-centered">Crea il tuo primo immobile!</a>
                            </div>

                        </div>

                    @endif
                </div>

            </div>
        </div>

        <div class="item flex">
            <div class="dashboard-block has-tabs">
                <div class="dashboard-block__header">{{ $user->fullname }}
                    <nav class="dashboard-block__nav">
                        <a href="#" class="current tooltip" data-tooltip="link 1" data-tab="#tab1">Mese</a>
                        <a href="#" class="tooltip" data-tooltip="Link 2" data-tab="#tab2">Settimana</a>
                        <a href="#" class="tooltip" data-tooltip="Link 3" data-tab="#tab3">Oggi</a>
                    </nav>
                </div>
                <div class="dashboard-block__body">
                    <div class="tabs">

                        <div class="tab show" id="tab1">
                            <a href="" class="c100 perc37">
                                <span>37%</span>
                                <div class="slice">
                                    <div class="bar"></div>
                                    <div class="fill"></div>
                                </div>
                            </a>
                        </div>

                        <div class="tab" id="tab2">
                            tab 2
                        </div>

                        <div class="tab" id="tab3">
                            tab 3
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    @if(Auth::user()->tipo != 'agente' )
    <div class="flex">
        <div class="item">
            <div class="dashboard-block has-tabs">
                <div class="dashboard-block__header">
                    Articoli
                    <nav class="dashboard-block__nav">
                        <a href="#" class="current tooltip" data-tooltip="Articoli suggeriti" data-tab="#articoli_recenti">Recenti</a>
                        <a href="#" class="tooltip" data-tooltip="Articoli Recenti" data-tab="#articoli_popolari">Popolari</a>
                        <a href="#" class="tooltip" data-tooltip="Articoli scritti da me" data-tab="#articoli_miei">Scritti da me</a>
                    </nav>
                </div>
                <div class="dashboard-block__body">

                    <div class="tabs">

                        <div class="tab show" id="articoli_recenti">
                            @if($articoliRecenti->isNotEmpty())
                                <ul class="list hover">
                                    @foreach($articoliRecenti->take(5) as $articolo)
                                    <li>
                                        <a href="{{ route('articolo', ['slug' => $articolo->slug, 'categoria' => $articolo->categoria->slug]) }}" title="">
                                            <strong>{{ $articolo->titolo }}</strong>
                                            <small class="mlauto hidden--sm">{{ $articolo->user->fullname }}</small>
                                        </a>
                                    </li>
                                    @endforeach
                                </ul>
                            @endif
                        </div>

                        <div class="tab" id="articoli_popolari">
                            @if($articoliPopolari->isNotEmpty())
                                <ul class="list hover">
                                    @foreach($articoliPopolari->take(5) as $articolo)
                                    <li>
                                        <a href="{{ route('articolo', ['slug' => $articolo->slug, 'categoria' => $articolo->categoria->slug]) }}">
                                            <strong>{{ $articolo->titolo }}</strong>
                                            <small class="mlauto hidden--sm">{{ $articolo->user->fullname }}</small>
                                        </a>
                                    </li>
                                    @endforeach
                                </ul>
                            @endif
                        </div>

                        <div class="tab" id="articoli_miei">
                            
                            @if($user->articoli)
                                <ul class="list hover">
                                    @foreach($user->articoli as $articolo)
                                    <li class="flex">
                                            @if($articolo->pubblicato == 1)
                                                <a href="{{ route('articolo',  ['slug' => $articolo->slug, 'categoria' => $articolo->categoria->slug]) }}">
                                                    <strong>@if($articolo->titolo){{ $articolo->titolo }}@else Articolo {{ $articolo->id }}@endif</strong>
                                                </a>
                                                <span class="badge badge--success ml3">Pubblicato</span>
                                                <a href="{{ route('account.statistiche.articolo', $articolo->id)}}"><svg width="15" height="15"><use xlink:href="#icon-statistiche"></use></svg></a>
                                            @else
                                                <a href="{{ route('articolo.edit', $articolo) }}">
                                                <strong>@if($articolo->titolo){{ $articolo->titolo }}@else Articolo {{ $articolo->id }}@endif</strong>
                                                </a>
                                                <span class="badge ml3">Bozza</span>
                                                <a href="{{ route('account.statistiche.articolo', $articolo->id)}}"><svg width="15" height="15"><use xlink:href="#icon-statistiche"></use></svg></a>
                                            @endif

                                            <a href="{{ route('articolo.edit', $articolo) }}" title="">
                                                <svg width="15" height="15"><use xlink:href="#icon-pen"></use></svg>
                                            </a>
                                    </li>
                                    @endforeach
                                </ul>
                            @endif
                        </div>

                    </div>
                    
                </div>
            </div>
        </div>


        <div class="item">
            <div class="dashboard-block has-tabs">
                <div class="dashboard-block__header">
                    Gallerie
                    <nav class="dashboard-block__nav">
                        <a href="#" class="current tooltip" data-tooltip="Gallerie recenti" data-tab="#gallerie_recenti">Recenti</a>
                        <a href="#" class="tooltip" data-tooltip="Gallerie popolari" data-tab="#gallerie_popolari">Popolari</a>
                        <a href="#" class="tooltip" data-tooltip="Gallerie create da me" data-tab="#gallerie_mie">Create da me</a>
                    </nav>
                </div>

                <div class="dashboard-block__body">

                    <div class="tabs">

                        <div class="tab show" id="gallerie_recenti">
                            @if($gallerieRecenti->isNotEmpty())
                                <ul class="list hover">
                                    @foreach($gallerieRecenti->take(5) as $galleria)
                                    <li>
                                        <a href="{{ route('galleria', ['slug' => $galleria->slug]) }}" title="">
                                            <strong>{{ $galleria->titolo }}</strong>
                                            <small class="mlauto hidden--sm">{{ $galleria->user->fullname }}</small>
                                        </a>
                                    </li>
                                    @endforeach
                                </ul>
                            @endif
                        </div>

                        <div class="tab" id="gallerie_popolari">
                            @if($galleriePopolari->isNotEmpty())
                                <ul class="list hover">
                                    @foreach($galleriePopolari->take(5) as $galleria)
                                    <li>
                                        <a href="{{ route('galleria', ['slug' => $galleria->slug]) }}" title="">
                                            <strong>{{ $galleria->titolo }}</strong>
                                            <small class="mlauto hidden--sm">{{ $galleria->user->fullname }}</small>
                                        </a>
                                    </li>
                                    @endforeach
                                </ul>
                            @endif
                        </div>

                        <div class="tab" id="gallerie_mie">
                            
                            @if($user->gallerie)
                                <ul class="list hover">
                                    @foreach($user->gallerie as $galleria)
                                    <li>
                                        <a href="{{ route('articolo.edit', $galleria) }}" title="">
                                            <strong>{{$galleria->titolo}}</strong>
                                            <svg width="15" height="15"><use xlink:href="#icon-pen"></use></svg>
                                        </a>
                                    </li>
                                    @endforeach
                                </ul>
                            @endif
                        </div>

                    </div>
                    
                </div>

            </div>
        </div>

    </div>

    <div class="flex">
        <div class="item">
            <div class="dashboard-block">
                <div class="dashboard-block__header">
                    @php $countUser = count($user->raccolteuser) @endphp
                    <a href="{{ route('account.raccolte', 'utenti') }}">Professionisti e imprese preferite</a> @if($countUser > 0) <span class="counter">{{ Custom::formatCounter($countUser) }}</span> @endif
                </div>
                <div class="dashboard-block__body">
                    @if($user->raccolteuser->isNotEmpty())
                    <ul class="list">
                        @foreach($user->raccolteuser->take(5) as $raccolta)
                        <li>
                            <a href="{{ route('profilo', $raccolta->coverable->slug) }}" class="card card--raccolta">
                                {{ $raccolta->coverable->fullname }}
                                <button type="button" class="tooltip hideRaccolta mlauto" data-tooltip="Rimuovi" data-raccolta="{{ encrypt( get_class(new App\User) ) }}" data-id="{{ encrypt($raccolta->coverable->id) }}" data-author="{{ encrypt($raccolta->coverable->id) }}">
                                    rimuovi
                                </button>
                            </a>
                        </li>
                        @endforeach
                    </ul>
                    @endif
                </div>
            </div>
        </div>
        @if($user->tipo != 'agente')
        <div class="item">
            <div class="dashboard-block">
                <div class="dashboard-block__header">
                    @php $countCont = count($user->raccoltecontenuti) @endphp
                    <a href="{{ route('account.raccolte', 'contenuti') }}">Articoli e gallerie preferite</a> @if($countCont > 0) <span class="counter">{{ Custom::formatCounter($countCont) }}</span> @endif
                </div>
                <div class="dashboard-block__body">
                    @if(Auth::user()->raccoltecontenuti->isNotEmpty())
                    <ul class="list">
                        @foreach(Auth::user()->raccoltecontenuti->take(5) as $raccolta)
                        <li>
                            @if($raccolta->coverable_type == get_class(new App\Articolo))
                            <a href="{{ route('articolo', ['categoria' => $raccolta->coverable->categoria->slug, 'slug' => $raccolta->coverable->slug])}}">{{ $raccolta->coverable->titolo }}</a>
                            @elseif($raccolta->coverable_type == get_class(new App\Galleria))
                            <a href="{{ route('galleria', ['galleria' => $raccolta->coverable->slug])}}">{{ $raccolta->coverable->titolo }}</a>
                            @endif
                        </li>
                        @endforeach
                    </ul>
                    @endif
                </div>
            </div>
        </div>
        @endif

        <div class="item">
            <div class="dashboard-block">
                <div class="dashboard-block__header">
                    @php $countAnn = count($user->raccolteannunci) @endphp
                    <a href="{{ route('account.raccolte', 'annunci') }}">Raccolta annunci</a> @if($countAnn > 0) <span class="counter">{{ Custom::formatCounter($countAnn) }}</span> @endif
                </div>
                <div class="dashboard-block__body">
                    @if($user->raccolteannunci->isNotEmpty())
                    <ul class="list">
                        @foreach($user->raccolteannunci->take(5) as $raccolta)
                        <li>
                            <a href="{{ route('annuncio', $raccolta->coverable->codice) }}">
                            {{ config('constants.oggetto-annunci')[$raccolta->coverable->type] }}
                            @if($raccolta->coverable->type == 'immobili')
                                {{ $raccolta->coverable->immobile->specifica->nome_pubblico }}
                            @elseif($raccolta->coverable->type == 'acquirenti')
                                {{ $raccolta->coverable->acquirente->specifica->nome_pubblico }}
                            @endif
                            </a>
                        </li>
                        @endforeach
                    </ul>
                    @endif
                </div>
            </div>
        </div>
    </div>

    @endif

@endsection

@section('footerjs_after')
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.3/dist/Chart.min.js"></script>

<script>
    var chart = document.getElementById('chart_utenti');
    var data_professionisti =  chart.getAttribute('data-professionisti');
    var data_imprese =  chart.getAttribute('data-imprese');
    var barChartData = {
        labels: ['Utenti', 'Immobili', 'Articoli'],
        datasets: [{
            label: 'Professionisti',
            backgroundColor: '#2f4cd7',
            data: data_professionisti.split(",")
        }, {
            label: 'Imprese',
            backgroundColor: '#ffed00',
            data: data_imprese.split(",")
        }]

    };
    window.onload = function() {
        var ctx = document.getElementById('chart_utenti').getContext('2d');
        
        window.myBar = new Chart(ctx, {
            type: 'bar',
            data: barChartData,
            options: {
                layout: {
                    padding: {
                        left: 0,
                        right: 0,
                        top: 16,
                        bottom: 0
                    }
                },
                legend: false,
                title: false,
                tooltips: {
                    backgroundColor: '#FFFFFF',
                    titleFontColor: '#444444',
                    bodyFontColor: '#444444',
                    mode: 'nearest',
                    intersect: true
                },
                responsive: true,
                maintainAspectRatio: true,
                scales: {
                    xAxes: [{
                        stacked: true,
                        gridLines: {
                            display:false,
                        },
                    }],
                    yAxes: [{
                        stacked: true,
                        gridLines: {
                            display:false
                        },
                        ticks: {
                            display: false
                        },
                    }]
                }
            }
        });
    };


    var ctx = document.getElementById('chart_annunci');
   // ctx.height = 150;
    var myChart = new Chart(ctx, {
        type: 'pie',
        data: {
            labels: ['Immobili', 'Acquirenti', 'Responsabile tecnico', 'Collaborazioni',],
            datasets: [{
                label: '# of Votes',
                data: [1, 1, 1, 1],
                backgroundColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)'
                ],
                borderWidth: 1
            }]
        },
        responsive: true,
        maintainAspectRatio: true,
        options: {
            layout: {
                padding: {
                    left: 0,
                    right: 0,
                    top: 16,
                    bottom: 0
                }
            },
            legend: {
                display: true,
                position: 'right',
                labels: {
                    fontColor: 'rgba(0, 0, 0, 0.5)'
                },
            },
            tooltips: {
                position: 'average',
                titleFontColor: 'rgba(0,0,0, 1)',
				bodyFontColor: 'rgba(0,0,0, 1)',
                backgroundColor: 'rgba(255, 255, 255, 0.9)',
            },
        }
    });

    /*
    var resizeId;
        $(window).resize(function() {
            clearTimeout(resizeId);
            resizeId = setTimeout(afterResizing, 100);
        });

        function afterResizing(){
            var canvasheight=document.getElementById("pie").height;
            if(canvasheight <=250)
            {
                window.chart1.options.legend.display=false;
            }
            else
            {
                window.chart1.options.legend.display=true;
            }
            window.chart1.update();
   }
   */



</script>

@endsection