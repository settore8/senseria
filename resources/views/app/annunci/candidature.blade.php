@extends('app.layouts.master')

@section('title')Candidature @parent @endsection

@section('headertitle')
    @component('app.components.headertitle')
        <a href="{{ route('annunci')}}">Le mie candidature</a>
    @endcomponent
@endsection

@section('headersearch')
    @include('app.partials.searchformannunci')
@endsection

@section('headermenu')
    @component('app.components.headermenu')
    @include('app.partials.headermenuannunci')
    @endcomponent
@endsection


@section('content')


    <div class="empty text-center col-8 col-mx-auto myauto">
            <div class="columns align-items-center">
                <div class="column col-6 col-sm-12">
                    <img src="{{ asset('images/app/visual-crea-nuovo-immobile.svg') }}" alt="Crea immobile">
                </div>
                <div class="column col-6 col-sm-12">
                    <p class="empty-title h3">Non ti sei ancora candidato a nessun annuncio</p>
                    <p class="empty-subtitle">Prova a cambiare i parametri di ricerca</p>
                    <div class="empty-action">
                </div>
            </div>
            </div>
        </div>

@endsection