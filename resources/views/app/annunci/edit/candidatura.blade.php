@extends(Auth::check() ? 'app.layouts.master' : 'layouts.site_page')

@section('title') {{ $categoria->nome_pubblico ?? 'Candidatura' }} @parent @endsection

@section('meta_description', 'Meta description')
@section('meta_keywords', 'Meta keywords')

@section('og_url', 'OG Url')
@section('og_type', 'OG Type')
@section('og_title', 'OG Title')
@section('og_description', 'OG Description')
@section('og_image', 'OG Image')

@section('bodyclass'){{ "articoli" }}@endsection

@section('breadcrumb')
<ul class="breadcrumb">
    <li class="breadcrumb-item"><a href="">Workook</a></li>
    <li class="breadcrumb-item"><a href="{{ route('annunci') }}">Annunci</a></li>
</ul>
@endsection

@section('headertitle')
    @component('app.components.headertitle')
        Annunci
        {{-- <a href="{{ url(route('articoli.nuovo')) }}" class="btn btn-sm btn-outline-primary">Scrivi articolo</a> --}}
    @endcomponent
@endsection

@section('headermenu')
    @component('app.components.headermenu')
    @if(Auth::check())
    <li>
        <a href="">I miei articoli</a>
    </li>
    <li>
        <a href="">Preferiti</a>
    </li>
    <li>
        <a href="">Suggeriti</a>
    </li>
    @endif
    <li>
        <a href="" @if(Route::currentRouteName() == 'articoli') class="current" @endif>Recenti</a>
    </li>
    <li>
        <a href="">Popolari</a>
    </li>
    <li>
        <div class="input-group">
            <input type="text" class="form-input" placeholder="Cerca">
        </div>
    </li>
    @endcomponent
@endsection


@section('content')
<form action="{!! route('action.update.aggiornacandidatura', $annuncio) !!}" method="POST">
    @csrf
    @foreach ($annuncio->dettagli as $dettaglio)
    <h5>{{ $dettaglio->coverable->nome_pubblico }}</h5>
    @php $dettaglio_candidatura = $candidatura->where('dettaglio_id', $dettaglio->id); @endphp
        <div class="form-group">
            <label for="prezzo" class="form-label label-sm">Prezzo</label>
            <input type="text" class="form-input input-lg" value="{{ $dettaglio_candidatura->isNotEmpty() ? $dettaglio_candidatura->first()->quotazione : '' }}" name="prezzo[{{ $dettaglio->id }}]" required>
        </div>

        <div class="form-group">
            <label for="note" class="form-label label-sm">Note</label>
            <textarea name="note[{{ $dettaglio->id }}]" id="" class="form-input" cols="30" rows="10">{{ $dettaglio_candidatura->isNotEmpty() ? $dettaglio_candidatura->first()->note ? $dettaglio_candidatura->first()->note->testo : '' : '' }}</textarea>
        </div>
        <hr>
    @endforeach
    
    <div class="form-group form-group-submit">
        <button type="submit" class="btn btn-primary btn-padding">Aggiorna</button>
    </div>
</form>

@endsection