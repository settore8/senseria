@extends('app.layouts.master')

@section('title')Annunci @parent @endsection

@section('headertitle')
    @component('app.components.headertitle')
        <a href="{{ route('annunci')}}">Annunci</a>
    @endcomponent
@endsection

@section('headersearch')
    @include('app.partials.searchformannunci')
@endsection

@section('headermenu')
    @component('app.components.headermenu')
    @include('app.partials.headermenuannunci')
    @endcomponent
@endsection


@section('content')

    @if($annunci->isNotEmpty())

    @block()
    <div class="columns">
        @foreach ($annunci as $key => $annuncio)
            @include('app.partials.previewannuncio', ['annuncio' => $annuncio->coverable])
        @endforeach
    </div>
    @endblock

    {{ $annunci->links() }}

    @else

    <div class="empty text-center col-8 col-mx-auto myauto">
            <div class="columns align-items-center">
                <div class="column col-6 col-sm-12">
                    <img src="{{ asset('images/app/visual-crea-nuovo-immobile.svg') }}" alt="Crea immobile">
                </div>
                <div class="column col-6 col-sm-12">
                    <p class="empty-title h3">Nessun annuncio <strong>{{ config('constants.oggetto-annunci')[$request->tipo] }}</strong> trovato</p>
                    <p class="empty-subtitle">Prova a cambiare i parametri di ricerca</p>
                    <div class="empty-action">
                </div>
            </div>
            </div>
        </div>
    @endif

@endsection