@extends('app.layouts.master')

@section('title')Annuncio {{ $annuncio->codice }} @parent @endsection

@section('headertitle')
    @component('app.components.headertitle')
         @include('app.partials.titleannuncio')
    @endcomponent
@endsection


@section('headermenu')
    @component('app.components.headermenu')
    @include('app.partials.headermenuannunci')
    @endcomponent
@endsection


@section('content')

    @if($annuncio)

    @block()
    prova

    <a href="{{ route('candidatura', $annuncio) }}">candidatura</a>

    <button data-toggle="modal" data-target="#nuovaConversazione">Contatta</button>

    @modal(['id' =>'nuovaConversazione', 'title' => 'Crea conversazione', 'class' => 'modal--sm', 'visual' => 'visual-nuova-galleria.svg'])
        @slot('title')<strong>{{ '@' }}{{ $annuncio->creatore->fullname }} </strong> per annuncio <strong> {{ $annuncio->codice }}</strong>@endslot
            <form action="{{ route('conversazione.store')}}" method="POST" class="canLeavePage">
                @csrf
                <input type="hidden" name="coverable_type" value="{{ encrypt(get_class(new App\Annuncio)) }}">
                <input type="hidden" name="coverable_id" value="{{ encrypt($annuncio->id) }}">
                <input type="hidden" name="destinatario_id" value="{{ encrypt($annuncio->creatore_id) }}">

                @textarea(['attrs' => ['name' => 'messaggio', 'rows' => '8']])@endtextarea
                @button(['attrs' => ['text' => 'Avvia conversazione', 'class' => 'btn btn-primary btn-padding nopreloader']])@endbutton
            </form>
    @endmodal

    @endblock


    @else
        <div class="empty text-center col-8 col-mx-auto myauto">
            <div class="columns align-items-center">
                <div class="column col-6 col-sm-12">
                    <img src="{{ asset('images/app/visual-crea-nuovo-immobile.svg') }}" alt="Crea immobile">
                </div>
                <div class="column col-6 col-sm-12">
                    <p class="empty-title h3">Nessun annuncio trovato</p>
                    <p class="empty-subtitle">Prova a cambiare i parametri di ricerca</p>
                    <div class="empty-action">
                </div>
            </div>
            </div>
        </div>
    @endif

@endsection