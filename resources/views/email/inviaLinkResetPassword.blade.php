@extends('layouts.email')
@section('preheader')
Reset password Senseria
@endsection
@section('content')

    <p>Ciao {!! $user->nome_cognome !!},</p>
    <p>È stato richiesto il reset della password.<br/>
        Per procedere clicca sul pulsante sottostrate. Se non sei stato tu, ingnora questa email.</p>

    <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
        <tbody>
        <tr>
            <td align="left">
            <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                <tbody>
                <tr>
                    <td> <a href="{!! route('auth.password.show.reset', ['user' => Crypt::encrypt($user->id), 'token' => $token, 'type' => $type, 'email' => $user->email]) !!}" target="_blank">Reset password</a> </td>
                </tr>
                </tbody>
            </table>
            </td>
        </tr>
        </tbody>
    </table>

    <p>Senseria Staff</p>

@endsection