@extends('layouts.email')
@section('preheader')
Conferma indirizzo email registrazine Senseria
@endsection
@section('content')

    <p>Ciao {!! $user->nome_cognome !!},</p>
    <p>Siamo contenti che tu abbia fatto il primo passo per entrare in Senseria.<br/>
        Per completare la registrazione ti chiediamo di confermare il tuo indirizzo email semplicemente cliccando nel pulsante sottostante.</p>
    <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
        <tbody>
        <tr>
            <td align="left">
            <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                <tbody>
                    <tr>
                        <td> <a href="{!! route('auth.conferma.email', ['user' => Crypt::encrypt($user->id), 'token' => $token, 'type' => $type]) !!}" target="_blank">Verifica email</a> </td>
                    </tr>
                </tbody>
            </table>
            </td>
        </tr>
        </tbody>
    </table>

    <p>Senseria Staff</p>

@endsection