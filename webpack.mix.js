const mix = require('laravel-mix');

mix.js('resources/js/componenti.js', 'public/js')
    .js('resources/js/site.js', 'public/js')
    .js('resources/js/app.js', 'public/js')
    .js('resources/js/ajax.js', 'public/js')
    .js('resources/js/dropzone.js', 'public/js')
    .js('resources/js/maps/mapsViewAreaOperativa.js', 'public/js/maps')
    .js('resources/js/maps/mapsViewAreaImmobile.js', 'public/js/maps')
    .js('resources/js/maps/mapsSelezionaIndirizzo.js', 'public/js/maps')
    .js('resources/js/editor/ckeditor.js', 'public/js/editor')
    .js('resources/js/pusher.js', 'public/js')
    .scripts([
        'public/js/componenti.js',
        'public/js/maps/mapsViewAreaOperativa.js',
        'public/js/maps/mapsViewAreaImmobile.js',
        'public/js/maps/mapsSelezionaIndirizzo.js',
        'public/js/app.js',
        'public/js/ajax.js',
        'public/js/editor/ckeditor.js',
        'public/js/pusher.js',
    ], 'public/js/app_bundle.js')

.scripts([
    'public/js/site.js',
    'public/js/maps/mapsViewAreaOperativa.js'
], 'public/js/site_bundle.js')

.sass('resources/sass/app.scss', 'public/css')
    .sass('resources/sass/site.scss', 'public/css')
    .version()
    .copy('resources/images', 'public/images', false)
    .copy('resources/favicons', 'public/favicons', false)
    .options({
        processCssUrls: false
    });