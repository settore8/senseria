<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/*
******************** SITE
********************
*/

Route::view('/', 'site.index')->name('home')->middleware('guest');

Route::get('/caratteristiche', function () {
    return view('site.caratteristiche');
})->name('caratteristiche')->middleware('guest');

Route::get('/vantaggi', function () {
    return view('site.vantaggi');
})->name('vantaggi')->middleware('guest');

Route::prefix('/legal')->group(function () {

    Route::get('/privacy', function () {
        return view('site.legal.privacy');
    })->name('legal.privacy');

    Route::get('/condizioni', function () {
        return view('site.legal.condizioni');
    })->name('legal.condizioni');

    Route::get('/cookies', function () {
        return view('site.legal.cookies');
    })->name('legal.cookies');

});



/*
******************** AUTH
********************
*/

Auth::routes(['verify' => false]);

Route::get('/registrazione', ['as' => 'registrazione', 'uses' => 'Auth\RegisterController@showRegistrationForm']);
Route::POST('/registrazione', ['as' => 'auth.registrazione', 'uses' => 'Auth\RegisterController@register']);

Route::get('/login', ['as' => 'login', 'uses' => 'Auth\LoginController@showLoginForm']);
Route::POST('/login', ['as' => 'auth.login', 'uses' => 'Auth\LoginController@login']);

//Route::post('/logout', ['as' => 'logout', 'uses' => 'Auth\LoginController@logout']);

Route::get('/password/email', ['as' => 'auth.password.request.email', 'uses' => 'Auth\ForgotPasswordController@showLinkRequestForm']);
Route::POST('/password/email', ['as' => 'auth.password.send.link', 'uses' => 'Auth\ForgotPasswordController@sendResetLinkEmail']);

Route::get('/password/reset', ['as' => 'auth.password.show.reset', 'uses' => 'Auth\ResetPasswordController@showResetForm']);
Route::POST('/password/reset', ['as' => 'auth.password.store.reset', 'uses' => 'Auth\ResetPasswordController@reset']);

Route::get('/conferma', ['as' => 'auth.conferma.email', 'uses' => 'UserController@confermaEmail']);

Route::prefix('/onboarding')->group(function(){
    Route::view('welcome', 'boarding.welcome')->name('boarding.step1');
});

/*
******************** ANNUNCI
********************
*/

Route::prefix('s')->group(function () {
    Route::get('/', ['as' => 'senseria.annunci.search', 'uses' => 'SenseriaController@annunciSearch']);
});


/*
******************** CALENDARIO
********************
*/
Route::prefix('calendario')->group(function () {
    Route::get('/', function() {
        return 'calerndario';
    })->name('calendario');
});


/*
******************** AGENTI
********************
*/
Route::prefix('agenti')->group(function () {
    Route::get('/', function() {
        return 'agenti';
    })->name('agenti');
});


/*
******************** ANNUNCI
********************
*/
Route::prefix('{contratto}')->group(function () {
    Route::prefix('{specifica?}')->group(function () {
        Route::prefix('{comune?}')->group(function () {
            Route::get('/', ['as' => 'senseria.annunci', 'uses' => 'SenseriaController@annunciIndex']);
        });
    });
});

Route::get('{contratto}/{specifica}/{comune}/{codice}', ['as' => 'senseria.annuncio', 'uses' => 'SenseriaController@annunciShow'])->middleware('pubblicita');




