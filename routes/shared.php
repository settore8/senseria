<?php

// Le route Shared sono condivise con il progetto Senseria


/*
******************** APP
********************
*/

Route::group(['prefix' => 'ajax'], function () {
    Route::POST('/getSpecifiche', 'AjaxController@getSpecifiche');
    Route::GET('/getAgenti', 'AjaxController@getAgenti');
    Route::POST('/getProfessionisti', 'AjaxController@getProfessionisti');

    Route::POST('/getKeyWords', 'AjaxController@getKeyWords');
    Route::POST('/removeAllegato', 'AjaxController@removeAllegato');
    Route::POST('/createConversazione', 'AjaxController@createConversazione');
    Route::POST('/sendMessage', 'AjaxController@sendMessage');
    Route::POST('/likeProfilo', 'AjaxController@likeProfilo');
    Route::POST('/scegliCandidato', 'AjaxController@scegliCandidato');
    Route::POST('/selectAgenteImmobiliare', 'AjaxController@selectAgenteImmobiliare');
    Route::POST('/removeImmagineByGalleria', 'AjaxController@removeImmagineByGalleria');
    Route::POST('/attivaDisattivaModulo', 'AjaxController@attivaDisattivaModulo');
    Route::group(['prefix' => 's2'], function () {
        //metto qui tutt le route che servono che siano impostato come vuole select2
        Route::POST('/selectResponsabileTecnico', 'AjaxController@selectResponsabileTecnico');
        Route::POST('/searchAllUser', 'AjaxController@searchAllUser');
    });
    Route::POST('/addstripecard', 'AjaxController@addcard');
    Route::POST('/removePartecipazione', 'AjaxController@removePartecipazione');

    /*user preferences */
    Route::POST('/setPreferenzeUser', 'AjaxController@setPreferenzeUser');
    //Route::POST('/updateVisibilitaSuggerimento', 'AjaxController@updateVisibilitaSuggerimento');
    Route::POST('/setTipVisibility', 'AjaxController@setTipVisibility');

    /*user notifiche */
    Route::POST('/signNotificationAsReaded', 'AjaxController@signNotificationAsReaded');
    Route::POST('/deleteNotification', 'AjaxController@deleteNotification');
    /* raccolte */
    Route::POST('/postRaccolta', 'AjaxController@postRaccolta');
    /* likes */
    Route::POST('/postLike', 'AjaxController@postLike');
});

Route::group(['prefix' => 'action/{token?}'], function () {
    Route::get('{type}/declina', ['as' => 'immobile.declina', 'uses' => 'ImmobileController@declinaResponsabileTecnico'])->middleware('protectresptecnico');
    Route::get('{type}/accAss', ['as' => 'immobile.accettAssegnazione', 'uses' => 'ImmobileController@accettAssegnazione'])->middleware('protectagenteimmobiliare');
    Route::get('{type}/dltAcnt', ['as' => 'account.cancellazione', 'uses' => 'UserController@confcancaccount']);
});


Route::group(['middleware' => ['auth', 'usercompletato']], function () {

    Route::get('/dashboard', ['as' => 'dashboard', 'uses' => 'DashboardController@index']);

    Route::prefix('annunci')->group(function () {
        Route::get('/s/{s?}', ['as' => 'annunci.search', 'uses' => 'AnnunciController@search']);
        Route::get('/candidature', ['as' => 'annunci.candidature', 'uses' => 'AnnunciController@candidature']);
        Route::group(['prefix' => '{tipo?}'], function () {
            Route::get('/', ['as' => 'annunci', 'uses' => 'AnnunciController@index']);
        });
    });

    Route::prefix('annuncio')->group(function () {

        Route::prefix('{codice}')->group(function () {
            Route::get('/', ['as' => 'annuncio', 'uses' => 'AnnunciController@show']);
            Route::group(['prefix' => 'candidatura'], function () {
                Route::get('/', ['as' => 'candidatura', 'uses' => 'AnnunciController@candidatura']);
                Route::get('/aggiorna', ['as' => 'action.aggiornacandidatura', 'uses' => 'AnnunciController@aggiornaCandidatura']);
                Route::POST('/aggiorna', ['as' => 'action.update.aggiornacandidatura', 'uses' => 'AnnunciController@updateCandidatura']);
            });
            Route::prefix('store')->group(function () {
                Route::POST('/candidatura', ['as' => 'store.candidatura', 'uses' => 'AnnunciController@storeCandidatura']);
            });
        });

    });


    /*
    ******************** IMMOBILI
    ********************
    */
    Route::group(['prefix' => 'immobili'], function () {
        Route::get('/', ['as' => 'immobili', 'uses' => 'ImmobileController@index']);
        Route::get('/s/{s?}', ['as' => 'immobili.search', 'uses' => 'ImmobileController@search']);

        Route::group(['prefix' => 'gestione'], function () {
            Route::get('/', ['as' => 'immobili.gestione', 'uses' => 'ImmobileController@gestione']);
            Route::get('/assegnati', ['as' => 'immobili.gestione.assegnati', 'uses' => 'ImmobileController@gestioneassegnati']);
            Route::get('/archiviati', ['as' => 'immobili.gestione.archiviati', 'uses' => 'ImmobileController@gestionearchiviati']);
        });

    });

    Route::prefix('immobile')->group(function () {
        Route::view('nuovo', 'app.immobili.immobile.create')->name('immobile.create');
        Route::POST('/store', ['as' => 'immobile.store', 'uses' => 'ImmobileController@store']);

        Route::prefix('{immobile}')->group(function () {


            /** prima di tutto verifico che l'immobile esista e che l'utente sia il cristo creatore oppure il tecnico salvatore */
            Route::group(['middleware' => ['immobileexists', 'caneditimmobile']], function () {

                Route::POST('/destroy', ['middleware' => ['checkismyimmobile'], 'as' => 'immobile.destroy', 'uses' => 'ImmobileController@destroy']);
                Route::POST('/archivia', ['middleware' => ['checkismyimmobile'], 'as' => 'immobile.archivia', 'uses' => 'ImmobileController@archivia']);

                Route::group(['middleware' => ['checkismyimmobile'], 'prefix' => 'impostazioni'], function () {
                    Route::get('/', ['as' => 'immobile.impostazioni', 'uses' => 'ImmobileController@impostazioni']);
                });

                Route::group(['middleware' => ['informazioneexists']], function () {
                    Route::get('/', ['as' => 'immobile', 'uses' => 'ImmobileController@immobile']);

                    Route::group(['middleware' => ['checkismyimmobile'], 'prefix' => 'responsabile-tecnico'], function () {
                        Route::get('/', ['as' => 'immobile.responsabiletecnico', 'uses' => 'ImmobileController@responsabiletecnico']);
                        /** le mostro tutte  **/
                        Route::get('/candidature', ['as' => 'immobile.responsabiletecnico.candidature', 'uses' => 'ImmobileController@candidatureResponsabileTecnico']);
                    });

                    Route::POST('/declina', ['as' => 'action.declina', 'uses' => 'ImmobileController@storeDeclinaResponsabileTecnico']);
                    Route::POST('/condividi', ['middleware' => ['checkismyimmobile'], 'as' => 'action.condividi', 'uses' => 'ImmobileController@condividiImmobile']);

                    Route::POST('/confermAssegnazione', ['middleware' => ['checkismyimmobile'], 'as' => 'action.confermAssegnazioneAgenteImmobiliare', 'uses' => 'ImmobileController@confermAssegnazioneAgenteImmobiliare']);

                    Route::group(['middleware' => ['checkismyimmobile'], 'prefix' => 'statistiche'], function () {
                        Route::get('/', ['as' => 'immobile.statistiche', 'uses' => 'ImmobileController@statistiche']);
                    });

                    Route::get('donwload', ['middleware' => ['checkismyimmobile'], 'as' => 'immobile.download', 'uses' => 'ImmobileController@creaDownload']);

                    Route::get('condivisione', ['middleware' => ['checkismyimmobile'], 'as' => 'immobile.condivisione', 'uses' => 'ImmobileController@condivisione']);

                    Route::get('informazioni', ['as' => 'immobile.informazioni', 'uses' => 'ImmobileController@informazioni']);
                    Route::get('allegati', ['as' => 'immobile.allegati', 'uses' => 'ImmobileController@allegati']);
                    Route::POST('/carica-allegati', ['middleware' => ['checkismyimmobile'], 'as' => 'immobile.carica.allegati', 'uses' => 'ImmobileController@caricaAllegati']);
                    Route::get('vendita', ['middleware' => ['checkismyimmobile'], 'as' => 'immobile.vendita', 'uses' => 'ImmobileController@vendita']);


                    Route::group(['prefix' => 'sopralluogo', 'middleware' => 'hastecnico:sopralluogo'], function () {

                        Route::get('/', ['as' => 'immobile.sopralluogo', 'uses' => 'ImmobileController@sopralluogo']);
                        Route::get('/contesto', ['as' => 'immobile.sopralluogo.contesto', 'uses' => 'ImmobileController@sopralluogo_contesto']);
                        Route::get('/fabbricato', ['as' => 'immobile.sopralluogo.fabbricato', 'uses' => 'ImmobileController@sopralluogo_fabbricato']);
                        Route::get('/fabbricato/struttura', ['as' => 'immobile.sopralluogo.fabbricato.struttura', 'uses' => 'ImmobileController@sopralluogo_fabbricato_struttura']);
                        Route::get('/unita-immobiliare', ['as' => 'immobile.sopralluogo.unitaimmobiliare', 'uses' => 'ImmobileController@sopralluogo_unitaimmobiliare']);
                        Route::get('/unita-immobiliare/infissi', ['as' => 'immobile.sopralluogo.unitaimmobiliare.infissi', 'uses' => 'ImmobileController@sopralluogo_infissi_unitaimmobiliare']);
                        Route::get('/unita-immobiliare/impianti', ['as' => 'immobile.sopralluogo.unitaimmobiliare.impianti', 'uses' => 'ImmobileController@sopralluogo_impianti_unitaimmobiliare']);
                        Route::get('/parti-condominiali', ['as' => 'immobile.sopralluogo.particondominiali', 'uses' => 'ImmobileController@sopralluogo_parti_condominiali']);
                        Route::get('/fotovideo', ['as' => 'immobile.sopralluogo.fotovideo', 'uses' => 'ImmobileController@sopralluogo_documentazione_fotografica']);

                        Route::group(['prefix' => 'store'], function () {
                            Route::POST('/data', ['as' => 'immobile.sopralluogo.store.data', 'uses' => 'ImmobileController@storedatasopralluogo']);
                            Route::POST('/contesto', ['as' => 'immobile.sopralluogo.store.contesto', 'uses' => 'ImmobileController@storecontesto']);
                            Route::POST('/fabbricato', ['as' => 'immobile.sopralluogo.store.fabbricato', 'uses' => 'ImmobileController@storefabbricatoStepOne']);
                            Route::POST('/fabbricato/s2', ['as' => 'immobile.sopralluogo.store.step2.fabbricato', 'uses' => 'ImmobileController@storefabbricatoStepTwo']);
                            Route::POST('/unita-immobiliare', ['as' => 'immobile.sopralluogo.store.unitaimmobiliare', 'uses' => 'ImmobileController@storeunitaimmobiliare']);
                            Route::POST('/unita-immobiliare/infissi', ['as' => 'immobile.sopralluogo.store.infissi.unitaimmobiliare', 'uses' => 'ImmobileController@storeinfissiunitaimmobiliare']);
                            Route::POST('/unita-immobiliare/impianti', ['as' => 'immobile.sopralluogo.store.impianti.unitaimmobiliare', 'uses' => 'ImmobileController@storeimpiantiunitaimmobiliare']);
                            Route::POST('/parti-condominiali/', ['as' => 'immobile.sopralluogo.store.particondominiali', 'uses' => 'ImmobileController@storeparticondominiali']);
                            Route::POST('/upload-fotovideo/', ['as' => 'immobile.sopralluogo.upload.fotovideo', 'uses' => 'ImmobileController@upload_fotovideo']);
                        });

                    });

                    Route::group(['prefix' => 'verifica-tecnica', 'middleware' => 'hastecnico:verifica-tecnica'], function () {
                        Route::get('/', ['as' => 'immobile.verificatecnica', 'uses' => 'ImmobileController@verificatecnica']);
                        Route::get('/datiurbanistici', ['as' => 'immobile.verificatecnica.datiurbanistici', 'uses' => 'ImmobileController@verificatecnica_datiurbanistici']);
                        Route::get('/catasto', ['as' => 'immobile.verificatecnica.catasto', 'uses' => 'ImmobileController@verificatecnica_catasto']);
                        Route::get('/provenienza', ['as' => 'immobile.verificatecnica.provenienza', 'uses' => 'ImmobileController@verificatecnica_provenienza']);
                        Route::get('/agibilita', ['as' => 'immobile.verificatecnica.agibilita', 'uses' => 'ImmobileController@verificatecnica_agibilita']);
                        Route::get('/impianti', ['as' => 'immobile.verificatecnica.impianti', 'uses' => 'ImmobileController@verificatecnica_impianti']);
                        Route::get('/caldaia', ['as' => 'immobile.verificatecnica.caldaia', 'uses' => 'ImmobileController@verificatecnica_caldaia']);
                        Route::get('/ape', ['as' => 'immobile.verificatecnica.ape', 'uses' => 'ImmobileController@verificatecnica_ape']);
                        Route::get('/cdu', ['as' => 'immobile.verificatecnica.cdu', 'uses' => 'ImmobileController@verificatecnica_cdu']);
                        Route::get('/postuma', ['as' => 'immobile.verificatecnica.postuma', 'uses' => 'ImmobileController@verificatecnica_postuma']);
                        Route::get('/altridocumenti', ['as' => 'immobile.verificatecnica.altridocumenti', 'uses' => 'ImmobileController@verificatecnica_altridocumenti']);
                        Route::group(['prefix' => 'store'], function () {
                            Route::POST('/', ['as' => 'immobile.verificatecnica.store', 'uses' => 'ImmobileController@storeverificatecnica']);

                        });
                    });

                    Route::group(['prefix' => 'relazione-tecnica', 'middleware' => 'hastecnico:relazione-tecnica'], function () {
                        Route::get('/', ['as' => 'immobile.relazionetecnica', 'uses' => 'ImmobileController@relazionetecnica']);
                    });

                    Route::group(['prefix' => 'stima', 'middleware' => 'hastecnico:stima'], function () {
                        Route::get('/', ['as' => 'immobile.stima', 'uses' => 'ImmobileController@stima']);
                    });

                    Route::group(['prefix' => 'istruttoria', 'middleware' => 'hastecnico:istruttoria'], function () {
                        Route::get('/', ['as' => 'immobile.istruttoria', 'uses' => 'ImmobileController@istruttoria']);
                    });

                    Route::group(['prefix' => 'collaborazioni', 'middleware' => 'hastecnico:collaborazioni'], function () {
                        Route::get('/', ['as' => 'immobile.collaborazioni', 'uses' => 'ImmobileController@collaborazioni']);
                    });

                    Route::group(['prefix' => 'capitolati', 'middleware' => 'hastecnico:capitolati'], function () {
                        Route::get('/', ['as' => 'immobile.capitolati', 'uses' => 'ImmobileController@capitolati']);
                    });


                    Route::get('visuraipotecaria', ['as' => 'immobile.visuraipotecaria', 'uses' => 'ImmobileController@visuraipotecaria']);

                    Route::group(['prefix' => 'rilievo'], function () {
                        Route::get('/', ['as' => 'immobile.rilievo', 'uses' => 'ImmobileController@rilievo3d']);

                        Route::post('/store', ['as' => 'immobile.rilievo.store', 'uses' => 'RilievoController@store']);

                        Route::get('/edit', ['as' => 'immobile.rilievo.edit', 'uses' => 'RilievoController@edit']);
                        Route::post('/update', ['as' => 'immobile.rilievo.update', 'uses' => 'RilievoController@update']);
                        Route::delete('/destroy', ['as' => 'immobile.rilievo.destroy', 'uses' => 'RilievoController@destroy']);

                        Route::get('/preventivo', ['as' => 'immobile.rilievo.preventivo', 'uses' => 'RilievoController@preventivo']);
                        Route::get('/messaggi', ['as' => 'immobile.rilievo.messaggi', 'uses' => 'RilievoController@messaggi']);
                        Route::get('/cloud', ['as' => 'immobile.rilievo.cloud', 'uses' => 'RilievoController@cloud']);

                    });


                    Route::group(['prefix' => 'store'], function () {
                        Route::POST('/informazioni', ['as' => 'immobile.store.informazioni', 'uses' => 'ImmobileController@storeInformazioni']);
                        Route::POST('/vendita', ['as' => 'immobile.store.vendita', 'uses' => 'ImmobileController@storeVendita']);
                        Route::POST('/responsabiletecnico', ['as' => 'store.responsabiletecnico', 'uses' => 'ImmobileController@storeResponsabileTecnico']);
                        Route::POST('/delete-responsabiletecnico', ['as' => 'delete.responsabiletecnico', 'uses' => 'ImmobileController@deleteResponsabileTecnico']);
                    });
                });
            });
        });

    });

    /*
    ******************** IMPOSTAZIONI
    ********************
    */

    Route::prefix('account')->group(function () {
        Route::get('/', 'UserController@editdashboard');

        Route::prefix('statistiche')->group(function () {
            Route::get('/', ['as' => 'account.statistiche', 'uses' => 'UserController@statistichegenerali']);
            Route::get('/profilo', ['as' => 'account.statistiche.profilo', 'uses' => 'UserController@statisticheprofilo']);
            
            Route::prefix('articoli')->group(function () {
                Route::get('/', ['as' => 'account.statistiche.articoli', 'uses' => 'UserController@statistichearticoli']);
                Route::get('/{id}', ['as' => 'account.statistiche.articolo', 'uses' => 'UserController@statistichearticolo']);
            });

            Route::get('/gallerie', ['as' => 'account.statistiche.gallerie', 'uses' => 'UserController@statistichegallerie']);
        });

        Route::POST('/share', ['as' => 'account.share', 'uses' => 'UserController@shareprofilo']);
        Route::POST('/download', ['as' => 'account.download', 'uses' => 'UserController@downloadprofilo']);

        Route::prefix('profilo')->group(function () {
            Route::get('/', ['as' => 'account.profilo', 'uses' => 'UserController@editprofilo']);
            Route::get('immagine', ['as' => 'account.profilo.immagine', 'uses' => 'UserController@editprofiloimmagine']);
            Route::get('contatti', ['as' => 'account.profilo.contatti', 'uses' => 'UserController@editprofilocontatti']);
        });


        Route::get('abilitazioni', ['as' => 'account.abilitazioni', 'uses' => 'UserController@editabilitazioni']);
        Route::get('competenze', ['as' => 'account.competenze', 'uses' => 'UserController@editcompetenze']);
        Route::get('area', ['as' => 'account.area', 'uses' => 'UserController@editarea']);
        Route::get('certificazione', ['as' => 'account.certificazione', 'uses' => 'UserController@editcertificazione']);

        //Stripe
        Route::prefix('abbonamento')->group(function () {
            Route::get('/', ['as' => 'account.abbonamento', 'uses' => 'UserController@riepilogoabbonamento'])->middleware('hasabbonamento');
            Route::get('/piani', ['as' => 'account.abbonamento.piani', 'uses' => 'SubscriptionController@index']);
            Route::get('/piano/{plan}', ['as' => 'account.abbonamento.show', 'uses' => 'SubscriptionController@show']);
            Route::POST('/nuovo/{plan}', ['as' => 'account.abbonamento.first', 'uses' => 'SubscriptionController@first']);
            Route::POST('/upgrade/{plan}', ['as' => 'account.abbonamento.upgrade', 'uses' => 'SubscriptionController@upgrade']);
        });
        //

        Route::prefix('impostazioni')->group(function () {
            Route::get('/', ['as' => 'account.impostazioni', 'uses' => 'UserController@editimpostazioninotifiche']);
            Route::get('password', ['as' => 'account.impostazioni.password', 'uses' => 'UserController@editimpostazionipassword']);
            Route::get('privacy', ['as' => 'account.impostazioni.privacy', 'uses' => 'UserController@editimpostazioniprivacy']);
            Route::get('avanzate', ['as' => 'account.impostazioni.avanzate', 'uses' => 'UserController@editimpostazioniavanzate']);
            Route::post('cancellazione-profilo', ['as' => 'account.cancella', 'uses' => 'UserController@cancellaaccount']);
        });


        Route::prefix('store')->group(function () {
            Route::POST('/profilo', ['as' => 'account.store.profilo', 'uses' => 'UserController@updateprofilo']);
            Route::POST('/area', ['as' => 'account.store.area', 'uses' => 'UserController@updatearea']);
            Route::POST('/competenze', ['as' => 'account.store.competenze', 'uses' => 'UserController@updatecompetenze']);
            Route::POST('/abilitazioni', ['as' => 'account.store.abilitazioni', 'uses' => 'UserController@updateabilitazioni']);
            Route::POST('/certificazione', ['as' => 'account.store.certificazione', 'uses' => 'UserController@updatecertificazione']);
            Route::POST('/password', ['as' => 'account.store.password', 'uses' => 'UserController@updatepassword']);
        });

        Route::prefix('notifiche')->group(function () {
            Route::get('/', ['as' => 'account.notifiche', 'uses' => 'UserController@notifiche']);
        });


        Route::prefix('raccolte')->group(function () {
            Route::get('/s/{s?}', ['as' => 'account.raccolte.search', 'uses' => 'UserController@searchraccolte']);

            Route::prefix('{type?}')->group(function () {
                Route::get('/', ['as' => 'account.raccolte', 'uses' => 'UserController@raccolte']);
            });

        });

    });


    /*
   ******************** CONVERSAZIONI
   ********************
   */
    Route::prefix('conversazioni')->group(function () {
        Route::get('/', ['as' => 'conversazioni', 'uses' => 'ConversazioneController@indexConversazioni']);
        Route::get('/archivio', ['as' => 'conversazioni.archivio', 'uses' => 'ConversazioneController@archivioConversazioni']);
        Route::get('{conversazione}', ['as' => 'conversazione', 'uses' => 'ConversazioneController@showConversazione'])->middleware('incoversazione');
        Route::POST('/store', ['as' => 'conversazione.store', 'uses' => 'ConversazioneController@store']);
    });

});
