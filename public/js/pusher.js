/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 9);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/moduli/notification.js":
/*!*********************************************!*\
  !*** ./resources/js/moduli/notification.js ***!
  \*********************************************/
/*! exports provided: createNotification */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "createNotification", function() { return createNotification; });
/* ***********************************
**************************************
NOTIFICATION
**************************************
************************************** */
function createNotification(message, link) {
  var notification = '<div class="notification"><div class="notification__title">Nuova notifica <button class="notification__close">X</button></div><a href="' + link + '" class="notification__body">' + message + '</a></div>';
  var outerHeight = 16;
  var limitnotification = $(window).outerHeight() / 2; // 

  var self = $(this);
  $('.notification').each(function () {
    outerHeight += $(this).outerHeight() + 8;
  });

  if (limitnotification < outerHeight) {
    $('.notification').first().remove();
  }

  $(notification).css('bottom', outerHeight + 'px').appendTo('body');
  repositionNotification();
  setTimeout(function () {
    $('.notification').first().remove();
    repositionNotification();
  }, 20000);
}

function repositionNotification() {
  var outerHeight = 16;
  $('.notification').each(function () {
    $(this).css('bottom', outerHeight + 'px');
    outerHeight += $(this).outerHeight() + 8;
  });
}

$(document).on('click', '.notification__close', function (e) {
  e.preventDefault();
  $(this).closest('.notification').remove();
  repositionNotification();
});
$(document).on('click', '#testnotification', function () {
  createNotification('Prova prova prova');
});
$(document).on('click', '#toggle_notification_area, #close_notification_area', function (e) {
  e.preventDefault();
  $('html').scrollTop(0);
  $('html').toggleClass('notification_area_active');
});

/***/ }),

/***/ "./resources/js/pusher.js":
/*!********************************!*\
  !*** ./resources/js/pusher.js ***!
  \********************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _moduli_notification__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./moduli/notification */ "./resources/js/moduli/notification.js");

var pusher = new Pusher('d130d2d1a3e48f95d28c', {
  cluster: 'eu',
  forceTLS: true
});
var messaggi_channel = pusher.subscribe('messaggi');
messaggi_channel.bind('inserisci-messaggio', function (data) {
  var classe = 'left';

  if (data['senderid'] == $('#conversazione_content').attr('data-sender')) {
    var classe = 'right';
  }

  var lastMessage = '<div class="messaggio messaggio--' + classe + '"> <div class="messaggio__content"><div class="messaggio__header">' + data['sender'] + '</div> ' + data['messaggio'] + ' <time>' + data['now'] + '</time></div></div>';
  $(lastMessage).insertAfter('.messaggio:last-child');
  $('#conversazione_content').scrollTop($('#conversazione_content')[0].scrollHeight);
});
var users_channel = pusher.subscribe('users');
users_channel.bind('like-profilo', function (data) {
  if ($('meta[name=user-token]').attr('content') == $data['destinatario']) {
    Object(_moduli_notification__WEBPACK_IMPORTED_MODULE_0__["createNotification"])(data['messaggio']);
  }
});
var annunci_channel = pusher.subscribe('annunci');
annunci_channel.bind('aggiorna-candidatura-resp-tecnico', function (data) {
  if ($('meta[name=user-token]').attr('content') == data['destinatario']) {
    Object(_moduli_notification__WEBPACK_IMPORTED_MODULE_0__["createNotification"])(data['messaggio']);
  }
});
annunci_channel.bind('modulo-aggiunto', function (data) {
  if ($('meta[name=user-token]').attr('content') == data['destinatario']) {
    Object(_moduli_notification__WEBPACK_IMPORTED_MODULE_0__["createNotification"])(data['messaggio']);
  }
});
annunci_channel.bind('modulo-rimosso', function (data) {
  if ($('meta[name=user-token]').attr('content') == data['destinatario']) {
    Object(_moduli_notification__WEBPACK_IMPORTED_MODULE_0__["createNotification"])(data['messaggio']);
  }
});

/***/ }),

/***/ 9:
/*!**************************************!*\
  !*** multi ./resources/js/pusher.js ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/andreaferri/Documents/WEBSITES/2020/senseria/resources/js/pusher.js */"./resources/js/pusher.js");


/***/ })

/******/ });