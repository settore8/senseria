/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 3);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/ajax.js":
/*!******************************!*\
  !*** ./resources/js/ajax.js ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});
$(function () {
  function $_GET(param) {
    var vars = {};
    window.location.href.replace(location.hash, '').replace(/[?&]+([^=&]+)=?([^&]*)?/gi, // regexp
    function (m, key, value) {
      // callback
      vars[key] = value !== undefined ? value : '';
    });

    if (param) {
      return vars[param] ? vars[param] : null;
    }

    return vars;
  }

  $(document).on('click', 'button[data-attiva-modulo="true"]', function (e) {
    var self = $(this);
    $.post({
      url: '/ajax/attivaDisattivaModulo',
      type: 'POST',
      data: {
        modulo: self.attr('data-modulo'),
        immobile: self.attr('data-immobile'),
        destinatario: self.attr('data-destinatario')
      },
      success: function success(response) {
        var modulo = self.attr('data-modulo');
        self.toggleClass('btn-success');
        $('*[data-modulo-menu=' + modulo + ']').toggleClass('disabled').find('svg').toggleClass('hidden');
        self.html(response);
      },
      error: function error(_error) {
        console.log(_error);
      }
    });
  }); // GET specifiche 

  $(document).on('click', 'input[name=destinazione]', function () {
    var self = $(this);
    var destinazione = self.attr('data-id-destinazione');
    $("#specificaContainer").addClass("focus").delay(500).queue(function (next) {
      $(this).removeClass("focus");
      next();
    });
    getSpecifiche(destinazione);
  });
  $(document).ready(function () {
    var destinazione = $('input[name=destinazione]:checked').attr('data-id-destinazione');

    if (destinazione) {
      getSpecifiche(destinazione);
    }
  });

  function getSpecifiche(destinazione) {
    $.post({
      url: '/ajax/getSpecifiche',
      data: {
        destinazione: destinazione
      },
      success: function success(response) {
        $('#specifica > option').remove();
        var old = $('#specificaold').val();
        var addNull = new Option('--', '', false, false);
        $('#specifica').append(addNull).trigger('change');
        $.each(JSON.parse(response), function (id, specifica) {
          if (old == specifica.id) {
            var selected = true;
          } else {
            var selected = false;
          }

          var newOption = new Option(specifica.nome_pubblico, specifica.id, false, selected);
          $('#specifica').append(newOption).trigger('change').attr('disabled', false);
        });
      },
      error: function error(_error2) {
        console.log(_error2);
      }
    });
  } // end getspecifiche

  /*
  $(document).on('click', '*[data-tip-ajax]', function(e) {
      var self = $(this);
      e.preventDefault();
      $.post({
          url: '/ajax/updateVisibilitaSuggerimento',
          data: {
              suggerimento: self.attr('data-tip-ajax')
          },
          success: function(response) {
              console.log(response);
          },
          error: function(error) {
              console.log(error);
          }
      })
  });
  */


  $(document).on('change', '#categoria_articolo', function (e) {
    e.preventDefault();
    var self = $(this);
    $.post({
      url: '/ajax/getKeyWords',
      data: {
        categoria: self.val()
      },
      success: function success(response) {
        $('#parole_chiavi > option').remove();
        $.each(JSON.parse(response), function (id, keyword) {
          var newOption = new Option(keyword.descrizione, keyword.descrizione, false, false);
          $('#parole_chiavi').append(newOption);
        });
        $('#parole_chiavi').trigger('focus').select2("open");
      },
      error: function error(_error3) {
        console.log(_error3);
      }
    });
  });
  $('#internoaworkook').select2({
    tags: true,
    minimumInputLength: 3,
    allowClear: true,
    placeholder: 'Cerca un professionista tramite nome o email',
    ajax: {
      url: '/ajax/s2/selectResponsabileTecnico',
      dataType: 'json',
      delay: 250,
      type: "POST",
      data: function data(params) {
        return {
          search: params.term,
          page: params.page
        };
      },
      processResults: function processResults(data, params) {
        params.page = params.page || 1;
        return {
          results: data.results,
          pagination: {
            more: false
          }
        };
      }
    }
  });
  $('.js-select2-professionisti-imprese').select2({
    minimumInputLength: 3,
    allowClear: true,
    placeholder: 'Cerca un Professionista o una Impresa tramite nome o email',
    ajax: {
      url: '/ajax/getProfessionistiImprese',
      dataType: 'json',
      delay: 320,
      type: "POST",
      data: function data(params) {
        return {
          search: params.term,
          page: params.page
        };
      },
      processResults: function processResults(data, params) {
        params.page = params.page || 1;
        return {
          results: data.results,
          pagination: {
            more: false
          }
        };
      }
    },
    cache: true
  });
  $('.js-select2-professionisti').select2({
    minimumInputLength: 3,
    allowClear: true,
    placeholder: 'Cerca un professionista tramite nome o email',
    ajax: {
      url: '/ajax/getProfessionisti',
      dataType: 'json',
      delay: 320,
      type: "POST",
      data: function data(params) {
        return {
          search: params.term,
          page: params.page
        };
      },
      processResults: function processResults(data, params) {
        params.page = params.page || 1;
        return {
          results: data.results,
          pagination: {
            more: false
          }
        };
      }
    },
    cache: true
  });
  $('#selectagentiimmobiliari').select2({
    minimumInputLength: 3,
    allowClear: true,
    placeholder: 'Cerca agente immobiliare tramite nome o email',
    ajax: {
      url: '/ajax/selectAgenteImmobiliare',
      dataType: 'json',
      delay: 250,
      type: "POST",
      data: function data(params) {
        return {
          search: params.term,
          page: params.page
        };
      },
      processResults: function processResults(data, params) {
        params.page = params.page || 1;
        return {
          results: data.results,
          pagination: {
            more: false
          }
        };
      }
    }
  });
  $(document).on('click', '*[data-remove-partecipazione-token]', function (e) {
    e.preventDefault();
    var self = $(this);
    $.post({
      url: '/ajax/removePartecipazione',
      data: {
        token: self.attr('data-remove-partecipazione-token')
      },
      success: function success(response) {
        console.log(response);
        self.closest("li").remove();
      },
      error: function error(_error4) {
        console.log(_error4);
      }
    });
  });
  $(document).on('click', '.remove-allegato', function (e) {
    e.preventDefault();
    var self = $(this);
    $.post({
      url: '/ajax/removeAllegato',
      data: {
        file_id: self.attr('data-file-id')
      },
      success: function success(response) {
        console.log(response);
      },
      error: function error(_error5) {
        console.log(_error5);
      }
    });
  });
  $(document).on('click', '*[data-conversazione="true"]', function (e) {
    e.preventDefault();
    var self = $(this);
    $.post({
      url: '/ajax/createConversazione',
      data: {
        object_id: self.attr('data-object-id-conversazione'),
        object_class: self.attr('data-object-class-conversazione'),
        recipient_id: self.attr("data-recipient-id-conversazione")
      },
      success: function success(response) {
        console.log(response);
      },
      error: function error(_error6) {
        console.log(_error6);
      }
    });
  });
  $(document).on('click', '*[data-remove-immagine]', function (e) {
    e.preventDefault();
    var self = $(this);
    $.post({
      url: '/ajax/removeImmagineByGalleria',
      data: {
        immagine: self.attr('data-remove-immagine')
      },
      success: function success(response) {
        self.closest('p').remove();
      },
      error: function error(_error7) {
        console.log(_error7);
      }
    });
  });
  $(document).on('click', '#send-message', function (e) {
    e.preventDefault();
    var self = $(this);

    if ($('#corpo').val()) {
      $.post({
        url: '/ajax/sendMessage',
        data: {
          messaggio: $('#corpo').val(),
          conversazione: self.attr('data-conversazione')
        },
        success: function success(response) {
          console.log(response);
          $('#corpo').val('').focus();
        },
        error: function error(_error8) {
          console.log(_error8);
        }
      });
    }
  });
  /* forse non serve più se le notifiche le facciamo partire dal controller postRaccolta ?
    $(document).on('click', '#like-user', function(e) {
      e.preventDefault();
      var self = $(this);
      $.post({
          url: '/ajax/likeProfilo',
          data: {
              notifica: self.attr('data-notification'),
              destinatario: self.attr('data-destinatario'),
          },
          success: function(response) {
              console.log(response);
          },
          error: function(error) {
              // console.log(error);
          }
      });
  })
  */

  $(document).on('click', '*[data-event="scelta_candidato"]', function (e) {
    e.preventDefault();
    var self = $(this);
    $.post({
      url: '/ajax/scegliCandidato',
      data: {
        immobile: self.attr('data-immobile'),
        candidato: self.attr('data-candidato')
      },
      success: function success(response) {
        $('*[data-event="scelta_candidato"]').remove();
      },
      error: function error(_error9) {
        console.log(_error9);
      }
    });
  });

  function setSelect2ForGalleria(select) {
    select.select2({
      //tags: true,
      minimumInputLength: 3,
      allowClear: true,
      placeholder: 'Cerca un professionista tramite nome o email',
      ajax: {
        url: '/ajax/s2/searchAllUser',
        dataType: 'json',
        delay: 250,
        type: "POST",
        data: function data(params) {
          return {
            search: params.term,
            page: params.page
          };
        },
        processResults: function processResults(data, params) {
          params.page = params.page || 1;
          return {
            results: data.results,
            pagination: {
              more: false
            }
          };
        }
      }
    });
  }

  $("select[name*=collaboratori]").each(function (index) {
    //metto questo perché ho un
    setSelect2ForGalleria($(this));
  });
  $(document).on('click', '#aggiungiCollaboratoreForGalleria', function (e) {
    e.preventDefault();
    $('.contentCollaboratoriForGalleria:first').find('select:first').select2('destroy');
    $('.contentCollaboratoriForGalleria:first').clone().insertBefore($('.contentCollaboratoriForGalleria:first'));
    $("select[name*=collaboratori]").each(function (index) {
      //metto questo perché ho un
      setSelect2ForGalleria($(this));
    });
    $('.contentCollaboratoriForGalleria:first').find('select:first').empty();
  });
  /* azioni notifiche */

  $(document).on('click', '*[data-action="signNotificationAsReaded"]', function (e) {
    var self = $(this);
    self.hide();
    var count = $('#toggle_notification_area').find('.counter').text();
    self.closest('.notifica').addClass('readed').find('.delete').addClass('show');
    $('#toggle_notification_area').find('.counter').text(parseInt(count) - 1);
  });
  $(document).on('click', '*[data-action="deleteNotification"]', function (e) {
    var self = $(this);
    self.closest('.notifica').remove();
  });
  $(document).on('click', '*[data-action]', function (e) {
    e.preventDefault();
    var self = $(this);
    var action = self.attr('data-action');
    $.post({
      url: '/ajax/' + action,
      data: {
        id: self.attr('data-id')
      },
      success: function success(response) {
        console.log(response);
      },
      error: function error(_error10) {
        self.show(); // console.log(error);
      }
    });
  });
  $(document).on('click', '*[data-raccolta]', function (e) {
    e.preventDefault();
    var self = $(this);

    if (self.hasClass('active')) {
      self.find('use').attr('xlink:href', '#icon-raccolta-empty');
    } else {
      self.find('use').attr('xlink:href', '#icon-raccolta');
    }

    if (self.hasClass('hideRaccolta')) {
      self.closest('.card--raccolta').remove();
    }

    self.toggleClass('active');
    $.post({
      url: '/ajax/postRaccolta',
      data: {
        id: self.attr('data-id'),
        type: self.attr('data-raccolta'),
        destinatario_id: self.attr('data-author') // destinatario della notifica

      },
      success: function success(response) {
        console.log(response);
      },
      error: function error(_error11) {// console.log(error);
      }
    });
  });
  $(document).on('click', '*[data-like]', function (e) {
    e.preventDefault();
    var self = $(this);
    var num = parseInt($(this).closest('.counter').find('.n').text());

    if (self.hasClass('active')) {
      self.find('use').attr('xlink:href', '#icon-like-empty');
    } else {
      self.find('use').attr('xlink:href', '#icon-like');
    }

    self.toggleClass('active');
    $.post({
      url: '/ajax/postLike',
      data: {
        id: self.attr('data-id'),
        type: self.attr('data-like'),
        destinatario_id: self.attr('data-author') // destinatario della notifica

      },
      success: function success(response) {
        console.log(response);
      },
      error: function error(_error12) {// console.log(error);
      }
    });
  });
});

/***/ }),

/***/ 3:
/*!************************************!*\
  !*** multi ./resources/js/ajax.js ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/andreaferri/Documents/WEBSITES/2020/senseria/resources/js/ajax.js */"./resources/js/ajax.js");


/***/ })

/******/ });